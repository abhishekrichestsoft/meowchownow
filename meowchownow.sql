-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 03, 2021 at 01:13 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meowchownow`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'demo admin', 'admin@mailinator.com', '25d55ad283aa400af464c76d713c07ad', '1234567823', 'admin', 1, '2020-09-26 00:00:00', '2020-09-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `app_logins`
--

CREATE TABLE `app_logins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `session_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` int(11) NOT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_logins`
--

INSERT INTO `app_logins` (`id`, `user_id`, `session_id`, `device_type`, `device_token`, `created_at`, `updated_at`) VALUES
(7, 48, 'h8E5kOKvZGJCLqESeYygk7jLOZ4NW3gD', 1, 'e-CouKT_7vM:APA91bHIR-dKNEFzxN9HtjngKz5uwaY-P7Pgz7o8aoQDSxK4784K6Wob8O6Knx489ugvAZ6T8Y0Rp3Mjf5SQBs11XeQ-KbvM0X43obu_hs1noyNgALZlxx32cqBNkudk2gZk7mfCLJMb', '2021-05-18 06:55:03', '2021-05-18 06:55:03'),
(30, 39, 'eRo7xgTBXZtbMrBxNaqcEhpDjZwt6UI0', 2, 'cOj0t82KyRY:APA91bFPg-5KOMUmn8PZhS5eXzndVzd5QO-2p-SNnssQwqNfyK4wMSxdtL5MraQkbRx1SeiU8qMGcYWVhwe3Wr3XYgQJ2LMXy0Mbg8sj8LYh6xwh7JjCJ4GA9Z81CSqAa8YfiTiFsj36', '2021-05-19 06:52:55', '2021-05-29 09:16:10'),
(32, 37, 'qn1xpaH1Rnsb89GzG18ZmHk3zCyefx9t', 1, 'c-U1r3F8cW8:APA91bHdiZuNBiKkNYQaGEK_r4gbbVZTlchMJkj4Q76LJJKXOhi_1hF8TilSV8ydT5rnUZ2hLgx7sA3aAqQgWi7gkRthMh7He8znsontkYAf74xc6sQWPMwtb0C5XByyL_xXDceJAL1t', '2021-05-19 07:20:00', '2021-07-21 09:23:39'),
(48, 51, 'E8urIlPCADMh8lLCKrvDBCaWw91sDCuK', 1, 'dQdZvrctQ3E:APA91bE4LEjBirRxRtwMI39l1VThhe_ueXC_QjcXerFv6wWX2OS93UPVtnthDLaaP5KGhANmrTjQhgoDmP5D3ebMGE4txpFtFdUohhkaajEZB6aQuSlFQDSkCbqJ5vBh-b0fzziTMubf', '2021-06-02 05:40:09', '2021-06-02 05:40:09'),
(59, 41, 'mOBjckP5KMH9a3umnH45TTJJZhtjfUoA', 1, 'dpViQQTfE9w:APA91bHp1wYf1BSm5PmagxU6PMzF-J8ogj5WfZsSVC2HpKdVu7OZHoyniuuyUQCFteLfil7kbv-_hFHL7a_7pwvovid4HoRuJuRmqye3yPzrsFcOW4x-n3p53X2lJU-YgQJ8oda_3KXB', '2021-07-14 12:23:12', '2021-07-14 12:23:12'),
(71, 44, 'l4djUsSD0NjkNTZXCbfu5cYjjVXsiEDT', 2, 'e3ORDSHndgE:APA91bFxeqn3ddn8-fDz081yszOWvyXu9MDoETavfZBA063b4yIKSlNsjyCZjIA9n12_9Yv1WHZFvtvcyilAiSjpvERxMo7qRcTQ6H1MH7AJxHwF7iDycvjZQn-sk6QS1BuJp85Y8hX4', '2021-07-16 15:12:16', '2021-07-16 15:12:16'),
(73, 56, 'RTPNBOEIuznTAHTEd5zMTS0k19PbwZlk', 2, 'f0edj2GC0O8:APA91bFpPHPkMGbKrz7rOL-XwQBrOoq7Ed6--bpGId5FAv6vUkloJci7ekKmX7jc9ymTzTm1KlG_zaSbdpfZQ9YpOx-ndzvs7MCgTImMs3gKIOEycPtHCotZRg4O1aHJ8-HcXh3p02Zu', '2021-07-17 06:17:32', '2021-07-17 06:17:32'),
(83, 59, 'ulD5TN9v8fGM0OGyUhfylQl36OyK4MGM', 2, 'misdevicetoken', '2021-07-17 18:08:58', '2021-07-17 18:08:58'),
(87, 62, 'xu8afNW1vMNw9VV1NokMDpFkcDcPfJyx', 2, 'abc', '2021-07-20 15:14:50', '2021-07-21 14:33:53'),
(88, 49, 'mxMsc1FGDiq2CRHrgO60JASIoBm0wY6y', 2, 'dCfRb06x3uA:APA91bFnCLsKVam0j_pJ0aoXOfu269LYRI5lDzuVR6gh6onRhVPDiQUcYLp-KJg1tQ6Y9vH1zj0jv34TDOE_MYkEMJ90ctFaGK-1FTdTjJ_PI26oRq5hOyCv0uDRiRTJ95VsPp-JmGSP', '2021-07-20 15:20:33', '2021-07-21 14:32:44'),
(89, 63, '7hYdGq3xWVGPK6RFS5liBz8BpJCLpMMk', 1, 'dZryTqdxl5o:APA91bFJjTco51rqdz_eoLgfoUbcD9tHmgnkF6IkZSVRkz2QZg2IXq-oML0e5oBm0zB41rTIgZN2LW4FNHZHbNIiS2YkdCYY-QZhm_vJwTUPyD_pTtBukFsOGbJ9LkRPYrJA8_AFeOrk', '2021-07-26 06:37:30', '2021-07-26 06:37:30'),
(92, 64, 'KZ2EJns25WO4pZJkenp0sNaa39uBRM5n', 1, 'device_token_problem_from_front_end', '2021-07-29 12:02:39', '2021-07-29 12:07:32'),
(93, 33, '0OfApTejQTQW78n9gxx3K5rWTZ3OSDbb', 1, 'e-sugXT9kj0:APA91bHY253jMXB8oXzW3rkfMBjqvqxVrHcZdu8dOGksq_6fOhMp1FbxxxDrdsDhlUbo2MDyDz1tEb4q8hP9xnYsgUZ3b9kjMDtkHPbuNYpp2rFJxqA8TIpJO5za9w0W1z9nATNQT9l9', '2021-07-31 14:26:58', '2021-07-31 14:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `blocked_users`
--

CREATE TABLE `blocked_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `blocked_user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blocked_users`
--

INSERT INTO `blocked_users` (`id`, `user_id`, `blocked_user_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 35, 31, 1, '2021-04-15 14:08:39', '2021-04-15 14:08:39'),
(19, 35, 38, 1, '2021-04-16 21:28:39', '2021-04-16 21:28:39'),
(21, 36, 42, 1, '2021-04-17 19:35:45', '2021-04-17 19:35:45'),
(23, 43, 40, 1, '2021-04-19 11:58:10', '2021-04-19 11:58:10'),
(25, 45, 34, 1, '2021-04-24 04:56:28', '2021-04-24 04:56:28'),
(41, 34, 47, 1, '2021-05-08 05:37:24', '2021-05-08 05:37:24'),
(66, 48, 43, 1, '2021-05-18 12:18:49', '2021-05-18 12:18:49'),
(76, 50, 32, 1, '2021-05-28 11:08:36', '2021-05-28 11:08:36'),
(151, 47, 40, 1, '2021-06-02 05:12:00', '2021-06-02 05:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `body_types`
--

CREATE TABLE `body_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_types`
--

INSERT INTO `body_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Fit', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(2, 'Athletic', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(3, 'Average', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(4, 'Heavyset', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(5, 'Feminine', '2020-09-26 00:00:00', '2020-09-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `chat_user`
--

CREATE TABLE `chat_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `chat_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat_user`
--

INSERT INTO `chat_user` (`id`, `user_id`, `chat_user_id`, `created_at`, `updated_at`) VALUES
(1, 32, 33, '2021-04-16 15:13:30', '2021-04-16 15:13:30'),
(2, 33, 32, '2021-04-16 15:13:30', '2021-04-16 15:13:30'),
(3, 35, 38, '2021-04-16 21:28:42', '2021-04-16 21:28:42'),
(4, 38, 35, '2021-04-16 21:28:42', '2021-04-16 21:28:42'),
(5, 35, 32, '2021-04-16 21:29:02', '2021-04-16 21:29:02'),
(6, 32, 35, '2021-04-16 21:29:02', '2021-04-16 21:29:02'),
(7, 36, 37, '2021-04-16 22:06:04', '2021-04-16 22:06:04'),
(8, 37, 36, '2021-04-16 22:06:04', '2021-04-16 22:06:04'),
(9, 43, 40, '2021-04-19 11:58:16', '2021-04-19 11:58:16'),
(10, 40, 43, '2021-04-19 11:58:16', '2021-04-19 11:58:16'),
(11, 32, 43, '2021-04-20 11:39:18', '2021-04-20 11:39:18'),
(12, 43, 32, '2021-04-20 11:39:18', '2021-04-20 11:39:18'),
(13, 44, 37, '2021-04-23 17:12:21', '2021-04-23 17:12:21'),
(14, 37, 44, '2021-04-23 17:12:21', '2021-04-23 17:12:21'),
(15, 34, 45, '2021-04-24 04:51:44', '2021-04-24 04:51:44'),
(16, 45, 34, '2021-04-24 04:51:44', '2021-04-24 04:51:44'),
(17, 34, 41, '2021-04-24 05:44:35', '2021-04-24 05:44:35'),
(18, 41, 34, '2021-04-24 05:44:35', '2021-04-24 05:44:35'),
(19, 47, 42, '2021-04-30 08:39:29', '2021-04-30 08:39:29'),
(20, 42, 47, '2021-04-30 08:39:29', '2021-04-30 08:39:29'),
(21, 32, 44, '2021-04-30 16:18:46', '2021-04-30 16:18:46'),
(22, 44, 32, '2021-04-30 16:18:46', '2021-04-30 16:18:46'),
(23, 32, 47, '2021-04-30 16:19:02', '2021-04-30 16:19:02'),
(24, 47, 32, '2021-04-30 16:19:02', '2021-04-30 16:19:02'),
(25, 44, 47, '2021-05-01 06:58:43', '2021-05-01 06:58:43'),
(26, 47, 44, '2021-05-01 06:58:43', '2021-05-01 06:58:43'),
(27, 34, 32, '2021-05-03 05:12:31', '2021-05-03 05:12:31'),
(28, 32, 34, '2021-05-03 05:12:31', '2021-05-03 05:12:31'),
(29, 32, 48, '2021-05-06 08:58:50', '2021-05-06 08:58:50'),
(30, 48, 32, '2021-05-06 08:58:50', '2021-05-06 08:58:50'),
(31, 32, 45, '2021-05-06 08:59:17', '2021-05-06 08:59:17'),
(32, 45, 32, '2021-05-06 08:59:17', '2021-05-06 08:59:17'),
(33, 34, 48, '2021-05-08 04:53:58', '2021-05-08 04:53:58'),
(34, 48, 34, '2021-05-08 04:53:58', '2021-05-08 04:53:58'),
(35, 34, 43, '2021-05-08 07:01:19', '2021-05-08 07:01:19'),
(36, 43, 34, '2021-05-08 07:01:19', '2021-05-08 07:01:19'),
(37, 44, 48, '2021-05-12 03:28:25', '2021-05-12 03:28:25'),
(38, 48, 44, '2021-05-12 03:28:25', '2021-05-12 03:28:25'),
(39, 33, 47, '2021-05-17 10:13:29', '2021-05-17 10:13:29'),
(40, 47, 33, '2021-05-17 10:13:29', '2021-05-17 10:13:29'),
(41, 33, 45, '2021-05-17 10:14:58', '2021-05-17 10:14:58'),
(42, 45, 33, '2021-05-17 10:14:58', '2021-05-17 10:14:58'),
(43, 33, 42, '2021-05-17 11:09:43', '2021-05-17 11:09:43'),
(44, 42, 33, '2021-05-17 11:09:43', '2021-05-17 11:09:43'),
(45, 33, 43, '2021-05-17 11:09:51', '2021-05-17 11:09:51'),
(46, 43, 33, '2021-05-17 11:09:51', '2021-05-17 11:09:51'),
(47, 33, 41, '2021-05-17 11:09:59', '2021-05-17 11:09:59'),
(48, 41, 33, '2021-05-17 11:09:59', '2021-05-17 11:09:59'),
(49, 33, 38, '2021-05-17 11:10:04', '2021-05-17 11:10:04'),
(50, 38, 33, '2021-05-17 11:10:04', '2021-05-17 11:10:04'),
(51, 33, 34, '2021-05-17 11:10:11', '2021-05-17 11:10:11'),
(52, 34, 33, '2021-05-17 11:10:11', '2021-05-17 11:10:11'),
(53, 48, 43, '2021-05-18 10:16:20', '2021-05-18 10:16:20'),
(54, 43, 48, '2021-05-18 10:16:20', '2021-05-18 10:16:20'),
(55, 49, 47, '2021-05-18 12:03:57', '2021-05-18 12:03:57'),
(56, 47, 49, '2021-05-18 12:03:57', '2021-05-18 12:03:57'),
(57, 33, 37, '2021-05-19 04:18:44', '2021-05-19 04:18:44'),
(58, 37, 33, '2021-05-19 04:18:44', '2021-05-19 04:18:44'),
(59, 34, 49, '2021-05-19 04:29:51', '2021-05-19 04:29:51'),
(60, 49, 34, '2021-05-19 04:29:51', '2021-05-19 04:29:51'),
(61, 32, 37, '2021-05-19 04:31:18', '2021-05-19 04:31:18'),
(62, 37, 32, '2021-05-19 04:31:18', '2021-05-19 04:31:18'),
(63, 36, 45, '2021-05-19 04:44:21', '2021-05-19 04:44:21'),
(64, 45, 36, '2021-05-19 04:44:21', '2021-05-19 04:44:21'),
(65, 37, 40, '2021-05-19 04:52:02', '2021-05-19 04:52:02'),
(66, 40, 37, '2021-05-19 04:52:02', '2021-05-19 04:52:02'),
(67, 36, 43, '2021-05-19 05:01:41', '2021-05-19 05:01:41'),
(68, 43, 36, '2021-05-19 05:01:41', '2021-05-19 05:01:41'),
(69, 36, 33, '2021-05-19 05:20:40', '2021-05-19 05:20:40'),
(70, 33, 36, '2021-05-19 05:20:40', '2021-05-19 05:20:40'),
(71, 34, 50, '2021-05-19 06:12:02', '2021-05-19 06:12:02'),
(72, 50, 34, '2021-05-19 06:12:02', '2021-05-19 06:12:02'),
(73, 47, 50, '2021-05-19 06:22:47', '2021-05-19 06:22:47'),
(74, 50, 47, '2021-05-19 06:22:47', '2021-05-19 06:22:47'),
(75, 47, 34, '2021-05-19 06:23:38', '2021-05-19 06:23:38'),
(76, 34, 47, '2021-05-19 06:23:38', '2021-05-19 06:23:38'),
(77, 39, 34, '2021-05-19 06:53:17', '2021-05-19 06:53:17'),
(78, 34, 39, '2021-05-19 06:53:17', '2021-05-19 06:53:17'),
(79, 32, 39, '2021-05-19 07:02:31', '2021-05-19 07:02:31'),
(80, 39, 32, '2021-05-19 07:02:31', '2021-05-19 07:02:31'),
(81, 39, 33, '2021-05-19 07:06:40', '2021-05-19 07:06:40'),
(82, 33, 39, '2021-05-19 07:06:40', '2021-05-19 07:06:40'),
(83, 33, 50, '2021-05-21 03:52:16', '2021-05-21 03:52:16'),
(84, 50, 33, '2021-05-21 03:52:16', '2021-05-21 03:52:16'),
(85, 47, 43, '2021-05-21 06:46:38', '2021-05-21 06:46:38'),
(86, 43, 47, '2021-05-21 06:46:38', '2021-05-21 06:46:38'),
(87, 32, 50, '2021-05-28 11:05:31', '2021-05-28 11:05:31'),
(88, 50, 32, '2021-05-28 11:05:31', '2021-05-28 11:05:31'),
(89, 50, 49, '2021-05-28 11:16:54', '2021-05-28 11:16:54'),
(90, 49, 50, '2021-05-28 11:16:54', '2021-05-28 11:16:54'),
(91, 39, 49, '2021-05-28 12:32:04', '2021-05-28 12:32:04'),
(92, 49, 39, '2021-05-28 12:32:04', '2021-05-28 12:32:04'),
(93, 39, 50, '2021-05-28 12:47:14', '2021-05-28 12:47:14'),
(94, 50, 39, '2021-05-28 12:47:14', '2021-05-28 12:47:14'),
(95, 44, 42, '2021-05-29 03:25:19', '2021-05-29 03:25:19'),
(96, 42, 44, '2021-05-29 03:25:19', '2021-05-29 03:25:19'),
(97, 44, 38, '2021-05-30 03:47:31', '2021-05-30 03:47:31'),
(98, 38, 44, '2021-05-30 03:47:31', '2021-05-30 03:47:31'),
(99, 44, 43, '2021-05-30 04:22:58', '2021-05-30 04:22:58'),
(100, 43, 44, '2021-05-30 04:22:58', '2021-05-30 04:22:58'),
(101, 44, 41, '2021-05-31 04:40:02', '2021-05-31 04:40:02'),
(102, 41, 44, '2021-05-31 04:40:02', '2021-05-31 04:40:02'),
(103, 47, 40, '2021-06-02 04:58:08', '2021-06-02 04:58:08'),
(104, 40, 47, '2021-06-02 04:58:08', '2021-06-02 04:58:08'),
(105, 51, 41, '2021-06-02 06:32:43', '2021-06-02 06:32:43'),
(106, 41, 51, '2021-06-02 06:32:43', '2021-06-02 06:32:43'),
(107, 51, 50, '2021-06-02 06:33:24', '2021-06-02 06:33:24'),
(108, 50, 51, '2021-06-02 06:33:24', '2021-06-02 06:33:24'),
(109, 44, 51, '2021-06-03 18:35:17', '2021-06-03 18:35:17'),
(110, 51, 44, '2021-06-03 18:35:17', '2021-06-03 18:35:17'),
(111, 49, 33, '2021-06-11 10:10:07', '2021-06-11 10:10:07'),
(112, 33, 49, '2021-06-11 10:10:07', '2021-06-11 10:10:07'),
(113, 52, 44, '2021-06-13 18:49:48', '2021-06-13 18:49:48'),
(114, 44, 52, '2021-06-13 18:49:48', '2021-06-13 18:49:48'),
(115, 53, 49, '2021-07-16 13:58:40', '2021-07-16 13:58:40'),
(116, 49, 53, '2021-07-16 13:58:40', '2021-07-16 13:58:40'),
(117, 53, 45, '2021-07-16 13:59:21', '2021-07-16 13:59:21'),
(118, 45, 53, '2021-07-16 13:59:21', '2021-07-16 13:59:21'),
(119, 33, 54, '2021-07-16 14:00:55', '2021-07-16 14:00:55'),
(120, 54, 33, '2021-07-16 14:00:55', '2021-07-16 14:00:55'),
(121, 55, 56, '2021-07-17 06:39:50', '2021-07-17 06:39:50'),
(122, 56, 55, '2021-07-17 06:39:50', '2021-07-17 06:39:50'),
(123, 44, 59, '2021-07-19 04:55:28', '2021-07-19 04:55:28'),
(124, 59, 44, '2021-07-19 04:55:28', '2021-07-19 04:55:28'),
(125, 64, 63, '2021-07-27 04:42:57', '2021-07-27 04:42:57'),
(126, 63, 64, '2021-07-27 04:42:57', '2021-07-27 04:42:57'),
(127, 63, 61, '2021-07-27 04:45:44', '2021-07-27 04:45:44'),
(128, 61, 63, '2021-07-27 04:45:44', '2021-07-27 04:45:44'),
(129, 33, 56, '2021-07-31 14:27:36', '2021-07-31 14:27:36'),
(130, 56, 33, '2021-07-31 14:27:36', '2021-07-31 14:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `daily_quota`
--

CREATE TABLE `daily_quota` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purr_usage` int(11) DEFAULT '0',
  `block_usage` int(11) NOT NULL DEFAULT '0',
  `favorite_usage` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_quota`
--

INSERT INTO `daily_quota` (`id`, `user_id`, `purr_usage`, `block_usage`, `favorite_usage`, `created_at`, `updated_at`) VALUES
(6, 32, 0, 0, 0, '2021-04-14 13:52:38', '2021-05-28 10:47:28'),
(7, 35, 0, 0, 0, '2021-04-15 12:46:57', '2021-05-03 08:41:03'),
(8, 37, 0, 0, 0, '2021-04-15 14:07:03', '2021-05-21 17:52:31'),
(9, 42, 0, 0, 0, '2021-04-16 05:14:55', '2021-04-16 05:41:54'),
(10, 33, 0, 0, 0, '2021-04-16 05:39:21', '2021-05-21 10:19:07'),
(11, 41, 0, 0, 0, '2021-04-16 10:41:57', '2021-05-18 07:11:22'),
(12, 36, 0, 0, 0, '2021-04-17 19:35:36', '2021-04-18 03:53:18'),
(13, 43, 0, 0, 0, '2021-04-19 11:56:36', '2021-04-19 11:58:10'),
(14, 44, 0, 0, 0, '2021-04-24 00:32:27', '2021-07-15 21:32:55'),
(15, 45, 0, 0, 0, '2021-04-24 04:47:45', '2021-04-24 04:56:28'),
(16, 47, 0, 0, 0, '2021-04-30 11:41:26', '2021-06-02 05:12:00'),
(17, 34, 0, 0, 0, '2021-05-03 08:46:58', '2021-05-29 10:35:15'),
(18, 48, 0, 0, 0, '2021-05-18 06:57:33', '2021-05-18 10:16:12'),
(19, 49, 0, 0, 0, '2021-05-18 11:40:00', '2021-05-28 13:08:51'),
(20, 50, 0, 0, 0, '2021-05-19 06:15:55', '2021-05-28 11:20:47'),
(21, 39, 0, 0, 0, '2021-05-19 08:40:10', '2021-05-29 10:01:12'),
(22, 51, 0, 0, 0, '2021-06-02 05:43:30', '2021-06-10 10:26:50'),
(23, 52, 0, 0, 0, '2021-06-13 18:49:23', '2021-06-13 18:49:38'),
(24, 64, 0, 0, 0, '2021-07-27 04:42:49', '2021-07-27 04:45:06'),
(25, 63, 0, 0, 0, '2021-07-27 04:44:47', '2021-07-27 04:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `ethnicities`
--

CREATE TABLE `ethnicities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `radioBtnSelcted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ethnicities`
--

INSERT INTO `ethnicities` (`id`, `title`, `radioBtnSelcted`, `created_at`, `updated_at`) VALUES
(1, 'Asian', 1, '2020-09-22 07:18:19', '2020-09-22 07:18:19'),
(2, 'Black', 0, '2020-09-22 07:18:19', '2020-09-22 07:18:19'),
(3, 'Latino', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(4, 'Middle Eastern', 0, '2020-09-22 06:17:16', '2020-09-22 06:17:16'),
(5, 'Mixed', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(6, 'Native American', 0, '2020-09-22 04:11:11', '2020-09-22 04:11:11'),
(7, 'South Asian', 0, '2020-09-22 12:31:31', '2020-09-22 12:31:31'),
(8, 'White', 0, '2020-09-22 07:19:19', '2020-09-22 07:19:19'),
(9, 'Other', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(10, 'Not To Answer', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `fav_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `user_id`, `fav_user_id`, `created_at`, `updated_at`) VALUES
(3, 37, 32, '2021-04-15 12:42:41', '2021-04-15 12:42:41'),
(6, 42, 33, '2021-04-16 05:18:31', '2021-04-16 05:18:31'),
(8, 33, 42, '2021-04-16 05:36:48', '2021-04-16 05:36:48'),
(9, 41, 38, '2021-04-16 10:41:57', '2021-04-16 10:41:57'),
(12, 33, 38, '2021-04-16 12:35:23', '2021-04-16 12:35:23'),
(15, 41, 40, '2021-04-16 12:41:34', '2021-04-16 12:41:34'),
(16, 35, 32, '2021-04-16 12:41:56', '2021-04-16 12:41:56'),
(17, 41, 42, '2021-04-16 12:46:56', '2021-04-16 12:46:56'),
(23, 32, 42, '2021-04-16 13:59:19', '2021-04-16 13:59:19'),
(24, 36, 37, '2021-04-16 20:11:24', '2021-04-16 20:11:24'),
(27, 43, 40, '2021-04-19 11:58:06', '2021-04-19 11:58:06'),
(29, 37, 44, '2021-04-23 17:38:33', '2021-04-23 17:38:33'),
(30, 34, 45, '2021-04-24 04:40:42', '2021-04-24 04:40:42'),
(37, 34, 42, '2021-04-24 06:21:35', '2021-04-24 06:21:35'),
(38, 33, 32, '2021-05-01 06:50:20', '2021-05-01 06:50:20'),
(40, 34, 43, '2021-05-03 04:10:43', '2021-05-03 04:10:43'),
(41, 32, 44, '2021-05-07 06:52:02', '2021-05-07 06:52:02'),
(43, 34, 48, '2021-05-08 11:27:57', '2021-05-08 11:27:57'),
(48, 48, 47, '2021-05-18 06:57:33', '2021-05-18 06:57:33'),
(49, 48, 45, '2021-05-18 06:57:44', '2021-05-18 06:57:44'),
(50, 48, 44, '2021-05-18 07:01:47', '2021-05-18 07:01:47'),
(51, 48, 43, '2021-05-18 10:16:08', '2021-05-18 10:16:08'),
(53, 49, 45, '2021-05-18 11:52:12', '2021-05-18 11:52:12'),
(63, 47, 49, '2021-05-18 12:12:22', '2021-05-18 12:12:22'),
(65, 34, 49, '2021-05-19 04:53:45', '2021-05-19 04:53:45'),
(66, 47, 33, '2021-05-19 06:08:31', '2021-05-19 06:08:31'),
(67, 34, 50, '2021-05-19 06:11:56', '2021-05-19 06:11:56'),
(68, 50, 34, '2021-05-19 07:07:54', '2021-05-19 07:07:54'),
(71, 50, 47, '2021-05-19 08:30:21', '2021-05-19 08:30:21'),
(72, 39, 34, '2021-05-19 08:57:44', '2021-05-19 08:57:44'),
(73, 39, 49, '2021-05-28 12:47:57', '2021-05-28 12:47:57'),
(75, 44, 43, '2021-05-29 03:24:39', '2021-05-29 03:24:39'),
(76, 44, 42, '2021-05-29 03:25:15', '2021-05-29 03:25:15'),
(77, 50, 49, '2021-06-01 07:24:40', '2021-06-01 07:24:40'),
(78, 51, 50, '2021-06-02 06:23:51', '2021-06-02 06:23:51'),
(79, 51, 49, '2021-06-02 06:23:57', '2021-06-02 06:23:57'),
(80, 51, 47, '2021-06-02 06:24:02', '2021-06-02 06:24:02'),
(82, 51, 45, '2021-06-02 06:24:12', '2021-06-02 06:24:12'),
(83, 51, 43, '2021-06-02 06:24:20', '2021-06-02 06:24:20'),
(84, 51, 42, '2021-06-02 06:24:26', '2021-06-02 06:24:26'),
(85, 51, 41, '2021-06-02 06:24:34', '2021-06-02 06:24:34'),
(86, 51, 40, '2021-06-02 06:24:43', '2021-06-02 06:24:43'),
(87, 51, 39, '2021-06-02 06:24:50', '2021-06-02 06:24:50'),
(88, 44, 52, '2021-06-14 11:21:02', '2021-06-14 11:21:02'),
(90, 44, 37, '2021-07-03 07:46:10', '2021-07-03 07:46:10'),
(91, 49, 38, '2021-07-14 07:10:26', '2021-07-14 07:10:26'),
(92, 64, 63, '2021-07-27 04:42:49', '2021-07-27 04:42:49');

-- --------------------------------------------------------

--
-- Table structure for table `health`
--

CREATE TABLE `health` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `health_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `health`
--

INSERT INTO `health` (`id`, `health_name`, `created_at`, `updated_at`) VALUES
(1, 'Covid19', '2020-11-25 00:00:00', '2020-11-25 00:00:00'),
(2, 'HIV', '2020-11-25 00:00:00', '2020-11-25 00:00:00'),
(3, 'Other', '2020-11-25 00:00:00', '2020-11-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `health_user_report`
--

CREATE TABLE `health_user_report` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `test_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1= positive, 2= negative',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `health_user_report`
--

INSERT INTO `health_user_report` (`id`, `user_id`, `report`, `report_name`, `report_id`, `last_date`, `test_status`, `created_at`, `updated_at`) VALUES
(1, 36, NULL, 'Covid19', '1', '04/08/2021', '2', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(2, 36, NULL, 'HIV', '2', '04/08/2021', '2', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(3, 36, NULL, 'other', '3', '04/08/2021', '2', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(4, 37, NULL, 'Covid19', '1', '04/08/2021', '2', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(5, 37, NULL, 'HIV', '2', '04/08/2021', '2', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(6, 37, NULL, 'other', '3', '04/00/2021', '2', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(7, 43, NULL, 'HIV', '2', '08/03/2021', '2', '2021-04-19 11:56:05', '2021-04-19 11:56:05'),
(9, 44, NULL, 'HIV', '2', '04/20/21', '2', '2021-04-23 15:38:55', '2021-04-23 15:38:55'),
(10, 44, NULL, 'other', '3', '04/20/21', '2', '2021-04-23 15:38:55', '2021-04-23 15:38:55'),
(11, 45, NULL, 'other', '3', '08/03/2021', '', '2021-04-24 04:40:00', '2021-04-24 04:40:00'),
(13, 32, NULL, 'Covid19', '1', '12/12/1990', '3', '2021-04-30 05:55:01', '2021-04-30 05:55:01'),
(15, 47, NULL, 'Covid19', '1', '12/12/2020', '', '2021-04-30 06:15:09', '2021-04-30 06:15:09'),
(16, 48, NULL, 'other', '3', '08/04/2021', '2', '2021-05-03 05:49:28', '2021-05-03 05:49:28'),
(17, 34, NULL, 'Other', '3', '08/04/2021', '', '2021-05-08 06:37:13', '2021-05-08 06:37:13'),
(18, 50, NULL, 'other', '3', '02/05/2021', '2', '2021-05-19 06:11:11', '2021-05-19 06:11:11'),
(19, 51, NULL, 'other', '3', '08/05/2021', '2', '2021-06-02 05:38:40', '2021-06-02 05:38:40'),
(20, 44, NULL, 'Covid19', '1', '06/21/21', '', '2021-06-25 19:26:30', '2021-06-25 19:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `iams`
--

CREATE TABLE `iams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iams`
--

INSERT INTO `iams` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Lesbian', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(2, 'Bisexual', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(3, 'MTF Transgender', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(4, 'Straight curious', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(5, 'Soft fem lesbian', '2020-11-20 13:35:23', '2020-11-20 13:35:23'),
(6, 'Lipstick lesbian', '2020-11-20 13:35:29', '2020-11-20 13:35:29'),
(7, 'Chapstick lesbian', '2020-11-20 13:35:35', '2020-11-20 13:35:35'),
(8, 'Stone butch lesbian', '2020-11-20 13:35:40', '2020-11-20 13:35:40'),
(9, 'Hasbian lesbian', '2020-11-20 13:35:45', '2020-11-20 13:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `lookingfors`
--

CREATE TABLE `lookingfors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `radioBtnSelcted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookingfors`
--

INSERT INTO `lookingfors` (`id`, `title`, `radioBtnSelcted`, `created_at`, `updated_at`) VALUES
(1, 'Chat', 1, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(2, 'Friends', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(3, 'Dates', 1, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(4, 'Relationship', 1, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(5, 'Networking', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(6, 'Right Now', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12'),
(7, 'Not Specified', 0, '2020-09-22 05:12:12', '2020-09-22 05:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_to_id` int(11) NOT NULL,
  `user_from_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `media` longtext COLLATE utf8mb4_unicode_ci,
  `audio` longtext COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `album_given_access` int(11) NOT NULL DEFAULT '0',
  `album_has_access` int(11) NOT NULL DEFAULT '0',
  `message_is_read` int(11) DEFAULT '0',
  `message_is_sent` int(255) NOT NULL DEFAULT '1',
  `message_is_not_seen` int(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message_id`, `user_to_id`, `user_from_id`, `message`, `media`, `audio`, `latitude`, `longitude`, `album_given_access`, `album_has_access`, `message_is_read`, `message_is_sent`, `message_is_not_seen`, `created_at`, `updated_at`) VALUES
(1, 'cb452054-0272-4e57-91e4-1ca57609ee1d', 47, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 07:28:25', '2021-05-21 07:28:25'),
(2, 'e49e961d-c403-44ef-9633-a0a4036f9de9', 47, 33, NULL, NULL, NULL, '30.7111153', '76.7054226', 0, 0, 1, 0, 0, '2021-05-21 07:28:28', '2021-05-21 07:28:28'),
(3, 'fc51020a-bde9-41f9-ae1d-71b7b9f71837', 47, 33, '', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAEsAOEDASIAAhEBAxEB/8QAHwAAAAYDAQEBAAAAAAAAAAAAAAMEBQYHAggJAQoL/8QAShAAAgICAQQBAwMBBAUIBwcFAQIDEQQSBQYTISIABzEyCBQjQRUzQpNRUlPS0wkWVGGRktHUGFZiY5Wh1RklQ1VXcpSi4eLk8P/EABsBAAIDAQEBAAAAAAAAAAAAAAIDAAEEBQYH/8QAOhEAAQIEAwYFAwMDAwUBAAAAAQIRACExQQNRYRJxgZGh8AQTscHRIuHxBTJSFEJiI3KSM0Nzk9Ki/9oADAMBAAIRAxEAPwC68njW2ZWRijq3bNuJAul7IodZAzNJKBHGZGUyLZVmot+TgFbibdAGYE6hSA1KzB4mpWk2aNNaYAKACBIBaEnDyxIWlV0pWYBgFURkOxAWQuzEAGntXZWPt7ElnzuPmku1ddxSFlBLIqghtATCVWypsI3jvEgOfnX8p2YFsnla5n1zylnBoQ1ZHuVYqhuOVQHVUZiAESiFKswJjcAsyOwHkuRJIgvuOmujXJglShtUVyVVGYqxKIwLaBmla2CqD4//AA0WRhY+WjPxwJISJbdmcJRYRRrIERiSfBQhi4LMx9wuzP7tUvGAMRXlDpso1jNo7MymvctTEvozJIVNM7Ro9HDE5ENXpKfc8oYldjz+fmKymxK2R0LBR3Gi7bMAu5TcLoGCkeHHbVH8RKUBUFLLiKYwirHHYCgKhNmIsHVBIAwK+4DFSlWdjEDdgS8cx8upJAoC9gV8r6gKVjQhiybEyrRJLMC3xnzMAiMOYyBVqDuXDDSgyBm+xZdgNArlm8ux0zqwibEE1ZmeUzy0e+jArI6yO74HSIMcfwtq7BQHejI5bYkt4ZNnIRAFVt1KIBHGTsESvEPYG0ZKUqoUItR1Q+xLEFUZgSHCsAGBKPKnw2VkQCOgUABojUxt2wi+RIPCu+zKF9q2jLFkL4xooAy6794hZFJaMDXUhmdwE1c+pNKykaLqiVYeczKYHDq13Z4aFAgAmcqychuEz+IisqhRIq/lKVL0FcElQwBAVlBcNQOkxZrYWpQyJZUYtqF2CH8ibVnsAqC7kkalV9VkNihtTF5DPCVZk8GgUFM0VAoRTH1VjJIWJKitSNSArRhulRm9SQ0dRnuIildTsWL35dh4oNIe2wZmLAqVzLw562NAd9aQwLILVBc8mHfpDQyKWdgS5FkRsyq3gR0rKmrNrsaVG11LAKquwVrmAgR1nkRUiBp2JJRSgZdjIWeRAPVX8pHHozmR/wBxMz7JCAB279WYtTKUAXQByFtdXMQijjJID7bhl1WFBkLukpjX8BYVvdNW9SKYpsKVxs+qh1IZYtWcZ1INC2/lOmlxuIENCtqQycvwhqkaNDdgglizSAAK6kanY9zZWJBbchUCMpC0r/CmkaGM3G5adY9lYKe2ilQSCNzbmZQjAx0khVbeFiwGM8Uwj7UJwlimSXHyVVBA3lozjExkBtSYJcSZUVkcSpOgx5MfJJye6d3eVZFdRGeyY2V3VXCBZFLmk92INKzM7x6sNlRBQRuwYhrEaADz4uiWqwAtrfs+yWKAvx8wMxdCAUKH0C7+oOplQsWNaEszNowtSrEKWLDBnVVagw7ikpUjgsUI0aMMW20O2zMR5MjKyq1uQWALWW/kYVRCoHRi6D8g1xqrPQPqbVSGpgKg4Ok+6Tr3Qk10vJ5a9N0HtM0gJ9k9jqaJBZSDuxNj7OyWdUO1hSW8Vt1lyU39pfs3a0xcTGAx0rsyyzQDNnlMZZ4oXneQKxU6SmOKUs7iLabSzF1JR1CkANZBWWjvsEcKWCAq2xZ2UmMy6tqPkB6kxnnihmhMZGDBDi5MCE7QQJumFlasqNJDJGjRmR1VEkRoQ8hGy5V5znI9PvLjDu98MsPISY00WRBK8c0MiTKwkkvuRSespsqwVvCspoOsep1UIDasuQs0MGdDoq58EcxVQwuaRW7sK0bjYzJKsUbPa6qAAIzpTaJJM8cSBpMglAI0b1klK7BNPJsEs7Je4qyAWY/LPKDGhwsIDZ8XEELkFd2yXeSSZWkoBlhnnMVF/FOdjb/FxchQvY5Ghc0NZSbpCt5FkVQzgFgCQCQRqSLUEbrQV0eNwArMSVAVwEzPHYDtqXGxJYFWDkswKq+hA2BBQGyLVW0RgSrGM6hmJJMnilkEhDECNVBK+LA9RagFRVsc4yZmTHiZO5MwXFVkKoCI9jY2UTKiK00iwOGZUI8EF/gLLBs9/tWu7QwaACQTQGwrS5uLBhOYvGGHH+z/AHE03b/ZNlxTYcjIhaHMlGQubmB37euG57CM8ZsTHMOSqwrJK3v92aCkB7O4YKDSMbICqDsVEaq43BATyLPxRkusuT2oQhhjVcbGU7+VRUBntqLGRj3Br5MbasWkJaRGuOvb7SBEiUusR31/bwuN+0lm1jGxESDVRJSQKIkGioYTnp7DqwgqTRiqFhKYzWx7ZJ/lDqQUtQ0gYFloakgaAWzeKdA4NrsFlDOV/L+NAY4tQUO0oAC6ohJVQSq/PGASNaIYoSIx6sL0VGEbMKYPs+zIzHUMNgoWM5ldiumhcragkFiQ5ZAjlQTJStYLBlcUjSBoyBUCQW5SnOCCXfRpZ5zMgb+0GKw9VKJUdu6EoEYL4VW2ZmcMtCJgAC5BYEkoqpGZxTIRbUEpGO7qCrAqWdWJAP8AHIPXbQsA2qKNVLRn12YC1aQtsEUCNy0mrFSvbV9woClVAUgn4ZEdlK+SVfuSAstOHC13C7sCoF7bs2yEsg9lVlEENKvbfMXskuMqAznInS4nuh23x/8Ar/73/wDrfB8b+3N/tov/AJ/8P4PlMOwNNdOg4DsnMWzvs/PbT6+ZHFSKFi9tmZTsSPCgEsFZgVtTIS3ksEOjqwmOrM3EsF9VCWdxsFA7bOXvekiJYgFgpMS2zSAkrrsP1D0Py3T3I5fGc1xHIcRymE6wZvG8rgZHGZ2KWWOWOPIwcmKOaBjCwYCREk0YNHoklvDM3hWJcyJoUcMxKhzFp+IIUAFAg913G5Zl3Y00n1nysNYdM7ghtC4lk3rHitsgsRMSyMUrlcU+xpa1JJVm9SSyrqq0AWZU9U9mP30KDyxZPEEMSpYh6ABMm6l7Qpar+Z/u/wAQWBLo4JU/LryuFbZvXuqgYW20YpWdrHcMbU5Qk91h+b2aQt8Yc7iWXyNmBBp+3GTIRe+7aBACitZFUErbYqfgHAUP2lw15Tl989YYnEpO1DSxys2ebZxS8vHysCNZKKMAGMlyAlW2CKwBUMGUEqfHl9rVRH8jCc7EoVSh4O32JRCyCRlkryKRVdCG/wAWqh7jyuJBayqq2pjAW3qS2VW1C7AhFIZmKdse/wDIWK/Irkca4HcCAGpELaqoXWTUKxBKkBztdsDaBCU8lCkvUEFq+2vEcjDUrIbN2fk297xVeThFmNOLsrSgx6hn9WCqUUALW3sykDyxLAqxZWCEDgowVtu6AZR4QspHu6lv8TEMux1l3VHBLWllYThjE0bsYgwCtZsFgUcBWK+C+qMQG319S618j+Rg91B96CMCH3Q/lqw0Zl2Q0StlSySF/wDXZELw6nO/K3Dt4eFhp19Z+sVvNigEAjtgjuDZm3AVAC4AFxEC9z7g+vbBA8Ms0cySMsaOnhGJSwUCncAg9oFGBX7AAWpQnVSbAyMNVV0KKq+iqJAq+Q1jwAArL+EdsASwYtIWI+RzIgcKY2UjwWUfcoofyxBIUAEjdgXLrUdMAGGReEQ5ArYzHCbDd+IahdTPJtZRDZQVd93RmUSUQCGiWONgqsyki2IIVFDhmY9oFAQrX2oowTZKBfQNr5QE/eiYxcjsCQrj0RS9nYSieJirMbUeAzW4WgjRqFtgo2Kg2EDEXRt9lbmxIxbyLrChHcjUSayOjKygOFWQMzId0YkMpKEauhbDiIqGmKA8JG0vUQ1KwGYtoAeUhO+VaRHs6FRAkQLCaYxu7yM6SLGryOuPYY9xVYO4DNIBGWConqGjpQxO0wQKzqwmQsUiLbdsSdskaTAgwmWNIy8SxxSdxIYAkin3aVmYF5C6+F0CoojJUowVXZVUNF2yCwRbQtqLaprkoUASK2UsCSKVz4JCNRAGpDEdxPayfmdeGbyJnpQV3e5Z4elTyNfX7wxFkZVMW+iA7C2LRsS0vufcBacOCg2IbY/ZSULszaohIEqsoOzUysy+ApXZQiFHJIYUuwIVVLOWTCEe1UWUaMeiuSFDbKyilagwC2WCyFVLAO4ZtlVJDsloVWOOSOyAtIQZvx0ZdvZAiqfUqVSQGkHLhP3ENSoJrdu/fhxCd2VQx8eV804BJk8MrNGu1O0g0UMskjOSgtCyt84IeGRJpYpFVhHMmitrr2nUxyiWOZD6lUZf7yNZFRJY4nRYQxZjRqw1t4JUhgtfYC7YX7qqggEfYkNvrt9ioRL7qJsSxu0XYuak8IAE8sShKuUQQxIyh0N8WKYntIONxgCgiONx0eLOgtgIy/7jI7gMZZGkhijsWECBwY83LiR2LItF3dyttHb0VZXJ2YkhaN72i+inf4YwLHywYL4DAtVsh1vYSR0+mpW9nIO2wRJAmbZnkLalWPr4MgSNdwoBbcUysCw9WZSv4hUKpWAGa8uV+/mLBF5twlJgCBWU3mzTpGalgwjNrYbVVIZAVDbagBbVQ6KwCigAGVQSjrNhDCWcF5J1VI07epWEMuw1KkfyIAkQcEkor1esaJcde5Ivu2tF2k+/bjjPuQ7qRqCbVjGgoggEMNvJZhNJ+CGOgoVgOy8fkGMKGVypDot6kobQ+ATGkhyJSGtXb06tBBTbzIaM3Gfr09tUQR2HIdG13IXb2FgKjOP6OXalDeoG638Jd3kaTal2BUlvuySezKTIUBRqiUKA5YEa7CRNcoyQ9O1EFBX8dh0LrUbI7BSsgH8Y8ONIwFJEZz9mQhzIQe2tIVagUVS7xilkdReyUpUEhvCraiA5lu3VFeBh6GYBVSwm72jJI2lVohIX7Kxyxs7u0sojElxOHJD+oV95FfZRGJkKqZpkjIe6ZGZ0df42ADqtMt1242BZgpIbbeRqKgt3BssS1C3YZAsZYt7AAFPDDRFUNbMVoqQNWEoRQa5lkZ3ay5W2ZO4kcy26BlV2lBlhZQsncZoWIWYOrs8UYksaSJE9ZVly1Fqwz9rAUJ6yv3RoTRIzCvDArQHbBMJdldh97Z3DB1Y00mhd3Hoyr41BUMGbQ16u7FdZAAjgI2xYqqyUGG11G+2pGEMXoQCwuSO91UEOPOxsHfUKSvbYagPodl1+OePAoNK5ZFbYbE2QVkk2cLIigMqyaIhCq5CMz7blapqbcAKVA7/ETaDGfCYqKT9QAPUlfsT/AKU/7o/3fg+OPcx/9Zf8k/8Aj8HytlWR7/PrkYDa381f/W/sy/QC+rH0O+nv1k44YvV3Ej+0seAwcb1Hx5jxue42Mu0nZhy2jkTJwy8kpbj8+LKwtppZo4Isrt5EfI761fpN60+l0k+dLC/UHSu5GN1RxUDiDHRpxBCnNYlyycNNIZMdFXIkyMGSWdIMXksuVJEh7n/Cp4IcmGbGyYYsjHyIpIZ4J40lhnhlUpLDNFIGSSKRGZJI3VkdSVYEEj57vw3jMTw6gP8AqYd0KJcD/BVU6CaXJJS5ePL4uCnEH8VWUz8w4f1yMfMfm9K6Es6SMAN45SoNPsQQpctsXATZGpT6myvbIieZ04xJAX2QxlSQgK+hBV20b1aiWDbtXgGt77h/WL9H/BdQDM576brj8Jy0jz5eV01KywcJyEjESmPipAhHDTyN3ljxHDcQzzQRRnh8WBpG5u9VfTXmOmeSzeJ5ni8vjeSwZCs+DlwNDKisokEqlhq8UqGKSCZGlxsiF45YJWj7Zb0Hh8bB8Ul8JX1ADaw1fvTQTFw9w6SSA4Mo5eKjEwSNsMKbQfYU+T0ObzracaYZ3TzDZe0pIJJAYbICqCyqxgNHS2BYJBGurMahmdwzxsV7bMDRAaixHguQ3ntgJ7KXW9k8OWUOm2md08FDJ2mBAs192Zx5GtCwGJUBrUBgNmAOsD5Lpy1kCxWKZAGCam3IU6AqtKCQaAZbAA2Yj4xeHIhQ3lt08xasWjEdiC8hKfofWNXMziWABRUV9lBtAQatj/IdGBUsoFAMwpbDhQIhmcY+pHaItXCxldn2Qs52a1daAJ0kkf1oLodB82U5Pp4E7GNdow5UAKyC9rIJBLkqUY0oUFtmDLd19ynCsugdC1ArsEK+pJII9Q5HctmZSKA9qNOuReGR9STtJ6ijP+MoejEmAXbJ91J8GaYyihs3B8kMtKLDMVVCdr1vU/6ddGVTStGjMWdB8i2ZghHoKgVyJGtdmJu1VAbChWjtW0Kllb3BBLXNyHFm2JWiA9an8ipKnXUKAwIsOyimQlmMt7QnO40hXUpVFnFINgPPbYMnoBqWZBTdrY62XJORaQQQJTYjkXF27EOGIkkiYLt6cWfheKplwz7EgKrnwqkKCGbxoV1kZXC6IrH7AIxZmVgxclD2UECkgWryFWJRmJQKhqyQFVSVYFiujeLCGzcnACh5pEcHYLGo1AZ0VgrBnLst9xgtIyC3IcBaSFZuIAWfXa3LtXtuSwIJKhWGrasNlsbqN2iB+c9eFtEqebb5hpMWDHsQ9KmYGlpcnc77ZRX2Zjbs48as0YqwyvqgtfamVa2N06miGI9dmKVUpiUUEsq0WKBtFdQGXdyD4BdKNhzoD7qk1yoRTWLZX/wqxIk22JUH1AAIC2ygBVKjQnaOZcdARFlMZUEE7PZLOoNsTX/sqSWBOpSiSMqk2VI5Xz73yvGhK3LOHsX3dZ20lEWyNWLGgFAsEEF/xD1t42Hk+fbZlAaxTs0vY3Qa2QSg1Pkt+RtFVKIj01HqbFKmzayWdRo8YRVZiDfbRRIArJ51LtRZSxIagPUFydvjLkl10VVGpLbFUDupVhbEBjdAL4O2qVIO35Ix4iSC53H26ekakqcPQg29YZSpZpB2zHMD5CkMrKjpUgLp4YxAlo2F48gYJvrFOzc+kSE1SKSHNfYEkX9wlbOqgk34Hsof0dZtQGNydsqQzU+pCm7ZQTsrqAWqz615fW29mJeRGVRL27imJpJItipjpWbSeFaJUgGQAmJtI5I4chBFYelQoTN5P3vhHL2go2qQdu2BYXsyyauUV9nYMAVUIbIJVmC6ypQANiDqaAABjUFq2KnZW8hPLiwwWtfuUZQ5G5Vw6KKVYgXC+AxUsSwJVWAY7KfKguRCAFKVFZgXZhGAQ3llGgX21L/k0pRVFoJImcyIHdCqrXQb+nbQx9BvImN2++cGSBxGIwE7rqrsTS7Ju9rqDGIw5jDMGJJjFOVFWQp2bYmiNgKUl2Ysi+NluiV3NXGoKnS3D/M2YvTMw1OxOiWqWaVU9NWAUhVWrZGHvqdvhZAIKIFoEsRZIYaFLA8EalQmwAIBTZrQsqohDFjo8wTGYcMyE7DX/CyUtNIF1OxZrJjkJdhQRVCKSaUzyoZdgCKYsreysb9SSX9me7QlvJZXZl1BCLvdgXbNqfUFGouxZwzaNbgkkELqGjLF2KkJIw1Bs2GFKi0APCpaeQoslZAa2LLrdKpYmDm/t3TOc4Yg2yY9lpU/LRiqyCIKgQllFnUAsFb8CFna7VlPctQzUq9uRgpUKDHdeBGEJWN3/jKxM5AagCshUEKWBIYgg+WOMcZBCIWGpIcMJDSqX3UGNn8BdV7bC28IPRVkC1IiWryadvx1Aco/5FTuxKLoGVWR7LBrZmDLUHBEOWpw1nqHEvWvpGMAvdewIQhdz2qaOvGzx2VLChsVu4XJjKtG0c0jtChCB1NpIp8lbVY4taIDk+S4baRS3cZEN6CwRixt3I3QBdH3GgcjzubYiN2RXUSRr3dU3ZUuyJWdoFjmrtLqoQoUEKqIyYyAkcYZ00djUaxuWVkEZKSKdBQDlR2lOddfl9ICC/23/vZf+1P/ACfwfHL9pP8A6D/2r/4fB8Njke/yOcSP0Qfg+D4PnqI8/A+QXrr6cdI/Ubjhx3VHFx5RiEn7LkISMflOPeSN42fDzVVnVTuXfGmWfCmkSJ8jGmMUes6+D4SFqw1BaFKQpJcKSSCDoRFKSFApUAoGRBDg7wY5R/V79M/UXQ3f5TEQ8/00pjvmMPHKz4odJT2uU49ZppsVI3DocxZZcB1bFufHysgYiak8t040ZP8AGdShXwiqho6iwu1k2rKQRQYWWG1/Ql81T+q36Y+A6pTI5boxcfgOY0eWTiQpXh+TmMu7GIdwLxM5jaREWGNuOcrFEcbD7k+We94X9XCwnC8YwMgMcCR/8iQJZbQ+kSJSAHjm43glJdeATL/tkvT+KiehL5G0cUeS4EEMWjHuG2GqjYFrIeqb0KmizebX/V0ateY6femAisKzdtiGbS/CqGYlgLU2P6P/ABx/kg+bp9cfTnm+mOUy+J5jisnjeQxiwlxspAGZN2QTwyI3ZyIHIJx8qCWfEmUiSKd4mDfKP5bgyCwMaqGttiKrxdm18BRqQb1Oy3agk9NeGCNvDIUCxcMUqBbJ3lIacIyoxJlKvpUGBBcHa1dyDTUXtGqHLcKbYmIvsJNaLgEA0CzMvkksn3VVZVAILFda55DiVSQqwFye2/2uzaslCwwsaiqUbABSGZ9puY4TcudaNOqk/wDWCCSSKN9sVQsUtre3yr+W4URCeYobVWSJV/62ZXb8mok6M1eXplAGyr85+Nh7TkBlDIV68OMpCHIUxBNbTpbiAG6NKuuXLccPCaUIyELMpDOzDYg2GZFjLWVYj2Hr4NCCcjgujtSnyrbMVACsriQ6V4W6ZSypts7uoIZFN6ctxYuwHU2yeq+CCN6a7dnsWxOzECgWLUK55LjaJ/1mYMBS29LI2jGmYqQpNgLZohSFA+YVJdgWCp6A0kXnRo2IWFAUf8dZxTOThlQ2wj/JFDgL5amZQ1/nagB/Fn2IVLLCLZUFlnIazSXqGVnIddhQpTIpEZBWwqBLHkNa/JYOrN/GAD7H0UhSxBFhvDLoNipKm0ZjVlWhWbiFNgwTWw4KSakbBCBqhLlhbLr7BHKq9qNfmPHTIKAm8zegbtoehU25HkGitsnGKOyobCga0gJLR7Asg2o+gCAkhmVXNaqFiaciNlvVtSfKAsSC6rXZUkt5BFmNntC+wqkUTHLx2O50UHSwreWAYAaksD7gkkBqFg6khSDG8qAaq5RtvN2hcXehNhCzbMo1osSykHyCfmHES4fgfY96RtQqhzr331iJyR3s13aqL18gBWAalCsAdyTuAyswQtRIVA8bdxRsVBLUmwcgrszyNb6kajthpFBCszrqST8kWRECrGiq/mp9SSy2STt9vJLFWQlQ+pIJJDPIgA9UVXJJQ/dgFJ18qz072LIrYKCWf+SsagGO0KcxujQlnD0PvTmWiPOZRIyPrJG7v2JUDgi0ULj5CDcAgMWx5iSCEaCQJL2pMrBgyqIm0Mm7MxUqQV9kjjZzELQkszrs5DgCPZW8vbwLIrSzIzottIl20ttYTtsymXueqxhgysIyrMElRfjL28oTHZllx5gWpVCPiShAp8LGA+JIyirBkgaNg7TwZMRw8xapoPcj0h8Falb2FMRZPkhmW6CgDVgACSCFB1ZXb2jUeqEJNnYU9soApSyr9gppfRFUgEqwITWx8VNGyhkdAmrEK9qbK2AWBskBg+3lQAp9P7xSFiN0NUJAUFfLitwANyAGIUbKq0dpAoJAIUayDCXoO884jm85Nd7Tck5T0LR7GDS7KzEjZXBUDRWVl1Cq4GopQ/mlDEg+wU+KILqe4jb6geqi2oClXywAAb1sFmBZgHJYmx45amNggG9bHuoLCitj8kVQVKqxfYm7tYkRLIpQMdnPvb0qkC6aiQpIQ7r5Zi0bgpSiUg1g0lrjdy9ic6GTx7FGTYOi2F3JK6WAq0SgQoe2wbxR9lDLtGHLpHEGW0FMQDsxYsCQ1mx60CoDK2zM6jwDsDhHGy0YyAVVaOw2W/uW0sNQKs1nVACfCLGFc4scn1UUzBg0qyKVjRWILIAGVSCN1vXyYmOv3+B5eRyk3we87wZUAJF6Vk9N1jwgmPGYuAoANGNGVCXVAQWdQ6NIWbYlTswKhZCpXZDIMXHdpFeqKht1YBEkYx7EfelAYWzuuwvuHcMWkLxsdXkWTXS1pY2A8QrsNGAGq6lbCr4Gi2AG2MkxccgJdBQyso1ZlVXIBJRgjEt+KqVLOlFCNNlenCkLFsnmW3cZZZTUpRqbTlanG0IdIP8AZT//AMjG/wB34Pkm/s6X/aY3+ZJ/u/B8vyz2Dp8+mcg805dBppp0HD79/g+D4PncjkwPg+D4PkiQPg+D4PkiRD+s+g+luv8Ai24rqfi4c6JUmGJlAdvP46aZApyMDLUdyCUMkUhQ748zQxDJgnjQJ85q/Wr9M/UvRSZPL8XG3UnTCbs3I42NebxsRUzH+1sFHkkiiiKyqeQgMmEdY5J/2s2RFiDq18JyJ4MWCbJypocfGx4pJ8ifIkSKCCCJDJLNNLIVjjiiRWeSR2VERSzEAE/NnhfHY/hZIO1hkucJc070/wAVaiRP7gWEZ8fw2HjB1fSsUWKhs8xvmLER86HN8I6SNG8XqwJbUMFWqtXAAUf1YFgFfx5sLVWc9xQYsscQCoHVTTMSNWY+KUt5WvvdAU+7Ne5f1k5Xp/net+peY6a4nD4XhczPC8dx+NCmLD+3xceLDOYmOkMAxn5JoHz5cdYiuJLlNjiSUoZX1r5fHSRXCJZG3obBFXerL7Elaa7JtgAVskekxUfQjEKSgqSlRSf3IUQNpB1SS16GccYKclIVthBIChRQEtrR2cA0BEa0c1wpQSFI6YuVcg0AR2wVJKBlUAqbYp9gVAAs1VyXHEsw1ui1kKzaga7KD96tdhRZjfkK3gbM8xDB/IpQBwjKBYDEsq+THrdEi9vKkf1K6r8p3mYI9XtaCuQWILAABFJZjqpB3Zjr4JGqgEMTzsfDcHEFQxZqzA+34jThLNDoOOdXGvvaheQwTdsoC6sNgfUq5ciwF11DFu63sW8qGWNV+V3yOIFJtVKbKyn2amIK+trTEh6aqOwiDV2wFvHl4I2WQgMoZAFLFSNd2LMRSuhZvugIBCspGwcfK55SFKleo2IUJQVhqb2YmmUquw/vGpq8USxJ5+INoEG4rKRYHoa3joIILHKvZbpvipM3HCGSMIzMACWKhCqgkAjUEBfVgfQUpkZRq1CJZmOLIBGpP9abzS/1HqIwNa9WVUjXYHyVsjkIkG5bRBsCJAQCoJBcBVRVrtmkHsQA9u4Jb5EM2MgValxJqpBHjUqLUHX1XYhSQBvQX7KvznrBYi/ZjUghwbESO+kQbIRSGUACidqQs12ABqWZNXBbUlSDsFJAtmZ5Ymp6WMosg1ul0IJUNGBGfKA+RISCR7BFsCWZSA7edhqQSKjJcneiFIJBK0CrSF78AhAAySRpGxZwxVSAzE6vuWYIvtuFuQrJsEYKAoJ7YsYcUPIXA9Y2JLjoOAAhjyolQrFsFcaPKEemMjLqqH1ayaRzsAH9HvZQ3xnkiBABILhnbRT5ClrZSzj8I13ZkV2GyHyWMZL3Opkb81di2xtKpkDMCwCsA6EFgQPOrEmj5SsN2IKulD3WFyF/p5JVFIVXsKoAjUGwb0d85BEiKw9BcMTPjSUNaRshD7sEYD+CREI+7ASREMqhWomWMCRSAkkJSTdchTHCE1YL5alpSJC2yAhhTbM60xfxfsRd6sTRGBqVPlSrWD4Dog+xK0AwW7cgMqbLr9zmsZDswBYEBmiDsquAFC+GOplV2tSEsqwVh/dlUlJDAOa8aVy5zalycZKiOCPuWF+rM22pJNN7lQrOxIN6qQAQCSqqNGJV1JY0qml0vUtUgFavqW3Ya7yAEEFdyTFQhiUC6qrIARYLFWA9iRbLrtYVCAWPgalVcPgM9KpYt/hT0P4g1deQ1+WDlCwX+7UfJsGVs9IjnPThlGcURUjwH2XSzRVWo7FfDIu2pOyjZQXHkAr8eYUDogVY1d9gAVAOopwTaAJ3WLFmCg6BVUBf71FAihwfVVYRuUKuxUClMZCe1saVFRm92WVlCmi8Q6LITaEgKV7pDMqmMINrIYKCXTZVLNGiKbBEbMRhzzplLPuvWITmecLoQyFF7ZB8AFxYojT28ARhPuSvtSBVRy3baSYS2oXVWtVYldQRao26ooLEDVQC7LIwU+GFAtGPVxsQLVu6rFGCW5B/j7ZDAow9PBKBu3dAayDFKkIncEYugVryZCzKVohWY60jaBAFWzJGiL81IQH3VNzpxaErW9HkNZvZul4WftYf9pjf5kX/AA/g+Z95P+k//N/9/wCD4zYHfDXQ89Iz7RzPM/PbnRvvi+D4Pg+a4ywPg+D4PkiQPg+D4PkiQPmsP6nevx090mnSXH5Jj5XqgEZpilKS4vAwuveVzDlRZEP9r5IXAUSQTYeZx0XOY8pR1j22S5HPxOKwM7k86XsYXHYmTnZk+kknZxcSF555e3Ekkr9uKN20jjeRq1RGYgHkX9UutMzrPqPlefy7WTPyGGLj91JkwsGO4cTBjdYoNlxcZVheVYoRkSCTJlUzZEp+db9I8H/U+I8xY/0sAhZeilu6E6sxUdAAQyo5/wCoeI8rC8tJZeK4lZH9x0d9kbyRMRQ3OzNLJMzLtt+As0SKO3miHJDeSQSCqj/T8qfl6Il0UgKpU2dWJoD7ghnS2oeAa+xJ2qyeXmLLI1CwzKzHUbBAT/QHU2QxNAsFQUWO71fzEoCsfv5LWbawqhV8f4hVbDaiylPBG3z0PiyGYMwLNeTTO9+gNy3JwB9JOZ77n1iuOalDHT2DMDTKbIFWpYELdsAVIb1EhIQEmQU7zhKiTVyRVltlJcsCytZDfZWJACkDuWgc3tZ/OTKQxW9tnu2JAHqa1UUNmvc2AxKr5uxUnL5GpkARWVnGyOpTZv8A9oIK35JNsQQHK/x2OXjFkE6Hc9nvwuHjVglyf9ydX7pwituXcktqaHcAAYEAkM2r0QzUFNhlq9B/VrWAcgwAcm2lqNgqEMAd1AXyQaIZr7iFB9lLH1E85dQwd4WYH22je/YetMFpmdULKLBouoCnZSgrflD5py6MAL1ChgyinUAgujIm1qRIGamYCwfnNs0jT0p87rxvRcaS6P7RCeRYbtYcL4dwXbw1sVVSwsWG1JK16AhjbN8iOay+WchDR9gx39lKhzancWVUEjUGPUtTIplGbICzAO4/qqkoxNakEaqFKk+SWR7WgR5AWJ5gX7jt+rFlB9QUBClasmqLkkfZtATqrB8OIGURa1Nw9GaW7PXhUAqymFpOMojeQaZtzb0GWgxALEINlu2YsASxAIZgHB8uWbM21dFUMY1UuVUlCaKWxb7gBSVHgOGUBaVx8eMh/EjBWqIXqoqnpBqAyk+vsqhZBq4sArsQw5J2v1Hvtv8A1LNsxLG2ssDps2wQa0ArKA2JSXndpd8+cbEqatO/vDY7SMHjFlHAjHgAar/hDKSCopSaACuocDy4ZvJKsVZiGU6raMxZVU6K/wCJLEKarUKjBdUAWlcrBGdbYgByFYsCa8XQsAk3an7VsrlgFKJpBuSx8epsXXhAoUWrH2aQ0znVmVwylfIzqG0NbO8PBYuOxGbkoAFtEpjS60AnsrHah6hhIwBItdi1aqxySglbZQtsGChqJKgnX8QmtDwWA82GYMSiQMqklvClQPYD/rcCy13tbqR62LLFTqmQZQVUAE2AWVwfQkGxuSV1UFxuoKlvKEoQqYcCDQicKRIyle2QXiBR02GrgOFClaY+ChAkWM9s6dsKDKkiqOeQDe2I9vDhCysSLVlQmzSsKVhEQK2ZWVg2Bk8FtKcHuAXR2umC3qhj1BdGVSVtkbVgpxsmSCMP2GV1eUjtur46FnkV4pGgR45EdcSRlQtE80U1ForEi4kUGUJI1KqGDL3lYsjbwK79kqQ80IMoIyI3iaKRUaKOXHD7AOkMvgRgOr9w0QP8VtqoVgzGQkRgnXUg7EKwVWjUGQ7gyMujkussQ0k7TV47JMcIyIa8pIVUJsQyo38SuMU6nY70zEU2ylVCtRCknQAsgU+AAWJVUBY/GIZjm/S3vAqDiWXOmrd85fjZARSAwBpUWlLIA5qkChy6/wChtCdACDqKV+xplLhzQBkOyhi+oal8i7FAbn8XB2UitnEFx5zqEPcY76uNVBBLtYF6tezODoVTe+0yqdi942WB21VvCKrqLDeF9gAWLOF8lSDIPsoj9gFVyCxaxbsv1hMS392n/vP8p/8AifB8iv76T/Zyf5kv/G+D4e0MxyPefYnTDIchH6EHwfB8HzXGCB8HwfB8kSB8HwfI/wBVdScf0j09yvUfKMy4XFYrTusY2lnlZ0gxcWEH1E2XlywYsTSFIkkmVppI4g8i2lKlqSlIKlKISkCpUSwA1JLCKJCQVEsACSTQAByTuEaw/qd+oq4eLD0Fx8y9zJjh5HqFo3gkaKBZkm43jzTyvDM8sK8jkJJFBKsI4ySKR4MuZfnOfmM7YuzMGB2IApq9ySDt9vUAKNtWoUasie9d9Y5vU3M8rzXJz9zM5LJny5PeZooRI/8ADj44mkyJ1xcVFjxcSEzSCDEgjxlYBBVIcpyCtuSRRJA/qpBAA9gQfJstX9Ao8AMfnu/B+GT4PwyMJhtMF4hb92IptpWoBZIP8UiPL4+MfE461/UzgJBH7UWHVz/ko5wycxkro62wFeQCho/n7EbKaJIYqRV+CPsKs5rKAV2BFAsKJFAKoO3k+xAs0QQpJosCAZNyvIMofamIpgyagU2oJ+6gsftsKPkVfgGq+c5BhuO4BsTtVsQVj21ZhalNgbdvt4UF291xeIxNtbTae6x9zp0hqWQAm5aXITvRrREeczDZVmZgC62LAGwUDz4LUVPsaDq6gm9gKs5mcsrfYUzkD7HwgYkoNdn2/pQNKCQ1hTLeXzTtNbCy3sP5B/itv6n1DKACQoOwZQAWVay5bKC+wE3kspLoSO2KpQVPsfGjAaBVI8sQCefjrADM+f8A+bbmyjXhJoRm55/k6ziK8nP92BXYOAjIAST/ACOrbB21UganYED+tDz8hHJyxOhGQwDkRiOVrYsxIjO4Ra1rV2LBI/FWgRgX/PyD7U7aC1Df1Y/6RY+5HhhW7IT4BopBuSmJDqoaijm9qDgszBSoem8GtjepYldg7lMCjIkSYegnn99841pd5Sl31r0tEb5JnjkZWJo2QVXcjZdCdlDakEbXIDaAfloitEctmJZLAMhI1N+llTodlYlqUi3KoHIR3RSxMiychjakLItyFo5SL2XwZAdnZSC2w8MpNgfkGEZyJVWWSaFi4h7iyRJuJlyJkjbtTRwzAaxxyW8TqbSbHkicGJXbDilzqWE7ufubSaNWE+y5ILl5Npv6jpDFmkr6A+UJD/ibdlJdiFX7oWKg1aMpohQxDDObaiaqzsoABZRTHYvttsGRVoqVI7jeFPxzyGegkkiMC5jZgFrVmLKNgGOwcAsWFWLDBgwLRMxo7AkLdHwvua7n8YKSVqW1AP3UefxvMobJZ3k8awXAOkNk1sp9i4K6R2osUGZjWvqBQJLBr1AVbKqULHypAPqniiC5BDjYFgUAVYjqhaQqHGoYHZlrdwtG1kE77BlY+uyhNjQ8eRexFqRVsCFb5NCWUKB735UBvUeq2Qy6pp/hdWVlJFkX8zkMSMo0Cg3B+UFLJZ9ltm9wTSxqQDRpGv18kKDuGW7JIIAIXySiAUCq2FI2UhQjMDYNnwx8ljsPDLi4ZgNCxXcMoANAKwO5P5KGBoMAGAPgK+gGMaAs2zAkFqIY0R9lLffWzVVR/wDapWb4tYk+XWL75wsQ6gB9rBLdxTW1qdwGHhl9KAVVFt51sEqDKqRKHZmac7sVNjVfWJjoWjZpH3fui3aPVj2+3RTRqjuqyWU/N0DIihNdnDbSrJ7lo4R22klRna0pZB88Mpdt3B0qQFTG6lgGVV0FhiCGeO1RmrYCxa/FQYWQz/fpLlKFCyeCdmWVCAkwjP8AG/b7ashCyJYVyRZ1k2MbHV5A64S39ggLxe4jZxHsgZj2lC76qoFxVHo3qXJXZ2pHWydkJA1CmQEE+F8tRFm2Kmy1h29T5B+5pq2cPYkBFDUAKF7gDsH2JKBRXqHDKNmBJLEd95wW1tAsJmQFKidcp2y1h3WdGDt5G2zev3URgCxStqoYsSqqVAd9FXwfjkmRZ7TbU9ghELMU/IlU2QeAshZSaWjRUefkfiDmlLq4ICgiMATEgOEq9izRBiVRD6qhRiCg+LopCQCSAVCbNutaEnzZkLMQWIOr0Q+pClHKurCmPYrEh/tCL/Wj/wAtf+H8HyO9+P8A0Tf5sX+58HyRU8zb+0af5b+uYb79Ogvrt0D14cfDg5FOF52YxRDheXkjx5cjIkGMgj43LJGLyPcyMkQYsCvDyc4iklbjIYxfy5vnzz4/WrpYEy2reTsVIIIVi1FfDaWGA2Oo9lNt82K+mv6ueuuhjjYeTnJ1T09E0CPxHNyzSS4+KDiI0fFcmS2bgNDiY37fEhY5nEYazSyjh3lYEez8X+hEOvwa9oV8nFICrOE4kgZmQWEyqsmPN4H6iT9PiEbJ/mgODqpNRJv2kuXYSjsd8HzXn6Yfqb+l31NOJx8HLL051Nk9mIdPdQSQ4kmVlSth44h4fkd/7P5Yz52YMTAxY5YOay+zLO/DY8QsbDfOBiYWJgqKMVCsNQqlQIO8PUZEODYx00rStIUhQUk0ILj8i4MxeB857/q0+qWPk8rh9B8dko+JwJHIc40bK6y81kRGPDxAHw9tuMwMh3kbGzJIJ5eWkxsiFMvixrtv9ZPqXx30m+n3PdY5rwNl40K4XA4MzQk8n1BnloOLxVxpc3j5cyCKQPyPKQYeQM5OEwOTy8WOSTG0PB/nuusnlszJ5DMy5s3OzcrIzM3JnmklnycvKdp8jInkdpS80+RI08zkssryCQK1gHs/onhQvFPisQOjBOzhg/3YpH7muMNJeU9oggumOZ+p45ThjASWOIylnJAIkMiojWQINXiV8rzJkaRu4aYkOyh7VRtQ8/c0LAJ9VUEAtfyvOU5cAsrSeBZ21/xUqjW1bUliB6/0awCT8j2f1Gr3UyAsnn32cnYAEAEP9gQLsFgB7atcH5LqEau4lJBJGzMzUqgkHUWAAK2YDwwNhfJHoMfEJBCSTuraQpZxOmeXJQwZnJkJC8uQB5zqwh05blkGxDWq7f1bUOrC/eyrFQASLrbY2QAy1ZzfKI1asx8tZJZksD7bbIDSkkg21FgAQPBPL88D3BuG9mqn8spPqe2VBBJZTVgEhlJ9kUV5yXKqWa5QxJKO3lq1tb0jJ+6lPXYkAUC2orlrUA5IDu2oLUf1sWBFo04aCVOWo86s4fcT7zo0Dk88sWLSaqfOpFk7UAx1YMAFWqDDyRTRkllrrkcsjugj2Lbg0BYoD86OzEAAstV5FGzR3I8lur6gli5WtiLXzSgC1ck0L122agpBDmE5/I2zeQEBA/xEu1t4LBgBWwYKNF1JC+K+c7HJJExOwlk5OU2ZzTWcbsMM+VO79edic/LP5LIwcjUj2ooFdjqFrxsQoQBWBOquQWZYZyGUWvuspJ8bAFVoA+SrMSCS2oJ2JL+BRon52b4Y+2z6n7kA+dmLFqBYA2CxpCnqoW/kZycpb1J80hLNbE/l421QnbVgQdldgthhQfKosknSHoBny176eyfJkOxUEbAyUAVQjYEKB9zYIFuzWSgpQxLCLZ7sB2opZYlxizB45CtyBu5NIDNI47ZlZkELtPEI4mVY3RwodJslceNnB92ULEStNYAJLUrfcKtEEMRsoNMKi0k+wdFI1AezuuxCqT9lWhTqBZ2ZSa87gjCS4JeorGtDbIAoA0CSdcksJtY5W2EcypohH5kSqdirUsqrsXRiuzDaYRxsmRuCyP6+b9iSCw8hgQuzKCxWwQQbOwAcfDjKAwNkWdbPhiNlBLA0NQxoMrtsSCNWJT4O6SrIy7xgCrZlK0EBkiJjDFkiDBEcMtoiqFEjA5ybnu0aEBhO8x0hlc7KgsOUXUsSUNJo2oFApGx9Qq/4B6UTZRvoyKSQppg9+Q1jRWlYH/QNjRJFhmstbPWTATGJsdmmjTusyFAHjCECpw3q2oP5KkiOi77xRMrBqk9GJLBiu57ZRjdIVFP7aC/6AeV9lH9ApQ2lSImNeve8CpYlTd9+hMJRZLK1gE+B+KkhSAxUGqDFSvsQTrR01PwxFYlTqSHtWFEeQm5VAwaNylqGLAN9lA7hshE8NsSoFA0pNAgBtmLJRIHoGIYHyri7U2KOOSQ9xJO2I5Gk/PfRQNBHuRYkL0FtTISpjPpQAixHAiGBQN4LdmSPtJdyhWcg95RFZeMlNnRjIWdxosMgQxxSA6Mqp0DAkuCyye2ikLq2jWddVoBiNQHagCaYgWrf+SRmlJALBwrq22rUXslntAhb/EVEYLKE/AFBWBFB2O32tmYg6lidHtFJCgDyQzIaZFJagAKQUBSyhnBUlR5BoBF8BSVABChWoqwOigne1Hw9TrbeNEFmyQHNVZ1VSwBD6qxCKfZjtepMigjWrtGKq5QUNqJtpWO3gqwVaAUNs5KgGLsp2ryFKnUED1VyddQbWgWvamsAAlVIjBwbiJcHLvvpCse1lnKljrTAk2Tq2yi/sdQNmZWN0HHgZxE9sLdy6/xmWVjs5H4TSMjmMuAdJdGtWlLKXCF06lLCEOdSPBGrByNR6NbA2GLAMoU0dmBb4p9ApNaMwdUIYgEqpDWWi9CF2QG9mVmAUEqVhBsSPj519qMBSoAGREhXQS+8Hdt/9sv+fi/+V+D4i7WH/wBFi/7Iv934PgbK/wCXUxNgfyHL76jnHaeLrCdSAcgqRQClgGQktYdtU8E+oZkqwCW3PlX/AM8JlNKwUmtU28ML9RuSQSQ2uwKqwBY6sfGvw5q2LGUmmUSLbSHVhoFBBqRCRIq0EoBmcXQCkc3tRaYhwFIF6mI7HZjQRrNAjWMAhfIUNS/TBjpyBepHCvvwaPFHAnIkh5NXjpXTONgYet8tFV0l9W2b8yy1fguKjYKCAzLL53oEgsD82n+kH67vqn9MGwuPy8wdZ9JYyY8LdOdSZE082NhwnBgaHgOaIbkuMkg47j/2nH4Zl5DgOPWfJyxwORkMa5vLzgc6u9KSSX8ghidbW421Zyp1pqUhgTQQqnbmhKLvZWPb1bRVQ2z+xalABN0PRo6eNnLEfM/iBg4yQleGFAZ8A6ZuN4ILcRBYQxMFToJByAlatiOkdHv1T/rFl+uWf09DwnG8h0/0zwfFY0g4PL5GDIebqXOx4X5vkJTBiQpkY+JNInD8PkSF5EwcTI5JoOMm57O4nB0ryOtJ1La35UFm3KFmLAhrjKezM6sAFAVfKkmiann5vYlY3vULak76yJqa9tfYEIyiRmAKiiSQSyvzIG2krEU+qSMbIejaEmTeluQxtsATZdCQGXhLGBhjCwU7KRnMvUk5uXPHcwrT5qziLBUokPYaBrDIPFn5nWUr+LGvsjMxWyxLG3UfkKJb1TS9vIavkbzerSVa5FBIYkXsbQAE6hVDmvUqWYgapH9kHytsjmG2LKwOynw0ja7BaB8K2wjU+5RyPBLFirMrDk8g9SMZAoOxSvJK+dwvkxrdVR3AIYWxI0peMpT7S9WDDLjYe0GnBAsBfMil/wA8Ynmb1KWJfuWgUmjfqdkckgrqQUU621XoyhtzvEMzn7W+8SS/9Qy0CQNtTXk+rFTpZOpPmjFMrkvNmQeTHsCTtuo9UjIWlLWAdJNQAaARhcbyuQtmK2wpjsCCp0amG4RmIYKllyo/0g2xGNeIBNTgWFchbnP7ByU2HE9HP25RI8zmwylo2AZ2IO5RClFCLOtsoBX7qzEKAKJkRYrlcpfdUuWpXYC/UksKBpQwJJBZj6hRqSPclmyM/ZmGwcqNhqys7pIosaFSQC6CmUm/V/uCfjJk5QCg2rrWzBlVqcgUGoP/AEoUqkb34ognEtZW/Q9Hy6aPD0IsOJlz4ZTh1zORZ1YuS35L4q9WYaqGEmjCgwrydlDsQwNMEmYztGx1cEvRAZVfSgO5TVcitsACTqSuo1tkOTliypF2FVRGSBdEhWOrAlnbYM0gJJpwAAitz5IjhenA7jlFpQWUMx2F7XXh1agBHLq6/lqcuKuTDcNZjaOUh9qxoAYAcMoOyclJTSsDGtqlGm8kbSUFUAMaB1AGx8ENsA2zSqsRbdWIYIWLggsxCV9zRFh/cqb2G5skJS/qgQAv5u2ClSzMSCTrRNEvfnyovwylK0rE2QoJBtdgWoGwdQa+4Vix9lskgjwM9n+/p7RoTQNlBtrSlC5LBWP3ptgfACB/QebYl0UgsKDBvidnFL7C2VQuxGw1C7FdTfhSKalCgXRBI+YPTFSuoKAkMzHyRIWLiwSrqdFICasykBqAU+ouwBIVSdkNm/LkNRIVLNeCwOwI8Akm8/ZhyVCQMi3Nm7/MZo7L/LHI0cqIwjmXXanYowCux3QsFZFZHibVCymhsc0MGWwUKmPKwTtvRkgkDD8nCoey8hJJBSRC+q6tNKApIVLYEkjQMCtAAhQQAocUNrDgqS67EN4I+e7Ba1ViaBKuNqoErsqj+uoqOgSSNCKb4AQAX5DKCcZircYQSxSwkrJEY3KmhIoABFg6n23BJPld0evRytMTyusKqFkV8hkkkQoUHbPjHhYHtuGCsZ5gyCRCwUm4lUqo7kAxpsYT40UMzvFKqnRANlhgkZVhaTItccYsuRHE0DMIyYsR4pDTjNI7ybCUM8ux7bhkEZ2YuhPdUaH0GwRkIeO4ilmElRGQuc5aHp9wO2O9+oE6nLWGdIyV8j8kokewUigX0Vf9PqvbUMPHqG+53ZLMYyqowtTQUEAsWP2IWkKBQqHYAAbBgfju0Gz2ivRoIhAQtRBY+QQp8jQLQC2BXsfgGMWdUVQpMY2f7sXtacbFAQsjf0ddgmpdALJ+WdOvxF+YJV+OsNQiLWxRQQTJSguAW/ofGlABjZHqpatiov3QAK4CsoFHUMATZRd0agb1pixJG21BSfjn+3ZxWtX+VsygqddfIGxJVdj4Fi6FDdTnxoyxKqpZVJbwQdmCKQPQBQQ3ojKE1ug1t8E4ZMmfcRpnO0GFuAHcVqXk0pG2WtoZuzIzxorgHb1akYGtrfYat7FkAYuosrRtgCbMCzeWBiRUjWirBih121XyVkepZNzsEb2vUAOgxyu0hDMShACgbNYpyzWo8r4srYLeWUowbBoCHYLGwBc7Bq1ZrNWxF05KShQ+5IQs3h1UDhNZVuvDsygwvMdyzd7/ADDL25v9eX/vx/7vwfHb9qf9eX/Ol/4vwfK8vfy3fPUcZ5hyHfZ7E9wV5tSTb/01oABFAEbKpJLeDGQU1awApbyCAd/bEYAYMgA2CEsARVNqGYU4YgFVVkBiFD2e/lTLyZ2csxIIUps7ghg1kliKDMpY62qg+wXVTqa3KMSwYkOTYVyrKT47ZK7BUAYlGBJcsnnRShPq/NULooDfQGhln7VbhKwlNII4OGlW13ztKsWkec0IIvyw8gghHf1JtfbwVYEN7FfYH77+NzgSMv3ULFAE9wV1KWKBUAlSaR1UgmOP8hbfKuTPfb7sAde7YKqKKhiHBDiSQKfJQkpcgVipIJk5YzG1J2rZGYmmjLfxBh4ZWIqVbAK7ds+wr4HnKf3aziU52qRyMAcJQaTmdJyl2b0rayJeWULqj0mh3AJ/MEnWlKigT53plUlS3obb35dGBRn7YcAL5/q8qoASxAGp87BfCNYBJs17JyLEgA0oH59sBQwGzAXuAQQFJKnXwxALC0cnIM9uWFkl1ql2jtbKgMxAJCg3ZZQBGSCoNHGUQAxVSsgKbn0dzKZq1jCVfp3Lf94nU/NMQC0pIeNxpICqKSPJJPj7LGu7RGmADBj5+NM/KBwqgnZlsmwWOrNEtsQXEgAZacPRFmRaQGFycgx2BKaqwjZgCUFFmQICPK6kU3hfFmmMhZtm5D0BBUKQyktSgFlNa+PJC0rJW1+W2Y6nOrGUZvsjThcwYwZ0pnToJ+kSqfkS5Bk2UAHtuzBj2478Bkv2AK2BWspAYlRbs8uYrFqfcCvuy0Tq4sL43JNgsSCHsMTaD5HpeRZnIDMVIbwShJJZiHYWDWwAAFEAjZCx2VvfKITwQGZtkEg2jYjbehsVYoKNli4A1dBXxJxATMk6/k884MYe7Vu/xDzLmEKTbkF2IJNfZWIUFdgqGqVTbezBQqlW+NE2USHUygKoNjZyfI11P5CxrXgITqNQoYl2uTJPsrUTZIoIAaVR5erI1ZTtH+NHYL60gbJANsQ7KTehG1lqs6EkDwh1FA6g+dgxSrFBvKchN6T4e+YhqU2HH5MPKMGK0BTEBgzqGQE7MAxTY0UDKQNn1NtQQIgy5gzkqWKx7hC5tSKVfWw4cUE/JdrMYtyQwSHJuBpADtNuqqyklY4iFZqeJGt2BC3sJY1ieMauNkzTObBYgsCupLkkUKUsR70FsEeVJNe4+IJcuaw4JAqA+fL3ppxhQX8ncs3gv6+CAQtIQFvbfViWOojISwzEfPVLDYE2UFup1G1kMQKDa3bEkm2AAS2LD4kJt9WUUxcMCpIA1C2FRvF/6CF8XqV2seh6YEgBnYAEKCNWNFFslb+wFsSrndCGUgVBQrCgg2CBSMTRUKdqF6iqDbBTqmqqQoYbH56o/EXsmxABsL5Aondgb8AlfspNMQDs2KMTsACVJLOSp9CF2BBT7A0V22tQCDZK6qYgrEEAOF13YjViCGYqx9j6BiPPr+TLoDXyiAagRIyhjpmWtLXZnNsA+raoSCrAirJKgDwV/AP8VpiByS5Jpn2NqR+OjhgSdVJtiDsCQt3GSAZGnpsrilFbEFQdRZPjy5ABjoAAEtv5qlsKK3+G1RGLKKKujKVFhkY6GQqhQ1ujOCwYENAgGQHr699YokCpHf5glIOzGrBSJZgHlCn+RFjBaCGSkVqjVpJJI5A2rzugd2UH5lFAQdtTFIjMtgEFZA+nlKYqpJL15WwhBGqOHWLHLOCWJv8AMsnk2oP5rXliHAtgNSb8Ix+OCYgK6v8Aiq/kxDeBs1sDqGUaubG2wN++27PTgyy3SFNx5ws4jZCcid2/vKGJceOR1UJo2nhtwI5Awbu3VyBmuh6lmDLdHdkzXCbcBozQJIIUAo1eBdFhdMQ1AAKVOvofkpTjVCijv/hY+RqL2ayFYKd3No34kDwQso+K1xFbWIpuakkKlX3P4ukZLsKR2VtV7dHuWHAQksGCBVvXLPjuMxA+bKo4cN5965SiS4clgBSCNmWiwZ3Ve5YZAwD7pZdNjfgOaseriPso1BPnywcF1J8mi7KrEm2LMR5JH4L8mhwFADoEcHWrB2BLq2jgEiN5AtshNAn2Fhj89OGwYlgCxop97bZSCTb7a0QI7ZAGKkv7+D/pwaX1FBs6PTV5ktEGMMweY607aIU+GbYtTqSCKRrJR2Mjq0isw2NFQTehAJ2EYdP+yYRuQjFQpXYuyAmwlBmjV1AI3DWwUblWY0Gm37NirAMPGgEYFEa6kLQLar9zsCVH5FfYsCJsCkFInggkqP6HZYzRpECWSAQRGor7MhFHwzChl1pNhO2V85wxOLRi06gyt2x0zeIp+zH+tL/mD4Pki/ZS/wCtL/2Yf/j8HwPI7npp2+6GebqO274nKUdTlH91LiRTSbhWBYLY9XLDUWfakK+sgCqG1VUeWJNkRUwsqdmAVCW+6vIuoA0FWGemelBdqii5lrp5O2XPk0NFPcKs3jYKNRr/AFu67rH3DzjcmJQ2RKxqPttszqrbBX1Tao0TypIUFWDFCxZTRvzlNMi0pmrZE75k3Z4hw6qIHZ5dtSLIPJOipG0vs7FnZKYFbBVNgAik+t7mUK242oxM6ccmSja2EkUlj59WGzXsaB8+iK2hUaE7KSVhcfLl3dlDlQ5BDUVVZASvuqkAICAHkXcnVmDshYj9/vrGZBUiKQ227DVWHqQSWUC3PqF1Dv7lgXrziAAWzNcg1JF+2gNhOVGz/G/7xK5OVb3/AMVEeWqwA6hiGZmXZdwTVl7YBrOzES58hIsg0QXNEHYlgirbaFgChtd9VZh9wCIxJnBQxDhQfL+dnsSeEZgTaqdiCKIFIdiEsmXLCX4QGgysrtQAZAB6swL3egtlDWoosCwqxjJm1YH1PxvivLEqnfemTW9d0SCfkPJkDCizD+paifIALOWW2sM5Ol0TZ1VK+WQdVJXwzFiwTUrRF7Da1oVstXt/h8fI+2W5BVTZYD+MlT/IYwoHrR9RYY+oQAECmWkxzCSSoHr/AKSVAZtgCurebpVLhT6rsxdTqy/MUdd7lpCk9IvyxkT2O+O5nuScqQvhZGIBOzABjRAIUlRewAoySLptqg2Kp3yWY+LoE9yiPAIAI8t9go2YsPPnb8LRofIUlSp8ksBoSo0BPka2Cym96DEMxIFE2W09M53Vb2YKoLk/0JYimvVhV1Tam3DFvgKWDUiR5FmP336wQw8k9ynP13nOHNsuQggUQA9d1hf4kBgwQgFCU1ZmIVRoBRQBKuVA7mHusshjdzA4t+2r6M0ZDqGVnpp2jeQIZYi4QkqUDN4VgWAogEE6gUqgMWrUjYtRJulYDU2uFgwjb7TEmUui7KihlRGV0U0Fbd472jIYEMGbVZXUDnytzE4sgit4cBLsCSugAIA+wi9SAy+xj1IRCyq5TZvQhftmjmlFBiLNK2h22ADCySKAY0FJKOtGiNmwF73WQsSdWDFSR6kqdh7KGdPBA10IIQCviuNvJHkANSKfPcdrAUAWQx8IoYo5MbeD4V4F5jj20VC2LyTsRTNXkBUYgEeEVyKZQRrq3krceyBfiiOySDsdisemytIWBGzEFzRIG3v7OQSyqAhCUs3j1bYuxYC1PkUFK7IquP5SFQgBi2x8qyOUSiyoFD+MUBtsXd1omwCQysV1AZGpVH4sTcGhBiQbFGysSqr9z5DBpAGI12JOymyoo6eD5f7kr4oXf0RHOo+6klGHhDt4DFa8jWxZYjtuQI8YoRTKUVioQebKoCRGhFA+TZFsNXAZiSACH7GgDFQg2dlNhXZGpFYKpLnU62quAQQFaiynYsQkGZtJoWs2zhFDiyOFbUgAG17Za1pyD4DINfJLL4VgCQBdv2PhSBlNatqDIWJdlMh/ELGXRgossULK1jXQj2cePxj3GYqqLFTC5FUl3ZO0h0e/ZnYFkRl1VkZVV9vkoxcMEHYxgHyWYAqAiWGZiqgb1vZA0Ut7lV2GjDSK6yFZnS27jClFg8R2Lj38lCx12DKAQShAVQFGtsxYhtbBZLKhlcCQY3FTuaMZIBYBjoEoBm+5JABA1NkDwGX7CpTh8UhdSVoqNix1UOxo2oolmRbH3BsFnA8MZngcN4VWjF7E3WpC2EBJDLVbE6+tBXHgjT5qw8MrIBdpOc/cPxvlGdazebu2Qp9or/H4aXypjYnwoDkEABDHq4VSxYsBZbxXkk0AXAcDKykJDIpYqDSoWA1K7AC1WiuhKKQtAmhVWrj8CxRKi1TchlCqvkEkuQSR9yQ1altmUqaBV+x+BLWfVaWxqAqUQqtYUEitrJ9moMNlsD5tT4UGQOjM8w178c6CEDEc/um+bPSluk51ikouAyZCscSM0sjoghOp3ZtpBA1MA23vGFLBgHJV4GBkW/Pqb+k/6x/SjpvpfqrrPovkeL6b6v4bi+Y4Tm0KZnFy4/NYb8lhcflZkDZMXEc2cRZHm4DlHxuTxooZGELY8au25v6JP03j6z/WfhU5jjhkdF9JSDqbq6SRGbFzMTBmh/ZcK8gaMSDnM8w4EsSzrkScYvJZcKk4LEfSzzvAcH1RxGdwHUvDcX1BwXJwjH5HhuawMXk+LzoA6SrFl4GbFNjZCLLHHKiyxMEljSRadFYYPErHh8VOGGURNYowLMMgppvO0mMbMLDGIhRW4eSSDlfViKBgZx8KUvDyK7M8btTIKeMI5JtLQWBtd/yauvi49mFFsyeONSUhAMuu4Hhg5sEBbpyTuSdbRkFsL1+hn9U//JdwhOS61/T9G+ZjRRZOfyP055LKefk4gk4m7PR3Iyoz8jCkDMV4blp/7UK4jLh8nzObmYvHQ8U+p+guX6a5LkeL5nis7iuS47Kmw+Q4/PxJ8LMw8zGmMWRi5uFNFHJiZUEiyQ5EMyK0bRsj6susevw5wcdLomoAbSSRtClQZWMwJzmawtSFYUyKXDgGgLGj8jyiiP2zf/8AJlf8P4Plgf2Rkf7Rv8sf8f4Pmj+nGlvb79a/3D5up5nT5PLWOd0gmibIxp1eCeB3gniKtDPHPZjeN0LF45YZVDOjotMRbxldT4c1o0CBilAKwIssVCHf2cUAZNtBvRtS3oC/e/rb6TfTP6q4WByPUXTvBdTw5GBhz8T1DjSa5snEuJMzj/2HUvD5ONyEvETnOkzII4OR/YZbZDZDpIspc6DfUT9CudiF8r6cdYNluGhVuH6zTtZJRIJnyJxznC4i48+TPlJjLjYc3AY8YSWR5uXLR6T+PGOxBVIGVyOnrPlTvHCUQ4YseLymx3xoZHyugYsxKkAq2xQFF8myGtBpGT48khaodz4tx+YXzs5OwNs9FwvsCAPcqCUbRI2QsBaG1tfOu/p91v8ATvNXD626Z5XhZzL2oMrJhik43MnWGHKlj4zl8WWfiuQeCLMhfKjwc7JOI8gx8gRzh0avZZwrXtIlsw2AcfcarITRLblWNgbMuzkmyHaMUGjHcbQGyrI8u9IstOSVwFqkMuviwx8ITTigzHwTSkggMPZ2+GrlK2xUneyQHtWBYtsSqEAkbM52AUIrbaahflb/ANpMTQJJASIF91UOVj2AV0ZiptdAqEFAW1VhsqiLlrobBlIZBJ7P4DNsoAJ1ZbAABYEBaVVDUO2VTBIFrWHP5eCGGWBcO1+FZc93KfHKZlLBWRgSPyGxKbBbcFmBUg+o8o4IW7r562QpVnINkKR6FS4U37eRqqqAC4OoHuEut4cnJK4B21q0ACE0NNr31ugwY6qdfAKhRbBWuehRSzKT4uywb8S/kD1A9hpsqj2IHcPj4JUGrZ2Jz9/WL8sZ7+98SNsjYeDZGtefIourkBbvVdmZjJ5X0vQsHBnUhguoIAB2NizotUBakOXLEkAizWhb4x/uWNbGMsCKBJUsw8C69XA8qWCqSNSQdtvnoyQwXx97KqqnZ6ouqWWACEtaC7DkulKWAFYt173xRQbT775RI0Ks8kZVREnbD7ewcbKBGoZjsGDuFBBsrTFSQDiX2a6VlDKBqpUCmAXytliD+IFkq1bfam4ZOoWEA7D2lYRkMCQGVWLjUFYjGQrUoBXVSwegshLWf6lfBoqwB9WVTa7EnYkDUkSMo8+xpJIcwBFiOBh3QkeaemBrUmywJBB8HUIgO1HZiGtdgrFX6s3sNhQYm/upFH7gqGGtWw/wlTQofGaOVSbIF1IDdkKoNk6/6jNdAL4Mbgiy9uENtqL/APaVqQ2aVWOwTbZfA11CnYBSD4+XAlKTblKHBB4GpWUEoGAWX+uotRsQAyV77uoClwoALK9QMAxX32JABYbKS3gqpUqC5cnQsylQAjKUL0zRo2zKdgVsltVCm/xaqHsBuQUQL7HwAzfHvGAI+xk1vcvICWQNGath7OBbs6/f8lOz1ISHeXHdKEkNWHzGUgKSzgrTlWZyqKfwUghhIvliEIGoUhQFIuQY9ll8X4aSiAfKqSQSABZBDHYhvHrYFiPwkKGDKfC1q21sWPltwKVtiQGtmRQAAfY/H3jXSVhcl6EO6CJ13mVpStSvGY3YKndCJIzxxNEzmNW0GtFDv9h8wlZBIabPS9Lu3P0eJlhInoq6kA7yHYqVZ0sIwaRjcQBjYpZcu4+70ZfgxLurBQzNoV9SoOq2igEgklgqsXPg+K08/I1gKz67FVBZLZ1VvIIUqTqGZgbLayeHCspH3+TnCiAUjwWLFQTsTvJa/hRZK2NAXQ2JNoT81oDhI0GWUIWZtl6xKeOxdgqtGpOxB/MBK1AVgFMhouyEgAKxK+S5X5OMDDFqyrfkqGZVYX+R9dTZJAavyHksVo7MPGxUAwNfbywBBUMrKdgxNsNQ+hNAMSoXfadYEBVUsqPszWWUgSUL3APqpAN+t7Ldmy3TwUMwbIZl5OeA4RixVE6P6C3G+bmHbCwULRsVLDwSzMwNUGtnJIJbWO/BsalgSTUt4ziBLLGix25IVbUHRyRezezMl/dnbVmTyVFhW7j4EtTRDUpIIVT4ItWVySGS6V6ZVBXdtrA31/RN9Dl+q/1X43M5jB/c9IdE/t+p+ohkY4lwc+aCYf2LwU6ZPH52BkjmeQjAzuOzlgHI8Bx/PpDMuRjqBuxFp8PgqxFf2JfU0ATK5Mno5FIDDQVrSkVJE8g8zwrHVr9Gv0Sj+jH0e4qPkMH9r1f1kuP1N1R3sY4+difuISeG4HJWXEw8yA8Nx815WDlrI+FzmfzaxytFIlbZ/B9vt8HzyOJiKxcReIqalqKjxNA9hQaCO2lISlKRRIA1kGc65wPmr36hv0lfSv8AURx0snP8evAdZw486cX1zwuJiry0cxwxjYkXOQOqR9R8VjNFiuuFmSw5kEOO2PxXKcSMrJkl2h+D7fKQteGoLQopUKEV+4zBkYsgEEEAg1BpHCX/AOyW6+/9fuhf+/1H/wDSPg+dZP8A0mfoN/8Aqr0f/wDEf/8AD4Pm/wDqv1D+GN/6F/8Ax250ZPlYGSf+R0/y3c9Z/np9C/Vnr36cZb8h0L1dy3T8zumTl4uFmSS8NyM8MUuMj8vwc6T8TynZjyZxijk8LMWGRlmgKTrGy7sfT39f2dAcPjfqn0vBy8KwQwv1D0l2+P5tzBiPFvldO8jknjeQy+S5BI3yXwOW6YwMIS5MuFxJVIMOTmcJ47ALRgtbPf3ChvFKq0FDszqrq4QAqNjoAYuQ5pnKh3KuD4BIYo5YrsyXYYh3layA0ps+PIBagGctkZvveO+CLgO2oIobHc4L2eO7vTH1h+i/1igbh+D6o4fLyuVxY8XM6R6ihj4zleQbk8GSXJ4ccLzdDqM4+LHLjcovCDmOKAUq2dLHKrS0r9R/0ifTjqRsvK4PEm6K5YjLkSfgafhpcidIY45cnp7KvDXGwv28rQ4nBS8EC0sxyZZN8YxckJJCdbdig1cqA7KHQWRbegTdSNvuR4BaPVTdfQ36jfqp0E2NBxHV+byvEYq4eKvAdSyy89wq4nGwywYfHYUGZkDM4TBjWVmXH6dzuJMoix0laaGGCJDTilNHG7g8u7QLAhjUAXczllWRuQfV96//AEs/VPpD9zlcZiwdZcTE2aY5eA/cHlkxo54Uw3zuEyVjy3y82GVm/Y8LLzxhePJSXIkCxZORrTljLwcvI43PgnxMvElnxsrEzY5IMvEysZ3hyoMrGlVJsXKx5IhFkRy6TLMrh0Qq19L+jP1mdHc5HiYXXvAZvSuaYcWKXmeHaXmeBnyI4HOfmT4Kxw83xePNlRRJx+DBD1LIiTEZOeyRGaa2+Z6a+lf1m4NM3sdOdZ8SUWCLkcRomzeLkYYnIPh/2hgzJzPTucyLhvnccmRg5gjZcXOgWJ5cd3p8Qo+8gDbORbSRsTAnDNQZa60D56SNo42/vSG7YcsyAllLEMyNrYUKVtQD5Y7KNWYqSWIUR8w3kK5BAJPkqq235jUj7sVI/I7D0+63up17+kDH7mTl9B89JxZaTux8D1GJMrADzZ38kePzWGn9oYmHg4bukEOXgc5lZEsERyeSVZmmTULq76adddCyyt1D07nwYOOwP9rYyrm8KUlnlxoWPK4glxseSaSAhcTNfHz1ilgebEiEyBy81KqqmLGR5ew9XgCkiogqDlbIILOFUlfZh22UlqXyaohXH+AhVBNX3XnE5ENH32cLHFIqEFyQ8pVu2xA7hB9UdAreshLHaPfer1ySnshD+r6x2LrWyKaTtr6s1KR91FGOiVWnlpV0jYPtGFIlVyokkZSSQqzN9whKMwXzsisqbAQKBMrO/t6nOm6Ki0Is1bQgR7M3e7akAuwLMzFVRtvLgEizVlSPZPjhFkr52Z1IBV1alLK1FVkpgng2dqVVAYX6+K0x+XBCq7+bAagpdiAihRtoWoswQbMqk7qqm1L7By6lTTDQAEOrEigy+FGjEsaVAgQbVTkKUYmFEU74e9YphkOUT6B7QH2DD2GrjySSQfRmLAg2UZSD6BGYlR8coJQxVQ6E+3t7kkj0OreAq+CqoPJo+jWCYTByQJT2UgAqABsQou2JJBJruAAflYDNcds/4uahAA1awSTt5awzMpBkQsdlMZIS/OjarakwtyARXLOX3iikMQGf8GJhiybXoPNkrYdiWNCtSWJACjw4LBiWChQC79iJ4AYGlJ1JZWYi1UE2j0GYgfioHv5u5BEIMtKUFmUFwEYbOykA7gEeAUYrevllVVNbKGkeLmxE+o2LerglV1BJUAEOwWiKOqmiCO4pLENSWINvUG+74jOtJ4i1atEtQBkLahyQoCAaszLqI1RiwRC50p5JEUO4b0f7SHAhWNEXwuigMyLUVly7yBmeVotyGFdwhQE18MusYwMmLtgh9W/oDIVLH3s7Bv8AAHj197SVQWCsBUm4/KgBYNKpVmGysdiVVAS2t+EU7bf4WZmZj+JOrDZyRcfE/SMiwzECten33xPeN3HqyMQAx1fZ0ViCfDU+rSMVJ1ZvDAgaqHFgcYgULowDNewYHb1sFB5IJIAVjYZApZtwPFecXNjmmDHzuSVZSYwdiWK+qEbCtFC6tTCyG1sTjZoGVVLxllOw23B1JDG7YqqKfKrQJ18Ddvm5DApuJVJMpB5Hd11jKuSiaUrSjRYXHpstEKA60w2AINgqbkAJuqBUAhSNW8hvk7wEUkMIxQAK0hO7MfceWABBPgsHqhQ+xMCwZIriYyx+mzJIzglLFKVJ1b2LBDZ0VQdSF1LTTj5lU0ZAiAkFLQKAmqIQQdDQEhJcKaBKix86+CwY1BJ+K6tWMKxMHRuX5iwOMgaVo1jUuSAiCyFpSSSbGv3BIYK5HrQIsN9Jf6Rvoun0Z+kXEYfIYawdX9VLD1J1W8kAizMefLhB4vgpjJiYuZCnB8e6RZHH5DTx4fO5XPTYspizSTyV/wCT++i2N9V/qp/zl5bGTJ6U+mowOa5SOYRyQ8hzczyHpji5YZZ0mfHnycLL5PIcY2Rhz4vC5HG5yRDlIhJ9BnzB+q+J2lDw6TJLKxNSQ6RuAO0QaukyIMb/AAWEyTimqnSncDM8SG4awPg+D4PnGjdA+aa/ri+tq/R76Lcpi8Zm/terevhk9LcC0cgTKwsGaCuoubiAKSD+z+OnXDhngdZ8Tk+W4zKQMsTD5uV9vnzUfrR+uJ+tH1h5nkeOyml6V6cvpnpFFaRIpOK4+efflwk2JiTLJzmdJlcmO/B+6xsWfE4+aST9ildD9M8N/UeJSVD/AE8FsRZsSC6EH/coOQZFKVC4jP4nF8rDLFlqknPU8B1IjU/+3Mv/AF5f++v/AI/B8jfeX/og/wC2T4PnqZaW/tGnx01McXaGvI/Go5x88uH1ziSuy5KmOcsgdUdGdR7PHsjKjhXQgBFjYFtZVaj2kkuLy2BkFYkykYCreGTZgQAAXBLMK93+yqUXanrb5GZ+LgymJliWSQCRVLwil7saq8qtpsZGVkeNbRkZdlNxUI43ASY7g42U8ciCOTtyztMAQIzFEC4Z6kdYwe73HYKw7gbtKnyMLSqh5to7VkcjPoY9yQRUPlKcwK042ocotpJilvuDsAwe9g2nsFCqwKDRipH+F9wVoFvimOZW0YECT3pnU0KPj7Ue4Y5V8opLrIqsGDj5Ti8tznEOI5byEUj2GsgmYsFshRG7exjjJdY49W11RWBV/wAHrTFlWNMuOXGYMA+5J0VVC9ySG0kUsAFoR7eKjbUIphKgaOPuNHfnW7GI445X5RY5yDaoHYEFdQq7Ky7AEsGF0wsFjsa1BOzerrxfN8nwmZByHD8nyXDcpjiSODkeKy8nAzUSeF4pRj5uJLBPArwGSKZ0eNZEkeL+RHIMJxuWwssRSQzbGRg32Bc+Y2jZ1JZwNRGI2ZEbRVDm1IDss4dSyuCFplCWLJF+QQrreq6gEKCyKDsACO3N2DVG+xf0aLGYNwR074xtl0v+q3r3i448bqXG4zrPDWVmeWdE4XmWVcURY+LByXHQf2WIMfIRcjIfJ4HPz8ktkRPmr3ceTH2B6d+u/wBMerFjhflJOmeSmaYJh9RxrhYhaHFTImnTkxNk8NFjP23x8YZmfh5eRkRhUwdpccy8zhP6/c3IzeNCWYgl9ggIYamzsoCbNRVgFpQk7AtTOSdnpmQBzR1pGVgFVySX138lb0iW5tl9MiXyk53cH3QQCTIluTESlPjHRHq39PP046jVMrD4aHgJmgiMeb0m2LxSy4t5EkQOFHjz8LKs37grJl/2acqeOOGL960CRqNQ+rf019e8EzZHBz4nVuH3oqEQi4blRrDLJPPLgZ8/7MxCZDjRJjcxl5mSZYHXCVSy48b6Y686u6SaNunef5Pio1knmbBhmebi5siaGPGnyMrh8lZ+KzJmiSNVbMwpiphiliZZIIZI7/6a/UplxiDF614SLkECoo5TgyMXkSIcRw0k3FZWQMLPysnJVHnOHmcLjQxyzPj4x1XGJJxloLgy/iZi2cxoAWiHDwyAHnMAjfcTHZaNH8yHkeJypcDksPO4vkMYxDI4/kcebCzcXvxpKgyMTJiSaHeCSOQCWOMnHZXCmN2Jxi5GeNvLMyKHWO/ALdxlVgW1RiWVmKkooYELqu6Dpu3KfS36sYUfGST8PzUk4ZYuG5mL9rzUOVNgyHIPGxZSw5y50GPNOo5Dgmf9s0ZbGzw1P8pTrH9LnDZTy5HSfLZPATLG8sfF52/JcVJKsCRYePDkvKvJYUT5KTPlZWU/NS9ufaGBVhWB3p8Yh2WCkgTNQ8uN8pZ3hasFQmllDryod78I1OxOa7ZB7jhUY0VIADa6gMBVsakKnW2KALTWjSjE5eNlDFwgJCtZUi2UsGdi1jVyp+5Kgkhg21J+qfpV1x0YHm5bhJsnjYFmeTl+K/8AvHjFhx1RWyZpIYhkcfAe8rxPyuPx/dAdo1OjmOE400qvauwJI82zdxaUPrurMVUroWJag/bZfHnYhSVTBCg0ikvO1DCSCDMEFqEci0uxKpi58blmdlAIWncBSsZorTK2zEgEkkLrsSfudBv8kWJyYbSqbwVFuTeiqVeRF3YRoPCkmtlU2Dsy0pi8pIvhjY2VXO5Gw121PksWVVDRgMG1XtkFm2+SnH5dbOxPcZERWO5JiYuHkc9wKpZgPugFICWDM5DkkECYcDvujwlX7jXrkKd6RcmJye9LvKg1GvgF5BqHd/fXzIw2DR/daJFqWEjwuWdNfMjbEKfNlmK+W8kuoZVuh2yAvgg0WqLC5AEo5IAHr5/lULpSbWY5LO6gKVV22ohTqRLsDM2/jZrBIVgGOypRqmSgtn/UP5jUsN/OlCiwaTSzsO2jOtIBZpGne+Lg47nSGVmZVLbUL2DbD3Hhx6al9Qw8hV9Ci0Zpx3UTgL/KyGIGl2ADWtlNR9yqtZGokWRQFtK+Uhh5DAKVYMwI2CmtthMwC+UAZlYizZUL+DD3WV42QHWOmBJBCs26s3gpsV1AY2TepZzsQGQs4bbhLdgS0wxcSDCW8Tr+cy0BmLsd1m+4Okt9/wDG9UP6Dd9CqFmkamIKhgwdmAoMGpGkbamtzqz/ACc4XVAKIiyguZNVUyK1FlFkL5Zk7bqSUkGpoUAfmuWJksAKZTqXVo41VAVLGwaCnYsSCQT4Lf4t3EkxcwR+UktbABorsAAr358nZdVLbMoA8tuCN2HilJ2QXTI1fJ3FR76GMmJhh3A3XenIDdraNyOgfq51b0BzOL1D0b1Ly/TvM4gMcGbw+dkYOSUMTNNEMnGdZZo8xQqZONOZ8eeBjG0LxmQt11+gv/KryouFwf124NuViKpGvWvS2Li4fKilnPe5jptXx+KzDJIqCTI4WfiFxYI/4+Kz55BfzyY/Jyrt/MKcA0QGBVtiuihSSpJ21VXIJj7ilGYGR4vNZKizIWJoUzWgVldPxJatQdioDsCpYLZ9dONgYWOkbaRtAEBaZKsxcTO4uJ6yFGIvBLJLi6SSRweY4M/WPtv+nX1U+nf1a4Reovpx1fwvVvFVF35OLyry8B5xIYYOW4udYeU4fKlWKSSPE5XDw8l4l7yxGJlcz/58T/Qv1h66+nfMY3UfRfVXMdMczilmi5DhuTn43LSOQVkQyTQSjeGdbjycZu5jZEYEeRA8Urp868fQL/lfM3DTF4L9QfTEnOxIDG3XPRePgYnL2e80cnK9NPNhcLyDyu0Iln4bK4JcPDjaSPiuTn225WN4HEQ5w3xE6D6uQd+E9I2o8RhqqdksH2iGe85X0pG//wCvv67Q/SL6N5fTvG5Sx9W/UxM3p3j1HmTD6dWGMdVckRJgZmM4kwsmDgkhabBzkbnG5XjJzNw8oX5wc7lEnYuxaRiJtnIcsWDN3GJSmD28rOI6piwbQuriy/1Xfqfb9RX1h5/rXFbLxemMZo+E6PwMhFTKwumOOkyhxq5cCy5CY+fnnIy+Y5DHTIyExuQ5PNhx5mx1h11afqIFm/n1JALHwTfmtlLRyE9vwY9nMaDUHUeva8CgeGwAGZS/qxCS0yB9JeyRLJ3NzHP8Qs4uKQD9KAyatmTxPMCJ9/aR/wBQf5w/4/wfKy/t8/7WT/Om/wDMfB80+Yf5DpCdg5jr8RxY9Ax19gQrkagB2DkAkTLoJCAGBAVgkdq3uo+AqjqAdjuQgt9YwACYiX1cEBi1sVUEDRrAKqRFICbcCNmIUAMRvIFIjaF5CjMrkHYbLHrIXJ7hidsu49ayoFKMAgjBp2sAMiEsXcgs4P8AIQEEgsl2T4+FEUj3rA1EYdpCoNAldRtThllHh3cMe2GRWSySSoQeFVzEEOXxeNKCZYth2zFIx2EnbVWLvHLv3H3GsjGJ9QPKrtohclGkkln2jDszFhISZGNR3J6l09SxRXFt/IfYE524f3ZQUobfmaYSEAqCFEi9o2wUlGWgyuzL8MYhG/MFoXsSrP8AH3+YhEnTbQOr4GZNAwKyKJQJt1UvprKr7QlHU6Hcgq7IKdlZck5bqbiSDlQvmw6qwaNhOpNKdCzRRy0hUspkhKruA4dXKfJjUalxvszKbFo+zHVwU7iv+LupUBSFZlWT+Qn5g2KlbKFLnRmaNVoFn7zLqAhXx59owSzBiA5JJjEBkqmu9z9Tv3N6RRQyakUozM4tfpwMN+F1zw7yCHIk/b5J7kSq52kqFFYhyoDL5u9lYWoBYhgnyWYfIY2UpbGlV1fRiIn3AVu2wsxFirhJELKwCgGnagY1iGZwmHmUcrGilUFgkkiK7Jtqn8ZUjVo1BZipHjthrBZHYpumnhlWfjM2fH1cMkcjmVUsOxKvITkJ7MrLcsgBJjCgSKFP6CAXYm1RahyrVs9IA7SZEA6iRoLU6+0W4JEJLFrDagEBWZlAKrspj2A22IIq2DEvIKVjUcbLZAI2YFAoJYtGQTqwAYEBSsa6yDUWSya1BDyfU3GsFyIjm44Di4UM7MT3FRD/AHeaSrEuzGKRF2Vv5CQqSLC6ywJ2Vc9GglDOrRkGQKaUsJlJVogxYnRoNL0WQhWDNCk2Y7i8RxKz2Mi7D3NuMWUMikDeujEgWRX3oh1/LYsa3FbBrH401k9NfVjrTpoxRYfOS5eCkeLjLw/MPJyXHpi4kUkWFhYq5UwyONgiVtTBxOVx6ydrGjyHkix4YlpfGz8TI7ckWRC4tiKlVmJtXVmRCQTTUQ6EV9tz5+OhkdVIUeChkR2ssQzOC4JYs7IRJRCmnGgKsGLgQDbuXxMUiwSJiNyunvr/AMByITH6l4rJ4bICwxychxoPIcezrExy8yaHtpyGGhyNRiYsEfM5CiUxyzgRPJNIeT+mn0u+peHLynHwce8s0k2Q3N9Mz42DyEWbyLQ580nIpjxdibkZVXaeLqDDyszGM847UGTLIx0a7jtsrBlYhgRSpaujAGyPYkoCtlgLDBRWhcuO5PM4/JhzMHNzMLNxWcwZuFNJjZOOJI+0wgyIXWaESRM0W0biRoTKkrUzWxKNlihRF67stxeuUXtPJSQoWfs6zk2V4t/qj9NnUmC083THJYfNYrd51w8spxPLRxPkRR4sEM7l+KzJYcZ98nLnl4jcwlocR3ljijo7l+G5jpzLbB5zi83hs0O6CHPx5cZZYYpZsdMnHMwMOZjCeMhcrCMsEoRhBlSROH+X/wBNfXjrDhosfG5cY3UeFEuNGP36/t+RGPFD2isedjUJZpiiyTZXJYvJ5E7oZjIxt2vHhvq19MescVeK6iig4yTI/ad7B6mw4Mnhp547yX15Ark8cMTEyEUxZXMR8YXYwGHHjl2RNOH4nEwz9SQsUcSLSylzAec8k4mEhU0Eg6zHDfUlz0aNEcXLnjbw7KNz5NswJZdSgJJKqC9AWRs/tqFdpbx3N+FFMGctL/iKFQ5JJIUaveuqUwKswo0JF265z9OPRXUSJn9McjLwb5UcJx8jj3bmuBngmnWeWePEyMuOQjJhfSAYHMYuBEi4sv7eSNGV6F6g+g/1K6ZZpYuFbqTAVVK5nTTy586GWaXHjhbiXhh5YTduOPInmx8HIwcdJ40TOciTs9DC8QhTMWMiUqllQzF2kXO6MysJQMw8qpJu245Wgrj+WRtjJSsFABYH+rhq8ltaA0LBgg8+aVWWZ4Gfu6sq6j+QkLuSwR1JYKxtrkCEMFZz48g1dDQSywDRSWP3Kt7eNSCKISmSMEBSCVLgFgi7fJPg8vkY7CMxhIwzJZJBpVoWisfZAAWUL5KiO/UAa04gq440O6dOO+EFFWnppF+4mWAioZFTUqxZgCSo23cFQzMgVgoY6qGZFU19pFiZ4pWWUiMBQKkKqiHSmIBAjQKrBSiMsjDtqjlSppnj+oEdU/lIkbbayoRVau0oZGIbe0JCqy6SCMqaYPLcLl4ipohgfAdUQkWVBFk3S0SQyEsB2xs7BhswcXMu+Zswn6sdWaM5wpFwQMy+gaertN5iZi1sfN3EYRNU19wD5UG9lVh4UizZAoqj+D7aOsebqI6k9Cf6tQUAkehFsVUHYglCys7g06sKzh5YHUmVSNhIpfZlI2AUKqHQoVtgCCuoDhgPZXePNUKoDqAVIYa/470BpWUBhGpApfWg5sKAdyMYgSMpSJGlNO2jKvBYyF6UsAADkJyfKLDbPQGRmYl2plBYsgFg0CxLUK0GrbFvRHK6hy3z+2UZZApCoqAMI1LFBdnwVpwHJNRsEK6qCQ0KPIhbkQFqPoBqXcBRIQyhLIsj1ZZHZz/EHZdBnJmNZRSA60GCyHyXLKXF2HZNdSzGSySVLlgCweIUbZWlZ+c7+jwryif7WMrnMCh5yMTSPl2jJBmZyAFAe6pyNSKcSNsV2JLD+NAIyqh7O/taFtyklyuzooZhG7bgFhqqL6g2xWpHVtroIR8r98sUFSj3FU/3gFH3/vRSrTiMN6Er5RfOoUkyZzKPuY7QhhJsFISjbJ6shogMAbDSKVCqW3s468hK1zxJA9IsYJqzOxZ52cSaeZJ4ZWj/AGtH/pf/APp/8fg+VX/aX/voP+9j/wDm/g+D/UHS2WnyeuU55Csx3x7Y6PzYiDn0JYowcauADIqM8MLN3W2AUgl49iIipjjd2RDEpLyFwQo7l0oMYViWIOzOVjZYiC5jZWCqq2A8jaBHI4eSVGezKWZmJjlAJVQpoO7Q+KACPIEuIMSE1Q13WUrPGxZqX3Y6L7qgBMASUQlgWV8jZ9lZCZW29fmJcGVzOmg3/gvKPZd9G+++DmkWg4CM1SIHKEAFSquoCQl2VpUqkcuSz6qrFo5MGd2kYOGUj2KggtFIVVm8sEcGpCy9wEOrKjEEOVx/cgEyv/HI6g9qUowRwGeFpNVCmKQMKWZ9ZFNMVWNgxQGqgIi7OCCssgkLOSr1CXDizDK4NglpD+ELIgYokKioVa7lsX86xxgRsHYFdw5MgaAR7MyAR+yqjBElHiyD1a9gFo7sVBIfUKUDBR5V1snYIQGcKxBKZnLUWGszx2zPE761tMwCIh/JhUYkD3IhQuWPbJUuFY0QrrEI3BGwaMBFsR1uW0Xdy4Ehr3/lKiRIcwUdlCar6eAgRSPzdAzAMCQbCq1Bb0Qo24IOujjTYlV0YxghCoI9iy7SEsXYBiQjFZSe2WpIh0d41RY9qkDSaKwVTGEtkRHdnDCmKGy0ZALB1HsTA24tl3C7BKtYbR4lQmGyhO2kSMBKNVs/yNYJF++wIogE6jWfTdBrQ2DRJtRbM17WNyAlFEDUSw1VdfQOrghm7K4vByXbv40czqV2JjiEi7kVpK/bT+IM5J2DeNlJLuYnGOTtk2EIUBW3vwhV1Uox9jTKUWQFPRhH4C0oSUKK9W8yFZEjtFXZrXXtgugoR26KzC2ZgGr4acQir7x2IFSAR7GlrcIih4JoKk43LyMQLKgEUzrLGpIBYo7aThgUG7DKOpkYKjFgrH/2r1HxfbGRjjkYlkYSSRhpS5uNYyTceX3e2FBj7Uscesm5bwWlVxlaOrILAUHVtVIMZciPuqKd1UbAvSgKy7EYCNQwZQXk8+gAYmvCmFhTpRLBnIcGONWayFpnmAsCxacwXm1DnzMzrCiggycZ5W4CQra8JcHrfjJ3ZJ+5izwKIQjxyExl0EZr+NZY7EqsToABRXYEEy/E5HFyw7QzQyREJTRuraEstXQFHVrYWh/JHB8Boblcdi5capPDDKiEshlCMROGDyNEUpVpimpAuNgGUhrKNEnT/YZjx+Tk4sivGVU7zKqH+TXYyLMzyB7Y/uCsbLtb6uoNCkyY7IoNpyGErAHc9gATcUxFZtW1WZwS0+GlYt3v7I5OyMWYoGJjXVhSgagFdUAYqyUoO4LDwFSy0AAWpdd7QsRqdtirpsgZl2BVloEsJFIUfKeh5XqjiWk78K8lBTdzIRkeQIrO6J2QYp5JSGYkR/vKcPExDmIl7w+tMOWRocjvYU6lTPA8bFopHQsVLNCXAdyoJaD7tF5L7EMBJDyM2cFxn3uinBlMHUNlSxr6uIu3pvrDqPpjJ/d9Pc5yHEStPi5eRDjZDnEypsZicdM7BLthclGivKgxs7HmiYyOk6SpK6tsj0Z+p7msHsY3WPF4vP4qywo3J4Dw8XyaY82Qz5c8uJDD/ZefNFiSLDg4mPFwcUn7VFy8xppGyE0ww+SiyUSSCWHIUI4pKkQKzU47kZsA6OpukJY17K1OsGdGCtMilkVtFGh0UgL2lZGYkFWbwzsSo9bDamggGwlXlwtv1gt/fpHTnjeb/T39aHwoeUThZObmXEx4MTnov+b3UwaTMyI8Di8TlsaXGk5JciaeaQcXwfL8kinNjbIhSbJZfkQ6m/RlI8Xd6E6pkUuFJ4/q2MsshM0xnmTneFxoJYCuMI1ixRwc0iyFsmXP0ZYk0GizEVUUBoy5KCNpXtrDtSgt7MSVIClqdu6KZA3y3+hfrf8AUb6fNiQ9P9S5n9mYphDcBywHL8HLAmQ+TJhx4earnjIc2WZkzJuHk4vkJhJIy50cwjkj0IWpDMZUa1rTA4UpAKw0qqLVFRThb1lMxn1Z9MfqJ0EFyusOkeW4rFY48n9oMcfkeIWWeSRkx35ficrP4lMuRYJicRssZnb0maALIC0VxuSyIGZQzMFJ1slFJa9gCGZjql+zWKdNShQE78fTr9cXESzYeB9Relm42eyk/P8ASk0mViR5MmfEsEs3AZ0hzuOwcbBllfMeDm+dzpZ8Utj8cVzRi4V2Y/0i/St+oOCLJ6Nn4LG5I4TyM/QWZi9LdQ4nHYPINFLPyPR+XiLFjRy5c4jm5PmelVzsqDJwTFmvjPx7nbheJoCJ0lWjftLvvB4PGdeE1CCLg1BqL3nYRy/wuoHVkSQGgxQKy0IxYMYIQPagPujFhYWIIQRQlWLz0T3IJKDqtm1ogpRBIVZVBGqiQhWDyE7EFW+bA9ffoP8Aqj07rkdBcpxnXuEBGj4Epx+lufjmknyzNIuNyPIScPk4WHFHhjupzyZuTlZUkEPDtDGXn0257hequi+QXjep+B5zpjNkgXKiwud4zP4rkJMaWR0jnTG5KCDJaCZkaGOWpEZkdY92VmXWnGcSPByC4Y09WrnKM6sIMzNvob69Lxbg5NQHKyLZUG1VwoseGYg6hBqxJqjI0akMCwXJOT3tS5YhjsQE8MPPlwZdySd1JJEOx8Uy/Kax+o2jREYBgpBNMxdjY3Gw86gro0jBu3GqFixPs643UMEiKyudWQ2R+JZaACyBg7UdHUEgkMqy6bMGYnGUNZcX/I4VZ4UcNp7OnUAa/Lxab8hbajRXJKvqBqxINsNgNvUH7ERqzuRqypr4+Swt3ckerKXs+CH9tX2DC2NBgFKofyZVBgicmjP4lLBlUqPC0pBNeATHY8lYySsall2PctWnIHQgsiD8Q3rHGyuXWxYUD7oDJI6hdS41RQos46zkHvMm2ZY0uMsooYehLi9Lbu5xIf7Ti/05X/ZJ/wAX4PjD/aOL/tl/7k/+/wDB8X/UH+Sbe2uo5nWK8kfxV/yV86+uRjStLLx6rqPaOSNhIIokkUF0Uxr3grxWsqAOQCx/NJi/h2QSNKhCoWTXuxukTK0iqxeMgTBECCMxmg8Sau6M0vxGWmtAGDn0KllFSOYgpQnshWWUMxdyW1s2mRs+uRnYgK7PE7sJJS8cMRAWI2BEWdS0SsIneJ3RjGskUhUxH54gn6XuSKZ/gWvHooUBWnD+wUM9KUfdQ5SKOXtSKhc/cTwu22gYRCSZnKj1gEEbugjSRgXVNQ4RypoERhtCQ7oVYuPZZCGdLKSZiJIdgiq4dQfZSe0HWYmL1Fk/3qzVO0AjZ3Vo9j12DKPUhpQIwzsO2naCeCzySKrooPpH3aDEkFSpsLF5SD0u2vfCJGNG2Ugjd+6XaR7ZwApDsEKwp210j0/Fe2EbdiPht067M+pWiqyJIHHtJu/bVAA6tq7FXvyXLJ4+YbRJ3WQapvIkjRyVIXNhlBVmAAhXeR/5CXbIj17JLEG3a91QA6KFK28RRo1WP1SVQ29ONoyLYbmNkAIT7EuUQTkO3l6ygLS7lFAH4sC0gHoz0SSPVV/HVdQwYp6OFcZowKtFDuAzMqiUM7SvSAOGdCiho3AdbtDcar7MoIViCdkIKsGBGzqbBAkJbde1IUIUSIWCElmQMokMQHanaMLIVKCXZBHq8cYCrfm2LnxIqkntTCpNTcNKRJgzBhVpM3SWfKDe4zkVsrPTOAys6TEixEsYACu4ZyWQaCQxts4f56VHhF9j/I3sxFAMS5DFCSXBr0KsF1U0+pYuNgSHLSbOFkVkChjMwQtEj6xFFYh2arOoEaSO3lTUNBrUqoZERZA34M0Si1AAYsS4BAUlRIrKoZbkCUS1q2kvTrKMrU+VO7rRkZ9rFLu7+UYltyQrFiwP3YuF0MQsoFGnLMp1DOygKzhXplQBmKoCac+Q1UrfCZG2jDKGp2JVizL5UKVRgF7wJUt6ERudmA0IR0zQ+hBkYMQO35YBSmvbJU7pubc/YMWAFlREwkBCpNlAV5NyCCql3LKUH2019lJ1ABAtrVQ0gZmy1jYvXps6FNi5jVV1cAoOxTKncIJQLsGvU7IiU7AgAoLYF5kBsoB5jYKY2Ps3bpgqaxpbKgUR+mUsUD+GsnxsSRYPgNSgmL2lAjZrkMYcoDICSojNvxMdcoogGoeD2UGNwAurMtrJakuF3LMzEFiAoEiEqBE7kxoSD8S5GBj5KFcnGilRQGCyIr9stQLxrIoXcrGPaNAx+3llQhR/eILF6gr3HFOylfABkC7KqNs+texIdajeMn90Oq/dSSJPVQ2q0UJS5FJUkRnZQWBOtetliVizi5LtlcHdAHD13P37REp+mFhaJ+Mzc3AMYZaWaaaNr1ZpanmE7EGiXE6sAhDTB1Zwrj5XqzjRU+PFykabCGXHdYMztKdu64aZCInaJk0R8uRdWDAF1UyJnAKgqArC/LrI2pCgbBkKlKVIwGEauHC7KKPzIRD2Kkx6le27OwYUdwrg0F1QtInshpImjDvsC1OKWsoSqJ2k4MpSvWFlGzpup1FZQx8d9TOIyuRx+MzIOS4jkZkmaPEzuOyomaHHYLJlHI7fb7fcm1SaQxkO4hVWcmIWRi8rj5EO8OVFIqNZkEpfYOAzh0Ryb0BCqQgSySAysRBsnj4MtGiy8dchEaxFKiyxghlkBEbCQ0JaCRqq7FtmVRqVZX6cCTPLxWTkccRDkGMI++HHJNGNXkiedZXjgei0OLlYidppkidYmVo2jFTZ06ElQtkKV1tAjaFWVqJHlTSW/SLkXKRvKMhUBm+yADXU2PAVAuhZVCtopclnRihc4uRME2PLDJJFkrKrxywsYZI2QFoXhfYPAwk0MUqEMulM6BFqjl5HqjjWRJI4uUhd1iEsLocpEQGWSeeHIeM0dWjVosjkJmLxSGIr3irvgdc4kzjFzIZcPJKRuIcgPuVieWNH7TpHklT22KiPGcKYpgWZ4vd6ST8gy+Qd4i5SJkTNjV5dZCl46D/T39an1u6CRcefqGPrjjBjyxjA65gn5uWKSXITLkzYeoIMvjuppsnRZcbFXkubzuPxMeVo4sGMR47Qbm8Z+tT9P/1X4qfpr6y9F5XTWHlNlGZeTwo+uOmoY1x9MWeLN4vAg6gxeWZZsj9pLg9NB+NZUlxuVina4eLEHMYuUhMMqTErZ1buvGvivLvULauPWaMAgsVVqA+On70LGD3SBYtWt9FAQrpokNtZ+wLKfAJdVKsacU2IIpMbt1ZTiihJqOXD4jsD1P8Aop+h31P4/I5/6Kddx8XFA2PhvJwPL43XvTGNkQxnJysfLxpeTflsTlZcbLxGfGk6gWPCi7QHFIJABpJ9QP0n/W7oFJMo8Dj9X8djRrNJm9HZU/JzxtLO2F+1biZ8XA52eZbSaR+O4nMxMfGkLSZwWHKMOvnCdU850zyeNzXTvM8j0/zGEcn9nyPC5+ZxfJ4izwTQ5DYmZg5ONkqMjGlmgyAsiCZHMcilHfXbroX9cH1X6egh47qpeJ+o3GRNjxtJzUK8b1AuHBAceKLG6h4pEjlfYibK5HnOL6g5DIdZjJkGSct8cnxCwBPKsw0pvUXzrCVYSXaY1cC4czNNJcWjTQ8tn8dNkYmQs0MmK8scuNNCcfKx8lDrkR5EU/8AJDkoV0dJFtZIxvWgX494nUodbt0aWlZnqNlQajyGLlJCrKpQUqMXAHcrboDyH1h/TV9boEx+ueATpzm5cfBxI5upuP3RXyMlpDjcR1pwhPI8fg42VcuTm8hJwGOYZZZfXGfkRHT/AFr+kzjszFHPfSzrGLJ43J2ycFM+aLmuIlAyJKXB6h4+MuuNjLG0MfexOUlkngk72SxuQGPEpJ+uU83ALgWYhpWLOeK/LUzj6g7SrxFn7s+tP9sYH+3wf8z/APv8HyR/+jz9Wf8A8v4b/wCIZH+98Hy/Nwv5p5xNnE/y5/fUc40+ciRkcq7pJLGwKSzyTASRTCTIZ4zqyliQAFSVXdqEiSBFKWQLNIKDMJHjPbNhXaJ3b+LtGOUOkbRCCRpGkxiQZm2KvlEjvEq/wMoLKgSIvG0KDtI7O80JVbLyRAr25HQQzIz9uYl7/wAcUb6IEERRO6jSKbjSSQqSiid1RJpW1LEGKdXZJnVPJx0SzTdg0sqVlrQvImtlCM5Hddipt40VohGI5FkbaZQRGWDRoJQokWNQyhFjRtnWHYkMWaMPKLQyS7IojUP/ACbPIhAPehERMKsRHKvcAYt6HSKt1aKOFVTuuyMUZPeWRsV4SCxZmRImjV1hiMLPLGQfZZPYK3tJEkTFow0mojIWTttC0iq8Uz5SKpR1Zp2XJjUA/uJFuXEqscyB9L7r5TGcLnfsvCxVU1cRyRo6CPW0VowqSNSUwREh1DxF3iVFt48YZU3aqIWnA/umKtG7wdxyrU4Qqk8pjSVSRUIp0VHJNIWZ1DPckhAVCypbyktG0ksgix4FbuCUb/l3Ix3iQ55yFLSXo2kjBWQj3XZJIlXRCjqx3Zi4leWMzSsVkjcO0JEtAD0YTBZpPfkYelpMJtMy0v7c9VpkUEsnlAQDH7qg7ZlZVeZZF1n8sWSj2gJFZIxGGby6jAYAk6u6vGWjZjoGMYJoqEclRJJkRvUeutSgJkyB2CjMh7JSRJIj5ZxHHW4V27b+4CnWwoeV/DAx+qx0RGRFu12MiyFiCdWCMq+ZVXQLKZYozqGskAHBwsjcL21LJaSsVDxstsTcoRwiyiNPaQ44lKpUn8aiZiMe5JGsa6hUYqzOGWMGIB7iN7MWZ1X+iABkJ2BOhazbKB3ARs0YIV/VrRVdYmBAU1JHJsFHrYkKm3LUFGhRgpVGJDBmLlZQVRSQo2SNyY2FrEokDuokQESJCruauUEa7v8AyM4uNWWRiGEALON1ZdfIXf3clzDomJcMzN+EZALaMhNEAlFYSFV0pn1VAdlAoGRmJKbaIo92VJCKKkBmZVkiC7FljK+6x7BFCjtj/ESgQwEjlEkQvuqhUYF3KBSgLj2YFQVGjFCdizRtLIHZFWnVq8Gp8QrVmRV1IRndS6uiglNfLH1YFkBV0c6KrnYOCFYmXduQTLIFZlcMFWrVkDlzsyFw7ozbOosAbFQlbfWzch2LRtaGRg77FQZBasG8BhIux2EjqCA5qyKwkXVVUKJCkiO0ruC0bKV0sBwRIkvcQlQ0ezKzASFlLKAsec2p9zYVrCpZWUerHUOQprXWT7FhqaCANf8AIvb2Utq7OSpnc87GQll8jTyVKmN31KlGbRA1F9LciRmCKZQhMzAFQWLFUBAlIDFSGClaLofAZgi9uOxuACrHwF1eTeKSQCtXr2QsQm1lERlChzqXEscjRmRGCGNZEKWvfs/MOTOGCgKzkKGI2Fk7A0hvwhIQh0kB37iL4ARDCQwt2cbIEZQwQE221kSbEkgOtoVsUCQApQCUlgo9YyPdBVk7p6hkKgxUGYqW8u2rKXF/DEmDkASAxmMu5Dko6hnJKM1HVaWRgqkAuYfZFFWCXE7gzo+ZgYWo+yKPRVDsV/oR21JICEKHHltUZA5KuGZvZgGZJoTsSuoRJQyqxWPuWpFtr/Hup31DUUZ10I+ECVlohQ0bDVpJPTVljopqiPJIsZkUsgsnRGjQMCVDvKfa1ETIGdRsDIGUkswYhlLIwISxYoo3tqGBYuZ5gFn0vAlImQ7536kfiD5AlOaZY3CgqXRm3UhHZbGxYtUWjWWUJ7EKXKbJw4Jo2XLxoZ4t7MLoJYWZWkcgrI0g1VodC3Zat2/M6oTElUuESRVpE9LcpY92BdNrY/xg2uqH+JgUkZfh0buQNVVYy/jcysD3ZCPuKBIVB59zYYJS+ysSs1BJDvP5qPmF7Cphh7NLOtybWhiHA4sUYbFnnwzDN3EEc8jqzES6UJRKceJZJVlBwkxmi9VEmlxlQmV1FxoDQtjZgRGARhMk00rshjKrNM0lsCVLPyCWCxYAXJG6pJGyqQpO7KCxew4WmLaqraFnkOpcuWWNZEkqUSqA6rRPlwN2CnYB2IJ7ZbbwxctGd1DFqC7n4zzSWcPm/Dus86vNhVNKils+j/MYQdUCEAcnA+Ey1ETkAoC4Ze0f3rGKKQu/bEiwtL7SFD3CmzSPE5mLJU9qVZSgEY1cRM5V2cpo5QMZNlFyCTyHZGcJQjyCM6hhorH7MVUhTqHStZNfFKpLEPS6XSgIW4rBaZnSJYJCshDYyyQ00gV3klEc0UU0rgANI8YMshi2DONDYxbOQLdMvxzgGIqxIlRjr3KLFjy2YhlDL5NUw/uhuAYq1VaSVQXDDfVG/ljJaSR9PdY8/wBM5mNndPc1yPDZgZAZ8LMeKPKRTHNHDmQpMcXkITMkZOPnRzYjiJYpkkVWJp1RymGpEeX+5hjACQ5QjYhEpVDzRLGqEMB3+3jlSgY9xAgKqsfmslCI8/Hlh+yNPEAyMQ3mRQA79sPbKJFUAbJ2QyCRyC3Ejw0lY91zMUEsNcx7PlGzv/pF/VP/ANZF/wDgfRv/ANL+D5rn/wA5OP8A9ab/ADF/8/8AB8pxryPxBT05/bf2ZVq3spkymhnklnhEpWBSsoXvPO6YQBcTyQsgSAzBlcyxlVWQlTlZXhS3xZzH2/5IVjMZjMwVWlYxZCezBZyAsbzzKzBjAysiTlJXgxMiSIqhig40hQiUzZM2fHs513LQfscd4SHUiRS8vdIXXDGb93luJAAgzVx1QbMqI8uDCzL3TIwcjOlbbbwyxqAI+5HJyIIkvWwLWLgEy1v94VR1kK5RYx2mjmd1keaSMsyFYw+0yNq5MrFSmotQrI0ztnL3JJdmLAO7AqsRRNjHjQskUkMn5OpeHVDpH2TD3Vk2AKicrntiEbQ488mMisz0yDk4uOd3QOIzLNFIZZ3VFMuSqyt/VW8muNpmDMyglQklOusccDEMSO5J3iT3TI7swI1ZCqFZECmI0lamVO7NC6XsgRCMAtAq7+UcrLHpDMYu3JIiRnyY8eWP1j1jV3CIpKOR/PkFY20EmoWQd6aBhFFGqdxkjdA4vy0tyf33q7OISRNJIUFrGJDCG7aIoBkxyQ6gqQGjErKlgrqSHVy8hdaiiYDuW95QXybIPYTIWS/uXjaQIpbZTHFGHV2Du7EFyXqwbdvrDoxC9wxjwJAJCyy0rNCh0YIzGiUkYaBg5tiA6qjRj0vCAFKMxPbVon3RXWjTaMApSMrsZLGmpRgFVx8SQSHIOU8ipvA0ckTBRSHUsyqhuNUdiS6KgUkjwNItFzjtCNYzosmRkxsqqlERLPf3U/3rRB5RemzP21jUqqm8yMm6w1KiZHe/EWjI2uszq67o7R00faTVmCIyx2IpHKFk7zLKY2kmCFdRH4zKX97oAG5lUxNta3rIUEb1GpBKl1i7bOiEX8xxwJGawAY3l1ZQFYFMe9i1WXYqNpD760isqJGqZxuZFyGI1aKSeFGBYsFhycmJG9ywD6RLbKBZvxTEfAKyHDCXfKv2rFkzYDq2WhzjwldO0aBWnC2pbaNo/C7lNAHY+CwchykId43iGextmViF7aKY7N00hBEbufdSjAuYzHTaN79uOxGqyQNOVAfWaTVS+myz4OP+JY/dMmRma996YOAXDl9oB5yzvJT2BIVYAqqAHwoO1fdr2JLMWLPIWtyEg1kHmdKDnfnBNN70g0yoiOAziRNfcmQF1MiqzbLKYizoEKs4dde2N41jJjxV95CoLUbKRmypWMHUqwkDdkqixIRIWOltIkjOQmY6ysgHhRiyAksx2c4q+dmYMFEza7AkeBepZWMgPdzuPx2+2UGMkgLGUGJAV1ZmZRau0RBUgRkhAre3wgXAPfvFFIJBuIVbEqJNXWmkJVNaK1RtyAzFakdozaSqWV1VV7jFmRwsmkrKrM6g6/xxtIjIhYgKCiCT1eV11vfVgtsVGAyY7v7ksiG7A/kjIsKuoXtl7jVQI0IrQq8iuWh2Z9gCey8wahsCizEKG++hKAt/iNkFqNfLgFOAzyaksxkzM4bdC4Sq6EszKgZlVGjcNFfi9xSIU3iMbgat2e322NS/Mw5CiFdW2lI7gctO4kJ3pnZiqilEa/x0snZRP4zGiCJmPbAJXurHK9Et7fuYoiKk3BUrR9wzBh4YKzqyoIQ2RCZJGWHGkK7EEsSyJ70oU0G+wUAkAsCQbkBSuh5zg9JAoPlECsWr2LEJfbLdxXVURmchhIGBKoFUM4bwu3imUFWBKzFiCptUYUhVQp1YswOoH4s67fEH7iRhHtRuaK9ra1Escap7E0qr9gtX+LbIWU4tO6pIw1NJ4BHgH9iJ2KjxqzvezKQaZtSrMW+SK500mc6Bu+DsjrcilSpLKfKI8mv9UQJCwJtlCOCVslI3j2e/UkDMqtaq2qSEUXoupIERclpJV1KJSrBvqSHHokkZo4HZT5D9ryAbRI0lAsi72J8gggVrqQD8wUn7klj3mS29rCsBZB8EkM12CCTYAIWrCiKZv6fHrEhwWVJVaPYqCo2K0WIYERH+NAw8K7uEWPaQtUZW0ClZg3hihKEVewXV13YKpjOzHXcKSSCwZtpF2LU8rmJASDtvZIBNpA04az9yzRqrk3tGSpH2INESKk6qKEUrKPJJfSCaVTIzWzkOWPk0S7kgk38MryHOISwJyDw4JlqxEcW5jp0Ro4jILFEgKkgVVDAIWItTsurqUDHCQyWIiHLhZCoG8hWo9BE40sgrs6soUbl0DJ5+Mhnk7KP620hQijWvcVSPvfktsfP5BaoIoDh3nWAzgKXWMGmBZWPcVLazs1i7tq9mH4mvg7atG3bqdbM5owaM8zXvQaCg4Qr2LKXKKkiv9l2PbQAKjJtH3L1VQXGilGDNaysvw/toIpA4VEi9hMNg4VE9QoWNypKhZFQAGIggJaOAVCnkkuzVPIvto3iGUBB5XxRGx1r2LEUGYH2TVDhuEQtmaiUuu7DdJZPV32ekc7Irs6q3tqWJJm2rP058YtIc9e5H0hR22/6PJ/lj/ifB8M78n+s3+ZL/AMT4PlOczzMF5ZzHI6a7+msf/9k=', NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 07:29:02', '2021-05-21 07:29:02');
INSERT INTO `messages` (`id`, `message_id`, `user_to_id`, `user_from_id`, `message`, `media`, `audio`, `latitude`, `longitude`, `album_given_access`, `album_has_access`, `message_is_read`, `message_is_sent`, `message_is_not_seen`, `created_at`, `updated_at`) VALUES
(4, '40349f7c-1831-4d3a-aea9-50e44c847f9f', 47, 33, '', '', '//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg//AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30o\nhS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS//8VxAGD/8AUAigKN9KIUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg/\n/AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30ohS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS//8VxAGD/8APQ2rdSGHBRIChCz\n5s6qfeKZqqTd5YFKlNSMSggAqkLP/fxPXONP4HGH6z6Zlv6n9T/P/k/3fb/+fR1Ahf/8fibo+73e\nD1/Hbf0ni81yhxZDHu21zRwhNVgbH1fK626BZt25Ug0kJyMaSDQiKKIc2zyeV7AhFA1s/rxlLJpQ\nCXl7erq6ue22cDFComLOACeXt6urntlnAZSFQRraDLYmAGbfzZ/w/M+b755RubSHC7PoAACUMghS\n0tLw//FcQB7//AEaNq3UlkIQVL5zS6TWXZV5Utu6RGISRc1VLSW/HkAg8IJ90250Jpf7vmCqIJDb\nKqAC32B9e236ZNj+xbPekP7/K8Hz++lCEU5GnzHvNox7MnRvuh+DLj9rrVc6x0DEPl8S8KAJr+aa\nOFb/X9ru5z6jsEY8vap5Egs3w92vRXxu72BJYG5GELx4Ak725dfjd1ToyjYFvZZR7ZdvV7vITURl\nJe1sKq2Qi6asbW8HwwF1QwPD69TMyghT+vN5bcIU592NFOMCbF+tL3gnlVdJXFiSQDETaFVLC7A+\nkyIWsopWq0gjQG5UM+tYFCeRbrOrB//xXEAcf/wBIjatdMYdEYqC0oB0Ss56K3rjOM1UqAlypSRU\nj0ri5rYBij9y6TkT81b+DmONvvNl6PSecUf9/+Q9I/IcUb5RVyUN5ttEa5GKpeiWCkONCtazYZGe\n6Fimyl25aQgxbypERSkhJDGwWKxG1LyldIiJcZENkSKjfnZn8S2RsaClLoytmn8zQ81hgyF8t07r\nwj3ghTApCi+gU6RoS1BcLPdzpmiQEMQiobZVCAPMM6bOtaUZCqvkPuXj5LZwXH1M6ArDJ21iifCY\nazfXFwf4+IKcXU88I/D64VfEUugc//FcQB7//AEqNq3QtioZgoITK3i93S+l5CJUtVIiVJFS9XMB\nrfpQWNIlaAJ6amOI2FouoAYU+23tn2Xr2O2HNsE11Kg9mXs/RonAWJoaZdjJ2MZTN5pBKIQKQuXP\nsWI+BFGOyob7U+2rZV+W3gNUfrVlFvNoXFBIVoyRuDXUBfKLONfBsFCJpy6tcRLZ7KhfNnplVP2c\nzDQ+mbvJ2KFGUdhSwPkIKXgUpvBha4Ty02rdtQ7bFmbCWRGwmDu6rruwXGhwBgJTa2gIAFu7gjf+\nJdC4lTPONnfAgA1tthRMEShekBS7UoepMgAUgvwsohYfJF0lGDvqOP/xXEAZH/wBKjatdHZiEYWo\nELN8157l01OKsCEIgh8XUvWq0CjkOXwKjNnZJXFuiKRDeepFlSyM29E9PXmkfcJ6N5O9woEbQV/k\ndKgbMwT3x9oWrLKgTK5ZD7u4fLMU+zSzrdh7Ppok8j/fPhk/Xbrtu7JQtBLV76XtCYscarrkGmfe\nmufx09vLHKiCp81HLKR0d1IlexKl0jF6yIHzg9/u7mt4tdcmjmNULtVlCMAo7yl0+auSk9iSKrNV\nYaKIOZiMcqjScJ1c//FcQB0//AEwNqmtFEMISqykxfL2qxKiJUElQiJTisEQ15x+HIylrT2TWty2\n10AAsrUF3aqoK4W3K3m/lS2FF7+/dpom6zlsA5a86QqLQ4KBpIZWWrZA8MoMWWiVYoHC7IbZ0iIi\n9grFjN6CxooCCgEtwgMwqUSlFjQYyQ2IIKbjK5eu9q23jLLHhNmgA4xQx5LLpDon24eH6El8RHNo\nOZWtI3jrNcOACLEpH4fR2+6vZqLkolNsdTVKs1M0g1QTlMVIbFEgQFlhAKu6FKfvCfqFcGgdhGpw\nQNJIvEBtA1jYHeP7wolEJuD/8VxAHR/8ATg2mimCEYmc83e9O74sq5JRqpeLkZVwu7uvAAtE0pcx\nIz0aGSISnVKAZT6KpYicahUQoslJFliC33gex9c3DfEfEMbj+/e2hNRINsIruM6etkLbNWAJey4R\nSR3Yw+fJsHqavuff0ZYAHseqytUhIrDCOr6FKjrgjFLzHVo/vFEu8JB2g105Y8UtlQaR7K5xssJq\nl+fc4r1BlodXHyZ/4DvJ8OknVwBiWydBHjRmK2scwI+lXglVcVLe9eyNchFOupZpN/8ZV0he/Tp3\np8jHnrCLBBbUp0aOem/9MLCCIaZw//FcQBff/AE6NqnM9CsHSIHRiIrNVmnOpa6iUsqSkgPmTJrW\npgiQoYZs6v/ze7bDULapwPf3iui2n3uu+f3sdjhYwjFrha9SGN3HPV5qiwrBqGeW+mk4IXnZUpuB\nb54oaBKqtRrBq1OqRLytoREulmACryvACsRpkock4RUkGQMhd31iw7bK34i8DzdEb0+W8bk+NQL5\nYg/nUP00FoENknbNrA3bDW+0Bi9f3CNjzbE8dZ6eYXFExHSLKE6XB//xXEAWv/wBODaslLampEJn\nN3l5l8VNUiVBdSpBT73wvU00AZiFqLtbu+o5IHHZPTtCAkE/tTPjkMEYeTzvMCnI2lE5DKUBNvah\n7gQki7FzMCMlqTqpUHtnsOE0xWAwOINCybndpqVrWSoiS6RJNts/j5Is3z7LOVnIYnEwPdflWk6O\nJxSdDUZMgkplgz3NSclzAZu4VVZ2NPNn2XZVwVWjQEd61BGROE+R+xxeV43nRhm5xDj/8VxAGB/8\nATw2raxkSyDDoyDpGt8yVUVd2hCKkqECU96u+tVgAzI7YWFra++ls+B2KZbXAWJXrtxsNBkBbHIU\nNNsH0v96L0s2Lp0JcRtgM5jGAySnoLn4+tu/jaZhzrc/xAw/5IaxQfFRGlKY3nddhE02TEclSYV5\nobzVM85tfEGl9zSJDMrwYUhFk9MY2bGMWz1RNfXcU1O0ocQeimuo1KWiRXXKASW1AYQMJsxnDX2V\nvFli9TOKx4KwkYu0GHD/8VxAGz/8ATpWqayUawUGJmZkvdcbms1ViXVSURAgL4qgBEQQmuuE/lbg\nr7Bf2ByeZvr1NrAVcf17t70ggMWm9HQCUjnGEsWNgqJQYjddLnpyypVYKW+AVqwQC/D/6h0g+szI\n2sCkP7gRPkb2SqyPBVnhav51sH54psHfhm0xlgDlw2nwJtvVAg4HldtScXGB2wcOJQ6XrWFIgODd\nbeUoxGpZiBeu84Aaywv0wA+aXVgsynoI4bh1vJej5y2QqKJ8AtpTAtNNESEv2FFy4AsZhNct17l4\nwUDg//FcQB3//AE4ntrzIoLtPR5tMkw2GPquNUVVyanPfVXUtVydX69XcSrInWfbJKiQvXsaYdc0\n0KA4DpzgH9/P3bPg/1zp/dW0eS8Q9/2ikZ9Ov9cInneoFOIUvmCthfJY18wseZgc0vYjkXfW7+qv\nLgduJ8HxhXh+hgaW4PX8EPjJgJxvaiXadecCZKIWADTQN/QUDUAARnXtUctbWyXZKc4tQkw7Yy5U\nq0kqUpGcelMEi1z2bOK/JJsA3OTSNhDrEzAHfFBLQ8fwreOPD/9/my/niHQUlBFKZW43wAAM73fC\nYaF5sUUTlfh0ICQUAwf/8VxAFN/8ATb2qayUWwtMJCuXXi9VEuVCVISoQHzNL11pgiQaGKBtsJcQ\nsC++glgATsA/0hMosxOb5jKrKHeSqrI7r/kajF/ZpW66LJRNpUxUUi7f1dUVdnt1wEamBr44LNib\nz1vXV8IaGZrkbxatuYOn3O7nDQX1udOysbIU7W6k2QlSYKyPNKVjQjBe0PrAWdBQoRjRUltlYAtE\noBiLkygK0xgH//FcQBaf/AEyNoA0i0QZjIbSCLSK7vXMtVToJUWQgg++kl+3FbDaVWnnoTmZe5Po\n26QzBBVktny0FAhFQLfu1qgeDxiJ2dsFMDYLbvrb5KXMkDcWVwGeV9ayjGhw60Zi12H2c6rVlyid\nupdzjDwogCSNESkNrIMECxDamFDvqac9qT0zYHUxtAF5XMjIFepqlRRwGbbOdy0dZWAnBNg3viMO\njHKgZwawDwzurkIqrWnlCZSn//FcQBaf/AEoVq3QljEVhCHUOd83M1Nt9S61Ny8tYhAPivMu82HZ\nohVYJFeNauNO4xDLCMSNERiOZ1heIaU3OPdUwVWcbHDr1EkAe1P+AJg5rFsMt6aAjpDhGt1qQYli\nD6mgBgaR/dOKatIERtlcOtUj3uKJPEBcjEUojed8xmIC2JGAnSfvlbvZB1tUb62BpjKYDAH1eGy2\nIgCJBhQsmJJYn286JrXrsgyRkchBob9+JGhw//FcQCA//AEkn6abMkOA0kw4DXCMbfLsWBVLhar1\nxr+lXKS6knHn+0vLuqhXHD+2XdXrVpk0gpSNNAgfSgJv1bGwnHMJyVNQu61ByMSkMgrzUCkgriwS\nsTEQ0LV8+1m2//scYdlNxX2eH6/VS9VQtNchcoIqhtNFKDq1Qoddh72uR+W82S/EyGPS5yD0b63E\nTcH07/eAtVrP82sHWwLJ0QTSx2iz5QrvqjfYl+fFewgfRcQQNpmushYJyBoi6x8ZNuTAdLrhrQB9\nLGTfnPlxyuUN9AEyg8OGD9r2CUNr2bHGslgWb999743lxg+J8f3nXh5uHwdLZpAe6kRs2ALbIhz/\n8VxAFN/8ATT2qY0iIgdU3S5dC7FkF5aWqUjwWvV2xqSELEEFUaHgQfqtb3TU+WbLvGlcRt+Mp9Ix\n221+UogbW3j3t4Hy75aO3KDDzWy3a6xsrwXRJfbOwMC0fNrrca6b5KZDonhOmnoc/ly6d/l2c+17\nNJEcHYNEuqhDsclPIKqwMSgVmG1XRc1Wogppp3Qs29ECYlTjw/XQ85MR7Rl4rvjYwwg4//FcQBV/\n/AEsNqmNBD6ozLy93ckuopdSVVxCVD46rS9XKEUlhH6uy8pyzYanpF5zKi3BhgbNR6clMwuUZvX+\nRU+Ht8X8vyrLi91+mvL8+c3UPLx3169G5deot3PHj4c1WqamWKNYbVDVr9+/0/vYOmc1TXLTo7aV\nDNRaLrD8PgbgaP6j7BPPwmsECkGAT8csoWgQ9Nh6x/lG/iNPUHPLIZq9naeOh4ljS6/g//FcQBY/\n/AEuNqnwhjIQh6IQ6UQqrbWaulxAJKiCBHik1qX5ESfFw2QN5f9g4Y4UyRmKeMbQDy/1YBrplbio\ncvkUXI6HF+iUi1YFu5enGhaZz9AgBNuc8+/IBZXMqk8/fWVeJFWEPuIptB7pK0olrtrtdQasYZsq\nWtVnsgjrKljtADBrCdZ4M/JROgmu5wPg2ryZ76B/r3nFiw4w2cYoouaHQpeecqykSsAK7c01SUXA\n//FcQBYf/AEmVqixJjIfUq57l6tVakSolREIgh5VaXqtgdKTTePhPbbwsIGb93qdhP2ek+dfQe/p\nP1mvF7j1hVY+kx+inV7G52rDBGipYOO5gJmzdrhagpSmIDnYWGt1t9VsBnFvXRhlMN0lYtFElm3Y\nuE9pO9zUGM5TJZdI8/O+Wp7KxS6um6Yvoc/eaq0FUDpI/QSmDlylVbv7LJY4JEmvEh3ESE1RoNtt\nadq6hwf/8VxAHx/8ASSfpotB7pNUZ2+2gG29HAUIa1WtcXn9iJJS6dc/13q5PZbc4r9OCoup5TSF\nKOqYdc00KA4swy6gg9KmwUfPqQrArcWBVtpVzqSFf43NCD6rHEJrKnYkYZOpNCYILAVqr5kO3MEw\nmBlnlGyqibgQ6k7gytEISXuY+qvNuZT2HoMj/4X/5Dd6vkLjg98HjADh4QRD6p3D6X0h07vr0Dtf\ntDpnrrrT/3zWwMLiimJ0ZPN/qib3ZBWlyEhhdxyT+Pov7eOnKy1qp1Ig/Lfamkrh/ASw4uzyAfQ8\nM83vj4PgHo8jZOOQbOhOBiBfZ8BOAGwyh//xXEAZP/wBNvaZqUJAkVS9VtI4uVz1U2uLvLuJS5da\nSKB88u7p1gSce3OJz6B2abAv1NcGMsl5rRDOjSA9JxQ/kAJQYDE1R36R9PgAYjjIcKLyZuGAsOhn\nxPafGlGbuQWYjBu2wSvGeql6Rg+rK26w5+TzF5/NdOIGEnKYuehez1uZIltVX08Ffesj9ikbZ5tK\nGmTfo1M0b09ElRdhz9l2xafySgOwIeYULwYL+jWcYc//WA4C9bVhpEAlMCSVEnRdVmcKJ2+LB//x\nXEAVn/wBKjatsKYqDE2lEOiYmXiSXVykpCEICHiuX9a2DGqHPEbtPSkevDj2AjZM5LmosSdL52xr\nzXyR+k9l1M6EIR20okz2CZZ4SU4tSjLJkCnwFJnoQJPgEwi9eE2mnN8VOUQbfK+blRDtcB3BkUpi\nCuVzOUO1aXJIU1+E4ivqSPiZwtfzgsGScE3zg136Sm0ZKZrx668rDRON8qvpn8dapHdRZVoBcBz/\n8VxAFh/8AS42qfB2QhdKItGvN6vLVeXqoipKiEQHjI1xfEwRTbLC8ZyV70hJxlTG1nTuqS+G07VJ\nDcSs1yrWYNynakmaw0oGd9WtKIYqQhWSmWAFI0WktktXciyd15OVS3TeHgUlV73YhM60yVrNaE8q\nVBLYL1Da0BYl+AjtoJZXnqMapHsEjC8y4KDXr6CKzK/lNhi342JQaCgNjs5YXXomsgyxuIJEKCyk\ni6pXB//xXEAVv/wBKFapzPQ2qVvd6lTF2SSqSFqlREe+olzU2tJZfeAAAAAm6uuKcyc85UQTaiGm\nLJI54EnsM7IgLFZrkYqlbegHxo+eivUe3TVWm/DHX/z7Se7ClEFL5X9T0ats/Z15quW+22azxlkq\nS6hfLsyyl0/Xb2dPHxz2yhNv6ssongFpIoPGoWVnSWVtjtxiqy5OzxSlKEskkWJagxgK2qgM/YlA\n3LuCDJjg//FcQBqf/AE0ntr1RELszj5osyY8AaQ/MmmqtdyR7iRd3q/avndWGk1rq/+iVGr06nGj\nTXUaUFAN0cofJ6+5U9b5G4j7H0h07vrtkFbJGrrHt0tLYDrjH9eMgJadiN4CVAfrv72OToF/k/8z\nMQ/ivfhXz35cHpfoA+1eFD1SA8rhDlc0NGAG6A3sAGXegAX0gCgABJCcFqCpib+9es6rYfbLBwdV\nbDbCvg5EbJy6zGjECioj5OQ6iy0Ed1bkhSACOsNxvqwdsw625KT8rKVwZSQdAOD/8VxAFn/8ASb2\noezEOQ9EIdIIW75mqbNauolFlWRAh4041J7BwPFhtdH9PvRk48bgbqJ0Yf+f+9AKPZeZpZaXcJiD\nV5BUvPuaCcbrd+yid6yzMWvOVOLfCyVbSbpPJLSj2qYJJE2kNc/4nlKiWDB5Id2a/lvmowe4R9JV\nOF9JlV0UhK/drXMbt3CE74J95KwrwJ5Vl6yZ3dyodPe3H6NhkiDPFWQAStU5kMBAXuLF5k3JwP/x\nXEAVX/wBHjaqzHQ2qbrK0ukmqkoJCEQHvJJpcwImrMP7NoM59OZ4T+ym5aYw8bxrQo+ExObwEuPd\nwbyzVPxdavxDDUoo3KeW7YAA4xBX5At+lNA6IBEJ4aMWsOZBtTOcs7TC/PreKI7pZd3foSHiofNs\nVayHO2lm43n8cvRw6rIuQP0kMFi+e97y47WZ6rCQfAFTDRrJkIUyFsJaG5U5+28od4vNQV8H//Fc\nQBb//AEkNqlwdmIOBapW81KvJNVdQlJKiVISR961nE1qcrSQh8ADoidc5vv9uzZxZMaZaZcVQiDa\nXwj/Oy7lUNIPffdNoUXybcEfFgWWwlCzeBRoVyW10DJZPweK4vuakyWJKWU6Nh0Vy48tJ20s9L0s\n1A0BgsoZJRFEt2E1Zj1yG402VriRTprjmqIn3JeVjw1XMUY1jv/S8BEeSkMEygEYCFRS0yDR+W0V\n2mXBXo1UzDP4//FcQBcf/AEmNqns5CsHTCLRExdTDqVJUpIlIiA9HG+J1rrYi01aubSr1rQQEcx/\np1CzK++Z1yzfbAKv34nAiJnydgr6SltXcIretaF28zO31T07sfALRoH69jqHcLWbOa++LZuFNS1L\nNLVofcNPrpmoAQOfuUMXyJ6hEJBVNfVmlFaRxLlJLxRpkwB0UyO8Z4i3PAAxgpfEG18d4zl45ipr\ngpHsvTCiiAq5tdZXBCAvUCVp2gIg4P/xXEAWX/wBIDaqLNQ2pbvclJk1ZAQSEJUPe7uXc5ESu21b\npXsmqToPJ5XB1Cs23EssrfeBIGzEF4lEQoUOblIMjE0mxpAioJ2yWFyaukX3UvJ0os1RMNXb58a7\nZNk2jLmxVbYq5xM4S09eurYGB3AWdwEq1LZb3VrJQdfacHRFebS3NMsPy7m0MgoED6qMW8YyCt/G\n5BLkBvfT9CLc7Q1PlHMuZai0VyXWWAytRiRsVeD/8VxAG//8ASA2qfBmQihMzd1KvIuyEqCEIIQm\nrvAiZeCt/g9WNg8NWqnnIDzAvFqKsbp3wFUHZrptUWzHK9Q1ulhS2yJUwApx7RGCTSuQprfLa2Es\nurpM8lu+SjDnnS1FN9WEaa7qmqsuuzmhFEXsaTOJr3bdD3RVXChOHHyJ3S+dBaKbMu9+hwKkmYmg\n+diuDRgivLZsQT0vXrH+6IlrARFBjnTeriMttzyGMSix+AARPrTr1gYgtPwOIvrsfnS6NQ1jo25i\n6E01OoA4VK2AEgikJxJNxHKy6FuTZTY4//FcQBYf/AEeNqmQpkoXUs3F4uGlQgirEgR7RLnE5Aav\n01LxgsDB5gLW2P7fbAXiFkZCRfp+mw4pNK3fkApzrCnraunChLsWBmtWZeGmk1V0o0LN++qmIopq\n7TlRnYPSSdSSadKQC37vxrx7ZaqEoqGULbK7IW0MQ96xuolaHC/miOr3xI8yupTA7UuMkz28Yk64\nyZJ+uoJEUQxEUT162ori6gzxEPfx4Dv7xXIwIwf/8VxAFr/8ASI2qgz0JpBDpBCm4nN3JVoEq6uk\nRAe3FL4lrEVC7ma8NMs3JOkr7Yd4Z5uxNyIUcywqzAZ03KRlChBYlcogDdhMCnx+X385OZd2mOdL\n0SDt4qE66ttyg2Or9dv71y67p0nWxr6a9u7lJ3rffhKzd9cX4UJgWF71Wz03NnZHKGuoMCxpVqhZ\n4rFrCe8VcxkU0laqzIdCrkDggKTziC+jxc/BaCkEy5ORWkCt747O//FcQBgf/AEmNqqMlDEPRCHS\nOvETm9RaVBKuiEq0qB4XrVzkRHfv8Tss7lYWZnWJDfWKseErUgI3BiSkRhZg1DGgJMRqWe/vypkn\nGam2MAYVvsRZYbVuIJZtuXKbda035Feq0567sZOqfPhRQ0++gcBcCqvoF5JTbIWjGe6meRpaIB6+\nq27XKfFaoLcwKFjjA23v+29HTFbjvk4Iir1XDkCP5h+/0641r6QoOdjd3F1k6nFIBiCSsQAhLpWZ\nWYMH//FcQBb//AEiNqHs5DaIRaQRNpUSJZRJUgQSB7Wl6l+aDpPFuO022m36P6Q87qY+2221oImj\nCn6P5mbpFmSRLkLuLOcWuX+k83zGz4M+7LKpdD9tePCuTThv+ES9sddOOymafwlHctir1TPRlPLR\n0vUodMgDJMnU6mptcpaImqeHflSliI9u5kBroCQMPArtSYQK0/emTYp4O7s0u9LzDOFrpwxVcFSW\nbNnZ5WoABQtYXxupS4q4//FcQBe//AEgNqkAGIMVC6YQ6IQuV5bNXVzJASrEQR7pdtdSwBZNCvFq\nyMM+eNW7SlxhR15yVA2vk59yDGLLn0NZf61zU6xqjEukjFkqbXKmx9egVVRBIlp5DLklceMnKbgp\nXKaT7kmqnNzEJkJAVxL/ONs5FLsriiXbWRYZ85d9PHP772454pvUlYQQtecqqUsaQ/cuLy6GIaQL\nZmyPsZ7qnyGw5d98K6Ag6HzHqDqeorJMIWIzxWLnbVdw//FcQBef/AEgNqqQZiIXTCHRua1zNWlL\nlLUioiIQfE4rU61NgKvLeS2Ea0NvBHzCsTPR1cfErCNd1qQ/r05eXwzt6XJ2t+JDQA8YK8iCxzKA\nVslgs5ADIqkzu7QSUcG/GZtCzttd9673dKDCWkwXSt3k4FMqskaVzo3+euUQeWXNNnol6ac79M0n\nlSCDqiICWaKDjKk2wGX77QXSpBoLd+lp0+n+0a+/IKPflPT46X06a9k2+6vEqUWIIcA=\n', NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 07:29:25', '2021-05-21 07:29:25'),
(5, '76395126-9577-48b9-a3f9-8d5ae9fdbc03', 47, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 07:30:00', '2021-05-21 07:30:00'),
(6, '4305d838-bf75-47c9-8e35-64ea877025bc', 47, 33, 'Fifuufjfjkfkfkfkgkgkgifififkfkfkfkfkfkfkfkgogigigogotogitoogototot', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:40:04', '2021-05-21 08:40:04'),
(7, '617b0b4c-3201-46e7-a1ab-6b06a4a4a367', 33, 47, 'Gigjgi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:41:35', '2021-05-21 08:41:35'),
(8, 'a9fda3f8-137c-408f-a430-e14d38828c6e', 33, 47, 'Ghh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:41:40', '2021-05-21 08:41:40'),
(9, '7812fd76-be85-4d45-be4e-909b389dca04', 33, 47, 'Chfhufufjfjcjcjcjcjcjjf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:41:50', '2021-05-21 08:41:50'),
(10, 'abd20607-c17d-47ce-8f1d-074ef3689c0e', 47, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:54:54', '2021-05-21 08:54:54'),
(11, '78c4dc6c-4674-4808-9b6d-cb8385dde1e7', 47, 33, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:55:10', '2021-05-21 08:55:10'),
(12, '54d7917e-7d64-412b-81e3-bc11d2940c88', 47, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:58:11', '2021-05-21 08:58:11'),
(13, 'd24ef5a1-f4ea-4371-8798-e2b0655e39c0', 47, 33, 'Hhh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:58:32', '2021-05-21 08:58:32'),
(14, '2ddc3f4d-b748-456f-8da1-4f55110c30c5', 47, 33, 'Hu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:58:35', '2021-05-21 08:58:35'),
(15, 'dbea0ad3-4378-44d0-9d86-ecab790a7262', 47, 33, 'Ccu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 08:59:23', '2021-05-21 08:59:23'),
(16, '2fd12db0-7dfb-4ebe-ab48-f8fe7b350eab', 47, 33, 'Hhjj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 09:46:59', '2021-05-21 09:46:59'),
(17, '236fc007-f0f3-41f8-ab50-3bcec133d777', 47, 33, 'Jffuf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 09:50:45', '2021-05-21 09:50:45'),
(18, '565de2aa-85d8-4de2-8fee-28dcac60e8c8', 47, 33, 'Uuh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 09:53:06', '2021-05-21 09:53:06'),
(19, 'ec180d75-313b-4baf-88ac-a8ab33e6406f', 45, 33, 'Fufu', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-05-21 09:55:33', '2021-05-21 09:55:33'),
(20, 'ab4940d1-321f-458f-a663-951e1bc4b6ce', 45, 33, 'Jfi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-05-21 09:56:03', '2021-05-21 09:56:03'),
(21, '86ddd7d5-570f-4afc-80a8-3572f7930ee8', 33, 32, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:27:01', '2021-05-21 10:27:01'),
(22, '2e77a768-e0cc-4bc4-8655-6638923844a3', 32, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:16', '2021-05-21 10:29:16'),
(23, '12727e9a-8bc1-47ed-b619-99f95427ab4c', 32, 33, 'Tctt', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:27', '2021-05-21 10:29:27'),
(24, 'e2175f09-0791-4b99-b1bd-c6b45e070bee', 32, 33, 'Ihd', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:32', '2021-05-21 10:29:32'),
(25, 'a0b3eb6b-cf5c-4032-93b7-46b3efa0eecc', 32, 33, 'Rjrj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:33', '2021-05-21 10:29:33'),
(26, 'b2c26007-31a3-4ba8-8ba1-88dc209c63ed', 32, 33, 'Rhrj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:33', '2021-05-21 10:29:33'),
(27, '63720c55-92e6-423c-be07-338448f2952c', 32, 33, 'Rjr', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:34', '2021-05-21 10:29:34'),
(28, 'fb63437a-d92b-414a-a6d0-4ec22b6bf150', 32, 33, 'Rjrrhrheh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:35', '2021-05-21 10:29:35'),
(29, 'd27b3079-d80f-468f-92af-15ba1aa61d9c', 32, 33, 'Hrj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:38', '2021-05-21 10:29:38'),
(30, '845e3a64-fb08-404e-917a-ffc2819b27e5', 32, 33, 'Dahr', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:40', '2021-05-21 10:29:40'),
(31, 'c9ad7b6a-344c-48f8-9606-6b785927b707', 32, 33, NULL, NULL, NULL, '30.7111181', '76.7054042', 0, 0, 1, 0, 0, '2021-05-21 10:29:44', '2021-05-21 10:29:44'),
(32, 'a8127ccf-6953-487f-9dbf-530476abf6e5', 32, 33, NULL, NULL, NULL, '30.7111181', '76.7054042', 0, 0, 1, 0, 0, '2021-05-21 10:29:45', '2021-05-21 10:29:45'),
(33, 'e9e0e97d-6df9-4727-af08-8f1539ec5280', 32, 33, '', '', '//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg//AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30o\nhS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS//8VxAGD/8AUAigKN9KIUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg/\n/AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30ohS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS//8VxAGD/8ANw2rYx0GwRQDkwX\nmeNM0wvJUqopKakQlP9UpSaAida4TwmKJ53VE9zwQnqNWT2/CifFOEE9FNIGnkKcwg4hB9whteEE\nOA6khcnEIqvpNSBGBjW2B7g7XQpNLdHfNLdR2eMpZRd1ZUZv1G+XT5wcOIrqMUsk74/sZrNvy/b8\nv2fEuBwvw05hKLeQiFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS\n0tLw//FcQB7//AEUNq2UuDMJCilVXbes1cq27qVcqiVKCFLtc2CmwwR785zbTXg28JsnhF7PIMPv\nti9s50fEl6TxPAxNh2PGqUhD/Y9cVaxs2ou3hMZWWi4QNtX1+Ok/QsganNNBQk/P4+MfNvxcHj8r\nkHhQsh/T392s9hoGzmx0YxCAMEI6ApQd+2iVFGrUMIiIg0iccbjyde2A67AAeqN9JREGoMXQTG38\n4zcD7ihwZNr/f/s8dIkwxZsEwnIqAa3p0ca6OeBPXt1//23DG69unS6CefWac4zaBUNtIQQGhQBC\nUeCsogkOCRDmBeNk6KVKklgioCdIHP/xXEAb//wBGDattPY+nEau5w2OHEwLpUl1eSCPm5EuXgBx\nMNxcZ/qJXBqLI/p2f/4lI4W9jn9VqpTmfLVqSLwKRO0GW4dSxOt8rgHqGzPRc8sqKfDRj6BabWJE\n7Y46jjFaZqzaCkA1MQaUuJgCzsHfNAmjiarHPDOPliC1uyrg0r9KIn0walv4N7sb0rrcoa6XyY2e\nOebWyXUS2ftHOXOmcUpeSSOq7FcKDyq2ToOV7zwyqss+298hKJzl0qEAM0cYn6/rqK0T2aWl1cxH\nARJiK4JybV5SiFy95T+cljj/8VxAG1/8ARw2rHW2LpxG77mqu5lrIJFS6VaA/bl3qcXgEkVrnMac\ntQNzfrQ62zq0JGqpGyogkQSjUmIr4aEq8430ZmTSSRKc00flMKzFk2cCzkhmb5VFq/OtAalGTClg\nKzeiBjqldNG8x9GNmRBI4mhutGlaEG1xkiEbN05MrHMX14ZESrpUmjZsc2cDG5iDVUIkLNEkmlR1\nirKc+dZSBjVnM4M/Cn4br2uSScE/GJPNWZI6SbJmBotVRqairCimSqKa9MxUksugAI9Su6FCpNBU\n0XL0l00ZnP/xXEAYH/wBJjaqEKZCC1KubvJchKkUiVaBED2lyXJgivoR72rl2uKsur1p50+sM5NW\nbLG6pnZ1OUHT8MX48vR6IUyOdVxwfX50lIZJFtJEYQ1BrgRL18Fq0mygGuyrOBt1KHv5utSsoaQR\nSIj0x5t+gxNmNz76fmvZK0ycJpvAshcpvCYk8M99kMBWdJZRbKII5nN6Z5Z6wcKbVZq5klQSQUsQ\nCmBtbvKEtd1AGWQdw5v6JNly7vvLv3+cf3S0nP/xXEAY3/wBJjaq8EYiG1DO7tEsiVKhEJVkIPa5\nLu+REzhGPe6omY6rndz4/1VsGoYWJ9fXxIurq+evDWBEMu7g6xmIdryVU8W7iVI2BzJDjy3MGvTu\nQ4Yhoyap0TQLR/MnNE8owQpnFMimWXINHhljLpkbJGJGXEqSrozXP2veZbSG9xn3zovZdTS3I0qz\naWULDAWai8Qf4W+9B3b27/06jfD/cNb/I/ilQ5u+qvNOcZlIhe1Hjl0dWtu9rxARFuFaugp1uP/x\nXEAY//wBJjajDEgLDQTC1AhZuFyF0i6EXQuogfF3bRYNkNfp/Ar2vJ135vw7LcZWJZo2dYwrhUuF\nV1PgzSNDtI+XemRt+9cVU1K9EnsUabTU9MiMzMlcVOplStOELWFPLo1M7fYSqaGVXp1UEbLtd5Ji\ns4ZHU+vPpdziOqauq5CnqZ2MamF56fqgG1plS802iEqgBK1troUCCzOYfwrDfveWtqlxZdMb1/Jo\nB6J4J2ap8lqbg7IpjAAQFDiz91CeZWOWkhz/8VxAGP/8ASg2oyyEFpBDpBCrnWSXCpJWslJKtS6s\nHxWrvhAOruAX/9Qx/2/v8zwP2875ca422VPuo8ls+Ho34l3r2c2OP2v0ibFO6NdeTaa6qny0KzcY\nwtdDFERFt8QqS+X0YhAicmuInQkAXZ07TBUvQntkiOGfnuDGzjf27Gl9a9e7WE+66INddMx2REbw\nebVUY4MwxM04CAWpcUXocUtWClcsVmdUU++z7Bt8Z7chcUtuhA814sdqVsrfhUi0JywldJkc//Fc\nQBkf/AEmNqzw9m6JA6QQ6Jnety7iF4hKtEpIQfKcS5q9hJYKvujguauUKZvKkXKJnGj9frLDE3dB\nm3eydFsdM1qa4awV2kFw8jDF68fe4p0UVHy5W/EGUvmWUis45rxM1q6VDuPPn0nJJApnmDhTYrXZ\naufbnck/Zyt5xjtioN/jO3PLOWiygiXwXODdzkK+igh1OyqgSK6iQNRU0Tk7ldPmryIl19y0teAM\noIhLB1kQENiUVURjOaxZK7/ev0HxCQUJJ37/8VxAFv/8ASY2quzNU3zeptqXTVURVpULIH3LmpqN\nhNdytt7QvfQTK2zVLfHkJxyWMrho01mQ1FXaQ3h1LbI9upnK426frImUtavDMo6PejC9FsgZhW80\nWuDGIeecGQG5LMt/Co7sfWe6XQwYVfR9N8Vag2T4b6qR4dugu1NtroOK+FNNcaFw1YDdbnZ15pZw\n7NZG5ihBTNPOpgqlDJVck4A5KdEREWqN8jGdIAQxEEiVjNFGeEf/8VxAGD/8ASY2qbC2OhNKIdIz\nu7maqXUiouquoirQR9L0nFthgkc0nj7p/BI0fr47o+Bl36jKHyMSKK3L0gzSPdFdFYsTIazxp339\n8GTOfs4keb3g+Vs8fwpjGGQF7IZjxUuMgbaNQse01wmk35/q6PvpaNNobsvJXETx1W9XG3yexgYJ\nLOzZkmN3sbPHu748J2K6gzO86xrua7PqKehagXvnPaWkQr1d5ePRUd968BgqjUfT+IHrCLRja/CP\n1jIc//FcQBef/AEuNq00mEsjVL2iXml0ggukIkD5nE1a2Bl/00Mcz62sSgGYnXR2IVY7IcdCaVln\nNeU0nYKcGrIQ1Xh+piWnqBhhcC663l6KTCInmzvevUUB6PYUMtOLjU9ATxIuLTHJaoJWzqtPerrV\nZQiMF+W2mmWg2yUzClv14/PLaedUDnbVxe/jsjXeoP2sdgV131yqffe6jYEA8ZrW7QpaQoCJbJhO\nZDOgqROw7rrlUDmTKSdWSFOtDwf/8VxAF3/8ASo2qfC2Tqju5rJarSUipJUlJJUHvLmsu5sBXJM+\neyCWsy97tQ3LMoyx6O7kVFbJtFih1d7kz4WtNSqenbKDU5uKGrasbrxPkQ1pE2vfqWu85LDVlw6h\nOCsYnXUdHZyPyYmpbvDozRtJQwWLbbBTVurdoierkfEmuXm/uym0z5ZN+53aPGRZwWvHaMx03UWw\nTAOByVwDUWZkcgXokQ7xAxabkDQQmctJKrxF623PQD2W0PLw\n', NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:29:54', '2021-05-21 10:29:54'),
(34, 'b139b26b-0449-47d9-8e11-463184ffaca7', 33, 47, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:37:48', '2021-05-21 10:37:48'),
(35, '20a91f84-cd3c-428f-a87f-79bea31c9635', 47, 32, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:38:41', '2021-05-21 10:38:41'),
(36, '43ef1599-8dc8-49f4-8c5b-3b4cea9d6ef8', 33, 32, ' Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 10:41:40', '2021-05-21 10:41:40'),
(37, '5330b0fa-66df-4e63-820c-fba3f43266ac', 47, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 11:04:23', '2021-05-21 11:04:23'),
(38, 'eabd7145-78b0-43d2-8c79-326305c7f363', 47, 33, 'Dhdhduduf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 11:04:43', '2021-05-21 11:04:43'),
(39, '21bb999a-f2b9-4643-b35d-37b1b15e393b', 47, 33, NULL, NULL, NULL, '30.7111175', '76.7054047', 0, 0, 1, 0, 0, '2021-05-21 11:04:45', '2021-05-21 11:04:45'),
(40, '3DCFA452-6F65-48B9-A1B0-D5C36F3ECD26', 33, 47, NULL, NULL, NULL, '30.711036665531523', '76.70552394390977', 0, 0, 1, 0, 0, '2021-05-21 11:04:54', '2021-05-21 11:04:54'),
(41, 'E8950EC8-60F1-4797-BC80-446EBDAB5E81', 37, 44, 'Hi ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 12:58:15', '2021-05-21 12:58:15'),
(42, '330168A4-E214-49A5-A643-D9C0C63FE949', 37, 44, 'What’s up ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 12:58:29', '2021-05-21 12:58:29'),
(43, 'd22494db-5898-4a6e-95e5-e84b5218a442', 44, 37, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 17:47:59', '2021-05-21 17:47:59'),
(44, '130b83b0-a302-4fa8-aeea-d148ab197da7', 44, 37, 'Want to come over ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-21 17:48:13', '2021-05-21 17:48:13'),
(45, 'B06E2ADC-62CA-42B8-8D84-BD6E5E48541A', 37, 44, 'What’s up ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-27 01:15:53', '2021-05-27 01:15:53'),
(46, '0C105B5B-DE48-45D2-A69D-1ABEEC0510C0', 37, 44, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-27 01:16:07', '2021-05-27 01:16:07'),
(47, '21a63fe0-69d0-419b-9091-2b148dbbd7c0', 49, 47, 'Cat', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-27 18:04:50', '2021-05-27 18:04:50'),
(48, '6276622f-8c89-4161-8051-038fe764a581', 33, 50, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:02:51', '2021-05-28 11:02:51'),
(49, 'cb3840ba-611f-4d70-bf81-c071429525a2', 50, 32, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:05:38', '2021-05-28 11:05:38'),
(50, '6b0a94a4-240b-4aca-94c5-ecc866acc48c', 32, 50, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:05:52', '2021-05-28 11:05:52'),
(51, '772f6115-4be4-4547-950f-717c745241df', 32, 50, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:06:14', '2021-05-28 11:06:14'),
(52, '49e3bdf6-c8cb-468a-8f3f-3ded79e61193', 32, 50, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:06:17', '2021-05-28 11:06:17'),
(53, '14ed883d-92d5-4c45-9aab-b31a9ee3d610', 32, 50, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:06:22', '2021-05-28 11:06:22'),
(54, 'b32d2066-2fe4-4688-ba7f-2fe058d18b01', 50, 32, 'Bjgjgjg', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:06:34', '2021-05-28 11:06:34'),
(55, '6be2eb8a-56a2-4f90-bb6e-acdcc2aa130f', 50, 32, 'Dy', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:06:37', '2021-05-28 11:06:37'),
(56, 'c6fc66ac-5a3d-4eae-8c13-db0d6bf1482e', 49, 50, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 11:16:57', '2021-05-28 11:16:57'),
(57, '401F8658-D61C-4EFC-B1C8-FC5B223E1E29', 49, 39, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-28 12:32:07', '2021-05-28 12:32:07'),
(58, '40DB929F-77AD-4960-A81C-C99F1F5B7B53', 42, 44, 'Suck my pussy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-29 03:25:31', '2021-05-29 03:25:31'),
(59, '15955ba8-bdb9-4882-952b-8157a215addc', 49, 39, 'Cjgi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 03:41:03', '2021-05-29 03:41:03'),
(60, 'b381e40d-c1d6-46ce-91e2-9ee07d5d6a51', 49, 39, 'Fufig', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 03:41:32', '2021-05-29 03:41:32'),
(61, '0703c162-edd1-4ba4-bb79-7ecd25e2b4e2', 49, 39, 'Fuf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 04:14:46', '2021-05-29 04:14:46'),
(62, 'eec521b1-1c51-460b-9b84-5fc6aa2ed2ec', 49, 39, 'Gy', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 04:14:49', '2021-05-29 04:14:49'),
(63, 'a45dd902-b912-4cd6-8b4a-3e449d076afc', 49, 39, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 05:40:54', '2021-05-29 05:40:54'),
(64, 'f44c7d08-9710-4643-88d2-073e15c5e529', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 05:40:57', '2021-05-29 05:40:57'),
(65, '909b2b15-3b75-4ee6-b436-9fec29ad4f51', 49, 39, 'Hu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 05:42:30', '2021-05-29 05:42:30'),
(66, '2daf043c-fed1-48b7-922b-ad5fc96a0d3e', 49, 39, 'G', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 05:42:33', '2021-05-29 05:42:33'),
(67, 'b95f0e5d-a012-4593-b9a2-328869d7821d', 49, 39, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:12:57', '2021-05-29 06:12:57'),
(68, '04069f74-69c6-4ff1-8718-82dcdf54ea42', 49, 39, 'Gguiods', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:19:50', '2021-05-29 06:19:50'),
(69, '4be26ad5-066d-402b-a6c9-92824e2fcbd6', 49, 39, 'Hfr', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:19:53', '2021-05-29 06:19:53'),
(70, '0853ad73-18b6-452e-a095-ed1f825f72f7', 49, 39, 'Cjfuf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:23:28', '2021-05-29 06:23:28'),
(71, 'd538eed4-2443-4d41-8bc4-0538832a1eb6', 49, 39, ' Cuu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:29:25', '2021-05-29 06:29:25'),
(72, 'f6b3a934-e471-4221-986b-f990c8df2b16', 49, 39, 'Fu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:29:28', '2021-05-29 06:29:28'),
(73, '14254159-0cd1-4e30-9d08-b531f354acc2', 49, 39, 'Uffu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:29:29', '2021-05-29 06:29:29'),
(74, '3831bb70-f4c7-421d-b04f-b0bffb6ead9b', 49, 39, 'Jfgu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:34:45', '2021-05-29 06:34:45'),
(75, '0d2ca41c-16a9-4de1-8b01-f7539417e8ba', 49, 39, 'Vhh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:36:37', '2021-05-29 06:36:37'),
(76, '452a085e-ea26-45cc-869c-935c27de3332', 49, 39, 'Hy', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:36:39', '2021-05-29 06:36:39'),
(77, '896792b4-d896-427b-ab08-77166d22f625', 49, 39, 'Ty', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:42:38', '2021-05-29 06:42:38'),
(78, 'e3f64297-da3f-49b2-bf43-96ad0e85e775', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:42:40', '2021-05-29 06:42:40'),
(79, '43fdeeff-87e3-4480-aa70-7843d8e94ebb', 49, 39, 'Gh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:42:42', '2021-05-29 06:42:42'),
(80, 'bb22ad35-6c0a-4187-a25f-ec31a95efe83', 49, 39, 'Jff', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:43:31', '2021-05-29 06:43:31'),
(81, '72904895-83e3-40d7-9237-a77d07ecd68b', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:43:33', '2021-05-29 06:43:33'),
(82, 'db449a5e-be0e-470e-9abf-743a530f889c', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:43:35', '2021-05-29 06:43:35'),
(83, '5ec0a9a1-072c-4000-91a7-e1ff331b6cec', 49, 39, 'Tt', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:43:58', '2021-05-29 06:43:58'),
(84, '924be478-1a19-4690-b10b-2879945b06a3', 49, 39, 'Uuu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:46:59', '2021-05-29 06:46:59'),
(85, 'fa050fb2-71f9-46bc-97e0-eb4dc8dfc33e', 49, 39, 'Hj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:47:02', '2021-05-29 06:47:02'),
(86, 'aa4ea522-07df-49de-ba03-454b38b63a5b', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:48:34', '2021-05-29 06:48:34'),
(87, '1104dad8-6cae-4a56-b068-5871359f415d', 49, 39, 'Gg', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:48:37', '2021-05-29 06:48:37'),
(88, 'eb82fe84-d6ce-41c3-93a2-0b936d38da26', 49, 39, 'Ufg', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:50:18', '2021-05-29 06:50:18'),
(89, 'a320c022-2727-42cc-88ea-2d44db25d3cd', 49, 39, 'T', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 06:50:19', '2021-05-29 06:50:19'),
(90, 'f8a55aa4-f1e2-4a4e-91ad-f9f8243e062f', 49, 39, 'Cchhcf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 07:02:40', '2021-05-29 07:02:40'),
(91, 'eede25db-6abe-4b18-9240-e83795a1d51b', 49, 39, 'Cjgjgjj', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 07:09:05', '2021-05-29 07:09:05'),
(92, 'e2c4cd7c-d386-4726-95ec-7257e744c9fd', 49, 39, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 07:16:27', '2021-05-29 07:16:27'),
(93, 'D07681CE-7F14-49D7-B94E-BBE97314263A', 49, 39, 'Ghh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 09:37:36', '2021-05-29 09:37:36'),
(94, '6879BC5A-9EE3-4C3D-8E5A-6CF07B6CD93D', 49, 39, 'Ghg', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-05-29 09:37:37', '2021-05-29 09:37:37'),
(95, '3353e23e-a1f2-4b89-a8ca-56cfbc7bf5d7', 43, 34, 'Hii', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-29 10:27:17', '2021-05-29 10:27:17'),
(96, 'e71deb44-33ab-456d-b4a7-dd76f420df71', 43, 34, 'Hihihihi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-29 10:29:53', '2021-05-29 10:29:53'),
(97, '44213cb2-9d76-4134-b8bf-315f62883ff6', 43, 34, NULL, NULL, NULL, '31.1017967', '76.6072383', 0, 0, 0, 0, 1, '2021-05-29 10:30:02', '2021-05-29 10:30:02');
INSERT INTO `messages` (`id`, `message_id`, `user_to_id`, `user_from_id`, `message`, `media`, `audio`, `latitude`, `longitude`, `album_given_access`, `album_has_access`, `message_is_read`, `message_is_sent`, `message_is_not_seen`, `created_at`, `updated_at`) VALUES
(98, '22736c7f-9f4b-4771-a58f-209a3c7d3497', 43, 34, '', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAGQASwDASIAAhEBAxEB/8QAHwAAAQQDAQEBAQAAAAAAAAAABAMFBgcICQoCAQAL/8QAQxAAAgIBAwMDAwMDAgUCBAMJAQIDEQQFEiEABhMHIjEIMkEUI1EJFWFCcSQzUoGRFqFiscHRCkNy8BclNGOSosLx/8QAHAEAAQUBAQEAAAAAAAAAAAAAAAECAwQGBQcI/8QAPREAAgEDAwIEAwYGAgEEAgMAAQIRAAMhBBIxBUETIlFhBnGBFDKRobHwByNCwdHhM/EVJFJichY0gpKy/9oADAMBAAIRAxEAPwDEIIRwrKFSveQeVN8AhhvJ/JPJ4AoE2usRen22vLMea20NppmJI2m12g0Pu2mr9pEpbYS6bmLMAthuFIViCf8ApNfIN/P4B3gWwR8EOK/CBDS7jwFYggVX/ihXHrQUKsZlU0ODzbISOBVbdqv+Lv8AB5+BfRccagEMJBJRWw25Qn4G4e3+f9QHPLAWQZ4wFUK7xmwCfINp9osbSfaedpvgkV/BKpx0AVl33YFnhjfBYURdrwGBG4i+iihFisBxvYfdtfaVUFgp4UksBQJtgTxXB6IVAWUBDVVtBJs2DxxQ+bsmgPySR0YIbAAJdkvarbbTcDyAC3PAIBocG7/C2NAOFKkm2DbgA449qqD8ow91kf6f56KKSELKm0iwaJu6FG6ALEtzXKELyPwD0tHCQQHYFSQpDFiSD9tsCQdq0ALHHzyD0ZHCGFGi4dlG3aRsF0Duej8UyhbH8D56XWHa5scggncFVTVUSCR9otV4ogD/AB0U1W3GIjE8/wCqEMLAUiEMDxt5BSvncaCk/HzdAcc8t3aeL6SN9QHpGPqEye48D0Om7i0KL1FzO2IJ8rWI+2I9TZ9Z/Sx4xj1D9P45cYas+kJka7jaTJlZGh4Wdq6YGHkSpcYH4U2R8tR5s87iLYHiv/A/ksvcvamJ3Npv6TIDR5UDl8PNRQzY7stM21irSRvSCaEuiyqoIaOVIpY6WvsPqNMyW1V3Vrd1Lb/8dw2nD+G4JAKuAVMkCSCSBVrR31s6lS5ZUZXttcQw9sXEK+InlYhkmRAJxgdq6AfqF0P6kNO9YO4tO+irG+ozB9K5NO+mSf8Ap8Y30hRYB+g/O0vUe5fP68Z/1Nydoxzdg58OZ3G3cuVNld1STSanhZMWR302Z2fPhO2rT+qLpn03Yn146dpX0RYXaEOXjnsrTe5tE7P0ztP/APdNF62Ray8cWL2pBNPL2LJgNjP23g926QdMxu08PuXE1uHUjk5OTr8GJrS13tHu3tXS9Ty3mll0PDxsrM1F9My5mjGNFCxyXl04GLKyZDjRfuRY+LlM6KI1MlAdYYt9XH09IwRu/wAhiSK/9Kd7cMPuBI7bNEX7ueCaNHjrP6s6/VJctDpl4E3UeQpueBDM23TkIBbDSLe4T/It2rcHZurraZdDo7iXn6nZIFt0gkWheJVAWvhnY3Cu03IMHxrt25I37R0Xan3R6h9udzdk+m/qT3t3x3z9RvZPpz9TOT6efUh6jd3arJgY/rX3Z2X2bp2h+lnpX6nd+50WRr2g+mPb/ZXcEPpV3npOrxaXN9RHqnNrXpRu0lO1/UTu9o0bL9bu5+8+2e4ND9bu9ex+8+0/Qv061L63/Ur0j7iydS9d+6O7j336rP2N2PpGd21qGP3j6ieqGrejGr+mPa/euhYutRdoYPf2gHUfqD1jR9b7A1nuHRNBekfUZ6Na66x6X3gcp2FgHt7uqEADklmyNEiCAVZZyoFWT1MNM9Tux9YyP0um60cqcKG8a6ZrCe08AhpNPROeQKazRqx1Ua7qrbsbmm1K5DFWuOhCottFLMEDFwLYR7vldrbPbJBfeL1jTWNQiDT3rF6VIDW7SXFMs7MFXcVW3uub1tQUS4qPBChK3qdoeqmqa1r/AGx3LoDaH6LegXcHrb9Rfcn1w+iI7k0jQNCwdA7h7uzNd7i7N7s7Jc6ZqXcWij0PyNJ9N/p/wMzt9pY/V3Ru4sD0swdE9QMrKkl5o/qe7T1TubWO0Toejx6nPi4GoJrAkxvL58ObLxRpmNHLG0c8cyT/ANzZAr+JTkiVopZVgC5aYWq4GoTRQ40zMJJBGHaKWP4l8LMqyqkjBXsbgoRqsOUJYZW+mXono/csuNnZUUeRPOkMcc5x2FpsZjGpLGOSOYuyzoZpQoSMNBEu6qjdZGhuWdTctmbQZVXJ3eJbtWjwgCqBbN0qAQbju2A0V3+m/Ct7qRu2WZrVl9rNcYLuBt3HuDyuWLmLmwswH8pEWSV3VpU7T9C+80mkbC0qYJkRfp58coXxshGhbGYy+ZVSdJhmtiZGDmCfFzsX9Xi5emzY+RkQ5TzD6Bdz4a5mnRY7Pi5ORHkQRRCWZsdnDrIIZJW8m2KeCGWDJ8gmiijjiy3/AFAcx9LfaP059uxpJE2JjxQorIpMG4wuZllaFHxvG8e4oFVJHEJWaUbWG0tK9X+nDtrA06RtN02KbOV0aCOBcdJJZIwD4Yy0sXjVIMd5Zg0rh1QtVKN1TU/F125dZrVnyEqrQCANuN33e/pEgE5wK0Nr4G0qWdr3v5nm2usKSGIhQoJAU4kDAHYVzWQ+hLaZoyZOVAkmrSyRRYcJj3KuIr5ObukVnd5Ip2zY8LxYySgZBkLeKLFjU2fmelWn43pdpWTr8zQdzT5mTgaVogifz5GnI6SpnT5sMM7YEMjTyrGoxIFdUqPJaBkEO3TWvpXzMjLkztLwMbL1VvKYNJw0y8jIlMmNKVlyE2pBjZOPkywMYoBjY7y4sP6nV5VpXvP0u/pod69zZmm693fnaPpbGPFytOwZ0fOyMTAt5IpEiXyQrJIyrPLLiSPiFsx3wJI5IkyY5LXW7mpKMlu5cI5VFyeMAEieeN0YnvTD0LR6ITea1aKWigYwQJCneeScZgesgwRWmz04+mzUTh5GSun4r5Gp4OmR5+DG4WTFwdOWLJy9Jd/GscOf3FqWNhacojmH6fRZcuVpN2VtjyC0j6Ndc9Ve54MTKZ0WefB/uOsPA4l1jNiCzZc/62V/1MsUmoZWqavLJhwJJktmBklaP9k9KfYn0D9saDp8Iyc4kmFEhXxKkEUjSSF3lVITmzFvKsjtlZGQXdnI2ISr3XpfoF2N2NJG8WBDl5c0bRNmTyTSBd7SSPHErrDHGsjK3vkjcxruWORDIxDLl/qjTcdH06Y2+IyhggIzCk+p7iSDgCoLZ6UoW1ZAvuihYCsbdx5lsMqnAIMiQRwQa0m9o/St2d6fdk5GHH27j/rsLT1LSZmBDBHFjoiONs0UzNOIsiRWlk8XiGWCCYosXfBjt3D2BltqkkOBA0OLKk8OTLF7cPCwZ5pYpJtmG6nKypVfMxoFZf0wmGJGmFl58S5mP0X9y9i9t5AMkeDkHzipf0zySylPFIsnhWV3E8bRyNaTF5FdLjAGRIow57/9GNGwsbLbA7ebFx4nyplnfEKNmooSVIoJWiBQy5O2bwZKwDHnQeDb5TJBy/tWqt3S5c3Rjc5uTj1jaYkzwT612Nml1FtRtCMqKBbCQBgTGflMAAYOZzz/AHrDpup+nWLg6ovp/m6s+Sc/TtB0bEy9MT9TMi48WTk9waqIaiEceWoyYW3TxtDJj6TpObH/AHGZ6d0bWsHEy8bUtH0abX/UfVf0Ka3oGjzyZ+naXompExfqQJMhg2TqUuOP7c8KpgzT4a5iZueNBggm2Nes2g9qT63H6e6/qev6Fld+6TqmX2pJDkeJJO5NPh8iYckIm8BztYwsGSWWMwO8Wp6crZSrpM+Q0OsH0+9Nu7u1fXHB02PIOq6vj95aHqsGTJIZoO4fT7uPF0XUNZh1CGbLyp85fJPpWTpsmPlZkGJlzZ2Wqrl4GNq+LqNBfXUWxBdL5I8PBAuKCoaZjAMYIM4+VY/qNi9YuKC4fTk7bk+H/KYqxWNqgloBAJOAcg4Nbd/RXvXQO+vT/t3O0fKkl1XA0rTtO7mgyU/Sarg61Dp8ceRDnYl+WGaeRY5gzNJjZMLO+NPMqN1cMKqXD7nJDCyx3lmVyzge00PsWjQ9vF81BdM7T07RtVk1PSkjxMlymJqHjMZWbGX/AIiWBpZSuTIyibJz5oZi7DOR82Foo9Yy4jZGJDcN8GzZlAIO4qOSwZCKpVKhlO75AHPW405NxA5gEKJAGJgDA7c+/wBZmvOtUotXrgWDbgNbIfcxkjysNoggSZ7xwK8CJy5YJcbISU3A8hlCo/yQACSDQvmvz0QkXuNKl7hYKA/kkjk/iv8AH/sB0TElgF+LHFBZaog3ZO4GhRJsC/n4PSseOrG/b8E8kDhdu0LV8KzIGB+a4JJHVn+9UyZJPH59vpX5obCtGApP5+abcNxF8g17fmvzdDn3GdpVSDbCySwYn3KoquB93wf/AGHPRMcJcWyG9ihUJC7vcCxYHmz/AKSLFm+vZgogHlgbf7gQQBsumUHaeKAb4FWPhyruntSUkI9sgX49oYgMfd5AWK0rAWnCmx7T9psnpyjhYrHVhQwKgk2wPtK7vmw5RbN2TZ/jr8kaSmNo7raCxUi19rfN8E+w/F8ngXZ6cIow0hQiirFtzU24UopHvcjHgjaLAuvjpFG4x++1FERJ44SpLEAsxoN5GQ7mQ/dfHtAH/SPx89EQo0wDCwvzzYZZDYRbNktRY1yPtJHHSqRKWA/6gAAxAII/LA+5rNBSwH+P56JgUfYKAcBtpsE3ya2i72n8fF0ObAkVdpmZxHH+6KKhjCIGUMN1ryS1beCqkAGrPuP5B546IXyhUC0vtF2TbE2QTVgUpVf9lv8APXqEgqsZIO38Gx4wEUbh5FHlO+NSCeQOPzyTJHRGzao2rw7BWuueK/8AHT6TcowWjPpMcc5/tWmqKIWw+SC18KV5YkcNwKNgULHHx0sImUVd7z7gFb82x3NRHBAO3cbIsWFvo5ISSBYIPKgKwN/BG5gVLDiyzKTf8DgqPH3KQu/27gw9opuTuJFhqvaOSDtsX+efVjcI3Zj8+YoSCH8WzqXAe2CVdE0WIHzRa/mj0aIEJVWKj4LEUSRvINKDdBV+PxV/BokxwGuSQCl2aBVwKIBtAQwIA5FEUvIA6MjgVlF/6yCARypAAoFlFFRRAUm7BLHoqNmk4JGPl+lAohZQppQB+TtsnddfG5VUKfz9xr5NKRxFHjr3qeANwsALQ2natVYH3Hix07CI+NQN4+9d5Yg7eL4axYrgAC/5/PX7Hg3MFKhdp4cNusEVRDNXPBumaxVVfRQrRyTH7z+/7V4hi3EruAIB20NoI/KkbBuCjlaYixd/J69qu0kqpbbyWpVBtiBarQNH2mueDYsm3KLH23/qJBJLEA88AoRtoD8Glo/IoGiY8Oypk9xatrnd5FohrtD7ipo8Ahh8cEdFO3r6H8B/mhUjBdmHwFBJUsAGN3RXg1fyCa/njotYWtgu/k/8wngg/BI+TXNEf54N9FRwBiQdjAWi2CFdAxG4k39xs8cULFfPREcdFbZqYDxgqCTxdMaKqGuyEC7WFAAEDoqMmSSKYdT0mHUdN1DTcpGkTUMabCk2fG3JheF9wf2KGV9zNVhgL+CRyReqfZ+T2D6ld6do52LLBLoHdetaUElQxsceHPlGLkqrhWEeXhtj5ENKFaKVClrtI7BnxgFDFRsCj33tcR3bO6LW5AoYtXIIFWBzzMf1C9GydM+qzvuaelTV4dC1vEiK7fHjSaJh4JRiyxq9vprSsRv270JIeQlp7EB4IG1pLE+tsBh+ZE+g+dV74DruYtKsOciWgAcn6gDgTntT/p5HIJEWNPLYRDskK7U5uRgibnHKoAzMGajsIUr1m92JitDqenM7MLSNnmBJ3srKW5ZVJSSwiVYHJNAX1hb6ZQKf0hW3IZHKbnBA8jq7bwjbljYFVEjlCxBYEKpGdPaUccuo4EhVVVBu2LQWTY+xW43KYrUMRFGDa3a83nerQbtyIOCDHpIgH9Y/Gtv8K2Z06lpHnt7YwNqOrPH/ANlwBweCQKyw7a0aKXKxVQN+5FuaVvNQJcMQEDGNvBYAoERsjP8A6xe0r0FwG0zFj/URfqdgiVPYwYM6xyFWVSqniSMISSVEkbgAcda/OxIIciPFyII/3IDFTsQu2ZGR3BaPePHbssgWPySFSLB93WxX0l1bNXEjjgjCJI8BIEgUqYpABE5kKQtYZqFlUjOxpTtD9eZ9ZuM9tVPKG2DE5JbJ/Cvbej6cC25WASxInHlxgQD3zHHuYrPrtzT4HwxkSY6b8iJPJKVJCrC+8bQrLsILb/JQDqQdxNnqTtpaJDXhkFqI5Etw2x3KxOWdXsKFa9rbirldo2+5i7Fz1n07Ew8jyyRS0QFaMxxLIVQwsWkZwOBHvi3qAPaWUhjeGJ2/HJhQ4kSExQQLFGHDvIpaQpcshUu8cXLbm3llVnZiWN8/TQ6tbVgzkSYnGFwxjtmYkE+tWNQ3gFTcRgviEKYWCFP9MkGCIjAxzGKePTrtHt7xRxz4UcjNIMqdSgbziHf4o/CUk3Ir1LI745ZlQr5IyxD5d6HJhYUJXExIMVqjlZYhHM7mJRtaaVmDFigCgv5ysSBQyoqr1jZ2rJBpOCpkdXy0VIvJIkgQFVLiM7lj2o7gbWViFEjWjg7RZend1xZbQpjM1bCm7asUjRsXUFmKqVYLZKkKFKhGQq4UaLRX7GmRldlDkrEAyNoG6YHBknHI5AJMZHqemvat2uqtxrYJVSCBk7SN2ZxxPbECDNXpiajO+ySMxOp3sWLlAoLBi25YjFTEbCEHFjn/AFBt7hXFzMRnmQ45FBtiIxCk+5bDUUYsWt1DLdgc8x3RdY3sgGUI43pTvMbO4AKgNGzOyjaS5IjAUgLYQt17ztYSp4gU2hYispG+Nk3SHzEo1B2KqOOAgsAddO/qbb6cTsNvIYhRu3GOxHyPb1Ncaxprq31WGDAYLGW5IgEAkCOYgRzVeZ8MsMGROjsUG54zubdaxEEFWUoQrVxXNWD+eqv1/Em1THbHiRJ0jkmedZW8WNFjSI8ZfnfHKXaZU2T+VWVKiUZJhPVu6nnwt5NjszIOWQ1vLg7vbYCMAfYWBQkc2SAat7tkh0/R5ZTKke7Fsyh3jLzKWkjR3Xxgw7ogXHJRQZuEgkvOlFDyhhCcgnHIgkDHY/vjvpbcIJ3eKAAnoT5QSTzGB+Oa5oP6lPaWtap6g6bjdharkaHq3bkuPrWRmT5LPp+gSYeo+XTdQTJeGWJliyZ8TZkeTKgyP1+BhSRw5sup42Mh9Pncfb/rUezu6MTCxV17Gl1ZxkQtLHqGn7ZJseZVM/8AxBwcXuDU+4oNITPxIDLhwETuXwsSKOb/AF16tpmL3svdumTfrskZenaTJpwzkxxjYGvmbStT2k+bKbxQpqWpvNA0GaMjO07I0zJxElhZMZP6Z41jCztdwstJZcHP14TrKgV8WKQ9v69nZSQmNwkITKy9Oy1DQgMMyV4jisPEux6LZ8a3pH72boEr3QhpJ9SDAEdvkBWS65ebTajUyoJvaW4WVzCK4tkhkEGWG2e3rM4rb2uIqPKw9oyo4i+4yFDULRrJIoVdwWMjGkteIooGUbzL05Y+MdiEP5GTYPwCVDMSpNcqeCC3uNc/nokwgDdIKQ7ipNsdqiyo5Pt+Sf8Afg8CioISEoCxtRbAoliFZmNDgDcRVCjf5BHW+SyqRA4HtGR8vSK8wJLQxJYsFYkiCSVGSB6cT35xxQ0MYAi37vkAGypanUMyk/cQq2a+d7AD5s1cYKq+4PJ4lZmAorUruWULW5mMahixv2gk8DokQKx8gZdqkELwdg/1KOPaLFqw9zC+Tx0usTNwxdVAQjYFDBiSQBuBU8ubG02GIAuqkgeg/AUlIpAeH93JYuBtVgzLQA2sRQPwbPzdDr2kB3BmFiyVB3WPaxvgg3S8n/4fm+jRFyEUEWq+5SvuCiSwwdWUEELuYDn+DfX5Q6iitCRNp+LQjcAQVA9xVifwKN/gUsAcCKazov3mC9xM5+UA0L4wrEEkgn7CdpBogge4HjngUrWbFdOMKKrDn3FQDYA4MnDUOCbIBAugOAFAIQET7iy0qmj7CasfBY/c1/NA+0kiqro+FP4ALtQskgbmuzX8liSL5s0DwOkgDgAfShmCxM5mI9hNEtEFQkqQFKsQNtAbwu4XQA3e482P89LQReeRSSeCADShlG0AKQCSLX8cWpAPHA9pFatvILEKAxZtpUEECqIagADaHkGz8npxgi8dMNoVhZ4A3FSCpDXZ/IBtTXBWgB0tN8VhyqtB4JK8ADkAntwQfevqQ0wVRu2ut3uJYWeKX8/wSTQ/PJt0RIgo8iUzWxXYG2g/C++QsOOdpPF9fkjC7ZDtd7jAHG7d7d5patr3BeKJBFCuiwoAA2gmuSQWJrgG1eFfgD4Qf7noqBrh3HyqOMcxgdzBPA7VpySBmITaGttwAWOILVWLRQOeAFrn4BH5PSMAs4UqrlFIA+DYVQwLe2kuzZo/JNWVljosyqrCzyQTyfuVd1Vd0aJ/A4ro2GAm+CrEA7NyuFBPt3Ka4INkDaQaBJW1PPq+Su0gfQQfWkUg3ogKrTIrEC2XhUbcCRRBNjkEhh/sOjIofkB1VeFphdIx28WfbZBBKgEAe0g0ei8fHZbQghWIIO4Mu+qKj42A/fV7QTQAUABdYQ0pAVuCPn2WSAGKFTuBANg7ua/FEkqOgipCrVhzXwxJBBPtK/BK0DxtFMASbHXuJdpTehIahYBqwIySCA25RubmhRocHpzbGBFFSQK2e0OFJIFszlqP8VV1/nr8uOsTqSQXBJFNe5WH3bLoeQ0eAdoHyw56KK+xxBgdxX4IVthCAE2N3v3FiKrhTZBIAvouNIwoUkkgEsCfaavaUCWwO2h7tq/k8dLwRSPbCiy0HBKHYoP27ms2xrcqpYNUVXjoiOIggLGgAAX3kXZ+TRtlJv8Ahv5ABNdO2se35j/NFCrCyKVWMVuJUgAkUSedwIquaFfJ+B8uEOOCFHB4AARSSnA/0A7TZJNcDj+T0YcRuOdtKN4FEE/JUG7G1uLqyRe2x0XHDIpWROFBBJNA0rA1RBADUQeCapgR+XhQBLD59+//AF+yaVYmTxBP4CfrVe9/d59s+mnausd6945g03QdEjSTLlZZnnnkmlSDFwsLFhjL5Wdm5UkOPh4isoeaVfJKkQeReb367/Wbsr1u9Su3+7u1dC1/QM3G7bTSNZi1+HSozmw4+qz5ekagp0vO1BTNNHm5sUjTPIDFj4UULzY0EUnW5P8AqEY2bmekGgaTjM8EOd3rgy5TRTSRGZcfRdVWNCyBJCjTZW9kBQsVjK5CNGhHPF6laXlzPh6nmOS0bTYhyBsWSdVnQRyvEqxgk+SRXb9wNuG5/I5PVfxm+1LbQoqKjFzDbiHEQIwZiIgxycGujd6ar9Gva9Q4c3F2mRt2W9rEhZ3gxyWABPEmas3sBEi0zAlRDHJkQYhLeNz41MIpFJUkKVDGx7gQCfz1l16f5KiPH3LTwiSMOeE3GUFgXY0D7wQCfgfnnrEftaVsbt/Ck2kGHB8hjZwZGLQRyEFtqKzFGoWhBAo8gKVsH1S740lc1NI0USIANjqfPNEpLA5XiCqN4UhmURyRIQFdrZeuZf0lzVNc2MiiY3NzzOB/kdxjFdrpnU7PTrOn8TxGtLaUlLVlXcswAJG5lbkDAB/tW6r0ugyzj46pLH45DtkRQhbf+G2kyMrFStMfDI3vZUQGhsP9FsQRtGMt0VXyIFS5disv6hmlRSGjCSMzE7RLJIVVI3KoCp5ie0vqy9QOxFx5Io9SkaWWXKzYJYNQ8UzGKACMx5ErJEoHkjdsGDDRQqvHYMsr5X+n/wDUx75gztOhz9D0o4onDzSNPk+TZjC/F4cdsiB5oo0VI5nlbMkoo7RII45Mxr/hLWagQpR5Ib7zpMRABVGJyfkPWt1034/6XYCi4t+wCQrKbXiL5toGQYEc/eJEREEGut3trBGJlYz+SI0ImvzI22ItGJPJtL7FVEBRVB2EsqrtpVy807EGXpOMyFG3jHcnxbtrRurwMknMfjYIymNmhJjNOWG1V5cfQz+r1pWr6rjaL3P2xmwY7phY8GotPAJ12zxrM0kMsazrKMafLmaIhpEyMKHFm8U0hK9Ano569aB372ppGraXkrI2XGD45FYMSxWR0eInyRPArbiHCiIkIUDk1wv/ABl3pdxk1lo21O0wpVgpgFhPpMjzZMcVprvUbPWtN4vTr1u+1t7inMXFWQN72ZDLIA2HbtJmCwq9NXgGPHPBE4Zlba0QMzOX9hDt4xQZlIURNQiIRwSHKh67O02Tzgzr4/IZRLvkAeJ5G3MBHsDsx2hI6dSo93NNdLSd/YUuqZ6zztuWRU8Mal2DRSL44toEXukdAySPSvC24N+2A8R7t+p7sb0zjzNT7k7i0/SxgxTmWDLnx1leHHhR1eOMeRi0iIyQrJF5JZ5I0h8h3OkVvwXuSjXX858ibGld0Ad+0c/TinNpNXb05tkqqvbDtdKssNsXE4+ZIBAyGPNbGdL0QJjebG8alaWN2UuGIHKNKzL71skoarb7TfTVqGi5IkaWORaVV+YhGxRb2D/msHdQWckINwXaOa60R6z/AF4Pp2xMqTSsCXuaEQzkNn5GiHFxJRFJkQZLQmXLGZKmQIUXA/4dJ8iWZW/SpGIhNL9C/q+dl9xYRzO14jrOJkOmEMGOTEye4CZJCMiWXR0zzqYx1jycIQSQeCLLyXmxcKeZgZx3L3T7l6xP2XWLbJlX8JgCAq5VdslT6gZ47Vl0u+DfXb1Dp9xoZSh1FtyWByAwbaCBAgtyIwa2z6guVCw8gsxmYKQGANi1atzOwRStqVZd9n4rqr/UOaXJ7P1VIpJDlRwTuvjsTI8UDOgiij2NIxCOscSkXK4dtyx7TgZ21/Uf0bV9Ukwdf0DubRceQszZmvaJlaXg4ir+reAHOkgxxAWSLGSVM2afBWZsyIatlTYzq2R/YXrR2t6xdsp3R2vlST6Zk5GVp2TjzL4p8XPxC0WTAGTfFOoimDw5MUghcPHNG7RNC8uf1WmeyrW3V0W5sAZlIJDCAcgGV4IAkTkCa0elbxil23dsuFWWW0wZgygbl5IgzgnBAJBImua/1/701kyerEuW6/pIl1XG0fS58o5OQdSSbHkgnmwoI5MuDFgxMuXIwg+yN5GEjnGkgiORln9APYi6d6PdsdzsjiXUX7l1DKfIpmzM/OzNP0eOWNk/bWOHC7cWZYQqm9SnLEtG90R9eHa+r6H65d+9u6DpQ1fQu+8eBcHRJ4jNjQarkT4mdPlY8okkkw8mDzTSiWJjC2LmZOLkRywyEJst9Au0c7sL0a9NO0+5sjScbunH7VwG/tMOpJk5+THHjxPl58WO8smRlRGTJknydQijjh3FZHSFMmLbs+ga/Q9PTp9nWXWW5rWFu34VvxIkgIWRoYAsuXOASJwcYz4h6L1fq6dS6h0/T+Po+nJu1Kh2s39kbXNkhSLroHEpEG34jTKibRGMGULXDsKDG9oAWgLPC/JNE+2/56UjiKqgCjYaIPNG91n8GyVAskilH82XBIVDEMQWokbrFEjbdgn/AEDdx8bqA45VMTbgu0fbwAWaqrcCaUD3EqOD9pIv469APJ55IyAOMcAkDjsYPPevKCQpgmSAPuqR2EDaJyBg85BoSKI7gCqXtQsABtCtaUBwb93A3Dn5AuuiShDJsCkEqKG3caPO4biSOAfgCrr5sEJjk7LUhi42srElWsrbAfIokj2stkcf6lPSJJF9wCm+SQQwo2GoMCCSOPdwaJ446KiuOQJSZGYMr6cggdpoKKJFfcn2huWPJ+1ywF8D3KAR+LN3fSUqFbGxNvw5INlmLFBZKoAeL9ymh/mw7yxuA4o0gZdw4Lc2WNA8kfn3XfP8dD+IMBQ3L87dxr/NswJB5Bojjnn5HRSB0dAbirIwTBPeewPHb9fRuIcbVAHPusA0LItaJ/AHPx91/gjozHiIkXgMQFej8Blf7asG6B/n8j5+FTCAqnYAt3zRClqBX+TVcHd87uBRJXjjbyoAo28KoHAcBr8hIsAqBzfJNnjnopJJgE7tswx5J74Hb5gflgxImZg1LaqEVgOd9HaKuiGvabBA2tyNwAc4I2l2oQDtRSBzW69hYD43A2fj8KSPm0UDbwIwRakFvwALIajQJYivm655+enTHiKqW2G2A9ykWWomqobQaIL2aJoqeLdtY9vzH+abvX1/I/4r2mIS7Ve1d54IsCy5kB+RQZqri64/BLr/APpn/wAn/wC3SsK/tuVAAYNYJD3/ADEoIpEJr7SSDZ4vr8tNuLQ2d1HcNx4A+CV5H8fHT1UQJGf9/v8AZNRsQSSP3itRYjPk2ogA2m9xAK7wrb6BreaA4uueej4oJASSCZGYswBsFeeRuIAQ3xySOK46OGOAST8qrbhsBFrtC7mFnavItaYmjtFE9LIg33VrYCqwD7QeKIBXkHi+PmgPnrlgEkYMTV6ko4QYyRFe66FEqXI/NCga5ssPjhroEhcdfxRNg1tApqUV9xJ/kEUAfkmq6cEg9pB2oVFe1VFWa4oMfwP9R/8AoF44rpuQqMGcj3bioulUkhiRYIIphQ+OpNi+/wC/3+fyooQQjlQVNGzY45AoEALyKN8H/t18TEsoWQgXRalsgL9qtfC8lf8AFMf93MQlrALbASVWxakgWykrto/ngLYoFuaITHICKvwb27j7RZJJb/8AuY/xZrgdLsX9n9/s/KgggwRB9Dg/hQ8eOaAbkEkmgdyg/AsMvxwCSbHyOi1x6YEKCQ1Elj7QtAHkkkkCzyRfFV8HQRjcoQ8MDdNyDyLAAYMAf4b45ANCy4oCPkkbzu9wJC2b2gKD/sb/ANv9nAdgOKKESNa+PzcmxTYFD3cABiTYIJ+P5vgmNNylCoIYflSDtANDkcmhTfxSj4NA5YN3CgjbyL9t8gW1ULBJWrBr8D56UjgKyAGwyguVF8jgEJd8ndzf8CgOgqTIg/hnsf8AFKokgcyCPfIIrDP629BbO9EJNSj3x/8ApruTSNTlAit/BJHl6ZHtoqoMmo5uApYk+xVWm3tWgn1T0zFxe0MXGjxUaaXCj1J533O0cksi5ivGCoO4ugssGVkBQ7RTddTHqp2GO/vTfvLtHYss2v6BqOHgiVigTVVTy6LkEDcw/S6vFhZHAYkR7NpVmI5fPULG1QYWG2ZgzSYulwS6FqaoUOZhTY6S4pSaEEOjTwhciDcNhDGnKA9crVobepsPMK5AIMgkoRBIMSJMmMRWx6Ts1XRuoaMkTaRhkrgXVCK2eQh+8TAHr6RXtnTZMnB0nGKLLeHiExpZElQRlTwT9+w3TK3kUNQ2gPYU0UPbmK2d4tk0SySJAVd5J3iQBo4bLssjNHuBk8e9bADOdvQnYWNG39pWKRZImxsYxzBxukAhAFja2wFV8hBeo2BQkSBgMytA9NsfU8eKXIw8ad5EilQyQl2V3YsFkRdyogb3byeATICdpUUNTeNq4SxYDlgBnJHbnucT6ntV/pXTBq7PkAW9aBtjcRKm3tBkETmcjuPbFYi9jZnrZ6nTahH2r2l2zi4uBg6nm4susaLhZWVny4UMs36HFxWfz5GVm+NVD7YMcM7TTjYjOPMHaHqz3fmxacO1Oye5tT1LUNF0/QkxtGxIotSm1aeOMwR5mLqcudp76ZaPkHO0h8OLS8VcvMzsdVYJsw9KfRXuLE1CTJ7ew4sjAebx5EWXC/hxpo/E9xSlUUebzoIMmMeItI4iYtFIE2N9rejs0nbmRDqvZ+kLJlY7NkapLqedk5Ujv+45lheCSRVCmSE4zZgxDjSJCESLHQNA3WdFpLe19Ml+c22tPqFurIGCHZRIMnGcSAAJq0nwb1HWGU6ndsBSBet3bVgWxthiyBLZJJ4g8iO9czMmi5vaPc2paXqPbceka/25Mr5o0XVF1bSyv6qOEZqSpkTYuRgTSvFC8sDr+nYCB5YciMRjeF/T++ovUNUjl7J1DOb+4RiQRzTt+mlZsiZiHjZAJI1Y/qJZYnaWLIynV8gARKrV/wCuvo72v2pPP3fP2zo+Rq4XIwIdUii8WRkJlvinLwiryZUOfhSASGWOeGNwVWYzjbHtxo+nqSXS/XbRJ8Iph4eZrMkUioGjhEeTMhjjUFtyC4tpBJEe4uAN7beR1QjqWh1LMrJZXTKyFm3srIqkhv6jM4PYyMnNaPoukv8ASOpWNPbvi61+5ZtXmNtbIKEqtvxPIACmASQMnPEV1edr+iOu94adNreLkZbtmYwZpEfZujAiNwhZCqpFCq+MBJRGTCu4qCx14/Uz9L3cWP8A3LJz4dZ7hwkgyXmibFOf41VjK+XG2YWwtJMirHiS6iUR518e1o4JCj7qvQXW5cL00fwpHKRjwRSjcCI8aQrs2K6bkZ4Y7VParptdC/Fyr1E0mXXewtZwNL0bF1hdVxcjD1fBYyQ5qYOVgDEmSLKxnxsr2fuMkcMiSOAVRkDK4ytjTountXNNeuWtQ1sOdkBmFvG1Q4gMdpgmCTE1rtVrdVa1b6XVWbV3SC8dP5nIRC5Es5TzKksQRORiQK4h+9/pt9QM6J+6+0vp5xM7SE1rM0X9f3Pl5cmRnalpelT52owQ6ZA2DlwZ8ceIygDTR+pnmxzHk5PlWZbz9GvQH1HEK632L9K/or6wYmj+neV6g62/b6+qHp7rmiaNjagcKXDyYfVTtHtnIh1bORmy9NwsUKM3DeZTMWSBJ98MPoHr+ZqeZqEHZ4m06dsfMyMHHxo84JqULOH1FP7sX36kXZsmbUdQ8+Y+S+VJJkSSTsTlR6c+n2m6bo2s6ZB2oNMOspjRa1FLp/iGY+L+rjxE1LC2nCzPHiZniGNNBkRRKjJjBY8i01Gh6h0+7owup6dq01GweHcu3bsO4Il2Nu7sAYiADtAgmIisZ1bovUbHUE1Gj6r00aE3Nzae1pQWsLcKg20/kknaIYlmJJJJIzGmr6TPUz0J9bP0vb8PpFpXa+s6SDMnbmVixSNFkiN1/SzGXGj1LLyMfExcnJGFqKvtx2x9RxvJhTpk9bX+2OztL7evK7e0bD0bAylbIycTCxFgiknaNADGID+nfwbFiH7ZbahKTbWWKOZaT9Mvpr29mvr+J2fpOJrhkGQmrYuDjYuoHHRT4sWSbCgieaBSkSCJmEcWyNlUlFqX9w4uLpuGkUC04gOwkMzH9tgwEe5tzoxIISyHSgLBHWYvu7s4dWRTeOxWZnMggqoZid09gMmMjitdp1sfyV0/8x7enZXZLQti44QEnYoEiFZoIMAE8Sa1x6L6I6B6/fWtrr90wYuV2n6XaQ2s5WFPNjwy6nmzYuLpQwYlyn8ikCf9TKFjnm2SO2OccnfjNOpejg7e+rPX+8dG0vH0/ScDsHVsLUfHCjyxjJ1zB0bQ4DKQ7+SZMLLCvK7ukWn5MILACRm3C7P777Y+q/G771SNcDC9SO4s3B7Vj8mXjTLoGFqOPomNlZQKRK8moZeG2bGHmmjyNNfAZj51aLFz09V8PS9Pz8qTDd21juTUJ59TFozRadoOTqONpiGRd9xZU02qZhQNbSyQSUQrEWOl2n6v1vSae0oRtLeCXGbylE0+2/dCk4DRbVSMEFwMSJ6Gs6mvwj8Ja/XsTfHW+i3l09pFG23qtd/6W2BcIAO7cWW4CYZdgywqj0hLxmQISRsClQo2UxDWTQNqw4u6AFUB0uI7XeF2KQfuFAyWGkJAYkruYqvKgChzZooQkAqwI3GwCrVY5PJoA1zuJ5IIBvpR4VeS9zbXpCqHlQAGB5v7mFE/454A69rMKSN4fuG4kNkYnBAIB9SCe9fJguFisnbCorR5SGRArD1ncpB9TPrSMKKrEgBSHJVlXaQQpsAljVgVxRJ+D0ZjQozBmU8D49pB9273D4JqwPnkfNG+l1i4cEFQeB8G2J+00f4H44/yOLWiSgBQLEAsdwUhTRO0k0G22ACBRPuCiz0AE5AkeozSMxkiSR6kyTgd6SkgoFrFbWPJ+4k0L+SCD8XQ/k1z0II123tJBCA8BiWBO4+1+bA/PJvn4B6exCCWQNu5s37BweBZB+P5PBr+K6QMNEp8UDQHwaag10KJHNfNfxfCwfQ/gabJgjsabHxwQas37lF0CDt5J45FHg88/wCT0sIWQqdtn3EEFPiRfigbADWAKoA/9ickFGwGUKW2qRwxolmNm6IT8WD+Oa69sadApUuWIZRuIquCAo4Fqw3MwJINC76IPofwNO3t7fh+/wBk+0KRRbWDVZEQ4v2HgLRUAEkfKncACL5HBccZdxVW3Kx9tkgrZNhCQFskWxO0cCmIIFpLHtILUQ5QKoDkhhtIYUWBtfncBtJsXW7pzSIGXghiCWJ+CpQK1MeFG40OLq6FXfUw4HyFMpYRgRmMlaIVgFK8M704Wr+AFIJ55NfPSXjRrLGU8kKUFgqPg2CRfzf+eidgaNpasgqigcEMxsBRxYDAHmwD+RfSojD8tSn4IDUP5+Bx+elorVQY9roEkNWAWI3LVG7bgEHni2qvvY8BaGBQP9G69rAEtbGmYnkgG6Ne2gfxddOCwEH4K2ANqswVabkkFAACTzR/3/HRWPjbiR7rZ29x9ysFBcEFaPAZj/JFX8dc4CAAMnt25NdCQMnAHPypGHHPwW+a2qboncGIsDmlsmgaF3VdFJC4raopmu7BI2sebsjaQF/NiyxHI6PjxnVQUG4tRXcCKSgx5YPtJXk7QCeQbuujY4Aa2LVtSgXac1upgARf8jaf5IBHUioSPNg+2fr+uPzpAVYSpkcT7xmmmDHNUOVVmoVz7qKgDkg3ZCGiBR5DAdFLDYUlaohdlvYP+Nigk2Kog3d/xTwYWF7gCGIZ29qkURX2KPwPyKs/jk9e0xAvvANs68NY5s0Np9wBNCwCKI/xa+H7/LH+6QEIB4jyeJIAJiP0E/jQ0OOC1m0I/k+0NurcLIoByASV45BAIoFCAgqQOQF+BZLBqsUCbIo/+5r8mRxqF59rEgjaSCRXIBN/PzdX/kjopYDuaje+uOGZkAphZUFT80VZj8UBwAqrtMzOI4/3TPFBMDP79vn+4oNIhdWvzvXaVv4C+7kf6iBQHF18nowY7MV9vKbWR6+CFvk1ytk2Pg0BfHS3gKtupd421wQADscgk+2xtHFi7/g9HxxNShT7TQVSPcFXhif+7L+fwRyen0viN/SM9v3+OMzxnuEELKpkO4n2HYDW0tbqfcGUOoZSV5VbZbYDrRx9ZPoM/YXqpna0mC57J9RptQ1zDykvxDV5J4MnW9HcQRskU2NnZUudjudrf23OjjjiZcXKaLe2YGFAqUYHawHKsa+5iLA4v28EgWTxfUO9Q/S7tT1Z7R1Ls3vHTjmaRmNHPFLjucXUtOz4VqDUtJzFEkmHn46sdkkSNDLCcjFysTOxM3OxculrdMdTa2pi4DKviUAIkj19SMfXAq70rqn/AI/UEOly7YvqEvBOWCulwhRB3N5YUd5ma5WO34V7b1ZNOxXdsbFy3jxomdZGWHIkOQkRdljUBXyHVeQnjZKbknraL6ManjZ2n4eJkLHujiiKNMVUBysbncVWZZBtLKS6AIthdzfOIv1NfT13N9N3qLi6dq2o6drWg91wS5/amp4W+PJmxNNzJ8aeLU8Fo4xiahAuZp/l/TTZuLNsOQuSob9Okt9K+5GxRjBZjA/lCANIoZNhDFpAXDFGHtaLb+4hKggX1kusadribZbxVUKIJEFSOY5k5I5z716l8Ka22ut1D22R9M9+5BwQNwQ7CY+8pO1hEBsSJrdT6Q5GmYwRz4ooTKwgViZTNIJUkjRZVaNz5Gph5gzmMsFtWA6yc13uPS8TQJJ4GjEX6NpRP+5B4kbHMsb7Rteo4/2jGqKwiWRlpgL1uemPdMbxxLJl5SxZERdZWaMtBktkwCMRPKtr9waIMHDeNQE3lesi9S7hyZNNRZMhzDFBHCWleOEyBBKJQRCsZRwo8w2QrDGJIQlx/PnoN+3fbxGbcrbYJJ9IMnPfIx/evZLui0d21Ze2bShk3swYgsQo+8PpH7AGFX1Td0nOyUx5JhvewEjXxRKVaN9iRg7Yw5Le9gJXSnla7PWJvo+Fb1M7YTE4ZdSw5WmjAlIJzELSFA29rBArgA+3cGup99S+oSHV1jUySZeRPO5gBZmjjBkE2TZVTHGsaww7mXa0m91P2npy+i/tE9zeoGJPOBHNh5OO0MbBWXdjs8pgVvFIFZhCqJuIDPItAMSTpUTZ0u/faf51q4uTiXAIicTnOMfSKx9s+P1/Taa3bLql6yzXAo2J4TKSCTlvuxkicnmY61Pp3xZMT00x8xnVBPJBjKgLOcmBoncS2yASBI5VSJPtSMqySyKSVyi7VSEaxh48khSHOjMZZHZVjaZpFQEhwr7GCtcjFSDwTtW6m9LtClxPSTTvC4WOKXGeRTHJbyeHYxjkRkx3YtGxkYABAyhGUqx6mOkZRgz8CaNtkuLLUwcFmAXap8alzuKFjQYkH7jfz1ybCNYfS7hCizaZTjzeLeIIPby59ee1T9WujqN/XXQqspv6m1cVR5VuW0UpuAiJAHpwDySKunUtCxdGn/XwY+NuYrIWSEbZYZG90kqqoDKWbaSLIDlwG2jcnJk6XqOO7lMUGJtyBBHQYWBGsgYO8jnevwwjKICy7+HnU9QGTgxqXikYQlFjlZGEJ2L5CHiaFrfaPGyyL7o5IyG8Tg0brUq40ryJO0cTT2GEhhUmSQCJSwvysZGACuT5LAANgDQanVrpwICFNo2gqJBIHlEY5Mj1n1rGaHpo1TXdwXx1aLbQ0MJEgg3OwE7oJzx2p411MOOOYAJtHkBL2C6hW2LG0V8qw27wGQqFcr7lPWK/ceTJkaiMKDHytQlUssOHDOZsnUJcRdgxfI8p3yZhjMJkkLXKyXKbbqydX7ly8ZGh3fqFIlExDneiFeLVYiHUCybYcj56rbtvPxV9RtNlzMuGHHgGo6i0006QBEwcDICPEVEweVp8nT4gp2jeXkJaQiJ8te1FzXaqxaClYecqBgbTvEc7IJk4zx2reaLRr03RXrrOC/hgIwMw0rgg4AZCy8nDHuK953ogdS1TtT1F75k1D++9rx4n6Dtb9RiDRO3XjjXLiXKmVRlNDpw3MEikDzSJ4y0IR3SjO69SOra5mZiu74yKmNib3ct4cVQiMF2A75pPJOy2wuWYfn2336j+rSaxNkaV28zx4xhXDys6N5A0lSEf8MLG1XR28kyu6yMzKLU8Y4PG/ktQCwKs1+0MW93+R8seDVUBZHPXpPwv0BdE9zqLoUvXkYANPiBmXa966DhXuyCAMQnPevG/j74vu9bXTdHW5v0vT33RbIS0pWTZs2gq/wAy3p2LElol4xFedkuwBNpVi5BFMhUEgEkk01g/kCuf8H6yOg4oWBuY0QByDtIoGyQb5qhxzXRywgDbtBOw3RDEubIC/gGgaUfnn88fY4GcmwbVOOKU/aSt/JYV/wC/8X1syiHJUTAHHMACfrE15p7+uT7k5J+pk/WkYo9gVX4Yg/71RNlqFHkCyL/Hzx0QkBkDA38EhVP5qwvFmgaoA1yOBdAoQlgG+SN28Hjbuonn54ocH8dKpGQ5cciME82a9q8A/jlAPyaNfjpQAogCBRQMaiNmLksSCa3A/wAmv5oH8HmujJF4NBWZwDtHIUEA7Ax/6Kq/yefg0Eo0JnYFdyjft5I93P8AF/8Ag/8Ab45czGTQI4ABtuBwCADIeBX8GjVfzfS0U3iGlV7BBZQa5PjDRitrG7UXYHJs/wAWEYIOQ0gNsDXuJI+WC82NqhwBXwTwarp1aIqhtlFkUaNkjkgADbfNn4+fwOvPtU3tsspB/BreVAUVwQABXFgfzz0UUmiqaHACEDngEEFuSWXm6HtF8AH56eI4uUoqQUO42PgggWaI+SasXd/joSKMAsSECkc8ABSF+ZDW4kkBRzwdtfFh1SAolxEMw3WoFjaGKhvk/DUygGyA/wAV0UV5WIrKV4BfYEvgDg2RdACju4NsQFANkdLbVXggA/kGv/rX/wBOvTvGjqDRkb2bjYXci2WFUAov3f4IPz0h45SAVmiiDCwspp6s80CODVj/ANuK6KK1k+E0jb3IUUFIBL7gL3Kx9ovn/v8Azx0bDEyrYG0Cht5UAml4JJAok/AFD4+QSR+m2kBQao1UjMEogAFSKJIFlqsn5ojo+KIbjdn2qQK4Yn/HIAsH5rkc/HVQW1EGPNgznt+496sMXcpB2gHgycyO4I96QjQkVtat5AoBrPF0aB3HaoUEcngHnouOEEIB/pVgbUHcCSSN18AA0RV7tw5oWukGxiAxFkk2PzQPP88Agfzdcmh04LGq/INWLIo82V5H4+3gCjXPF9PpAz5yMGDBPtxnH7B70AYAE90YZVDsSR76UC2A/wCo3Rom1C/AHK0ca7DQsHkbgG2ng7QW5AIthxQqvkcHiK0DC6FcHkjeSKLWDdjkf6KAI5sqJjUQCoJ5JIACj/PB9p/AYfANXRPRQDIHY91PI45oSKEG/bxuA5AjYtbt7a9xG173AUQCPyOjPEVYlSCEJ52kAKVAYfcLN/wADxwTfRkcKLtBHyaJFE7CbC2RyLFj/vyD0UMb4N8WCvICqAXJBA/JJU7gBXHwRwUOyqYBDH29P0/OhBGGC2DwOW//AFKKFe1gRtB++roV8noyKNVG0qSx4ayLN1S/HPwAPxaMfkjogxKzBA3zXtUMxJArgBHYcV8HbRPHNkiKMrtJAYgqyhbNEFuV3Hk8kkrz/J56UCSB60KzxIIA7DJjI945nsPTuaQWPddoAxJUmh8CifwOSSP5Hzz8DoiOLxmMAULJVuaujamudo9x+Pk1+eiFRQQzNzuZdqjkkCwa5a+ab8NQ3WVFLmIyA1u2kIDftFl/gjjgHaTx8c0OSXFCATzAJj5dv1/L1wS3LZ2wViZDCDOZ7if9YGmb+rNp8oyfQXVEK7Q3qJpshIkFSPH2jnRcFBFTxYuVQkdbKkx72jIGu3s/UhHDjTqZXkx03ANGgVkVIUVzVozeOw+8qRtBHB63D/1SOypda+n7SO7oMZ537C790LNzp445JpMXRtdhzO25gEVWVI21XV9FlllNGEwxsGQe5dKHa2RNHIhRy6sArHaAHSYbXHtHuZggJoyNEm0VUa9Z3q1klgCpJ2j2wSO0zjHP4YrW/DeqKKhJyGbBJzIXMx2j59+2diHpX3XLGYzPKTBkbMeJR+nPh2eGXaUDP4AphdFeRAioPIXAYHrK7U+7MePRv1EkUxnVZpGinljkKboHjiRmgf8AT7RE/LxyszUWZBZB1z9ia7HhHHRZCAZJb3spKxFBEpr2gAgLEW2FlRWjK7ErrLLBysnVO22XKWGOOSV3kQuJNj5EcMMcTSkbXpgiiIAqJDI8YVpmvz7XaIm4G2n7ymAPQg5PvHERP5+49G6ibmkVGd1dQSsEeXygSeZB7wcevFYb93dx4vcnqHrq6zlrFDDprHHfJlSONYFckkSPUaDczluSdwYHkWb/APon9VOyO3PU6LBzdYwBgZeaBi6jg5WJl465bFolhzJUk34xk89jfJGC6Qn4s9VP6jenenalkS5OnYeSNRi92LPhmYZMizK6uLSSirCMq0UiSDeNpQgAdOXoD9HXqB3hqg7i7cimxJdHyGl1PP1XyY+FPiy7JGwZpsZQFyI4y8kMirL4541dlTbJd68+lu6B9125ZlGtC2ODdVceUwTJQgNwIk4NU9Je1tnqFm5asrcO5bz3zwbTFTJJnzDdLepiBzXZl2/636B276YR6XktpyafixadLlzz5MEJhJAKS/qJ5WxoknkZnLRxhirRq8+0k9TLs/VcTunMxn0jKx85c7GmleLFePOgi/ToWWdZIGkiYvKMcqBKV8jQqGJNnBjtj6a/TD1p+nTA9Nu/E/8AWuFGNPw9W1B3GXHj5mEFM0uDDLNOoz8KMjZl5Ss/kkZljvyg58fTv6M+mnof2XpvbPp1osmm6Zi4caGbUdQ1HVcyaBYlRVlydSmyJMYHwrKmNA0eIrBGjhjIAHFsm5qbmnS5dV0+zoQWuTctAhdqIsSXCkd+8irOufQ6KzqzaW99pbU6hr1kWo0ztdAHjNn7yiPNmJAyc1Pm1CZ4WhyFmg8D7lKTlUVVYAGXZPHNJIF8ZMYLJbUdyBx1Ae4Mk5C3M5dIg0xYKqrICSCxlR0AZUttjcqPcX5JFxazpuKznKjVVr91kjl3BgY/GyEIHBlUBOJdoVWcKA4pqo1zHUBnkVYQ28LG2+vtYmM7Qo20CxG0AsBRvno1/iBxZa4XVAAN2TODzmO2Y9cVz+n6i1K30CoSMqvHOQeJOf05zVK6uwjgmyJHdQqqYgzs6sAhKyHZZLssgZY2Z7kUKK6xp12UZndO3YRNg6UkgLHfT6pltEpV3Q87NJ2sVa/F4nI2uLyY7ihDeVQdscbRtLI8o8TnahBYOkXikVEfyF2kUCi7jd1jHAJMrVdd1GQq+NqGpPHp7b0dlw9OjGlyXEIIWgdNQxM8IjSZm+EQMuUYwiJd+EdJ4nXbb3U3Jp0e8R6tt8MD2M3J7gAEHmqX8Qeom18NXLVu4y3dW1izaCzLy6XGA74S2Wx2mBR0EADjcoDP72UEMqheVUlQAQ1/jjg/g118MRMzAKWPLEjivdtFH/UoFijyBXAB6NhRNgIUghStINu3b7T/AASB8KPhRW0fjr7t2XTMHZqWyfbxv5Nmy1gccUSCSOOvZwNo2+mPwxXz1zxmkwm08C1Q7WPz7ttmhZvaCvPHNVfz18WEA7gNzMT87aZLU8gEkCwas8sg+K6PSPaqlfcPliTYpgWeifja2wRi6A3WDS16SAb7DfICICRfj3FmYkWbTcwB+RdD5NFFeI4zsAFFLoXe6yd26gwP2hvn4o0PjpSOIIpai1XXNXya5JNfj8/4BHPS8aHhGt/bQLHhtwY1tPya+Ls0CfjolUAIUFuRzXAWjTbSRQIrgj4+RzXRRTXHGDIW4prJr3MpBtlXivcTRH5s88jo0RgkbkMYLsbI+aHvJ/FixyCQ9e38jpSKIGW2Ue5juADGi5KgE3X2kNVUxri+i2iUqVAZgpBAAqjHRQA0a934FUP8dFAyQPX/ACB/egJo3IUEAlgGYFQBtYnaTQsEEcKefmwevf6ZhRUDncrW92tcgD/TywsgWSDfJvpacBJULPZ2n7iSbHwRfz8/I/8AqB1+8hjeP27t217Jtj5AGJ5Nk/H/ANb56KK/Qw7SFLKFUryCQW5B2kf7H+fgX/s7ArCm5XUNSAe4LZaShYJv2/P+/AP8BgNL8lxRDjdaqw+ADfAYfNfwAQQOOvsqs4CIwBo2Coeto3AKNwAJHPAuh810VIihpJ4H+R/k/vj8V8stFQ3yiow9lKXNluRbXz8/I/npTwtL73jQk8L88KB7Rwv+kcAEAgAcVXThjQoN7fElAJGDxErCt1fFim3EUSCtk7R0oUFn2hQTwCu7ihXPP/j/AO/RTDE4ED3/AD/OtcMcQob2YE7qock8ECm3AgVzQHF1xdrRREFQT7LZW4H3EFjQBG754LUeWHAA6KWEpfwdpAFkDgk2bIFiwPwPx8nooRMBYo2QRwWVgVb/AKQeebv8Efm+q/NTeIY3AAj1MT29QeD+nypJI1amJHJUjizwFKsOOeSpNlwCrD7flZo43NHcA17rCDhQFB3Va/Bbggg8/PPRKQkqfgMADzW0D2j887zXICpwbPSyrbclGsGiNu02CvNsbo3fzyK/x07Y3p+Yppukrt2qJIO4ABpEYkCYMcT+WK8JDayAkFQRe6gocUFooygXxfyLP/n6YgAG4/hR+Qf5BP8AgE1fxfP56MjRxuACsCNzAkEEoCRYDKB/soFi/igevbgt9ooBbFbiw5H+fx+T8Dj/AD0bG9PzFNVgCSeTGeT+PNIxRhyx3g7bb5sBQ1AgjkmzRsn+ebvolYwwYb2NXQCqRxzXuC3+eGNX/j4+BTXCqWIHFKt7RfzRPJG4i7Y38AmnHxs5YsGVyo2j2hVCkGwOfdZoEtRWhXA6cqkHIER8/Smd57+vf8aHEW4qu4lW2kfIYfIauBtG1a4JP/xXfRSxgsNm2qUNuBILC+F9pKAX87gWtTdjpVY3F2AF27iwu7sEgD3f6FY/A95X/bo9YgfgKbr4IscfgfBNkg2CKoVd2+B6D8KWT6n8aGERHuZY23BQoAa7IJNWSv2qPx/8+iREoiH2qCaoE7wXpSCeTz+LalBsVZtVYw20EjbTcCtrAcAH4KkXyKrd8GgbNijpdo2mxdtyCKCjnjgKKFV/knpaJPqfxNVx6n+nOnep3p53n2Bq4Vcbu/t3U9DbIT3PiT5uPN+jzI1YbTPgZy4+bETuAlx4FBi4brlJm0bWOze5dc7S1yB8LWe2Na1HQNVxCzAjN07Nkw82IOsdGAvC22VLTKoSArGwUdgJQVzQHAHG5WUH3J7iF9wJCksAAbJ+COeT+p92jhdn/UfpXcenQR48PqL2HpWtZ+yLxxZGtaHm5mgagQkfjRpv7fhaRNkye5pZZJJy7uzDrndRt7wl0kkghXByCkiW92GQJ9MDsev0a/4eoNpp2OsiBMNwIHbkZj+9Y6du6nUJWDcpx5En2kbBZYCZhsYlGZnpFYEuzF2Nlic4tB1nTNL7Fw9R12bCw8Yj9XjxIwOVLOI4pYZBukQEBl2qFaTbIpl8cWzauuHtLWZUyUieQB5fEnk/adCyE8ERyOGQvGrFywdlBAokVlFl6+83ZM+iZCplqkJVEyFGwIQZCqgiSN1WR5A8bKA25jyAoTCdQslmUKSAXUMfu7RJmD7nkfPivXujagOoU3PClWIfMSAIDRJIk5HMAkin3P8AXHtnG1HLyBlYyaX5IcjyHbJLIQscci+UMDthBkMckxoP4zuILP1tN+h768fQDT8HWu0dXjbT9R1fGlhj1PVIcHUNCy52fxiTHaBZEikBWJMppZVklV2qYE0dDnp/2R2nq3dIj1HSsfL0rNzsfFz8aWKPLECTOwkleLIG14pFb9sEgOyiNmUC+t3fpp9FH0e4mjadqumJk6Dq+RFiT5E/b+pa1o6tkMk+VHJel5uk7GCtjoJEDKDcUbhmcnk9XsaSwgE6u7dafC2GwEVoAMbgI55bzHmTJr1H4R6Zd1tpDfu6A2dSypfL2tWbqKsMtyx9kHhhGYCBcjytkczs97C+tX6W/RmDI0fVu/hDi9z63l6lp0/9pC6bB5zuEMQikjZJJBEsiqCI40kkAZz5D1sH7U9aPTDvbQY+6+ze7e39Y0jKx1nh1LBz8aWNXp18MgjYMoAVYyvkEjeXaCsgBOmnN+iH6T+5dLx+5e4tG0XvXWcDCx10xu68Ze5tQiihEf6XCwcjWW1HVYo4Ekly3ix5ceJP02R4o48mfFkkm/pN9HXpXj43cR7byu8+19CzAJP7RoPeOvYGhTapC2PJG4wP7i8eRBE4IzII1MCIHik3EvCORa1h0dtECOpdRtt3vs+xSpAm2dONxYkEnxJE444tfEHwt0rULqtYNdcs6i1chz4brbdSqbrfhsih/NhHEnaAPUHdFk62mqYEmVcHgyI0kieuXaUb7IUkxpcLOYyBseVRGAUUGtNbzDL+05CqkpFNabkuZB5ASCGshRdAIdx99nquez9Ql0LSsLRRI0uDhwrJG8mRK43CwNjTsF2PtQxxsVA3KEKkgkjuDWfDg5udlqMklcuZUWQD9oPNJAIyLaWbxwTA0GEkhjACf6k1d8XgjcOUEkTOD2I5PvzgfIef2NGukum2NpQXQFLALvGAGCnhT+RmoH6ia02l9v5+Wg3ZL6bkzw40eQu6fyQypEvKsVjndeQWRWFSSsuJHPJHSmkYKYmHg48cbQqmLAWgdY/JG7qxZXeEtHIbJlZ9xt5WKBV9omfcDSapBk6jmyGTCh/UY8UbI5E+XNltDm5cKtEgOEgeeLFis+Nw0paOPHiQM6BA6WDW6xt/gBQFs3agKAK/k8k9bv4Hs2yNfqDD3EurayAYXwlKqCZjc0swkCQDzE+e/wAR9TdD9K0yki2LVzUBtxnxFuBAVGBAUsgaPuFlEBiK8mMBhe1FZtltwHUD5o0LJUncBZHIPNH0kZZgRQAO1aABZjVDmwECBQOKZ73CwK9uN8viAAX2E+Qqw2r/APqBKtRv21RHHHHRsaD3i+Sb3fNDgHngWxN3zybXih1vxwJ5715h+XywPw7UNKo+A4CsPGFpT7qJbkAcGiD8/wAg9KLGQaAVQQVUoSCwQKwBN2N3JFDkAjjjpVo6BZSCpJos1g7gosEAcGiQSB7iwoiiCo0NIwqqYnd+GIoCx+CLv/YG66KKSESn4Yjg2CoAAQG6IBIu7v8AIBBPz0oihrXmwQPgmizAA0bJFkG74/8AbpbFQv5VjAO0GkscSycUSTzuP2mxQ5IJ569hCpElgEmwVoA01buG3AXyBXHwQeeiikPE8b2vypoAg8kH7zQuhya/Fcc/Pjey2q37Tuvbe1ieSKYbhVDaQeb+fwaA1jxuANgYmtxDM3JYmzb2SP4+aodAiMrOwUAFmZaH3Ou75SyeWJN3W6zRAoAo7z39e9IvA0sjAAjbyoI4HIHHHG4KTyLrkg/g9YmDgbgascqCeGNiiDQFDbVCrqvnr2q0LVlZ2s3/AP6t+aFGiP5PJ6+rZIvaPhbF+yhdXZFcEgGyF4PPRRXqUgoG9ygsAopSeR/l+QSV5IumAFV0RjwiJveqh2D0Sofx2tEUCTuK3R3WpJAo2evaxhaaQWxLUtAobIIKrfJC7SPdf55HR6olFzYZQBZFEf8ATxZ+fwfyRYHHRSgkcEj5GkStWQAu4xkN8Fr3VGUq/Zt5sc7huJodKOxjIVbUFVagDXIu/j89fhE3vdhQI4kHNWQCWHI3c7gfgVfIqjkChQPbx/1Frr5/6x8XQ4+B0Ula6I4yTuIYsDyQNysCPyCKY/wbFX+eKM8bA/tKqUQQSoLA19p9q2QPjkc/gfnyN1oVtST8C6IPB5UhePxd/I46KWN9xG5ua9pH/T82Qzc3/JN2bF3Tdqjt+Z/zSyYicelflDlCGDc7QeBa2HBYLweSSOX+TuBHAPtIiTul2iyu3baiwtD8DbYUbhRBJLWSaBCqbPt2g7fbRIaq+N5IFEGtu3mzz0qEBII5Kg7uLAWuR/Ngc0BXweeadSV8jhK+7aBZIrduHABBZqFA3W0jn5J+Ove0V7UDClslq++z8kWfwKKgGxz+CvHGSu1mcHceSBtI4ILDdtIF+3cQbtSOvQUSbt4IKqoWx8gkGqB4I4JH4AHP8FFeIYBTAAAA0SQWFAjkWRQ/jmqJro3YKKDkUosE8mgTQo17SD8mhQ/z1+SEqAwJG7bzweNw+QTXI/8Ab5H4BkaEElrYXd1bUVFiqv8A7mv8/iyihkiO4AIKIb7nYsNq/gDaaN/NUD/4DjEhI+z20QV/m/8AO2xVH4Iv/FDrwsI3bqNKzcfn8AgUR81f5u+jli5F8WCEAH3EkGgQK+K/P5H830UV4SHjaVIUj5Jorzd0AGYfI3Fr+D8k9FRqVtCtJV8j3XwAA34U0DVH5+Tz17MbuQCAACeCLBHxRs0faSKBq/8AHPRMcQKlaN/g0OFBFcWfwP8APP8A2IKKH8YJBUAkWGVmJAB+47SpB4/kC/jgX1pE/rHaNPBL9PfdkHtME/qFouSxUUkjr2rqOETQUtuii1AmNXG5jIY4gZCyby1ijJIaP/JfkH/bj5I/Hxf+LHWpr+r5o8eT6DenesLj7ptJ9WcPEWf7fHjat2h3WZYmJWmXIn07EQRmVC0gRE3FiFqa4E6dtokiSfYeXOcev7ir3To+1Wc8uAf/AKyD9Mj549JrQ/pGcFlxJW2mGSVS6SAl45gV3A1exgSpQWdwZWIBEiJmP2to66zpELo6mUCJVJloyKIPI4ZfbcySSkyqFEUkgZthZg4wciBiDxpfjYoJNrbGjKAiN1kKOwkj5CBBuZC8JanLrlF6Keo+LgaljablvC7hYL/UhVSVVG4kArGWYGFFJIZwqkv7zRyGutPess9rYrIQGRploiHBHeewj5YNemdFvrp9Utu+W8O5bJttCwGlQimcH6gjGayk7T+mDuPuHJxdR0bKk0jJyPGkM0Itp5W3SBdo2Iy8E0GtCrfzXWb/AGf9JP1Y464WPpPfujpjSxRlDqWiNOUhinsu80bxMrK6ufe/LeOMbVKlSPp79RsHUtT0Zl8EkEEUEaRrE0ZLT+FXmVxE8YCtKfLZlURcRiKffv3Yemuv9vxS4oy8hoYiIY3jjZ1IVUEn7SlpFeMyCa7VTD4iCXKKX881er1NzVHT3msoimFNxQwBYwxG5CWJEwTIESDgz7dpH/8AG6Qajp2o1loMubemv3ES6VAKApbuBc92EEzMzWKnpf8ATL669vpBD3139DmxVB5DoOkQ4CyibZFHjvPLLmTMG3C0SWESIxZgxAC5odt9l61pmmY2FLEsCQwCLkqZSpSYTRoxRYIpSHMnlG0uu7eWkCqcv9Nzu15NPiy0yMZJXgiZ4NwlcIQpLgBvGPIfCq71dpGJKj5UVl3D3VoGm4uRMxRREszxY6xorlvNOQ2yl3MxZo1IEbrskUUVrp2p6dZsFLn2g3PEEKAE2CQsgBR5BxkAHv6muI/xL1PqDumpW5dW220eOXubZjCrcJAdQclRO4TMk1WOoQrp0UbeZYikfmQL41VJEaSXfLbP9pmBHuMfjCUpMOx4gDl936ni6JpeRkJjYsqz67khCYo8aamGEA71+qzY8edkRXHgQTMw8jLEWYZmteoupZB0eafG0eUzxTZzrIP1DRz5OJLBjY8uM0ckuJLG+P5pXmgRYRMi5KMokyH0Xt3G0PAxcTTYREgcNMS5UsZd6SySs+6TInpp5WXezzStEZHCK0r0tjsxAJRVG0E5Bg4MkHsfljioXuoqKNgZzGHAIC91DcgzMZxIPNVF35jR6djYeHDjiPDQxCKIGTxqiNH7QhBRZFUIu5y6mQhiXlYg15LF4k3kGgAC3yFJJAsck38cD8c3werh77wzLitKakLyARhU2tFZBCE2DG0TD52sNwsgkDqlJM2TacXOjZJy22OVVuKV2egu5bjiyGIK0xCzsv7TMxVW2nwR1TR6O/r9BqHay+s1KX7DMV8MHw9hsszEmSyFgZwHIJEADz/+IPRdfrtF03qelsNqbfT9Nc02pW2yrcM3EdbgUqxbynYSI+5xJBK2KoctIfcG/PIIbeRYse4WLuhfwR8dHorWVZTRc7CON3jI+eDV0Ap5s2K4JI+JFajcSoUhmLfBtiRRFgkghuPbRHIbjo2Me9tv8rtJ+BySVog2Ddhh8G/g316mDPsfQ4PuQDBZZxuAK8ZyK8Y/9uD5gSDyMRIJAgNn7rbSeQIryVThQpchV3KCBVcE3a/ayk/ya4q+iGQJtA/N/P8AIF3X5+P+346+MiOHULyoFkUCTuZqv4IJ+WB/P5+OlFCsGJ3XuVgSbIBpSAKAG2xfxx/njpaK9Yy7Szj2Oxt1Ng2QaIXkEC/uBBViPbySF5FjZRt9rC1+L/IYtViyP4NWf5vpRFAO5r2imBAHuG0HZQ/jctn/AAP9uvrICSVFfbSe4EFdw+dpBqg1g8hlF810UUMilNyv7QrA0wFKoJAPFHk1ybAJ4BArobajNYKs24GoyTIACxK2dvwb/NgAmv4MUHczg+0Uu++SQaJawb934oKPwx5PQhxZBKrFyFKklSACFJY7+CfcTfBIBA+eeiilAHZDIGG5uV2oCSrKyBNtiyqkWb4Pur3UqzruRdikNtSwp3Bno7wOPe2ywfg2t/kgfVQ+TfGKVvu5ClQAFAAogg17jYP5o119Eu2RUWh7mK3ftLEUQQK+zcLsWf8AF9FFEpGpAQn8BlAvhA3FEkm+Bd3x7fjnowKWBEnADAXXygDFT+DxRP5+b/FdDRoFl3KZBQoCgSaBVFVvtBZyWai3PH54McHbQYHYCXIBG7ctKrAMASp4sKbNj4+SkBBwDP7/AN1999XyibSoH3BmP+kfG4gFSf8ADc116pwSABSkj3e4mvyTt/P8CwP569RjeqswIKKwT+AXoFq/lVAAFDmv449bS/vW03+5lDITuPyWtvuP+OKr82AUta//ABVsANAngUQwK8H/AB819vz80eeiocdgoB3csziwb5pSCxIvlbFH8ngc0sytftBaxyKJHtCgWOQfz8g/k1dnpdFDAoSoIPytqUot7dm1UPN8A0a9vHIKKTWI8BlJW/8Acnk8EEgj8fbZ456WCLZAVhyB8hRzx8Ebj/mvx0QEkKk7Ver4H4HxZYsD8U3tHzwQeeiI4SRuWlFkGixPwPuF8H80fj56KKGSMBgCxIKkgEWpYgA+4cgivgAk/FcX0r4B9zEc2SAQQCaJPB4Br5uua/joiOP7aZbBNj+DY5+WomjzwCfwaHS4DA17qr3GqIHHIIA/x/PFfyLKKSiW0KkhiGBFsOVA3WCWogkcgG+fxyei1CgbaILLfABr44NHaaF1bVx8/HSfhWiwIpr9zBS3zyVKmgSOG3AEc/nouGPajhfdQu+DuP7e0Kf8qWsAX/45KK8o1ML4DE0aJs3yWH+n8iuAaH5vo8JRA93yeHUr9v3MpP4YcgXu/BFnpLxmxIK4KhkIHFkghuPz/H+xr89GqAw+TfDEMKaxZIFm+f4/2/xRRX1ApNWQeNtGv8UT8UeOP/nR6JVGUG7vjngAruA4Fg7uTR4BFVfXzbYQrwTX3fP2Gr4PI4Asc/B+T0SgcH3c0oG6/m6JNfzfH+ORwRyUV+2nYQR8fbdWDRIsjgngkn4PHWvj+p72zHrn0id4Z7IHn7U7o7D7kxNyX42PcmN2/lSkEH2jA1/KXaaEhkUEMqsOthrUSoZWawLAWvdTC2B+BRPBrjrVZ/Ve9ZdG7O9DML0mxsxJO6/VLV9NaTAjLtkYvaHbOp4euahqspQVCkmr4WjafiJKGObHNqCwRk4szx1tUy/Z38w83l5HeOPnNXNArnV2NsffzM8RyQO2ec5itAGn4gfHIZTtcljv5Z/bv2hlQD22FomqIv4brwYsjS8lcrFDl4iNo2gSHaSu1HdQqMvLI5vc4UyAgnqS6TilsaEoq0SDRO0ruKKQd3vDbKbkXQ3Xz1ZeL2RPqkLKsDsShAWy5BLc7in2hAVIUne5dwPs6xD3fCYgkwQBmcnB9gYFepafTePZW3tzsUBshiDBmeInsAJ9uamnol69ydo5uMcjVhhGFgZYc2N1MtvC0pDus8D47CMq8e0OEkNQhKHW03QP6guTqeNpuDo2HjZmpxRiNpNOK5secokRfHLGyRGKSV7HlxpEm9k2Q4tVDaTtR7BzcLNRXxpFDutjZe4qQFdQQdyKRxd7eQwtaGQXoroWbpmv6fk4iTtWUn6iOEi54FYNIsbIqgh1IHwIgA6uQXo8fqtvSXUe+9kXLiWht7RsVYAiIPMgn8K1vRdX1OwbWjNx1t27wVTgjwwwAQSpJBWAJJMQCZM10semH1T92944MGJhdp94PnQ6dHEk8+F+k0+STHCxxEZeXHhIkZ2pJIWbyrG5DndS9ZI6Dovc3dCYT94ZiY2lxtBkLomnu7xPIZbKahqKW8txrH5IkUb5jM6TsHHlxy9DtVwsnt7TjgQSR7YkWZRjoroVNh5nnZXG2NlCmEMCQ5QKfuzV7YCyCMCJTKz+RDMonclYwFliSSMDchLmJwm5mLLvJUg4+3c8VyVti2cMysxZjgRBkjgSIAiQOSSdZrU2GXkqgJmFiWIMyABMmOY9ZImrA0rTY8JIvBDHjxlYwyogSSQRKHKpC9pGrLKycMI1b4jtVkMvwyGhBO1FjsIqLuLKd6GSR12xxNTGEbiGNmrB5i0ckhdo5IShDABiWeYzssZIMpHjAUpLSkOxEaE0HvqVYUhLIqxLtRmBupHMZZEQSbE8hIY7go9rPtKktXViOO0+v6/KuI7MW3GIB8scRzkyZMzPHyFRzuHTZ9SgMAjSN5Nyh3ctI5IRlZo1Ye1G8khU+5xuCbrF1TN2rFlJk4OWiSCLakJkDBJ4JJCJ4NzyEiK2kClwHQvIhWnYdZPppftmaSICwoia9wtVS5A28EFn3M3OyiAtjjqIapoK4+UziIoMgbWVApRlEm+IFjxE0dHiTanyps0BytXp2DNcHmYOsGWEZy2GUgwOZHp3Bq1oeoFS1mQVKMzBgCpJAAAnESCYIJ471iZqml6v2w0bCJ9T05igRWlvJxxIbHilbdvUuRcWSHdHcgZEUZXHjov1Z+qr0B9CV0eT1f8AUnSfT8a+36bSm7hx9Uxo8mWMb541kh0+eGNorp2kkRNoLKdtkbAtQwMOaaNZQJMeNNpi/dSOSWSnbYpIKiNgwV+Pcos8MBjr6vegnYnq327q/avfHami94dq6xHjHK0jXdNg1HFkMRGRjZDwZUGTJHl48wOTjZGGgkgmRcjEYTFZDqOi/GfVOlC3Y1Q/8joEOLRb/wBVbTygrprzyxiNzW7hZT2YFQKzPXPgjpPXB9o0pXpfUr6z4ttGOjuXFE/z9OpG0sCR4iFWBjykEiqq9MfqQ9AvWaefC9KvWX017/1GCA5D6d2p3joWsarDipsJnm0vEzXz1hXyITK2MEUuCXAPV1x042hn9pZdxvaSVBayasEbSGBKkfaSLPXGt/UP+h6D6HfWbs7vf0Z1/UdC7N7w1Q6vpmn4uqy43cHYncmlrJNCul6mky6jPphKyvpGU2Q+bprJLpz5eUzY2ojLH6df66XeHacem9ofUR2HF33g6dFi6bP6idpZcem93ZahZYm1DVtEzyNA1zV/1E0B1HIhyu14chjNmGLInMol9m6Vq9N1jQ2tfomdrd0kC3cUWrqMuWV13MAywQYbupxMV4P1nR6noGv1HTepLbF+ytgrcsMblm4L6m4lxSQrG34auCSBFyATgg9P0QUCMlmYWN3BI9+080aW0VT/ACKoD5HRlWr2C1LuBBNW3AFtzxSnnllUlQQecF/Sz+ox9G/q7Hgp2365do6JqmfjxyL293vPJ2VrEGQyxIcKaLuBMPAmz42mjSNMDUNQxpgGONlZlytjZu4cuHqGLFm4GXj5uNlQLkQZWJKmRi5MDhvFNFNE7xzRMqt45YmaNgpAbaG6slXEyjLHMiY95gCP3nk1EcMoO5W9xgfmSfr+QpRVUxyIxAci91EgAmiRQI/NV8n/AD89BTSCEM5uyNhs2DVUPmwrk0QKo8tXTlEhSORiC3ABJsVZViCR8WWUi+aIr56aMiDegW3CmRPcxIA97DkmvzfyfzfTRmYzHMZj5xxT6JglDoRRJPIJPHBIurHtJugRYq655BZJhkmQEexlCK3K7SCStXYDBTsJFCgT/PRMd4+1SCxKxggAEi7oVuWjtCs/JomrB+SJBsViLZiSWUotkrewqWJ9iLd/j/P8KQRyCPnRIPBmhoM93yFjjVjwrAUAFUtdsTRYqaFiwSOLHHUnh90hjcqosiyDu5FlyRdUTZBo8E/FdMGPEsG2R9qk0lkMXA2hVFqCLPyo5DEfBN0+KA0KSRnhgDuNbmsVZpVPI+AQTQFE/ISlJmMAR6d/nXsyHyWqnYGCkAVdckhfljyVsKfs4/B6URyQSERATYDBnP45tQRX/wC1dImykaGzbRgr8CubNfPCiybsDkmq6IAZFUbALUGlcqBf42qQFPHIropKwYETM3KM23byGYr7gDfwf/mDR/PBC+1lLAABfaoXijQJ9xqzwL5HDAN8gdFFQfbY5/ngUPkWLC2PyxAocfnr74SQAi2vBQDa4X8gE1tO0cbvg1/noopBKKsEC0aPwFCgkNZ3cMTwxIskkk8jonax2qtbTtv3Bq/nhaKqOL+QPj89fljeirFgSeW27CACedo+Ofb/AJHN810QiMGIFfiztFkA/k1f5o1QH+/PRRX4R7AWIHAAPFtQJAZuBSjkA1yPm6vpUqQAFJ/HwWFGhXwVJv8AzY/xfXsoSbALe2rBII4r8fgj8c9fERiD5AGarL1RJBAHwdoq+fbf+eiikyH4Vhf/AE2RdNweWsggciqJqr56LgQul+2jtJLqVNrwbs2PirFXXH46+BADVhmAJIIolQ20C/i/yefgfANdLxghhVFeAUFMVA4O48VdbuBwDdfjoor3sLAq5Q2QBS2AFuh7ub5P4+Pnk8KxRsvJKmMUQSqk/nd+OQeP/wBjz+2j27rYtxuvkHkAi7o0ALvkD+eeiEoMoJApWIA4X/SDX8fjn89FFLxEMVJPtU2SBXFUP+9n8fHP+OiNrEcmh87tvHz7TZJFE1/56Ekkx8Ub5pYoY9rM7SPGoVB90lyMoAQ35NzIFraC7lEkw+9cfrl9DPR7SNZg0/uzQvUT1AwR+iwfT7srWdP1jVZdVmxpJo49cyMF8vF7ewMdDFLnZWpATpC5TH0/MyzBh5LHupaVrlw7UQruZh5RMQJOCT6Y+fcSWrb3blu2i72cwqjJIBG4kdgJyZ4Bq1PqG+onsT6bvT3UO+O9chMjOYy4navauPPGus94a6sJki0fTYWBaOMHxyZ+pSViaZiM+TkkIpZeTv1z9Vu+vXL1I171L73yf1Gt9wTocXCxzMMLQdHxZCdI0HSopy4jwNNxpV8buzZObJJJqWeHzcubJycg/VLvH1I9fO7s71N9VdQOZnOk0Wj6LipNFoPamlFxkQaJoeFKZDBjxMw/VzzvLm5+Wrz58zToNmO0+hvl6ssUUVKDtUqOHBmtydwK8KaCJucsN20gGsrqupjVOQHXwkcwFwG2kdwI5HvBn0rZ6DoraSxau3UbxneTuJwGAKhZztHvzE8VZ/aOmM+DiO6h5VSNHikCtYIRXr2MwbagO7d+TQHz1mV6VdspnTQY7Uv27kcqPKFUiVY7Lk1KV+/bbEqAnAFVenfaP6nHgUrIDEkbFQgcBQFWGUqgLOiJGY1Y7Syye8syknK3tLQ59DyMObbIsSukgmjZIlZPJ41Z0dmUxInlLH9gxl23T4yyTTS5bWXg0nxI7gxkdsesemBPrXpPSdKQtos2FAUiCFaIPM4mBOJ9iDUh7x9EMY4UWY2AuRG54lEIiewgldonjMsweNCHZLARSGdaNh09E/TTAj7ojcSxqqFZFx8pBDKsqkBXgdI5YJFR2SM+WWCM+aLySo3tfPHsftLF750HGhli3TSYn2rEjibcqERygK8kYjXa6OYUd1dXLyuzOWTM9G8vt7Wo5oceSJklYmYxTEFhIxZPbD5X8kdxyx+NlVWY77JYZi9rL2y7bDSCGTd7GFmDIEgTH0nFazT6fTi9bfyo+8MqxMiQYmQMCO34CsxfS/RotLwoFlEcaQqfcvAeNHF++OLxGSStxJZQQCB8c5edsZcUGKrwJ5DLuKCOFQzBn3bmaRVjVHtVQoGfdRRHJ2vib6exVDh+RZI3SOMsrCR3kLK6kooCedFtWG4gp7mr202S2jZSY+PBF5Xb7TuWJWtmK2rO6BUdtrKzbizqoULRLCHSAwACsRH3fNgiTunufbAgZpOoOX35Z53YDQpAAEbYOPefywLX03/ipQzFRHMyTyLGp3wFT5HYAohqEgKC0Yb5JUGgLC0nCxlRnhUCQkyPLIq7ndto3PuWzIEWlJ+FaqHVe6BKszHbvkjQEsqN9kZdQbI3EooskAALRJsA9WXDkFpFcIFQrvVAsSqNh27jMCqkEAN7mBCiyFF9X/Dn+ncPXiMjETn1/wBTWV1N4llthnEyIDYUjMEAQ3HJI7AQKezAQqeSRQpoRlUIHu++lSNo0jlX4dgTVmwehs7CSWOVGjkeQwimYqWjD7FhYgl1U26qWZaUndwxs+TqiCFI4kBZSIgfcEjhBO6RI1fcxcVHZT3lgFIUbumCfXoFlnV5JBHGVO+WmeRRO7JKVWOONwjK6rDDKZVAVo45SSogu2UAYx96PpHp+OPT3ml0/jEqIYFe7CJGCME8CM9hOQcio1lwgJJuWMBWaOfhkDeItdM8igFWK7gS18+1uR1TfqL39ofp92/qOsanm4ePFp+nvPLJNKgXZHGztHIhCqsTKrh2pUKMyKAQbfPUf1V0LtHAysvKyseMRRFiHnUCM7fG8so3ABFUAAk75De4EqQeWr+o5/UBn19tQ7I7Nz1THmfIxGkSR0XJZVszTSFYjBgxSLU219ksrJFGspdI1oaTpt/qOus9P0Cm5dvsFa4uLdpRBbe4DbQAImPvQCMzWgfVWOn6C91HqzXNNodMpcE+S7dYABRYBMs5Y8d0DntFYI/1M/qel9ffWSTLx8nIfQe1JMnTtIxDKy8t5Vj8sT+OMvJHPHkyOjMVnmlifayOx1VuyEtIypNH5KkDuDK9/cEYMYw0qCVNg37S7zCtnTpqWrTatqMmdLkyZbu8mySSWVZ53ctJPNIN6u0st7QOQYlSFeJaDbC8TzQwSO7LDkJFIkcfJlJUkx8yS7fGzEvQVnQoqhiSfo3ovTLXR+nafQWIKWlfczLDtcdwzMTubIIK85GYExXy38RdYvdf6vqOp3oB1JRLajhLFlCLaQIBJEMcAAzEzNDjLdY1RJlVo5AXeNeW2SeIMN6IskYDK1IfJKVDEsinqzOw/XP1g9OlgHYHqr372QkbzTwx9q93a32/BFPkmKXJSKHT8uCGOPMOMDO6IJZnYb5dsMAiqh44gMnZJX6h9jeTfX70fjYN7yFVRHbEjcSwIoWpTRSGRVhYbWRpSp3lvapre7EHcr7TQ2RjcApJG3q/n7Hg/OuGYUxLSO4YqJ54E/r+lblPSX+tj9YnYOlx6b3U3YPqvjuYY4s7vjQcjE1fEx2G1kXU+1NW7dhyfHi4zMZs3T8rMeadCz5cX7DZh6R/X3zX0vGbUfpv0uTVURVzTh+peVh6dLIrIm6KDL7NysiGRUEjtHJLkGAKgeWSyeubnGDgGREldofCVONJERFIsLDyTEmxtlEbRrTAoctGCE4y5JMMieVtsuOfI+0lUYxpGFAIIMZbyqqxRPT7XeSbaBuAEfhLJJAzHCgf5mn+NcAAViIGZ80nHrED2zXWp9OH9an0U9Xu6ouz/Vvs7I9DcrUcmKDSe6cnujG7p7MbKlcK0Wu6m+k9vah26ruR4subS87SoYxJPqeoaUkSrNumxMvH1PHxMzDmhy8PMggfHyMeVZ8fKx8pA0U8M6HZJFJHKGjdDT8MCUPP85OAymVtxldWD74VA85G92UI7I0rx1IVU+SJmDe5vbXWTXpD9YX1P+hLLielnrT3p25pUckc+PoEurf3jtfFmQNG7L2r3B/d+3ogXyZnZziQyP5IjJOTjowjuWA4AXDdhAzwI7VPa1rosXRvUCZRAsH3AOZ/2BzXevlQnKREDsrAXa17WC0DRB/5fBFAEbQSSeQ94rFMaHHU0pUOxINEJYLjcWIWyB/HyCAeeuXj0C/rneo2n6zpum/UR6a6B3JoGZk1l91dgb9A7i07GmyZpzKug5Uup6Tr0eMs2NiY2Mc/RJ5YoPPkZ+XlzvkHpG9LvVTsD1l7G0T1B9MO5dO7s7S12Ez4esaZIXjWYxLJkYOVFJWTp2rafu8epaZmQw5eDIPfEQbFS7Ze0RuEz3XIEgEAnEHtEcirlnU2787DBz5WwxHqB6fKasQqsZBZ41UHcxX8Xwpsi7Y8HkUpJXnokGSTkBeODyfkf/8AehdzuoaqDFQTXxto2eOSfzzxXz+CXv3BTEGjj2javiogc/Pyb/yTfUVWKw0ASgyrZBFqVAFc2GpgCRxXyOT8dfI1v2H4skCtwoACjZ/Bs/nn/e+jBHySQDS82Nt1xvA4F888dJBCJGAYjxsKJX/f7qVgw/mxt/2FWUV9eMhRRAZdoLNVAWeD88GxwARdGuK69Bdg4qyDXu+4gcizZH+bH5+LPSigsFIcMQSASLFliCDwAeTwCKHFcAHr0VC/Fm3HJAB/6TQHIFqQTwf54roopFSeeSDxY/APNc/kEC/j/b569qGU7WJdQBxQ+eDf/wA/+/8A7eKIY+1uNpJshQRu+eGB4H5oV/Nmvabg5slrK8ECwD7Qdt3VkWQKANn2gkFFer2jcoXfZoHbYJ+SQp3jcb/k38g8jpXd7D8F1vaARZo3wzEAE2eWZRzZIBvqCd++oPZfph25qHenf/cuk9odr6VH5tQ1vW86DCwYlLII0jaaQCfIlkeOKLGxxJPPJIkMETzOiNo8+pX+tNpuF/eO1Ppj7TfVc1ZcrFj9Te98eXG0eEKQoz+3O1IJodby1enODkdxtpUmPK0U+VpWRAhx5H20a4JAgDGZ59MA/nFQvft222sYYiQvJPHEYnI7/Ot6neXf/Y/pxomR3P6hd39udkdu4nsyNd7r1rT9C0uFyfbGcrUZ4IxMWNLGD5Hr2KykE6Y/qY/rV9hdjajqna/08dp4vqnlYqpBH6g65nZGn9lnOfFE7x6TpOOmNrfcuPiPNEkuS2ToOPl5EcsGK2fiomXPzuesXrx6uevOvRdx+sHfncffOooz/pf71nVpujw5MaR5GLo+i4yQaNosWRGgSWHSMDEidFAYOAD0zemPcmh9mdz6Tqvcvb2P3P2pj56y5mMEhXNxY5FepceLJEMP6mBXYQedlxMhoYsWaTEEq5uI8obNu7cVDeuhSyWxkO4GFG4xJMAcCYkgSQ1X8a5aR7n2W27qj3mgmwpYA3CF3TsB3x3iPWs9Na9UPrN+tXU5s31T9SO6NM9P9QnBi7F7dmye1e2JMKNpJDhpoOlyRx5+EI3lU53cP901OUJCqyMywSx5Wem/02ad2fpEEX9vigjggZqRFoyNtYkli1tJIXlZ5JHaRmYmWZn8j5I+jur+i2r9j6d3h2ZqWm6joEkEYiyYXiSbDykSCTIw9Rgkiil0rPxcg7MvHmhTIhcqWE0XiZmXub1hwdWml0bQcaSaYOMaLKCeOEudwhWKSUkkglbijDyRYpXctUD5J1PrvVdfqrmna1d0tuw7pesMAuwrht20kEjkkMwAjaSa966J8JdE6Zo7WsTUL1K7q7VtrWpB3NdZ1H8wKT5C0wFkcHconNSd5aXhQY/9rwYA8lo0jsrMGhYbo2oFjcimKiqPsjCiyCVEV7H9NDqXcDYqxM3gxVyMkKGJVpG3RpRIczMRtcKu+2QKGF7Moe1fSbX+4ziajkLEyy1sxBEzym5InqTdFKquokMRIIURoInCy+1csvTH0Gl0DKycvM0+YZepRbkaaI74YVQHHSSFnUkOJnMr+HJ9/uSEpGrJwtR1qzpbbKt0NdC7SqgkK0gGZAzgzHHfHOoT4fu6u7be5Z2W22EFogqoCiAD/bvkelN+lHpmFzDDkwsrgiKKTaSgMID3k7Y3dY2j4lkWYrGJbKpsmeG+tR9PSscEWJDTxIDNCqojHIjZGkeHICvCZGRW8KsYi0aw78p+SL27T9NcvTMuaVsY+GaVJWQqVeIws5MkYYSPBJEjtTgBJovGRKBvXqwtP7eU6pCs2HchkqbI2wqXITyLkYxidpYCq5DxSpJtinSZtkghhjjl47dRN4h2uSzwIJYDgEk4HpH+YNd23063bFq3bUbUXMD0yScHntnnGBmn76Y9DmgxV05xujhPkVZ/ESIjLJEqpXieSOMsCrb0Zpo5FSCSZGvPzVPSXEzsFMoxKxdE3lVWw0XMThnJ8m41uBVwUfhyVYdVj6TempxZ8XU8aLbhuMctGpeJGRNp3BZisSQDzld0YZ5UXwlnjjWU5+aNg4S6dOrJG0bQsUjVQNoKyMSoHmWlYUEWIKYyjlvfx19BpV1KtcuQC2FnIMwQ3ygjkT685z3WNUdNeU2CSyqrMAQCAQpzEgdoPHPfjApO1f7BLkokSRpBKRFIViDiNpNyrSx+RXUFlvbwxYKdwUHzka0mPOnhkLNs3E+RDu3FgPkLIXCFmdkLkErHONzSRi3vVLFxcMysuM7XvZ0eAS0WYSMBAoolX2HdGSGDDmK45Bg/rPd5OoZawykSOWx43EeSYkmjjSRovKIyysCULo6j3zpIGCyKXg1Hh6UsiEEAlTtj2mPUxmYH1qzobd/WqLlwxKqwEk8xzwAce+TmazM7Q770/HBebNhx5ExzG3nZOWjViXfy+ELExKISyNHtBANAEzPN9RsTcskuWsMFqFWlaUsVdS6B1hBkokiVJJBsAIUqFB1ZZHf4w8iOKdDmxZBkM2NIiZEKSxSPH523rCqlJ0XmnIaOUM1qlZFdv9xxZ0OPN5JDm5RdlWN4kGOhZtjNIVVkL3uldpmfYotZmnYrRPUXKFVDDa0CSAWA28xwTFdQ9Bstc8c22MwZwFXiRBMye5A4HMzWR2pd+5e2XyEhGiZhj+8JGgtpGWjK5CPFjpIyA+xJ1Vn8zI1F94+p2r+ORMHJbHqSRfKVcu0ZhZjBFtkheI7EeSXxxggKFWQkRq7w+DJmYxyJ3mDSMCwhEbROsbShI2lbbJI6RxBwshADSbnuyetfP1ffUV2t6KdsarkajqUGHlDFlMcInrwiNCgZlXyq6NNHAiPv3yyLHGkU80m01j9r1b27Nk3A2odbaqFJZmO2BALbZ9RA9T2qwo6foFe7dtWgllC7O5AUIIDlixn0gZJMwCaw6+vf6tMntrQM/SzrLojBY8iPGmT9Vm5TyS+LEjG/aHmZVk35CjxRvktOu3csnM13d3JqHeer6jqmpy+WOSaSeQu5lE4jDCDFhIMTjCXe0SuY0nyJo2llVVdleU/Ut67a36w965OTltLFjZU0n9uwGLMmHpcskvnyMjGDORn6qrIXaHc8OFH4kaRpJ2auv7ZHFgRRszLHKWWYOrAmJUR0ik8iKqNNLKpeVNgXxNEhsqG90+Dvhdeg6U3dQk6/VAuxMHwLZCbrSnvvcBiTkFSBgmvnP+Inxu/xJrF0uhJsdH0b3LGntqlxPtjWdobWOGVVCjxDbtAsS43naoGWyHZGkpgKF/1EKwBIpVJJlMcv3AUDSKWamZ0ZrJolRcdRKrRBoozlENIUEL7nZozOShkYOzrEIy4jYNHvtjtdCo40dpDCXeNzG0a+w2oN7yQE8zLuhHhoBpRId+2gf03saOJKpZFMsJ8gJ3vKqhrNyePyxm2cuhQqXYMrJta8vwgEbiBAHc9h6/sVHMqZIZo7kVDULyQuZpGcht5eTYhCKsciROGUsz0Wu2PSMcy7AqlQEVkjURIYiwRISpk8TTI4ZIp4S/ETtG3jILKGjNyjLkKyrJuZFll9ytssoHEiqK9rAxD2hwY96sBIFV7xIgEiZwsAdo/FkzRRrDIsm9EkO5bKgMzNas7KihgwVAEBkkdx698Tj9PnUp8qjcDLcA8gGAPpx/gxRNyzO8SkzyKtq4iLWxQvkRBPGtKItiRzfsxN4o52jKkxqZK+w7nYkqBueGZdwCmQYzsGeNDGV8MqLIZJI4zEZQJUZECDOLEm+NkVCrrQeSWERI4iEchUoVWUiESBmWthaMo3RayFJ5URlmSMOgR2keOXyAzsRCZlKGR3kVm93uVtxJO4rUdesbOkjSSMxRMTC4jjZVQb1ZGd1ncqFjZsZlYBpTEWRfCfJYLwMmOWdxDjybcdVlaKNmTdsZISSccyTOEhkkYjyMSVK+JgQRGMvL3PHBE0Ykmkpg3Ch1L7kAeRlk3yLvXcgR5ggZSpYGS4/wCn0vGlS2XKzISUdXMcCgqCV3RRsxMvugQN7Y7GX5Nyx7ikIPl+8pKh1mQM9mzMjvggT606zz4+ORDjGVZMbwnJCtvQeHyGYoreS5CY0SaRHWtzqASvk6zt+if63PUb6PO/zr+hFO4uxu4TDi98+n2fqORi6ZrOFBOV/W4dO2Po/c+lKZsjT9VaGaGSKPJw9SjzNOyZsY6+w7PGvn3HeUUKULqnnll3bl3tIsIVYTGis8ao6+xETx9P+DkAFY1IORJEqPIoe0WLYWEbsynyjw/MpC1K+wM8jhwwQQcg8znvM/OmoGRg4MOolSCQN2MHH3ccR6V/Qt9G/V3sL119O+3PU70512DW+0u5sRcvFyIC0eVhTqfFkaTqmK0kw07VtLyw+Jn4OQ7PHIUiGVmqFyZbYSJshFkASiABxusUCDZX+COuM7+ml9bGd9Lnqtgdq9x6tqE3oj3/AJMWm95adkagRpnbOrzrFg4/f+BjJDk48OXpniMWvfpIjlavoTvjNFk6hjaF+n7JsTIhycXHycbJx58XIgimxZosmLwzYzxqYZIXWTbLE8dFJVLK4/1Eg1zr1nw3hAdhyCexPIPOZkiJ8ufauxYvhkG8+f8AqzjnMZmB8h8qxOKj8BgF9zfFDihuFsaN/wCLNfP49RqfbW1vtJbcdx/gkbgOfmjx0r4rLkxkoSAu2z83Zbj3cDgGh83Vnr5HdHjab5H5/PBFCq/Bvn818dQVaLY3EbfYdu1fRE21RSbmLWBQbaSTuFAAUeKvn556TkjK2wIJNLVigQCRQB+40W5P4YgUOF7aq5DckkDlUAtiVYqdqgGRjY9qn/HWk767/wCq7pHo5q2oekv0+x6N3Z3/AADLwe4u9dRSXI7a7M1CCTKwJsPTsd4ZNP7p1vDyI8mLIWDJm0jBzIZMDOkzcyDN0vFVVLtsUSYn0AExkmBzSO6213OYGPc5jsM9/Stofq16/wDo16FaYdW9WfUXtnsqJ8fz4+JqmpQPrWoRl3Vf7T2/jNPrGql/DOq/osHIAaKQkkROvWmr15/raaNjrqXb/wBN/YEurZkM0+J/659R4XwtGMYpYs/Ru1dPyo87LjkcB8abWtR0kBQrZekzIJ8I8/HqB3/3v6n9xan3h6g90613T3Nq8smRl6vrWoy5eXLbAiGOWVyMfCWUM2Pp8UMeFhrJ4MSCHFhxYYK4nhyCFltiXDKGBJUqxFNa8FkAoGzYJsWL6tpptuwt5iJJUwQeMEmJ4JH4d6pNeuvuW26AEDaZDOvckkEiDMccj2zkt6uetPrL67Z//qL1W9QO4e9crDWPKxoc/KJ0nTZBGYWbTNCxVxdF0guxaGdtN07GlzY2Byg9ljS74QkaGFY41BYJuDlpJFVowJWV7Dr44Nq+PxBP3CAVC1FINS1DDdZFnyCS8bMwmkBCqoTaNvBBXhtwILHcwPI6nWm6jjamMUPJ48pmx3aXbF72DPCTlIhVp9sLM/kVWlRY1Ri8RWBLK7DhQAOYiBwBgce1UnVreDcNwmGLHkGBIB5AzH7io3NgyS02wqyrJKSojP7McjEMQFAuFdwfgt7ohuB+7zDk/pCGmCvFxBINoA8JU+MSOVVtroocq6tEwXdIhAWpbNhCJSxjEgXHdCyxsolVJFYFBGrLUibiQ23yMpV9rBgoGZiQZmPkSGMxyAI6qzp412wRv7truEQtDIy3d1IjbPIoCkBQSBBHB+opVdmIRp2zlpB4gjP0gmpt6e+qXdfpjqn937O1SfG07KjWDX+2p3D6frWPjNIHXw7RE0kKDJlxMiExZ2C8mO+my+UJjy7ZvpY747L9Yctf0WoQYup4uLGMnR8vIjXVNN3yvHlzxxhY48zCEwEEeXiqQ0SlZI8aV41TRgks+JJ47ZIw+wimU8qqqw3lHsOVZjYHtsggbTNe39Z1zQNX07ujtHXMzt3uXTposvCz8DJOPJ54USR3MsbKElk2bCxqHIxwyZMZBRlzfWeg2Oq6e4lr+RrXhLN1PIhBI3C+EG95J8pUFhnuc7L4d+KNZ8OarS3GDa3Qm6vi6W4ZKmUCtpiMhhy1q5tttAEwTXbj6RdpQYmNiR5WPFjzRwRQzMsMiKCgH7m+ZE3BgoRhIchHhCtOGlWWV8z9J7SGX5JFw2WSPZEVZCrQtEAyororMI4gFoRxki8QNuVmE3Ov9CP9WfseefQvTT6pGi7Q1nGjx9P071ShxZn0HVSrvjYz92aXg48+ToOpSESRrrGnwNoEjzNLPFoeNFGsvUH6V6z2r3X2rpvcfZ+u6J3Z2vreD+t0HuHtzU8HWtH1PTp4/Lh6hgarp0mZg5mNJAVF4mTJAux5sZp4B5X8V1/w11HQatrXULDWgXbbeUXHtsoaFc3GELuieSBivonpvxd0nrOlXUdM1a3nVR42ldrdq7ZukKWtG2CCYM8DIBiRUTXtJMeE/qEjMyeNo08nhtMdkSYxsA0Z8QXyFndHQRPGrhciJkj6dqRzZyZYxoowkoWSFlZWDxSRiJZkMoMEcjIfdHK0cpCSY2RHEwQ2d3NruJgQxBLkaNWavJ+6lG926L3ghGRVUt45LihZpEyleOvcHu/Dzc5YlaJ7idFYMpUyRiV3RFLo6eOaLYpMh+RC0cTAr1y9RYt2iqMQqfeRyOSDHYGMZzB9fftaPWX7qs4QofuspViO04gyD6xGfXnMH04jxdK0rGx5I8X9TJCQ9qqyJIiSPIVnggVBZYkpKJggiuw8ldWHN3LJgxbJizQhQTLDIJDHHtRSGYRgOjyqqxyR+yR5JECgbVTGXRO7J0EWOYwpLRNKtzrIipIsb3Gfdtjbc7tLEk0X5BSkEl1XX/LjLFjNOEaON5wT4iJmZIryNkZSFpC0nLviyyESLDLkWq492zrxbshFcwgUAwRhYE5AmYB7eua5Wp6U1/Us9wrtu+YgLkLKlFjsFBIAwRwRigPV/U8XL08TQpO0k0DBYyhIJjd3hf8AbWRJEldAxyEBaLH8jhaQEa4O4zHiajlTuWgiXx7ch5R55vGS6RskkkqBSw3kqiqiO+7fkySgZid3ZsuXhCbIyhPGt/pyYYhcm9ZHDyrGu540V2VRIGMcao7OjgnA/wBVNWjU5MSZDyl33xieaIyR45UoVjLSM8bly6/tNHG6h/JE6sydcvU3mvP4qk7REZJ8Sdu495IMgd8YMZru6HTW9MgthsjarAgyokHAjupiADIwarUZs3cetRvDK8cEU8suyOQAyQxSKgAZZ4glSsytIrxiMskUcgVVBzi7BhixMTGfYYgIo/0sb1tfHMSvBDC0jYyExwNJPxMz71kDFijEaytb9avTD0Nxl7i9Te89K7cxpPPFiYTyRzarqpw8cyzYemYUAlztTzFgUsYcWJ5cWJi6COIxibW39Sv9Xzv3X8fJ7Q9B8SXsnQcxGhXu/UMLGyO5sqJZGd8jR9OnmzcPFQSKTiPm/qncBxk4cLqzDr9H+G+r9Zug6TS3lsggXbt20VtKCPKw3bdxJMHbOBmIBrnfE3xj0DoVt7et1lkusKmksXC9+8y7fKnghvAO77z3NoZcA7lMb1/q2+tf04+nztfIbV+4MBu4sjDnbSdMjlD5M7LBIEaLGiP6qURNSNJ4yWf2sZVIbrkO+p36oe8PXHurP1nuDOyJMOHIkbTNHkm3JiuQrx5mq+NpYmyI/NJ4MXHa8UyzRtI7b5BSPdfqN3L3lqmodxdz6xqOuaxq5mfKztZ1LKzs+cZKeRUTNycp85YMSeSJcZGndUEVbTZC0nrmep3pAp5U7z8ySO9hZKrd+5ageSyPwPkn2P4d+FNL0X+dqGGp1oSN8KbVssADsBEgDtt7d6+c/in+IOu+IydDZsDpvTC4MWG8XUXShHhNeuXCAEXO5QTv3GR5RLj2wk+p65Pn5EjyuHbe8hU+efIDpwkYG7ajElVZFjXlQU9vVtarmSx40YV9ke7dGo3RqGQGWJCJAqhRwrTJ4yqSM0wZlEixHsXTPHhicuYXs+YylBsZl4LqwHtCtFsaO6bahIII6kOqyhy8KR7Xu28jRuuySSCMO4iBcvIZHlLEuqkiMhdu0a+2NqAHLSM8yIjn3Ik+uPp57cJLk7iwiCcgYOIUmAOTAgD6UPiFHqV0WTaVlVIY2UK6mOoQq73RnSRHZiQpiIK1Iy72LUssmCTdKyrIrxq0gRnebxssquAWHsMh2MDyqRFixDdOSJEmMf3GEzKoQHZ8HewmjUgSbEERLyE7Syhfb+IVquUPK4SUMEVvHCshkIO6Q08jmQkhWcAmTbXCKoBHQxgEj2/UU1eR86Y5JmyNRSEAFJJkeQRs+4qzEqsW4GmDtSnmiF4IsNZmZH+nw48cjZIyws8RRzG8SxtIZGZySts8bh2jDFJlbgRSFq77Xxzma2paJZhCFYI3IJVCQgqgGcDarcAMVNE8dTzXJo1/bVQVhMqt5BHQVpURV3yWpjVh42UAgPGoLKxCFE4J7zz3OBye/pntipLzyFUKDG0E8R9317gcf5JpFUWJkijjaSS7cRszFpBY2BGm3KgjdlDJslEUVFT5F6HzMk44JkEHkEO77ChQSbSCXhlUMxp/cftZJBVSIEQWSPJ5KxRGMFmASNiroC8REIeV1UxxyjcwRE2qu+TdxGNaypGkSAJEJZCkYMYVpGjBbbxtCqTGAUQKNzFhd2xcSByaZbQM4loEQRnORnj6T6VK+0McZOdkavlrMcbEWV1lRAqe1ASw2kBHLFGUiPxmQIJldKQv8rifNfUMsRrH+ycbGj2CDHxk8sUCKEdhGREpbyY7W5kBLUFXo/S8WfS9BxsfHco8zIMhQEjLNHFulgkdxG7o88gDtIyIpRU3BgQWDMy8eeczDe2IkVwiT79sJVmT2LvVlG5f2txLEgPRJK0OSzTJIChR6ADsB7Z/tyaOGQJpkEkkkdyHwwlkVnjaaOFWfxptYOrRRh9u7b7pPfZEixIpCjMXohQHkETkRhA53O7p402SSGOM1J5JBGvhcbnEV0uHIzTNkG4cfzyJCV3xytsCIpMbhELyqiwNyGO+S/JIPdLykocrIhjmWlkkj8SnnwuquEqMAIkQZih2lwH949xUbGHB/p2x7bsdvXBzH1zUo07JCLJtmjMsau0ZKB1aoRGoimmR/E37VkiaEKPG8b7wGTpI+hr+qP2N6e/T12z6d+tSapndx9g5eV2poOdgPI7T9labiae3b0Gd+nlEC5ekLPlaEiRNIowNKwtzmQuq81mm5Eas0L7TI5ZvOspdkgZysaMqyqkKsY3M6lVbYo3BRtBf8NJhG7R5TwiWRpGCyMqM7KoZk3YuQSgI2Kxktwm4gEklCAw2nIBmDxMRP4Utq8LV0sVFy2yjB4DSuQPkD6jNdy5YjnY1D7iKO2h/qbiyOfgDkfx8+dl7mVT7ubFEtuIUALW47iVFANV7mAQO6lODxY9thLr7XNkHk8KNps/BHFjjrDj65/qb0/6Uvp57v9RP1OM3d2fC3bXpxpkynI/ufeGr42QuBMcVXV5sTRcWLM1/OjLxI2LpM0AmhneFxyVG4gepitAWCgtyBmBmfbvWs7+rD/UMzvTwz/Tb6G9xjT+78vCKeq3eWkODn9r4OqY6vg9p9v6imSTp/cGpabMM7WtUx4Ys7SdNy9NxMCaPNys/I0rmoxpp55DkhTKUcSzHyVIvmLsZCSN7Bi3vB9jOQxdfanXvW9W1/uvWNZ17uHU9Q1zXdY1HN1PXNY1PPyM/O1LUs/MfMzdQzdRmJmysnJzXad5pKeWU7ygPCtuFktizxo24iR18g4XYADHVMhck0rEbuVtaBPPQRBbCqCCWgkjkTGJB9uMc9pgcy6Guy8xBwh9Mdv8AvjipK7RtJaGNW/c2ndsjqMmZWI2OqFQ7EsTRdQCQvS0+L5FhMaxyLMqIltGQJlk/5PEiq+1pBjsoMMQjeaSnaJCTDBC6pkRmJtwV/BM423YUMQrKWVLLBCCh/O2gCRJCI2BLsibQFkdtpSaijhKaRCsMcrGqCqwdgw3UZ6qA7cL5TiYkE+k5qI5OmsQXiZpFIUKyirsMwJLE2HCFghNKCGBZASGbHmm03KiZZJlQOPIYyA4bcCJFJYU6BYyvNAqVPHViCOQxRM4LMSB4mBjdRM/jSONY4SY3iQCQLSyBZZFVQxYBu1rTTJ55kgk8sfLOy+5ypMTOqFVAjceOlIBExYxqwF9MIiSoyTnvj5fOpEuEFUaCtxtrEzIBByW7D8p9CZp7wdUx9TwWLqgQYkqTCFKCvGhJdQdzLJTBRtViUjJYLfJWXC18LG+NMIpI3U7D5Vi8rwohEg3SqV9rrxQB2gi6xwcmfT8gJuKxO0sbqQLYOPGd3IooNwphYBc8XRsjGyYpsbzqzSRkR74iRIX2L4yN9pJalNoLM8e1WViooMoIIgkT3HBx7c/P/FNNo2+GBXlTIlhIyDOYMzHyNRrVsMswkaFCFiCbgxCgqVO5tsShpKLbwNo2pYBN004MjxTABSfI6bQUem2FEaMbb5IWjRFrKSSpBAsPJxADbBShjKF4VKyGWTHlCR+GbxOrqPGJpImbGaZCS7QTq7RXJw1eRY0WNWXYzhjKRbLGiF2ayELyKbJ3E/uTUWYgCqO05HPBiMEdwYz/AGxTrbb32XD5IABJI/Azz/enHMXFmhRpxPLM0srRSROsc0HlZmCb98m4qAA0LUoAVgxPBvf0E+rD6kvpe1iLWfRD1X7i7dxROZZ+3f1UGp9qaxIka7k1rszVv1vbudI8Ylxxky6edQjhkjlx89MtFlWhYsgSokTJtKp5GLq4myHZgN6M7xt4/wBOYWVeW3rsBLGum6mSTZtNK+9m/cuMA7IxJIZTtQ+NXayPu23Z29RXrNq6htvZt3LbhgyOpcGSD3JIjMd+/NS6W/qtO4u2b1+y1pyLZRim1cEDygSD/wDIkn1rf76af12dT1PHx9I+oD0qiGUiNHP3T6YynFYnbGE8naPcWcMZnfbNPkZsfdcaNO4SDToYmZOs1Owv6sv0UzQplax313d2/krG+R4NZ7C1+XJkKMyCIDtuPXsB4njjLuVy5FgaRFxVXx5wPJ7+3PGFkihkcBopFVWDhXZRarEHK+QO6xASN4wu1gLHQOTp0EBtMdJghZgCbXxxom4c3J5FRGXcEIRQIm9qhRj9Z8BdD1txr/hXNMzHC6e4EAOOUuSAp9u/JiY3/Tf4o/FegsrY8bTalbbhVGptLeJQAQd41GnaZxuYsO22ZI7Z+1/6xf0DwQzK/q1rcczI6L+o9NfULdLMHx0icLjdtFiZFlZwzSVGYVIFsUWX63/Wg+gPT8NAvqX3L3VlZGNJuw9B9NO+BkYw8EmQqg61oujKZHaBYUjkyICZJ3Z5jGY64esWDEkLDZGZY0SU0CWURtNagig5AcEMG2sI1P8As9R4eDGv7WPjq7/tq5VHsRrJJGQJiQqFlCMAQjDYark1rf8ADvo1oQbutfABUvZIP3ZAAEAGPoPrVy5/Fj4kbcWsdLEsG8mnvq4MzBc6y+pA7gIQx/qUV0yet/8AXY9KBFNh+kPpH3f3HlmSYzZvfWq6V2li4/tLpJjY2jTd45uY9swkjeXSXCAIZHUoy6mfV7+pf9Rnqmc5cXUdG7B07NdAIu1dOdc9Y55SI8eTWtbm1HLdUWRSk2lpp00TK0kUce6VZsBXHkjPicOghpD40iDGT9QHEailjjgIR0pCsmwoWDPEHEONGX3TOZHiyVckSghVlQ+K4yzgmo943RqZGVSwGK4duzoPg74f6eQ9vp6XbmG3aphfYNyCIOwEegBHAMkGs/1T+IHxR1RWtv1J9NaIK7dGvgeUgAjcRvPHfv7U7653Zr3cmoZWp67q2ratqOSsGa2o6rl5eo6hkCUO7BszUZpspolaDDKFZpgibvHHDukjLVEixe+cJI8OQzvVSNGVWH9tEJaOSUJk+YrIIi8Tgkg7wgTSpEKZQ26KKGi6InEu9lBWjIfIYQgkRQxqxSsU9q5eLzSF3LJUUE7PINyybRCu77CsJjsz7dqQtEv3xq2kW3bt7diKiqoUKo2oFXIG0EDHaflxisdcvX7rtdu37ty6wO65ccu5nklmnJ7mk9UmaLy+8gwSywwKYwSYkMrKilghTY0YUKFK0D7uBdcSh8zUV8fD+cOSVK2RIhReQFLBitryAAasgAyjVsqTiRi08zIlu0Z9khSVjIyLYZt8oaQGvKCzMSb6ae2YGzdWUL7AlOx2R0A/jAUBlIZgHDKpO3cil/cB01hLrGd0zEHjbx+P5zUtph4EDaYC5wSMDBOSP36CLv0aGHHwAsgNLDErI6MgbaAAWNxhWYDc7kPsZ1HuskMcmQzZW3yOWiVTEgMZUzEpHPLErRMxjUsZUmIZiVNop5Emzt2JhR+OBU2xhpf3A5LNGjxck+1mIRX+FBZSaABWDpOWmkeEyOsjy7RSAhVAVQ5IMTsFijJHuKtIzsAXFy1WozNyYxHu8rTI00reSSMwlnlpVaWZ0KOoUljG9vISAG2rGvVbarkbyF3qGLM1L7DRBsiNIwwpgxLK4LCiwBBAmOpSxiJCv7exI5WMJZYWMlqIXViEYhRvhDAJv8gIUqpMF1j2ObG0KAthw4dSA26wTdlipAJUUK+emt90/T9RU1tVLICZJInPrzA/H1qYenEEbZTZcw/bjeiSV2FFiMjR2/HLshJcNVXtcCiVq+bBk57OqM2Os0cioshkjjj8sjskju9sGZk+wMvt3TCWVRMHL02cQ6NqWQY2lSPGnm8dbEVSsi0ZEBl3S/tCPgq7opslAREZXWTOF3HGZQYy0jBwrNsWjRG+mBkVQUL+yXaSehRAz3OPwGPnUbZuXAMwxAjMCBA45HenednEaybZNwQrJJGWcFlho8KqI1EHlmKE1ZosRFcJlzNcxwF3LHOspdrCXG6sqMPt2KVZpCG9m4EjbTB7ypBHHMrh1/bSPfscMFkIUIJCitskQ1IG22L3ivkLtfF82rxvsmKhrC8gOoL3TkOaCl22hGBKglgfb0pAJAPPYd/3inpCW7t0821kL/VMjMdxNWbr+UmHDBCJFiGPGqiWMmN8fKyBFOrPtbeyAyQxiVZXVVVy0blyBE8SJ9Xn8kjFMZdksctK0bSb+RGKoswt2KERQgGUhUQlRdSlm1HUsiGOUyxzTPLCCFCxKHTcGCqkZtY43Q7QxG1SDsAEpxwmHj4yrGDRWZdsce5GBQtIgeL3r+5tKsSqKxAPtNLUCElAW+9JntjEY7d6OjlWNI8WJFYgLMqiR+WLK7LOV2BWBIjaZnd96sip4ypJGOzlZpC7M43NI8soQEs26YkoAdxkA5DcAgtIvB6bElEMRlyS6Sbt7g2qywyKvvTYYipiTmJUPhyEFsWiPLpoqtmztkSAmCKNVm9odlBWMbaTaHlkGxgdxEfO2Sq2lBKnBI/GpLhrEwEakyKRIoeN2kmVXWMqWe5BIpe7fysVUHj3cu7riEhZhkF41EfEeNLQX4BabLhdSLops2p8KzDnpqxd0Uc0bOksIeNbSWJSkrNFs2FKeNQysu1RHuYy289ErI4nxzGpklMTFQdjOIGAItbUzxlyVIJkMcZf/o4slJsX5/X9/sn2jundFUUWdiAy8haP+DQAHF0x5/8APXKb/XI9Ysjuf197H9GtO1CLJ0T0s7Hj17VcTHMr/p+7u9MuSWfGykllkx5crC7c07t+bGaLGV8eLVshGyJ/MYMLq1fkrfwxYn7a4uvcGYLf+RzzX+eH7+p3rY7h+u/6g8rHlVo8DuXRNEAWVpE82gdn9t6XmCMbisfhy8aSGVa92R5SABwOZZWbgzwrP9VIEfnXcdotEYyY+hOfwn/PrWDGmhZfIJWKxJvHHLKBENihTTgWo3BC5XaoIAJ6W1LDeGTcyspOxyx9r7Su0Fa2kcIdpIuwb556Axsg4+UsiEl1feF9nL8upoqw3EnglWA+a4vqxoIE1PT0iCmRowSGXcS6iR1uNnUtKApSQGh+0pQU24m8ig57g/pBqheYoAwJEc+nI5/GoxouqooWPJXyhTaqd6CUMiyMpMRVlBWKWJAf20kmUkAgdTFVScmVXI2or+GQyMSTIrSeEA0WnG53I2rsR13BbU17n6fJC0jKsgjSRKIQrQBbxMQKIJb8WBQqqJHUg0XWDaYuSxWb2jGyFZXkYvykTxKyMQoViF3ABQVWibEtQ3UkC7ZdbquinHY+pjMZMDk/SpI6yeOOeV5G9jsCfCf3ptoTyvtNCJlm8vlAQ+aPxSSLHKrFviCSJIIg7yO6B1XaWktY3VI4mb3O5aLaQhXcQkADEHr5KoMIn3o6XucBl5U0GlEZZi8bgLtFnZtBFNZ6WhSVGCzOYp8ggSSFvE8aohljnRQC3jDPHIoQB96WpA46KiEwJ5gT86iOraVY3xo/sZxSoyhIkdlsSNSMyUFAs7iWXkrw0aflPiM8MpKqzuB/zHEYdJFIHAYEs1Sqh2MStrfxOpkR8eQOC3gaVC0LssbLFFtEwdl2yhSvvRVDWS12T1BdVxSjjIKLUrqTIpJrllU7hXJ2Ne2uFBonnphABLZx2+eKmQ7v5ZzIgHuIyAPmQAcHBNTtWWWGFDInjeUyhZVJQtHCXlmYI7bEZaVFUoVG1doFqEZo0DyyRlFMUHikijRonjCFGZQfInuZ4drigSEIkDAN1GtGzpfGFM6lkAhVSAS6s8gUBQN0wJBV2Z7UlBfwROXia7R7AUpJGYiWypm3K6sSSUeQqY9z2A5jZCoHSqZGfl+QqMja0clTDcRI5iCcVDFaGVV2M8ZYItsoWUq7yiJUYxg0I90dTOzWrM7GPb0V+mk3zMdwY+FtkZ3sVlI8kg3s4UqUUJGaZS7kgUK96rpiqZJMdpXnVWdCABGUkORMyipmLLTmXy8UXEYQfAbdLyw0jRTFWknYFiyx7fIFiVkkcoSCs7Q3XBDPXxYdT22vaYss5IEEjjae3v8A4pzwoBiIs7pvlBpkKh/ZNvkaSMrbe2FjaS2UCPG48u09OmPBJLjFpHSJ5TIWamMgiSNHVmFkfuiUspoBishUn3UmmO2VkxRhpIkjVCSkO+WRnkjnZFcCIqXk8kbStC22AEGwWJNM0iTSTv8AtIq/eLUMssRhiiXbEyOCxDQBgjIJWeisjJ0VGOQY49ee2D+FRd4ZMbKTKDeQNI8WwIdsgtlbbHw25nfdaFVG0/5BfMLLjmISU7KAR2dgF8nIBUja52RoGCFmV0Y3uC9e9oZZUlSQxRlXkYvNC8vmd4IECgRhzG8QZlkYkROylhQPQxxiokREMTiNcoPuKyFWloOm9VYfGSsbLahCiEnZXRTy5IiKPEiw46uJACMeLYIGZ2Vy7ftskLsiFVUylJVAYqAq0XPQ+TkTZWTMdsIKJixo6wJBEUxIv0ZH7aRQPNJjqzCYRFpskz5Ukkk+UZJAFklWNgpKElpo0LF2cKm2Qkt5N0Q3AsSgWN7JkRWFradMMxJXJV3hdVaPyuSrKo3eJGuKYRKPO4U2HC7TtBBByO1MpeWLwTKjAO0TzFIwx8gLOSm+ZZPG4Vg0cqmPenuBAMdqNkZUcQkQIribdK0wQMNyrG8qiMDdTLxSlRalUrcxK0/kcAK7bfGwdg6rGySeSUuyiRVBZ8h7V0Ie6Nt7umrKB8ZYK5lU+RBw0ciSMAXZbcJUbGOMCgyBQQCAemqSd09mIHuIH+aKjWpyvLvZQCTtAs0NgUBSu5nYHaB7SeeTtN309dhQucqWUiMFG2btrO17w1igQVJUA2Q1cJyCDGdTcvI5jYFQCq2AG2qgC7gtAk1zxZPI6sf07gQY60HCTON7uwiCtvZkYKjeRlUBDvDA25quo7f/ACKewmfxFTyEsgqomVB95j9Kl+vZFkYwAkkYqF2eNZDK0eRGVDLI5YhkgRinvdghDbitxWBmMe6VAgihdAqhIygeVlsOJKcCOJo40kCNH5JWJZFBJncuR/xMhJkkkubxo7htlxmMlJC8hCiEI8kAKeGVEJQPJ7R8RmSBJpHNIySkyMFBZmdoJCjq6Mje4xIWQEWCbK9THD7RkRM/hH6/WoQYTZE+adx+98vSPxPuabcqISqG8QEfmjt3Uqdw5o7CioI1G8UGXmRtzJIgEI1xKdY1sBFqySQw4oj2haB//UwsWftAs+OMSCGNBQkQRhiGYkLBIf1DW8LyoNpjZVkZCXePYzKT1X/ckDRzsOW2gClY17VFMRVoWHz8kAKCaUU1/un6fqKfa/5U/wDsKnfp+hm0TU445GMiwTe0MgUxxkTzyHyPHTpjx71ZZEEUXkZySyBoI08hzZ/IDbSnYpt0jbcWMaj43GVQ5HyaJFjcepr2AY/7TqicxyjFyAEUFz9ySiTaHoyKyKixtjzKUBcgFNywHUHGNqpYfuKQKXchsC0XkGUl04XcAT5aIIHPTThLfzJj1+7n98TSQzXL4XDEsF+exAD+OfaidQzHdPAz09qrL42ZnKjYvvI33t+QDfwRRAPUi7dxBjQZ2bwzwYhazIqUZYtsboWO0N5GYbWsknkdQeJmlm9xBdH3birhFO6/hgz01UaUhjyVIoCxCTiaQ0ch8TZUmOWkQl2cRoWKxhHCAbY09pj+S3NEU5DJ3RkQI7EYP7zgfjT7i7bdlcbiwW5H9XHM9u309ppHSMZoldnNbNxCBtvCPJwWZVUTIkW0FiUohz7msvLzTNLwEJgm3qzq4l8a7f294pPC7xxvS+4uxBJVlHTbijIkjMMJaAeNJbYgsocP4CWVjvQhzcZAdFZF3muPGbqsjSx6bgvvymZEnkDykQIJBHvkLNIFZYwqiMBAGCDgAguqGi5Xl1LJGFEI2EfgXJcbL/4cDdTOQkaOFIMjkLuDNRVWU2LhxR4Onph45LXEZElvYXZY/KwuRo40jI3swBcwsrI43I46jGl4EOlYweSYNPMpbIlJDKZN7KyPRdTsYinJQBjtX3qtOcU8WRIGph+lPDb5FEsjM0k2+eWJkjCKrolAXEzU4O6yo9g9T+4/f1H1kMOOqpCE2y7BOJGmtFljkRAskTqXQBnUuisiGhuAKuNrsj5QUeDKhRGtilbSrMxNMFO3cQQ1j5BB/PTNgM8jSY7OniRhtVFbcJYgnkAf9tZEkVJdqBpSrqrSsRIq9PuKcyCLYrbQWYlSsibSDsoRqP2wQgbYSSCSQQpABTwIEen/AHXdXkSeGOWWR1AjiZmDl0QkD2mRlDPtAKu7iNrjDJQJDD+fZ6793J3764er3ekOZNqmN3V6m999xYudOZDLl4usd0anlYs5aUeULJjyxSKjqGUOVYClCd7fqvr8fZ3pn6j92SyY8Sdr9hd4dwO86n9Ps0Xt/UdSuR1PnSJP0w3tAC4jDBACQV/npSFmkWZ3Z3kAaQmUMzs53sWc0XJJ3F6Fn3bRzXOsct8q7N0QEX2ZpHsQI/MZ9BxQbxBw/tUlXZdm5w6jYX8pASigPt4kHJ5sGjKdC1mfH8e0RKYlk86kq27ezHc0bjYu1GU7gRYJFLe7pjwoVyJ5efdTkgCvasR+9rJCoaYlbLAn4oDpXJiOG6CMAkqJeOAWG1q3x72I4Xarke0AmgQBaBgg+hmqjC252NAYiVY9vb6wfr+diarGM7BxslQzQtuXa8UmyBozIhMcwLxNG1mq8Q9/BISzXeTjSRkSKvj9+87HB8bAEo4JAYsygiqAqzRoHqy+3XjzMJsOeVFjMawiR1MiRsqK4dvfE7jy728ccyyPGJGolOWPUcQIkvk2ebdbFowAEH7SEc/6RtDsN4Ej0C1lupWXdGYj/VVrbMl1VmJnxF7xgr68yfSlO3dXGUhSeQtlJEkUHudWSMCXdSxKyMW5kAbYDJTsC3T3PC0TPLAjNHLGWdmjDskTRojJIIlT2MzBVZ2UtIfcBwTWEWTPpeZFkQM6kOsRpioc0VNEAgEhmBqgu40w5Is/DzRlYsMkQUBhviYTLVqxc1b7WdXjC/PuAjWy9qEVpxHA/SKLqhXMEMpzI7Ewdp+hOZ5HHp6qQo8LbUjdHDLyWAyHl8QqOW0XdKhZUcCIBAVJcsGfVcCZYyrxsREqKFc7RH5lTxopMYLhlcSsRuKmQbj7uJK6KJYUIClisaySSoAkbFjNJLF4XkUtK8BIPKnwst1YTMMH6gMsVJsDMFeWSNJoQi7pFMzqI5QX3EXGJI0VMeLa/TiJBHrTAYIPpVTwmXTMkSjcYxZYcFANpDA7kcbaHFKD/wBrBn2n50WRGAjFw4iNMokImJUyOkUSkuFLPKPMpoghDwjhmysFhNJDtEiTKGXhU8UxvdGR4iWVqLMD7VPAu+o//wAVpmRHJEAGVxvUHcK3A2b2WSqEDZt9u9VNnlgO07ecjPzipSqNb3YVwSSZkkDbAjB7R9atAq8+6JY3cGDHj8pLrLGEMSxKybiUptyKQ9qqT+1RKdkT1vTfAP1sTASB0hoWWcMqFJXILEln3OUJOzcqlmLhupVpmXi6hHjCJYwRG0kjbmSYE3k/uP5GLGNXEAsBXG1KLOzBfJxF8f6UoPEAPK6NuP7hx08yN9zGJUDf82NlbkBttrJUYaNuPuvv+eAI/L8+O9Rvt/KlEc8aSENKyESR2WZSXknEjEqFCKxAL8o6CVQWAXp3dozJvVdzco+2Ui1sgSIBTSnHWLJyJCCyKU8ZJ+8s+HCcfNiR1CI7PADRbyS+LHdJXDhljE4Mbqz7SrGVTtKSESmJjPBUr749+SkaRpIDtaQGQmRAhZH3fugEn3S7d6OykptNsQUKqOqhFjMhZUV7UGN1EbhEkdkjCLvmkfcWp02KE6IlxJHkabZuYKQ5TaHhSNitOvO1FlLx+WJVkkosWf7FVDCPcoLbB41SR0pZxIx3qUYEuKAsFmQn2EhgyAiUzziMyoCNlcUrNL+o5Z1uAttlaVUZgUG7coIZiSios4XHlZKjLbEjE00jWvhQxS3YcRMwQPGNhAUlVK7r68wSwxyP45AEYwu4ij8bnaj2Y1SgrMAnvQh2KI7bm3lnDP240s8kipIhdo2k42OxKLtRkQLkodqMGU2aOxQGc9RucbXGydkZXiBQ1GjEf9JYKoL1RlqkQyLyzAkopzJIZ5oiVaS1EWRuBNEb08myZQ83kSU+TaSRujY1tIM7B4o0UyM4l9p4AdniZYowzMxKvJUh+wIxHzV9foZ1njSxGJ1kcq29Ek8tlVdBtkEylWsowAhMW1WG4EfJwuN5RS7Vc/tsiMdpcAB3LvQPjO1dqqAxFhSQSgCSB6mKhWoE8Engs5Vv5UqeSPwQbBAuj+Td9XR2BE0enKWWWJAjqzFtxf8A/l5omaIGJ0A3+5FDsRHccLEkLSeazGQAVywQKUUgjkAr8g38sVsfHzXV99pRyQ6X44moGGPyyBI6O3dIGYlxGplVBH7vldqAgihHb7/T+9T3RttqnckGe3liRHuOPQ5io5mZDS57zIS37jy0jbkCqqsLdrbxvbRgLYeL3kXI1EQxADahDB2HtNl22PAKWFaRd8cZJbm/NuEajgoSFv1rsxZ3klLK7KWcD71ZVV2LBpMd4/axDIWJLobY2B49xWMGR2VJAoWmJWBWVlLmUMX20wVkkUUG20D1JUM+Xb7zP7/69uIXx2jVIVVGEnjIA8b3YSIhVSJka4leYlmEiEAFUjcBxEu6dPZYVnCuyyMz2zE7gf8AW4O6RrtgPIRSjgk8idxxjyrIEjVRZRSYkjkMnldm2ygFHCBELJsYweIMQzEkDuGESaZTGJfFEyhBalSB5YwXaPglG3DbuCqdjN5CFUoVtrI0TDp+bAUwenMbyvkYr28csc8RLN7Y3eIru3EOF3yN5a2qJNoHkXbbV9rLFMokFiyGmBUFVZLQgPuO6mHBIo1+DXU27FdoNQdxtWpFVTtkqg4IYLuQMiKrEo7G1awasdRfvbGODruoYhCrty5JA9OKWW5EUbnYhNjhgaJaiTX5Zc+6v/2Py/p5/t25nFWMfagSJBDSMeg4/Y/ChNGjd8iGgx3yDyEsQrBmtlFkE0lnllBPFrZIm+uZPkODgpJIcfGBeYosaxwjJdRF4nDLKT4kkWnL+E2QzLSCP9tYcjSGfaRGjqymn2lTEQSamS1JPFL+SCeAB+1zUVydXzV0zzyI3gh/c3OQYseFJzaGRhH5/N42W2KNyq2R0Jwfn/YVE4d7jooLbVLKCcAAxBY4Ht8vlTnnakECYeEHbIYxRozBhsFrGEBjYMB7wxoEM9hzRFPGh6SmE36nJqTKlDGeSRjtSmS40VZlkL1uBplMbMDRCkFj0rGTFSTJk2PkAXPJRKgyPHtRXtq9jq4ce1KbeARXT9BlNnBYcYmOFF2MpCoVJ2r43lomR5A5UygCA2pkdEJIfTZFsC3bwRlnIlm4ic9jkQY9hT5HO2VM0RPkBJKhmdaClgQRJstkZgSxclirMGKAXIcdTAwx0DBSHHGx0dKVAheJvICA9gqVZtgJcVw040EOPUJRlYE+RHG1l8TK6q72oC7YwqRKkbBbHkYXE50JMjqWktaSICQCcRxRxyDyiORW2kWTYYHdIGKsAOiowpBLEyTM/M5P50+rNHGsal33kLKQIkJR2VEjaWRXEm1vGxZHZmUAcybiQ94JmliZomiceQh2fzTMz7EPMglYNtQovz/pugCB1FMydEjTGeQt4mSR1UMSpO1QxkR2H5VjsVY1ICSj3LUr0vGkOFFJUv7u5/2VVk4YoeYdyWChBBO4AANRFdFIUkkz6f2rrz/qE9zr2l9Gn1D6t+tx8NZ/TTXNBSaYZLbsnucwdt40EEuIzMmTl5GprhYiyQSY82Xk48c0kMRYvwqT+1wCpBAIIKgkEAkBtu5BVfN7SRwSCL7Lv6xnc+X239D3e2Bj5cuLL3n3b2B2wfCsIkngj7jxe5ciBWLLJGJIO3HaRoAWdVaBiEnZW4zpZg+8mNiCiCJmaS4gGdzQL8lgQjMwlagGLwlvEaNgDw7hjO9RPePJifTJx712dRJZBONrf/6U/n3/ANRReiI75BJVZADu2KoYSKSGNiil1W7h6HtIDHb069xqIjjyRh4zW50sqEkX8KoSyDC4IobbG0EsK69dpReVp7ZA42PuNUty7dqCjv3rQKKCVCqzUOSt3QLkZQwVRE67mC29E2aTkLXtU0N1FRZA6smPCJgTLZ78L3+tUD/+zc9ioHsIGB6fSjcCRMU4aFS5Cs5LLQk3KbRQyx+QJtO5mWVCgeIkeWjJ9QSLUMMSx7y7GPyoxkHkWMM0cyRvLLJMSG2SPKdwWEyBv3NiQTR5VzMdA7I8mOI4lVHp9h3O7BTEwcqzFWuio2ggm7nmj50M08envK6FcdsRoiq7iPGokhqwJZfISFIeOKwVVyQ3T0IKD1z+BAI/Dj/VRMT4jHJaFkjn0+9z+/eKrjUoFqZJEaOSOR45ACPuX2HxLIqOoYMHDMoO00G3Dg/tfUxHKmJlbJEXdsErv4kpWUKxZ1CoWVHreKY2oQ3J06d2aVNh5LsGjkEjswdaIkHJJj2FkKx7ViYBigZJNsjbSBAMaRsbJ37mT3FSU3Eq5YEOB8+zj4qytc8Hpg8jQfQfnB/ClRPEtsGZgRLDzGZHAJ9IJ/7q8NyyqY0TfvmaSNU2rEsKrsdBKzK2OoeRSq7vHIBK6s8ojoVJ2SRI2cKrSx8uXvazqImkKh1ii2O292ARmIcnf5NrXoeY0r/vSSNIXR1jKxOZCJokKsmQdjf8yOgVa7ehzw9OrF2eQhpGPuALpvhjYs9oqGOlPjkbhAoiMYosWEoMifWo1kATz3oDLwkNyLCJAiyAhpWJjeRN5kDlUYlmWVkdG8Q2ERl4y+1gnxGllCyw2+M+1JJmYmQCZGiDMTGjOWaIfuKwZLIJWyZlC7NE+Orq6ZAaRVcxCRECZIaJXVVZlT9QYmUjcFVdgLAki5uL4NkokaYRJL7STH4yxieNI5EZjM0kYRoxuDhIwDyzRlIHMCfWKDkEcSKhOlyvpOoQJKzNhZDSAxm39sksUthSwCnfGFBNOgUUAhA6sGHIZY2ZCpRkUskiWCru1Ww8ixO0Tq6hwrNwF2sGIiWXgrJFJLH4m/bUqtbnSQIGkZyIZWUhQpBLCxL5AAsT7ven50uHNHBKBtJcu0hDsrmNfH43pQfHSsqFixkDcoA3S08sWCkqqsBBCgAY4OAJJ796dM6C3TKCiO/AWjjRWfyEqpJAUyRpIqybnmVjAdxd0MhDuqeVI4pozHt8e6MN4pWBmkWlUuZSHEiOCQdsjGgqjavSkBx5kn3KgjKGG3ViqeTd5JC5Ak8jsiAEMFUyXzHbdBxlovCrMgVW2g+92ldASvDSBQzTWTR2I7ISCLBKbXmObz2siQq3jDOCgx5GJj3bEtRtBe2dJiGAVioSV1s5Ytp8nkEjn3J/zE8gK48qr5WtQBtJBIMZNQizwRlhZvJKTGxYNKZPBCsYJbcryIJX2yMGidgQ3taRiOeDgwkicrKwfzSbUWNUMvjIBV5FKvF4vETGyLslIi/bVi+8opqyINgAVd4iIijWR4pSFhtYB5GZQjttISQAM6OSjMBKRD9SGxiAWl86bD5ArlbdzsZXVjHGtCRX48iuApsgpMsuPYhRomVkeKWIlpCVTxoWUEmiytJCoDfbGWUEbt5jGpSDbGwoAqCsTqoWowAlcMQI22QqGlDspGxtpPSEEgxg0QTgcyPbv8jj1xTdA6RIzLZaMq7RBq8gLHe9BdjqWIAUU49r0TGGHiWdZDIxJRyoUsA4DMW3GzSg8MaBNKPiukoE2tG4ZyzgiUkS7UampJVaM87FVgVlAFhiQvyhPLugdhuDMxUjxr5AlEAyUrO4YmiA+0WBZJsiggZMmf7Af2qWQCF25iZgR6c8zgmO3PemXaJcyJPYv7kYJYNZ5Qs5CEu22pOQDXyKG3rIbQtsGlsIgioFrxUpEhVPJ+1CVtFjdqDrvLqQQ6qQOsf9Lg8+pxCgyb1dmcMFVBvahtVnAkYLGx5AB+3kHrILBDHTHjnKmOOLe0aqqhtk8iMSUkZl2oqgqzBKjViFUElid/p+/wA6LshgpMkAH1ENxz8s1A7ePKnRSzFZDIznybBLFGafcSjI8TWGb3D3UZXUWX6BQz+NgzM0chDnaZFVoponMXjkjZSKJXdI5kFEfgLGPEJZZWLMiBmtDUckYYnfYX7wm7Y5Viu5SNpUbjL8WJx7pD+2q8FPvZFBZX8cqkLGzMyoylQPIl2rqTJUVEyeIzqFfxyK6sqoV/c2GMKkiyLM6iWIuZEmHjaxGxIjjYfM4B8F9qKtwSFSxljExVdsQ2E7mQxgbQ0YnMqvMwIJboTxMpMjv+2weJQ0ywSIwcFSziNo5K2sV5utwLlFSz8pVkhmicmNBGx3tMpPujZ0jeIxmyjWyupUrHYfaH9xQORPZlP4EH6/LvxUG7ZCJkZDsGVRKSyoJJnAQsL2EH/TGoG72H5cHm231LgV9RgzkXxieCIOg924oUSQkqFjUWAwCqDTbBxY6ftJSPy5MYEcSIhRyXlKzFGkPnBJYl/GyNKY2URLKEBckWx+oLho8N1kkmcK1nc5RQZG+0szjaEVHHjb4l8bhHWumXJKeWCwkgHjgf4P7iprZL6hYzO/bPERgH0xQGjmQ4kjRRqzw4zuGWyxWNPIy+NaDOPbsNmrChDTDpswZYMSBpJWHkcvIaW5S7uu2PzcFQD5RzxfO4WvRmDlxYWj5DyQ08mI8I4VmLyRuoCBrMRqRSrKFI2nady2I/gwPKYw6UiFN0bnctlyzC7NrbE0TwT80DUfCrk7iAWHYGBxnjuO/rmnTh2nyliDn5YI/tn8xUw00ZGpMTM8kUKpI4iHkJcB0IZlFgAo7O6KpLFQpJNASrH/AEkMDqV2qKbhUSQmSFJHpBcTITKAyjxgAqrSAKELTA0GMgZ2cI6Ajkh7ETlSpEkXuR1QNZat4Gzcy35hbK1HIEUCsnG1lEZfeoiRQSrurFTQBIe7K7gx2nqZRCgEknkk88ARz6gn9moWzuuT5WMieYkASMxk8VLsOafOzA0hkKN40Jd2aQhFMW/c6MYyiEl3JUFX8hd6EhkErnHYbywRBHTqI2aVWCFJLVFkIWIKBYeNiQXoqQQtNwUx4VLqzBY3V1Owsj+Ix0sbSmbbLbFgtkOig1HuJEz8qMTTRAGXhY40krYsTJDuLRq0qqyqF2FSQyrsKfLFagQknk8Tk/KvSQyT5gVJS5kMSJ4Vm2h45S/kuJUmLF1cI0nvOyNSaJBsppYMdMeESmLx48asEV5A72xkdjHH7XMhcOrgSBlJYcgmE9tYYEolmrZEhcBEBiRwqLGK2+O/Ma3MoqT21ZI6lU0olffJiJMWsrIRIpZGZnH/ACSENFmG4C+KNkWSpa6Mv64erY+N9IHbuFPkyRTar6ydpQwQRgH9QuJ293hmziW0e4YlgRyo2EzmAlwFIPInKQCVtiQrXuAH5+bHH+LAF/ND4HVp/Xgy54fp19I8IWkOV60RyysWjCrLj9kd0GI0ffIyxZM7BYXtWoSKwPt5UMqOzvYEK3vCgMpF2yo4ZEK8E3wCCtbRTAUbAi2//wAnUj3A2yfyP4V1NRm4gGSFYn/+yH0+vPapR2WymZU3qjfuFSDTIeAJTYO5EsWqhjQYgMxA6fO8cCpN53ewyRsU3BGocUpUESWxLEn2MWQ0U3GM9oTGDUE37WAoiSSVU2FZA6gBloGx7aO6hwQxBFk90QefTlyBIsjpG5cqsocK77mkkbZvnYsKBdpKF1ITSi2olSDkScfQf4qhcMXnZe7CDzIAX+81UWj5LYGfFMI922ZS6MCRIg3KCyOxDlA9kAgtY4N2JF3Dsg/TatpomjZ+GYCMAKdska3FJvLbA1SMYyZS6n7LYfExcbIMM6yiNldN6soKbvZtDFuKYuA1WA5DCyldLZQ8MU2LNJaOXVFZw7rvCeMLsVAVJcDdvIXaSR8Ui+UQcDt+n+P3NK1xBdtgKFlBvkTJgTnMRnI5nk1PsNoe6NCMSFZMvwmZ1UeJI5NjKyM4c7kkjSIgNIWaV2FszkmldQx3x8p4SFV1Y7kYmrQ0Y+SSStBHG47SNt1z1Ie1dWn0nU1w5JLgmlV1aQFkDRycE/8AMNLtsBRfAIAo9PXfOkpG0Gfi7tmZGTIwJ2HIjCLLs8iRsQ6gMo2hhfIA5ZWG9dwzEAn2jH9uKFYJce2BIcjYx7BgCRMCADHImPSmTRchbIZWc7C2w7ZD5IalQx7thBpCFjVmMkgjQ0G3LPI5v1cCyRlGZC8U0LFiqx5G0LLGRIjMqbJUZAGMc0sasT5Aeqq0udEmVmL7N6l1jYIzbRVlirE2rOOOLIv8nqZZeQdPyosmB5fBIIt8NKCUDbpInkDljdFSxAJikkUfYtisAACfyPrSFIZAvmBmT6+U9jxBH5d+alMMSqBIrIjzSgKQksQ8m6QtMkqJv8cgV1PjkBLsAxFV0XGhdzEUV7j2q7K+9QIwrMiyIu2QiMrE5DEPTG1fcWfGzUyFUMyyoyExrJIxqS1CITSp7X2swNs6K4B9xPTgpleCZmiESSCZlkaKQALF5EiSPxsFCUqlfc4HjCj4rpwIPFRZBIPY/wBhXmaJYRIrFFFylgEkCUqKzq1Re4rMJGUrYHuQMwNBjyImUPIysDGQS5REUSB2RmIdgfuUqibRvJHK0LlSxIoKtGXMiTrEyyB6ZSysxIYIWEpceMvuO293uBI+Zp8gyGglVI1SRoXR1sp4R4yvmLyo7r5HkxwQdwiYKSWUMtFN2HkRwuaLSQpvcRuHSQtPG5jZdriPxq7BxG6qaWi9WenyRC8q7SYyYvKzRsNyiSRJlDbHj8kiSB027W3AWXZDv6jscaJsA3MoMIUmcB/d494eEP5fJ+/HNGpjoPLLZQIbLhyXV1cExhJRM4DqZOHcFlUyNK3ljljjYKjIfIfdtTcCinhoi0bxlNoSNxIFRxkLuI2F41JjAtSrlg7oQxLqtbi0aFTMzOLyV937apI58llXipdx2tuZpaSMMtBWcM32OaOTaVeWQtCoddzFnyPHKpRgYxskLJHbtsBglaNGZURirJibmbcjy+Ie0MirMB5EZ3YbhkBJBIVE0u/axRWlXYEUoqO5kYkSS8hGmDRvKSYhE58kO9iiyb9qblXbHKVTxl9zGIoYfqMUYVmRWZvbb0RMocEKok2jYxP7lMCXRFeubWY5SSRyMkhZQJGMbuAyRwKWdmjDgSLHIQ7LUhDMfkM/MW1PazEBWZZGIkViQFHk3AgfIbkAoSRESAC28npGJAJH7zSgwQT6j9aj8MsbAoXkKqhfbZcbzGoBRXBQlCpLWCSCxX3AU2Ty+wcCza2Vcs1nchsLtSvwD9v5r56IlkZdpSkUlzGFkYFkDBSdvP4YGrog/kdM2QVO69pJWiu0DkKF3Aglixr7qv4PzXUW9vX8h/irSKWghiJgRAzwe8xUl7Ox1bNaZlkIV7hO0uDZrxcWGLj2g8FT7uaC9XOrRLpWYHSTYY1WKfYCbkdXKqSQbDEOxZUjNNEWXcGFd9kwSJjSkRq0j8FZJBW5Y2faUZi4Dh0Q1C/yTuAU1MUy1TTc87E86ZG2WNysu87wzkXH5D5VBsyBiaWnIsdSqAFEd5n8Fqs7FnYkzgLPuoyPpIqJPIFlkVlkO9yN5Y1J7jYAcJyPFuTe7AFtqxpVdTTAR3gUhDLFL7wje2VrgdS4kNoZwHjMwR2OyMr41EgZYPBvnnAZ5SJJRGXhDKxRXUtIyFL2hJA2x1VUkG9iCxAsbDxA8BgcqVJJKKAIVYuajCqKdxuCIQNooFmDDb0tNoMwF2mI3BV3SK3zsoEOwjZNq/t7g7MSCF+KPAxnkkjQohLDxxPvdlQo6ygWqqlgKw96qN9qOFWwbkoxMf8AzH8zLFvUG12tCqhqsu0ZXa93ypUAhbIwjZ5HVHKBpIQnk4a9wgVWjUJucb12tyE2SbT8WUUDixquQs8agKSjyRARpGrSGONgpjKhSVXcZGcxAAh+aIgXfCyTTFU3lsZ33upZkjPnkjKWFoBzEGWt25S1GgD1KcvURExwcRXlzZ3ZBBHIjmN1QkPIVUyRRNZG1q22VXhj039zY0ej6PJiZMwn1bORpcgsWIQMAS+0+QKiAvHGFZSd4JFX0h4PyNSWyyujLgzAOCYMTjMY7xiq4mKthwRjdyxY1uIDKC17WB5vkhQp/F9O+FL40QxMULRszHn3U1FXAbaVJ5q/tWRvlNpYDJ5WRLJSEWbrbZNG2F1aWouzfzzfTrAm6N2r3IE8f7gUUCzX8gGwrC6PyB8c9QVNdAW2QMZnv35p1aSXPyP06UCy0WdgquSqXKZCb4jQkvQUkc0XHViduacsOI6OtyKPcdoUsVDZAAdyF8yuhLUNzJ8MObhGixGSWUhUkZL2B9xDxgbiCQSPu2qfa3saSgDtDWNG8ccDRo8e2W0LMSzEMjR7ozHJCPIl7m3Jfj/FKu6dZ2jd97M/lHGPWq7Hm3/Tzt/D68+/yorJyGjQhmO8MFdijsIwu0ZLKwZ42KzOq+1iCxWM+Mobb41kkyWYIFDsDEmySFnDKPGNw4baF3UQbWgQAehY1myJthdWh/UeCRbYJc1okybdxKSB2AfYPG1ltrMWWTQJG0kJMg3xsm6IvAtgKgVvIxBfxlQrGQmUkH93Zt2rUYULwIxHJ4+tPsPij2ok+4QmUq+x47ZWaRXU0WEhki2bZEshiwHuHXppJMc7BNOl0xUeUhWP3AFmG6iCNygKa45voOPKZGdnVVrbvctOtH3rbqInJgQldhcIHLsEL1wbkQK8m6PIWNWG6lWIq25mYMnmkkbxhSqR7WK7EUgAluilLAGCc1vm/ry5N+lnoLgK0JeX1F7nywplfyMuJ26uG1wDcrY8cmoAPIxYwyNGsO0SV1y75cQhVlBJo+4k2bJuyR82pNGhuFkADrpO/rx50rJ9MejCaN8eR/UzVf02yIPLcfZuJDlpKJTPINjtsWOLwoXTIeQSuq9c3erwtC8kZBT7bUruu1VlBKBVpVIUcCm3qvArqnaB8NTGM5//AJGr952+0lcQAFGMwyqT9Z4pPSvJDKciF1V1LEAjeVAJtyqurLSldr2u23N0Tdu6fqkWVgEEQ+cMIxDKURG3RgW6pTuFKOeNzNuBN7RVb6HjhsVpSwPuk3KqAl49oLAsWBACgkj4IIPIrqTx4RnW1lijKxs6O5SD9xpJJbZV30EU7d1LwQgZVLDqygIGRGf8VTb7x/favGZ28gLy4ck0I37GjIDQqpdmVldh9xtdpiQgomwSRlW3RnVUzAAsgDSIqp5CsxYKwChAQFOxFCRqypvpZJC251uUYeRlYn7coMsSuwKvG7usbWxFsWjgJZRaEEuCpEbiz0/wwYOoNCk7O49u47EkdSgYSttlSyrOlxbRvVHIayAA5lnmRE/s0K6Lt3ANtypgkmYBkj6R/wBCqs0btnuXuHJyIdA0rUNWn0rFbUMv9HizSth4kIDNNMUUhYmJCRtJ75H9qKXB22VjZC9xdu/o54WTKisqoRneNyghePhV8br+nkMoaMkuLaRGahtH/pVyem2N6sd99g9+YeG8fdeFp5wW1CFQJ8OWEwRqqbkYwRZj5CMEkjY+dJQ6CVrkX9RT+m36jfTz3pP6rem3bs2telPdUjammdoYGZh4nmVJsiJmIjlxJYnLK+O2N5H3tOj3HK0mZ/8AyPTabqmp6Xq4sLba2q3WmGLhSoLFgBIMliIEQcnHoL/A2u1/wtofiHppbVvfa8NTprKjxNOLE4zHiMygGAxYAiVAkjRxLC2n5zwSKSgcRtfFhCRuDWQd1X/BJ5vjqysSOPWdIZNoRkhkO6MAAywmOMlfaZAGJU+MAG/IQClHpPX9H/veM+TjwNFm4sJE8HMUoeCRI2Ekcwj2SKWYSAsWJ3cGuofoGptg5KxZACUxvf7ka1KOdrAku5/CkG14o+4aBBujmGG4H1BEgg+hxHb0rCsrvICvbe1tW4rgBlYEjaygnaTDYkkwe4NKpLPp0kqyxt4i7LZLR7KILsBW5QQQVrYVCvZXcSZTp2p4+ZNFjgRp5CI0ADEGQLvgCMVkILSsoQsShZgXIjJpTNxcXVMd3ULHMKZn2si7GaaW/H795MaKgDvuA3FqDqBA5sbKwmlZFQ7fcAQQFOz42+QbSoHsG/hgKFnb0+dpgCVwSe54mDxwQKZ5bpAA2sMEEwMR6iZ5+sVcOLkQyRsGVlBCRAmWSSONXB3WPHs97+xgVqIlgoAN9OiTNcMUigbpotkikAhnqNuP2wiRr42cuNtBXJAA6qfR9dKho3l2NV28TyKWDyKrMG2gEKI14HEZLK1m+p/p2dFMQG2bCPZJch3MYliVJdpIdWKoST79kgLk/iQENkAj5n2Ht++2KiKspIbjtgjH6HPeiXMigCJVidFk2sjJR8ZjSOYLDQLHYZi+xaLO+4mwQZcSbkjxqVZSpqQAbKmlO+NVJXeZTHIwsxyou0BCOniKT3BkEsysJ4woNEJHE42FzUZKA3Iw2JIDakAkH4WV0Yo8rMI2Dva0CUeQFCrllXYq4xgt4922QzIWMfRSU3YrhCURwHlZFBWXeq2kh8amo1QFXioe4MkhLW+6nqKVXiRkv/kokviYOg2mIbmUsQ8ZbwyMJqEwCn3SRMQ2yQAbGkNAspTxiIbWWJZELorPsVGeFbKkxvRABIHXvGmKVf3RUAqF22mNlaNd0fj9jKZRyDtoEFSBtKKCyJHaUNEkiRBFKsX3FArlmsnxB22AMQqOwokyOPiK5SqxiYvT+8hzRjdS7BEZAShHIdB9wdFIHAqUzndHI4Hukx1lZQyqLeA2I02uR++ptghLM5B+43HJbMjFF2I5ATajG0QMFaQSbSKIDrIosBlpQrAdEAwDwSJpDx9V/UVC8rhyvIjU7QTdIFVl2xhrNDgUL2/wKJ6j2SzFitsoNj+QQDyx9waubIBu+AR89SnU41idxZYuS25iTRB2kVwAzFib2gkA80TcYkXySBRf3gWAGsEiwAeCRzweOQD1BHmKniYE+hj8eavWfur8x/arn7QiMWjTTF0JjhlaNWdY6kKsB40JVm4LIWt6Qkb6vqCx6tkYkawTxmfEMmWwaZFlMc00iCRpSzGUxgp7Yy77TvYBTJu6sbtjyDRJgyxspxZGjVkZipOwyrGduxRHE0rWWurYHgEV/OYcjCiMcRM6jxeQtVl5ZGcpVKfG3tYEElefk9T9gPT/AF/iqYALsvYXLsx2grAPpT7pOoYk8gpkXwh2OyVh5VKqWJ4ZxuSiqAEMI1WQWgHVi4zbVKJIQWSXxyEFw+0yNazALGfJaOZHQOAwH2gdVfo2PiSRJ+piUlZAu0xRUWYmnYP45ShEgBSEngDeSRfUun0qaLBGoaJlzweMo82KSZsb9xSdp8qlwxV2Ris7FQCOHQBQT9fanFf5jW1yd+xJPJifaSRJx2Bx3pxng1N5WAgYjcZYnRxsETIytGW2gl/+W8bMd4NgnYo6GOjdxZhKq6YSMm2WSYsJQoccqFcAM0jDIiG9VRSiVddNem6x3DKl/qIUQAl7x3AlWIIjOV8lqQyktGTuDvSbUsF009+6+4c/D0rShnapqma6w4el6ZjtLNOSskkTJHFHKxRRIA29kUMoIsqSrLlxbaG4zIqjO52KpE58wB4EnAPEU5NNee4lhIe65VVRQxZmYiAoEliZwIgnE5p0x9J0XsuKTOy8g6hnyLOEoRidZWVS3t8hMUUXjeKPyuI0ZWcJUqt1U2q6V3V3INU7kx9I1TM02Ni2Rl42JPNjY0S7tkfmRGBRFb9xl3EAFpAo3N1m72l9Ffq/3Bm4+pd8aHrGk6O+TBPlbPHmalJjGR2nmjxxLI0hiVZGSFXM0mxhHyrAbRu5/TT009I/RjM07TdV7a1DRMjRIJ9DfGnx/wC55srQyDIjy8JS+VFNg5W7GzzIN8LhIsipG2tw7vXdKysNJds6p1BLC28jmNu4xyZHAGO+a3HTPgjXnxLvURe6eGtlrK6m0Fa6Qo3MArOLarEjcdzjsvFc1GKFdnLoqnfxYA+ywefzyODfPyD+epLjBmRgKICgqtspJCkUSFZSoHFEirP89B6quE2tawcHYMJtZ1FcdEBCpH+sl2qLCttVOEFUEUbfaB05YbbIybDqtcACmFjcu4EksKvg3X8ddVG3IjRtLIrFSQSu4TBjHy9qyV5be+5bDblS4yTgE7TAYAjgyI96eNPj8G2O9wndDuJKsBvQsdsYVLSnUGuV4HBI6l5kRIfds8ke8CowGjlJ9gpZBujS0JcCjyKHx1GcCNwyud5A3qwG9VcWabcKWlJL2eNqnigbkIIUKLp2Aogb6QKHUMaU2ACpKggmrc0D1ZElQTgmZHpxHvXPOTu7kR7fv/Jo/ChUKXlQpK2SXKutRyLO8rArEJAygMwO0tQjaPaNwIMhhDXuLyq6eOJBTGjsRapnUELxbvuaV0eUsQ1iNwguV8qsUYOVAcIsQdQUZmkJ8dSkSkGmZSQtlh06Y5bakwlDfG0+YJLRaFSrK6qURkLx7iDYAO8lqC0U6mUCSRiXjVZUsVs3AlTI6lR7SrNtFq1iiprjp2XHnmVXhkxXiKrsMkaEgbQaAeR2RSTuVCRtDfA+S0+WiDKKRETfEzFoxJuCh2KkJQIJYD2sFNg0acIHkaJSpG0ewCOFWUCP9sCzKTe1QTyfnoppQEzn9/Stq/8AXO1QS/UB6DaP5MlDhel2salIksr/AKASap3Vl4inFiSUQQ5rf2WQZzJjRSSBMaKSWWHHx4odIOqaNn6vnpiaXhz5mXMsaeGOESP5HO2NWG4IoYbVUsUVWJ3OfgbWv633cAyfrG7bwsfxrJoPop2phTAFpJIJ8vuDu/VSmYC5RMmOPNhmdYiu+CWKQu4yBsojtPReysbN7FwO38nHyIINP/umZq/3SajlZWPjQ4uRluGH/EeaLOnnUbWg80EaiONwvXG1evXQ6PcbRuFlcKGHkJ3Hk9wJkgelarpPRv8Ay3UxbOqTTIGSZbbefCibKyJQDyu8ja0iDVLaD9KfrbNoa6inbuAscwWRMf8Av2gpnRElW3PjDURIrmI1GCFpiwKstDqRaN9I31G6iznSPTDVtVLJG8mDp8unZ+WV3EI36SHLfILT71CIFkWUuAgbcANgun9xHQ9JXExXjlzJHi2z7gNslqXdBYBeVjaszSBCqEbShbreB9FvaXb/AG92DpuqZ7Y+TrOvTSa7reqyJtkjEzxMsePkALLNNpyNDiwxiRjCmbmTRwmPzg+b67446x06yjPb0Nw3GZbSNpyJXccFg5JIEnjMRzge2dM/hN8OdVa6U1GvsmzbRrznUgSxUeZBtA2knBmJkQea45u8vTvvj091ZtL717P7l7P1KGKf/hNe0rK03IaSN2Viq5iQeaORRt8m8KopkdlFCNY26Ga2K7SN4j3QxuhQSSxksz+QAxyqXCIUcKLjoG+/P1C9G/SL1w019A7/AOz9B7w0rJiCxjVtNhy1QZJRTJjyNHFNp2QHmLeTCeCYusbtISNraffqF/ofdpazJqOuegvemT2xl5C5E6dq9zLPn6CZpVlUxYep46/rcNNxUrvx80P/AMM2Qz7Fdej0f+JWgu27VnqmjuaF9xU3rDWrmkk7Yc2i/wBqtF8mCjiB5SYNZvr38GdfYZ73Qdfa16runTa4nTa59kF/DvsPs2o3bgLO1rYYyW2giecXQO4dc7J1vR/ULtN2g1nt6SOUpAzQfrMFjDJkwtIqIzAPGr48zK7eRP8AlFon8vWT9Af9Q/05+pr0803049QYdO1bPhxk03uLtrWVjyJ3dkVBJBBMX8ZmjL+NoWZXMbPGxV4wOeX1O+hT6qPR8agO4/SfXtQ0rDWaCbWO3Io+4dNhw0kJfKM2nzyrGoChXRl80UbES460yjCzMzO+PSXXB3N2Vqurdra0iPjTPhyTafNHI6TSGJ4/ZLt53RSulxsQUaMAV1+s9P6Z8TaZdR0zW6VNbZtE27iX7ZW6DDNb1NsEu6FgsBgrKQq4ODyvhLr3xD8B6ptD1rpmuHSb77tRYvWWRrD5DanTXVS5p0uNbOx0S6xKhnG4Ax0o/wBSf+k32/kaJmfUJ9JeKuTjSYs+p9zdk4sZdBhkl5psGi/76ETIgVCY0ZgdzbXXl07q7dlXMyC2DNh6lhSPDn4E2M0ORiZeNGwmSVJQhjkWWNrXxAbNtlmth23/ANPn6x+2/XDs/tKpY4dPxO0cDRta0yaWORJcpYTBmJJE9ljjSJKj2h8hO4q9V1iv/Uk/pO6d6pZGo+rf04RYul93ZOHk6pqXaO+PGwu45pJWklTHkLrDBnHz7MQO8asrujuWEanMdA+LbvTbx6V1dXtaW3dW2rlWNzS33aEa6BvcaS8puXFYlgsKY2yRufjX4As9f0tn4h+H3ttq7+m+0sFJ29TsBLbbLbWlTTrq9J/xEOU8SWgh4V+STRtbAkXEzQyuoH4BWm3pv9wVGUhyPglHFUQSQ/apiQ5CGTHVTvO47WaUgKjFyzBtthzu2ILSM8GwSPfqJ6dd09idw6n2/wB1aJqva/dGk5TwalpOqYsmFmYs6kAjwyorAc+1qdJVJaJnQbhGtJ1TeyYWQNsqOqRk2oleSQhi250VSSRwWoEBj7SE69Xs3kuoGtMly3czbu23V0uIwEOhQnymcAw2DIGK+d9VpL+nuG3ctPZvKQly1cVkuW7gMMrhlEMDJxuU4hiOGDMxJIpPKoaFVYIwDb2NURv20qqf4KsSOD9t9OOn69Li+KOUrHJvTabjDEqVCkAgiqXkAD/7PudAsjGyzMeACzABmHJLKoZwl2aXaQPatHc0My8VW4UgleN1UQVeRKr7hZWxYHB4Fg9PZSsQ0Tzj0imB0ZGt3CNymD7cEgc4Me34Yq3tM1ePLW42MUg9qEsGk8bRRg7xuV2VhaZPtVW3EotDaX8wjaGCpMjHc0TSi18kjtMoVgyOFICClCbAPZZFY/Ymo5GmSq26Rox7SVouA1+4blaiLoMOV4Iuq6s3RO5cfJ8KSIm0eFEL0zb9piYAigFHjLPaE3ZFfPT0bcokbW7rO6B67v8AX1qG5buLJVd6ATvyuMcCDPPr2qcEClXbtF++Kwaedd0xDeRAVVYIJTtSQ2AoU2bbWjJ3QBC7tGWHjSMCk9wlJrcVjgjcsCWY+I+4HjpwL40sXkDKsZY7QrRFWJRQIypriQm9xQqAbIPPXoKpAKoskasyqj+IgOjwFkVWkKeMqX3MBGWSwmw7iX1GDIB9QD+NNseGzRNjs67wQIS67iqbXZxSKQpJZVAK8NH9pBFM+pwyI0KISzFwSzKGXb7GCJ7mO5VQrJajxqQ4oDb1J12zTwz0saswMigEOGYiRonWi9xwpI8xIc2iopQksWvVoHWFlaNI6ETK53KQHiDOhG0OVAk2fZyQCx/HRQePqv6iqr1AEtLxQYyMEO4FbYkEEqAUIHB+SOeLA6jDqYpUI4N7qFm/cOfmxyfww5PBFdSjLh2OQRYVXVQKpQlIOf5JN0PgHk8g9RnJ3eQEHjbTG6tPv23YC0Rd388/z1E+GB9AP1NXrf3F+VXv2irS6dFJcu4JKxYkhXDRnyFmUnaxSNQF4FMxINqBW8McizypH7PFmO0TBGCSJ+olWRUNsoIYMh3FQjAi2Hu6mPZGp/psQLPIAiB96yoirGrRxxSEPZI8iIxRULqwbxy71BVo9puPn6n3Fl6RoWDk6jl5mcyYeFiQGWWWSWUvUMUW4KzF1FIoQKoZgpLMxdurZt+K0BBl2Z0RUURLFnZeJGAPmRVazautde3ZtteuOwZUX7zM7qoUAAk/emfaIzNSt9Pw8rSZRHOIp0n83mLCOM+NZliV4mJCspTfvikZpBNyiEgLkV9M/wBPfq/64ZH9l7W7cc6XMxR9Z1UnTtKpI5ofIkk4himKrkEuyNJIN5ZULsmzK76ZP6cneveSY/eXq9BJpvb+IBmY/akThsrOSP8AcC6htLKttFQx0kMjq5WXwi0PQF6O+imP2npGm/8ApvS8XTsVcWDDxYoolh/T46hQuJBjIfFDFGVQSMnjZ3RbLbQW84+Jvj6z09W03Shb1WqG2b4Hi2bXIICr95pKx5sAnvFe1fB38KdV1EjXfEe7RaC6W/8AQ7jb1V0AIVF1sNYRisgqCWAiQCa1MejX9EvufVdaxZe+fUjEi0gkZGXi9r4Wc7mGUQukQy86DBSN9gmR5MY5J37ZtihS0u6T6dv6Tf0++lIhztF0aRc1YvFm6xq7tqWt54oSSpFlzY0ceIWZFEhxMeBJPE0bmReTm92Z2731iYWDDj4umvG0a3i5cU0ciLGADIZkDzeJyCWYqQSQAbW2unK7g727d01oouycHNKB2jn0nPSV1IDIzPFnY+KqKdsandNuDEGqtuvMdd8Q9Z6mGPUL182kUHwdPbazbHB/41uTtPBBY7ctJ4r1fpfwt0DoisnRdD07T3rzADUHa14qImH1LuEcICQylSTgIWxWDHrT6C9kdl9sajJBpWmwYEkCyR5M0scbq0KBwJ3yZatl9yZJeaaVWSWVVmhmeXlK/qE+rvb3bGra52P2hmYuRqmql4MrIxpVlkhg3ujzZDgtQSJUix4wY1ldvIqKSVG/767u2/rQ9bNMzNA9JMDt/t3HyfL5z3Bq7LqUSgMJJMbBxcaXFbc6LHEJ88JG1sYpLATkk+o/6XfqK9GNeztS9X+1tXkmzJ5pZe41kOo4mXJ5C8jfqozII1WQyVG/iWPgRoQbXTfAul0L6xb13W6W1u/49E90+KxkGXUyVE8nzbgf6eK4X8ReodY0nTBpdH07WawPaI1HU1soLVm2UEIrOUu3bm0spu2bTJbIl2HNYs4KnxktZJmkYbjyNwLWfyHAIDEUNwJFg8PmLuA4NCidpHDFHY3Q22wAUovxIxpjXTLg7mjppVVhIwKsOb4BUrTAspjpSSOeaWq6fsdCph3lQdm9UU7l2sTyrHhWJsG/iv4rr2pSxndtgQFKcFQBEHEj3718xXGJRWgjaoSCIcss7i/qZyfTOZBqU4LzBEjY7XYxrEoLAMZSIqKuz+JXL80SEK0L3Cn+F4RI17XDgIrhATGpYNJtjMjONo+SbIHPxdxrGZj4twLlAhP7flYDeS4QuWCtyfGeAHRZC52hWkEcaRNAqGWipLPTAJJSRDaoACnbGFIZL2yEEijdqqlOeMqPDHLBISsklMC7qzXSWtEuoUEqrOHiIUqWQoxLtGhWEqZaX9lrAaUSyKUlTe6iRQCyrLCo8gTftB8gYhk4XayFiQQrNtRVWQbVVEUptO7lApBthQIJtXhFDxROULIrJCdrghpljraFsHYGiYxkALXuFFgOiinijJK5jYkUCyxELskMsg/cUhQIieSDGoRr2qCzAmfpFnCtR9qiP3CFj7LAtrQsar3MCT+SeAAVEUoVxtWRA3kkUShfPIWcFwpMbSEKErbsBIfbvUdOkZgCKspLlAEUqsZAQD2jcwRmrnlhf+eihPMzLxtjPMyAf71kp/VQi171K+vr1a03tXTcnuKXt2HsvtxYdJxGlKvgdmaJk5S5DAGNTDl582NJkzHGhV4hCYsdoZi+JGhejvrxiQxzaVpmdpeJ5EVZZtT0yHHaSZhtGOuTlly8jBKTHiZ2O0Kl0nWaOhanrXcmud39y65qmpaxruvdz5OVr+uahkSZOqagMmW3yc7LZjK7ZM0kuQ9MoMhcIpXYTcuJLi9wd49paJjxytpzZUAzcSBrklWSSFZWiUm5N6F4tm1irKSNzNYwOv65dAuKmm0jWEVm81nxGAUAkknzElgTEd8A9vYej/B+numxdvanXLeu3LKeJp7raVVN1lAU3lDBwikKwO0BlIkxmrOy/oL/AKiWsduY/enamn4+t48mP+rxdMfuHRsjNyohCJQq4uQYEmBUbT4skyWyxxnzUgtnsL60Pqe+ljUMPtj199Gu9O39PxpDFLmJpep4KHERpsdpzFnwywzYsbBmhlg1CXcU/ail5jPSZ6c9w6fp3aOlafjTSxkJgY8ESwVFp+HjYkSLA4CIPBEcVPEUEcQE7PGHTHUm2cvD7Y7w02TTNe0zSO5cTLiUyaXq+Fg6zi5BkFGDJx9Silx8g+EB0SSJgsK3vG4HrzF/iNdeuzqvSdDrLDAtbW0H0mo05bIa3dtgsHK7QUII7gmYPtun+FH6YVt9D65r9DdKhL4vseoWNWBCkXEuhV2hgdsGfYc1rV+mz+o76P8AqnFg4GJ3Rp8OfK0ZyMLMyFxsyJmCMTkY05EmKUKqpIRTEULFkUg9bGMX1z7c1XUtPwppsdY5cOQLko0cuMyCGklMsZFNONwEbUWijVeSFAw89Sf6XX0g+qmXHreJ6Zxdg9zSZTvHr3prq+X2nqK5jsTHIuOgyNJikcePYkOGisFdXB2gB37K+h3RvTTOxdOxfULvHWMLCURqvdOspnSYrAqIi02Jj6cjShd25jAygWA4+Bw9b/4xybukfXWiVWNPq9K2pfcMlF1JjdbT+gmD5mLAcVounnXM/hdTOge5YQhdRpriWi8wGuXNIWJs3GgSB5SQQpO2ayqm7xxu4mxY8SSFXnhWJnxGWMMRNMm6N45IqZhFGwjEu7c43qyk3Xvqf9BPoT9Qfb+Tjeofp9o+dkagJYzr2Dgw6f3Bh5OQpIyP12OkGVJINgeM5bZaQt5pRiv5Rsm+m+lOA7Yy6dqcPk0rIgySkMxbySRBl+1hIAjLtZyy7jIAw91kZ19g4aLp0MGq4sH6zIwEhEwlYmZFfIaIIbsSSCZzNsjG5ABLutb5uhbVJqA9i5csuDIuW38PzBg23asmMTtYAAgScVf1jdMOgNvUae1eGxldb1sOLgccgXCMHJLAggGFkGucvsH+mD6wfRZ6mZHcXo13a3eXphm5f6vI7Y1plxNewY7je8HNQJpueke5/KZxp+WJmLriOX2Js37F9RO5GwsmbvDTMrt21eGLTZ1kzM6aKK4kmlysUNhoj+6XZDkZk7JKjTTxPuiGxnNw8aJo8PO8eRjN5P05YH2B1jTYyp798R2l2Vr3MQRYIELzvT/Q5pmnix8Vk8amSHwhkPkQswRtqELu4PC0w2k0Axua69r+o3vGvXDcvbRaa6EW3eIAVF8e4D/6hgYCllXauOBnl9Ov6Hp2kXQ6fSpZ0pYPbtWbtw2EJcMyrZIO2ZLnzEbiWmIFaefrh+i301+sj0V13XNM03TtN9Wu2xLkdqd04+JHHnSNhLIY9L1jKVVyMzSc9RFBPFksVxJCHwxj7Wjm4j++O1dX7P7j13tnXtPydJ17QNSytI1PDmRlmxc/DlkgmDh0CsTtDBk3xzxOskTBZFI/pUa52hjYRyosFpMSOdA0qLGPH9n2qVkEsiKNxYMGKKWA9hJHJh/Wg+kXM7f19vqO7O0mRMXIkw9M9R8bDx2GMsruMfS+5ajBUiYjG0/OdhzIcSQHd5gNr8B/El3Q61Oi9QukWdRc2aa5dI22tWqQFUgmE1CfyzuiGIMRJHm38UPhWz1PptzrnSrW7WaKbmrs2gS+o0ga25Jnm5p7ii4I5UEbuFrQlpurpNHJj5oX9RZAJDIpiB2tIqrIFI2rHGwZXJ+1Rt+feWRuVmjpfcmwjaCrszIy1dVanjm754BMZlJXJjctb0SFVWsAjgtQJ2spLRmtrAhrKsrFzfJDRqWYE8Ehn+NoNWrEFf5+Of4569xBLAEkE+3EQI79/wA/U183FCIJEbhImJKzAb5EggTmVOBig8hL+3kkH22pYjgAgAnj/Jrn4HTeGlx3R4mNKySBQLBN7jzwwscH+SeeCQDH95LKAN3yAf4P4sLVGv8A9iekWQ8GqPCjgE0t8USAR/kX/PPRSVNtA7mMYgTII2BSqqxIIL+0lNp9oUAVRagC1A8dWng6piZEYkDxSF2az7/dDIwMcSguLCqSysFQ2wJJBKjHKUUibVZGjYEMr/6Qb+a3AWeRRofF/HTxpuvT4ZCTbZBuUjhl2EAgm/gjxhAACCCDwOSXIxWdxEEkjHHED51Xu2QTvQQeCo4YnljJ5mTj1q/snIE6PJdSoxZ1lUyPLC0wWUqqwjc5iGxv5Bb3e8gseo5MKqimVBujQKSSrkxjm+BTPyKYqd0agn56hsHcRn2hFUnYUUkncxYhr5JQkbiALIrcCCWTpHMlSadXdCXVI/II9qRgxq21Qo2ggsrliu5rNkEch/iNPlOMR9Y/L/vsIj8LIDwob1kTx/nkfQ9w/wCd2dquRitm4hhyoljE0kULIJURwXsLW5iPaxp2PvFXTBasyseeNmJWQCOQoy1RVhywZ79nIoksKIqx8i88DK1Bu29Rg0qZ4s58aKWlKCRYUyVMqRWvkL+F5CyxlW22tFRtOW/0/fS53/8AUBjw6N27pMeRi4bKNT1bVIv+ChlndgxaYgh5XVvIqR1KSyODtcN1U12q02hsXNVqry6exbUG7ef7qsx2pCiXcscQoY8SADV/p2j1vUtVb0Gg032zUXbgS1aWd22JdyxAREtgAszEffEVrk0k5uRLDp2HGWnzJ0x41iLeRzLIEBt/xZ3A7WraXYqKvoB/p7/TJBocem67L2dPrvd+reGeDUpsPzJBjlfc1yIWxYQyMtr45GDK0ruxbrPr6RP6JHp9282F3T3jjav3dqwdJ0/W4xxNKxZkkJVcYvU88YBYKZEjLg060ARvp9M/pZ7b7B07EXRNFxsPFxoUEaQ46LHEqMqRxtuUl9jhgHZlO75NDnyP4m+KbnVbDaPpyXzpi217ly2bYuMhWXQEbirniYIwCBNe7fBnwda+HNQvU+t3dG+vCKLOlKpqfs4urG0sGhWUgbjJBIWCSDWL/pz9Ouuz6diSdyZWHpOEIlZsXDUSTSryqrPKJDH5I0DRvtAiqqie76y27c9L9B7dgxocCDyYcCoIsmU7jubl5mEaxhmZSAWFcrds13amF2dLEd0izSJFIDGJPtYbNv8AoAYUzGgzDd+S3U+0btyeWozAyCNSHAUH2IqxhBFZBJFsHs1Xx+Dg9PopAL2gp3SbsEsR3UgY29yRxEYnHpGq6q7KzNqALYA/lDCkkgAqwIEiTCnAEgHimvt/R8OIY5eMGWMMhBJJe3cI+8BSVZERha7uAWBIrqazaJiJE7lZUeVWYAUzIzqWtuD7wFdgP+kseGNE6DR4omEcIaowSrkMVamYMpkAJUqWLDjcQwCqAoPXqWGMzyK3wx2lXdXEW3ajFjy1RtZk42tG1BhRPXUQIgZBaR1PD+YA/dOQc/MD/rgvdNx5W6yopBCQCcFSAM9jkyR3qoNU7W7fyZJXzMXFcTnbG5C+WS1cuwMSkgKu7gNvG5yKIKnAT63vQXsr1C9G+7IdS0DBfTY9HyGjycnDxxNFlLBMYp4GkQOBGCA3j3NsLsRweto2TomJmpEzzsCr/tqiqpjCyuu41yJdiqyoVVWV2B4DBcWvq+jhxvQ/v6WaPcMTt/VJzGAu2SODTsgStGtCpCFZlLbnNAruFnqlc07qTdtAWXDo6XUIBU2ysbcAyIGOCfnjQ6TXW3uWrOtJ1Nt7b22t3BIS2whuJOVJmAT2j1/ma+qPbkXaHqV3j21BKJsbS9d1HBhmCoCfFkMUQqtKGhDbG22P229xNgxiAM4QUjIlDbY2mzVkcUwPNAEVyD+OpD3zlz6x3f3Pq8h25Wqdx65nz7kaklzNSysmZAaG1UmkZU3EEIFVuCQWDEV4ZA6AFmCmiu4EoxI2mmpW432eV4U3V/Reg3fZtKzMWb7NY3MQAWOwGfme5PNfIvU2tLr9ebS7bJ1d9rScBUa40Ln0iPwFSvDhWQwjht4PlLUDJvDgoAfj93xoigDduVjRB2vaBXCWZdtmWVCjUjg18UaCWWJIAO3mjQ6ZcZV8cKhWNK9NTe7kCRifuJBIIrmNSp4NHp0SRw7sGHlCuzgyE7olSmlPJAVCN7MWogEWSRfUri06xRIrhyqkLIm2SIxndfuSTaCdxZtjh7RkDEFV2kl6hERAZUUlxxuTem7fGiyJZVCVMisQjGktq4UFlhmJTaA0gXxS0N5ZXVQ8wZdyiopLARnXZGoBDFWXomNQJE2DYfCwLMu4IN6orIVZyqq0SKwRtgPN/JJSMVAncCZGBz7/AIU/rNFJtV5fF/8AmNCgQo7yGRlYRqrPbLthG7aFOOWZgz2XNDhFd08rF3pwUAW1ZVJLK5RlYvvNUQF28/gM0CY8yJINglbcyeKgCW3VvJ3EqGV22Ahg7AkqLt8iheTyFTFFUrKwcK25qBLqWZTsYEFRXHIJsEAoBBEjv/1V0+m/Z/emt5upyx92HTsc5RwoF0/TEmbPzV+7ZFl5MscUCvSFmkkfbGwViQSLdwfQ/wBRdE13QmX1fxm1efUov0WJp2gRPm4xM0IhEmUNTgiVZMl40hjhXdvkaVtsQJ6rD0w9S+08jTnx9O1dP12HmjPEvkeORYmcbXlF7o33y+4Mp+75+Acmu0++dC1Xu7tzLfUceWHHy8TNzMyKQT5KuPCVMqqRKsMckSOrKLG+R1BvrynXNrbbakgeHcUEMhs22UiApEBvEysGdpknAIgn6F6Lb0Gps6Eb2vI16y9u8msu29w8QFywXykK25SMEweTIG6r0v8Apb9dovT/AEDXNH9btN1rWzhYWR/bO5NL1LScSSEY8asianBPqWSfGoZFil033lSiTG3AcZtc+qH0uYy9x+mWu69pOnkq+q9smHu3SsuBWBbKCaOZdU02EJw41LEwpkjTkrujBtPsv6hvTrN0LtfC0buLBxMjExI2mwEnlZGyEgeAnGE5MjUJyztKhCe6aMREyE5Sdmeo2H3JkYWL+txI9xfyx48sUqrHG8sU8dRSsZZ5tjkvMWdrZQBwB5ddQ+I73F+9cYMNpt5BUABmACqBAAx3yMV7hbd7VloFu3pEJIvqzXgEcgruuGSpYGIMCTODNYqdsfU52z3Xi4ejY82odq944ksEseDqmLkY0GU2OQ5hZ8iOOWCRgDYmW6AG47unL1C9TdRw4IdTkUy4U7QyZodjDPAAHMvkljLIwASlYblkVqiLfIzG1ntXsbuLC1KfW9H0TMzMbPi/tcuThR/r8WKWGFpIzOsDAr5lnYhHpkdDZj8V0f3r2Z2vlwvouRHC2HlxtjTQIYpGEBVhENrs0w2MHEalgyBfbd8U9Q6j+Wbbos+U+JAXiSCTIxE9+AZxM3T73hahGtWt53OPDdQ7eGSslmgg7RJUFi2THpVt/Th/6a7twIu4MHJlnL46NmR6iweMKBJLKsQUyAqvj2gbSwNAsDZGbmkxaO7YMsUSI2Kp4l8irCjhzv2k0TIUWKMMGcE0m29i4N/TZ6UydiaMMHTNXdtHly5J8ePKFS45yZnZoA5TdJHuDeMurVv2mqvrO3AiXGikxpZv1Ll0WOdFS43VTvYxwFGUFKG4KGCKKG73dSdPsvbAe1aZ1JLbyPN97LeaDBn70ROfQVV+IdVo9S7BXKsLYVULbfDgKCLkgBiTlQJKruXvlz1POglSWAqsiSRSGIsikncCigHZHYjHBj2SxoAoSOOr6rPNLomSkc6gshCMgkWaIC5Gi3K5YUAVFMeI3NDa1TZkyIsgrKdgstHMd/mhTd4SYx5F2+XySSqzFiHWBwD4gC0Z7RZboggxZZnUBCCI3RJRCJGBUqWY7pA2yyARf3X10b9nxwNr+Ew4jgk7RBOPT8TiKzemvvZYBV8W20E7ZAwoBYAwRAEe/btNT6rqUYxTI0wMpjlJQrIJv3ajFSbFsqimZF8rlgqhgbJ61Z/Wv6pekGl9ia72v6kviyafremZ+l6hh5jK8cuFm48kOdjs0zi0njyYpcbJV7hyYGlVowqkbJ+/YY9PxMrZkkvjrI0kG47Adm6HYXuRY4vy5BWVuD/q65Jf6oXe2R3B37qenRTqMbTpciCNJHUguHIZ2pdhZfH+0EZWjBv/AFWOf0rQvrut6fS3LtxQrtcuXbQAZWUDaySQC5GAZMk5iCD2OodR0/TOga3qXgpfcp4aae+7JacPHlusqljZYxvCBm2TtkxOm7vDsDtntn1H1vF0HWMburs8l8vtzUIJSwm0ydtsMOdE0lwZ+nSb8DKx2pfLCZEqFk6qPuPBw8bWchMJaxWWOSFFFqAY03lTdbfN5FUMwfYquRtcEzMSiDH1HIjdVlkLRI1XvMjEbrJLsyQgqzGQ7gLJB56hj5ePmxss8cayYxJV39jsrRhjHvLMaJa1IF0AOPz9P6S09rS6a3cvG862bam4baWpVVAWQDJaB5mPJyJHHxbrdRbv6/VXrNr7PZa/cC6dWa6llVuE+Hbcy5tAs3hqwBVTEd6ZigWgsgYBq5A+BX5P/wAVj55/B68yIq1yTyTYINE3dfgC/wDf+OpD/wCmnyYkkhnVS62se9mJ5JKgFFJK0Fbb8NwBx0FNoE0JqSZ/5pFYGxwFIokWxCkfPyKsc2djen5j/NRK6sYUyfSCPbuBTDIQaUH2kgMTV/PK1/kc38D4+ehSrFQSDdi6B+doJB/wDxR/838yGLARpmiUPJQBG8nmhyKAB/8AuR/ngeVNrPFHGgpd49pF1YJO7kEVXFAmqHz00jsR9DUhEEjEgAkSMTxSuhpTSGQ2wdX3N8oLAUhvgXz/ANx1IckvHkpPHTRorIyoSyvZYkSAD28sQAObr/PTR27s/WzQS/uK0boxcBgCyFgBZBXcxBH8bfj89fpsqTDyDDdRrIxj5J37X2gUVA+AD8n80bohwBAkYEcgjioGK3Xa0TBtKHAg5GDyOf19uTVu9jahC2pokoCQ+RqSJjtWMLGotmIbbIry7iKDEBCykX12z/0yfp+7T0P0Q7A1vGjxNWPcmFid0ZGpxBZfNLrjxZCoEFsv6XGx4MUgqAixPuZgSw4bO082JMkzF1j3blOwkMzlgyA0QQR8ggge388r11yf0Mfqhh1vtzV/p57h1CGPX+0snL1zsnGy51M2p9v6hJKcvTsOJrEseg5qjIEUXklixs8HbHBjyyLjfjbSajVdLt3Em5Y02oS5qLKqS91GKrCtwoVgrNM7hgDmt7/DPqGj6f1nV6a6VTVa/R37eivuYCOgRrwAI3OxQjaAZBzwDXUpoGDpelQxfpokWFVRxCqx7FAUAOpRHVlJBoH4vg/jqeLDBJAyxpUc+5pICpJUyIzGSPYSoYsA3u4JNqOT1WfbuBmahHALMYYPHlK/BRhuRS6jktuUFiONxHA6yG7X0TDTHhWZvK0cJQoQCzPHYQOQoHAIADX8EWBY6wGj0z3QihLdpSMkgExAiFJkR3HJPpivTdfrLVli7XHuuGAABJ8xIg7oOe8kxI57mLQ9v4+SIJ0i2wkspZKqU0SHoICCQbYUKYEEKeOnuLTcTCWLwYoXe7RfqKpmkjXe0YUturxkMd23fTEWAALIyosDAxA3gIRVKlVQDxrsVrStwJEh+a5DAE8c1nrHdWiYRUzQSFkkS4/EHDAMylmBJQFbYKrC6Yt8EdW7um0+lQr4igADcxG0wYBwckGRIAjuZrn6bW3tW/8AxXyOCguAgkBYlc8CMgYz6VDdS3GVhiQhFMp3qxCCM7iCwVWJNn3IpA3BgwNDpuy2xIoZlmWOOeZTIC7lilt7yDfuQfBCfO8IASBuUzu+9GkmjEcE2MzhFM6w7wsahG2Oo/BYAqAVALH+TbOBhOmTlpkR6hGY1aBlJMigxB2DKbYjzHglQN58Yv29cS6q7ibLrdgEsm5ZAgEwpI3QDOMj5itDY3DaLtt9PuZQDcBJuQR/UAQJJiDg4A5pmy3yEgV4xIPKzRLIX2lJFJoqKvdJHbKX20u6iN1dYufVFpDT+jve0eoZcUOLkduaxHNLnZAijdv7VnKFJBLIHjkKBmAQLGZAf3LXKx9Ri1LFxBBE0iBlmaWRTGx3xhFvyKtOrMA6C2Q7QfnrVV/VI9UZu1/p97s0jT8mOHM1TEk0sOsgWWJ8hUS4yZDuBxmfftT7qQAgk9c64niFFC3Cxu2UtmYQ+IyDEkDysc5Hzrv9P1As6rc9vFu3cu3DifCtQxVByrMowTHYia/n499YqR9392Y8BRYYO49ZihaMB0MB1TJWPZY3U8O3Y5NkMriwxuJxJMv7allN0CeQtct7lsAMOF/Ib+aPTnruqzx9wa08m55JNUyzICAUfyZTyiWpAAVptoSr3EL8dK4mo6dPMvmjQqNoYiogxJN+7dShSaoMqjbVc319D6JY0elIJI+z2RJ9VQAwMd5P1r5V6tdt3Ooaq5btgW7uo1DqpjyqdXqDbEHmLJtweRAUiQY84Wa+OGBjV4wxBVnFrvI3OTZUEUOAav7vmjJsOV0lR+JRskViyIVeDJRoZ4XDqaIVyAByN+5SCQS3vpsMsbTYrGNi1hT4niYkErYAcsrDhaK7SfcfivumNLBlLDkIYVaOMEHlCs3jaORSCQQGYJZokpxZuujXEkSV7jkf74P0p+xtoSN6IViFlUgEIQviVi/Dn3WJeSSbX5FF28Xhy78htgQdnDqXc7g0bEja13Ei225ASAQQGxUkgCUVeJoGYLs8gMkhLPTKWkRRtDG4yA4aQUtnp0kJlijd12MQxRdxaQe4MfhlLEq52HaeCLuqBSbUEnaB6mPWKfocFFQlTHIY2FtGwjJYgGvcpNmyGoAEiwSfhaKIlB+40RBcFS0YJqRqYDykgMtEBqNUaojoLDaV8cTAICrIswUqd5qNlNovlq3A/dhU3u2lqJDzjZBEQMqSO0hMlq6MAHolakZnjKtuBQn2/I4I6KURGOKtP6eu9O3ML0f1DtfTNP07/wBQ5efmY+rSmFJMp3zskmHUJpGU78eHTZIY8NT444XwFRSXJvb16ffTh9Mnc3Yel6R3TocsveeoYOHPldw6bqbaVqunSZcSSTTY2bik5Jkxkkx5S7yupdpVFBSOuZrt7X9X7Yz01PSMl4cgKEkT7osqIbD4J4T7WjDorhgd6OiSKCy31sK9Ovrll01sYdy6dnYTriR4uTJjbsiBkjTaJN8RSWJWQe8PAFDAMXUjnz74j6F1HWXlu6K/cRWd7zvaulbkswYA7TPl4A4AEV7V8G/GXSdHpPsnWNLZS+LVu1aHhLdtlVWHZiwIW6WEgAjdMnma25ek/wDTm7g7y7y13C9Kfqe750PTtGeCXSk7j0fTO4cKN50idfJmSzQ5UmGRkQsZF/cdSwX29XRrf0ff1L/RKWXK7Xl9OfWnSsZyUk7Z1nUe0e45Y+QH8Of5oHyTEELxQft7QojbfuPVTfQd9efphoOvdwpnd56GmHqyLJhYmpahi42Vgs0EMUZfDnmjy58cLjeMT4ssqokZUKrDaN23p/8AV32r3fHk6lpeo4WRE+XMoxYsuOfyYjRvFHJF4vIVkkj8BUbZZDNJI8bRqYyPPdSeoaS/eta62dRbBCq17Tq0qIAIYpMzJ3eu4k17Jo7eh1ljT6rouoW2jWrbXBZ1ZDtdZFZrd2wHhkUmdpQqpmIwK1M9u/Wd9QHprlpoXrt6Sd9dmZEMxw8jE7w0Zoosho1QmTTu4sAz4GYSx2q0kplZ4ShRGHuysyfVftL1Q0WDV+0dfyNH7mVMJxp+U7r+okZ5C8RicI7JSyrGiOEmSQqRwQc9+89e7Y9Re1MvG1rRtO1QHDIXGzocbIhlSOIGUOkiOFS2KO0qSLEERjHuAI12R6H6W4fcDafi42kaTIJwiRPMsS44aQP7MiRtsUQmEsKtMQvscKNoPWf12otXQVawbYyAyqbkYEzIbaY44n6VpOlafUKE1C3it5CUZTt2uQFZgZEAmDtjIG7uK2e+kvceQO08CHMikzJngjfJTGxN0qSKrB5TGQA4LOVjVhS7d9ALZkeq996x2PqEmpadGcjT4Y/Nn6VKAAELLtmQsoaOYqHjDqf3AhVwFqsWPTz137O9NnOm6r3XomXi4alpcSDW8OfOxYkiiDySJFOZE2m1UCNX5IPtsdMPqt/U9+i3sh1xu6PUTt3Uf1COkun6dj5Gp65hyosSSrmafpkOWYMaNTIXmmOPACzxoGkDAs02l1eoCLpG1BvhQLNoW3AJJUgkADchxImDI4gGuV1XqGis37l/WJp/Aupuu3ku2VChQo8puEgFQSSQA2Ccia2LYnfWjd66Dj6/oObjZWNIEjzYlmxxNhzGGVJIZl2xmAhFmRCCSXQqT7lqP61q6YOG86ZQLHI8hJ2tHCWlcNca7AR4pmdI0YptV+V2rfNJ6n/1kfQn0571fX/QXSu8u7cGaPJi1rS5cY9vduZ7EwyGQQ6jlR5oyYxGsSSnA3OjZBCm4m6xE9Tf63f1Md7zh+w9B7E7F0OWJhiYmQmZ3NqmKryywmbMzTlabihxJivWOdMVkV8cP+oDb21Nj4U+ItfbI+xrYuHZm/et2bZIKncgxcIwSRMz7c4zVfG3wp0h3tr1P7Sh80WbL6llVgso1yz5dwJMMmBHmMAz0zernqNDh6DqgyJ4fI0EjDIDBC26Mh5n3O6pEaQ0NxUBIk5bri/+tzvfI1TvTXV8suTn5uo5wMgkEkknnlWR5zQZm9sjPuSgZGugAemP1E+v/wCqv1JxcjF1z1R1MJlBllh03T9J03eCriRBNjYDSoI5FEsLKbTYrkb0VxiLm6tqerDI1DVtTzM7KBciXNlaeR2lZiZXdi5NRorEe0UEpB9vWu+G/gjV9M1R1evuWmuEq8WmVwSCDt7wIEE85OSeMN8ZfxJ0HVuljpnStNqBZcOLuq1h2lSYG2xas7XO/I3XtwQkE43RDH/uD7ImJgjCWyu1IzIAopXpiVSqFG3PHPTDNgS4byMkvkUlj7EoMzW33FVYVXwoNmj+B1PUhZ4nnam8gX/qjXc8ayBU2GlZvJQqqAWhxXTTnQgRIhU7jbM53EFnMjMAGJ2BUChVHG2/9uvSCpJGCIAGJA7AcRkd/r6V4ut5N8BSN/mLknaDxHsfmPzJjzpWdaYA32WeOLeCAm5Z3O0iuS3kCgnhT7v4Alea+Hj5BgyUMYnjaWKViGKlWZmYH7jS0AWJYk3ZHVd6fHuyf0xADWJYz/8AmbgwJCk+4DmyP4H8gdWzrmknU9GgzY1/4nGLRSEM25/EsoZF+4FOObI4FtYA6ktsSPNG6TjHFRum24oVtgwwJPI459Ocz6Uw4Gkwz5DuCrAq43owOwFXdS90BVMx5+SCR1DtZwJsLJfepABCklgxAN3ypr28En593xYI6K0fVsrTc6GOYskZPIYqaO9Ceaa3YmRCT7ipq6JBnevY0GpY+RlftmVEAoe0usgQkgLTB0Y80SoIYVYY9IwBmPvTxPPr3/6inG4bF07wzi6toIQTtMhYIjB+fb64qPTMg4+oxn8lw1fhiGUglj8naKo88mhfTl3CgJWVqDPHuJ3FiSPwL93t/I+Lr/fpoyl/TzI6i2SUMRRsU4Ffim+RwfwfwOpNrATJ0nGyI1L0rK/IYqz2b/2urs8WBXPR/QR3HbuMip3XZctPEMxCj/5BowfoT/Y03aFlNGjHfTIyNGwANNuomj80t/i75PPWbv04esfcHoh6j9heq/aE88eudla/h6xA0Uhi/XYqKi5ml5gj9rYefivPjzI5ZSZtzLuijBwN0xjFIgKsApI+BtB31/P/AIsfjq9/T7MknyRjIFlEsEkbnmwUHmojayBSFALAgkcWeB1C6Ldt3LLgm3eUW7i5grM4GYYTggT+Ap63XsXbd6xtTUW7yXrd0gTbZCfuEiVDgkXII3rtVpAAH9I/6MfrD9Mfqm9KofUfsDVmd8JsbB7j0Sdkj1btjWkx43yNM1GKxvVWkQ4OYjzY+RiRqVlWRZo49g/anduBlIXjkjZ29wdH38ksFUEMdpYqTbUPg/7/AM6D+nd9X2sfSN9TXaWflZmQfTD1Jmx+zfUzRPJKcOXT5J0xsXXSjOfDmaE87ZMU8TRuMbz4IPhy5lbvr9P8HE0XGXUNOOZl4GfBDk4zSPHlY0mPKDKjxupVm3rJtD7BvCBwaKk+X9a6dqOla5UsBW0twFrb7oZSuSoHdgIB5I5g5r2TofUdH1zpDagyNbaYJdtqBDMu2XJjALE7O3bg1k3m6zK6MjwvIk37jSWSFUAqlDawVqCh6W2Ipvz1UHeE/wCq02VlVosohmRihVWCsQoBKKpDK17SAQb54vorVe75oBEYY0jMittRtzNsClg1LbllDbCDzaByN3JojvXvTOBjx1yVkWQ7PGm5XlYMV8TyIym5ENxSbdyo3vPA6y/UL9p7Z3m4ykrLFD6jAYiY9Mx+FaTpOk1SXrNy0lu2wckBnEE4DE7jmciIIn5U8jJgwsUz6m8GOVhPmjLI4KAAF1B3VsNVRAPkb4Io1B3B6gZmoz/2rS5RpmJG6iXP9qO9Fl8i1aKpU+0kkHabquqj789QO1e0MHL17vvuTTO2NJxY3klzNY1MYmHjotO8hlypIo44zzauWVTUbObRRqK+qT+rV6I+n2g5On+iut6N6q93zxy43j7by0ztK0yaykcufquNM+OrLIp/YjZ52eoxEli+Hbs9R1937NoNJdub4QPbhCdwUKWcghE4NwyD4c7fNE629qel9ORtV1TV2VW35irjxRbMCRbtL5nYEzbABUNBPlDTty7u9SZezsfIzP8A1dJkywxTMYsjNWHHRk2uTucKZUBRWcKKF3Z2gHnj+v362u1O7Vyu1v12Jq2qKmTBHgnLiLS5LqfIypvqQKTtjJo7WBoH50sd9/WL68+sHfmoT9++oPckME+XOdN0TS9VydO0zSDM7LH+jhxZIHcpdB8gzMVYsHWgDiv32dfxdf8A71m6pnanOsqlc7MlfIkZXWN2SWQ/cEKbTZdiaLMSOt/0L4A1Vq7ptT1TWFwgIuafTG5Fpw2G33yy3wuDNuC3bOK8561/FfR7NTo+jdLd7l0Nbt63WNYU3bTrtZls2QtxCZMLcJAjiDUN7nzcfI7m1XLlwpsWOfPlmOEwZJIY5ZL5O08EWbCilIO6uejo+2cTK0+XU9Pych40YHhrZELEGyF2DxgKGV18lbm+xT1YOVp+nd14EGqNEhnMBiyZowzHy+ymJCEAKCvCsLjHBHwIQ2naj2pmrkQhpcCcbpcdS21lcSIVMe19xCs6bytMHYX7j16vbsi1atWkkpbUKC2DHAkcj3rwm9fN4sVA+0BmZkkbCCxJQNzuBb1mIkmh9Ph1eKIzadkRajAiK0uNGGEzRDdvIhG4e0Bo5KDMY3exwtueDq+PkTxQSg480ToypKFIUo+8xsHHvpogwbYpTaNoAsdJZ2HNpc0XcWiO74s9zy4ys2y35kRlY2LcFGIZiLYCkFdPGNBpPdWIcyCOPGzo0UzBW2vEQ23dy53oHVWW/wBwHaa2hasVE4cjxCJQkEXMCA0BUdRADA44H+JFChnigmWVjZtWRgFLWTvUsEu5FElgV7uOfh5WISw4zyIgjnMro+/dv2CSOVBGpNNvQsVCuw8m9gqEHqvsE6hoOUfJIcjEmMglLEMrN9xNh7V3RJCWYA7trgsVHVl4rRTxrPjhfDtWTcGVWE7HgyKdqvFIGcAEOylwt7UI6Khb70HC7c//AG8pyeR3/TmvkCJcSoJHK0vOwRqqk0y2G2g/B4QXXzzTzEgKkySpAxYkr5ACwNU7B/8AU3ydvtPyPz03LCgKK1nekhDEtYUgnaGckMXYAoZFZ0237QV6XVXdVJ9oCqqhlVvaoABUyRltp+QPiya6KURGOKxbSxsRrHP37ft5tXfcRt5FEfnd8H5EgjMISGAQLLlzyqiWwZTJMxRttE7VV5A29yy7FOyl+Y8GCFlKFmVuAgVbIJoKoWwCDbUVJABsEcHY2S2AseWyq+ZK5XGWQklSaCFm3MtIzXfus3fHPVdP6eeBwSD9CII+hq9fDBjtALEgQwBX+kEEcERM81PDj6Pp2NHNl4seZqNgwQRq7QhiGIdXYvuWLfw5VixYICpDdTPtb1O779PXXV+3+8td7cvwph6fpmsZuMmTKXjnRP03lljSNSu6V1jHsULtCkhq5xIZcSAZuWGndYmkYykIA6urAIFsCJeQFUANZdiT1GJc7I1bVY5JDuSIKIhuG2MEEWOKB2BjtH3AG7+AlyxacgXLa3Rj/kVbhAmIBYEwfSYkz3pNLe1Nq8bunuvYK/8AK9pjadBjFt0IZQSCYJImTyazo7S+tH6pseLNdfWTuyLTJcfIjynzJos2AB0CSjCTIhb9OMdVkWFkEcXkdmKiqNM6t64erOpagdYx/ULvMvkyv5MqfXc7Ied1lkiMpTInmEK7jkPjRptaFXZ42AKnqEa9McLQ9N0WFismTjpkZJTedxecs0ZABVVkV42Ht5UE7gvUShDQ4uQZWchFMapGSNzubYKAQRtHjQNyUjUKGG990DdO6eJI0GkDXPvsuntqXjiQoAxJjHerw+IesnYw6pr9tt3XTgau+oBCqrM8P5mIZZniBCjMzId0awomzBreprk5iS/q85srLXKzVnliMj5cwlMsxkIkZllkINv7Px0L2rJ5583Llcyq5eNt24M0cjPESSTvd/udW3KTYtwwUgQx/wD8POzySlYYmcsg9qtaeTdsARvNkJAoW1tHsHyKQZ2nJJBpMr2jM0xeWgGVmxjMxjY0HjGxYyyo3vVwAAzAm0mnspt221XaABtlYA4AIiAO0RFc37TfuW28W9dul2g+LduXBI5IVmKyYyYySYwSKbodk0GSvJj2lYmAVUpYkCnaQWTyM7vIt0NhCgA8j9uTu8z4LybpGaWOFiA4shJVa94tztQV7r8IBAAAZzxEk/TyOylpB7ZG9o22AqMNzEMCRzQFgfzZ6ascHBzv1aOC+5ZZS1KSwSA1EzbypPlYGyQBQ5PPT1tqsRuJE5ZizGfUnJpiXQBdDE7m2m2oACiNswBxwZ4mMCk5UfHkmgYS74SUDW0bH3EliTdmQKykA2bIvkk+mjqKFFe2MqMw33HIoZY6IbdZO37SpFGwKIIkOuxRSZWHkxVsykiLU3DEeaVnIPN1KR/NgAgUwLNOiodyuWjRopWADAAbzytMd1IY9wIXkGgObfUZYkkkmIjbPlGOY9eD+kUZLJGmFj+MbAJWK8cKviAu2a2ZQV2llqj925QQ3ZEDTLFKjJJsj3MCygMfcNrlVJJBUjcqHdzVAEFeRHfCjI4RHhcg8qxEe9SSS1KwZAwKngH8WB9x3hTEjZgFYgAstEK0cXv4oft7mJJVd274vnopKr3M8uPlNkRi3jfchBoUpYqOKq1Mf+SCCQbJN+9ratBrmheOIxeZE2TpsDNFugK2ok3WZC5F2CQwIUe2qZ1LGZnllQqdrIStAGQsig2eRwUHwKokfNHozsPWhoutCGZmXD1IiKYm1VJCVEbpsp2KiRVJBUAAEk0SY4CkRmcGfpUs+NaUpl7YAljnbAye/Md6I7y0eXAyfPFSRykyQuvsKuG3FC26gR+Vs1X446P7V1mORVgyCxicRxqRSXIwYuSBSmySLPBUWfcAerD7s03+44GSY41ZuZEssdrRlmKmi7EMoABerofJo9Y/LNNp+aUSPxl2ZQGo7HUkEruLKpBBUgKKN0QenMAr4OGnn3AOOO9CFbyQYNxQIByo2wMSPwPGJyKlHdOjnHL5EYAimcyIQVLbiWNCrN7WAI2sP4AI5atGkfMxsrTpCAUV2TaFB3LGGWvJYLEqi0AByxHww6nWDq+FqenHC1NTsjCL5ASCGe4zciLS8lCCNwpSDRrqIarpWRoOZFqGETLhM6FMmJiFdN3tRmNGmNLyOVdnYnb01wJU/wDu549Yx9M9/akDM6tacxcUeSDETBBEmcgesSe4zUPk3QSvbMiu9BwbZWViqmwVBLA7TyLs8Ejqa9oa/LpebGjSlbKoGMhCyKrhj5CZD8fILFkNVtBO7pu7hw0AxtQiG7Ezydhr/lzH3MvB+VahfINGh+RHPHII9xILwSgqw2/6fiyQeCeK5BPHIPKFQGAzmP1qyAtyza2EglR5sbwwwRxHI7jn2rJPXcibL0vTNcxXDPpOYrTW7FjHNKrmVWFMtEwlitUqe4gWD3ff0+P6lnpBr39P7s/1B9X++dE7Z1r0g0OPsHv3K1vOx8PJOb2zh4mNpmc5mmaTIk1bRnwMlJFTdJlfq8RYZmxht4GO1tdE+jz4mU6SJlQPFMGKig+P4yz2FAt9gOwKKFjaRfTpo0ub3B2Rr/ba5Uz42Is+r/pRkS+Jp8CB4IMkYikRF42NrJXkjimyWjdAylOd1vpVrqOns2w5s3rV0Ol4W2uEqRsuWsYD3AQUkQSCTgQel0HrLdB1Ooe9Y+02NRaKNYF0WnN7cGt3TugMiEfzAsGCCImR16etP/4gL0aGfknsfSe4dd7cxHSD+8QaZmRxzFZ4Vinxo5mgyfDIGXdJOmNIImJZQ529Yd+oX/4gHDbSMmD099KMvUNVaGQYupdx58WBhQ5MkQVZ5cSH9XO3vCPsj+G3lmtlQc7faIj1js/JwJHcySTnHl9qtGuPlY8wlkYklg0cxwJwxVvep/0qwFP5cWZgyZeLOGjnx55YpEP3iSJyGraQKDj2n7WtaABHXDHwT0ZQGujV33xPi6gxOCQUVVQiRBUgjMdq0J/iH1u4Li2U0NgK4VUt6VW2jaRKtcd3DST5wQZn1M5q+tP1netn1Ld24vcXqz3bl6rpUOS76Z2zis+F29oyNK8aDH09A2O89BA+RktPOQSA8Qd41xwd27e7ojy4ZnkwM7KLNLW8t5JCx91r9oplYW22hwq0GvScWLUNBmVWcZEQEi7lJ2ORRWtxJ8gBI3DarKxsEnoH9TNk4Hhfc745DISTasrHdtHwu8LtWiRRYNbBa1Ol0tjSWbNnS2k01q029bdhRbQuwhmdFAV5BMBwQpIZYYA1j9XrdRr9Tc1Gtuvfuumxr1ws9xUxC22JJtgEj7oBIlWJViKlHf8Ap6Ry4vcGBuj3MscrilBKbGiZVjJbc8JQMQa3pIBYrqQK+LrnbSw5EfleaISeQMGZGAJlX3Om3cWQCzdptU2xBYtF1ODWdHydKzG3PGrSJuYFy8QQIY1ALKA6JzuO02qgBmBC0HMfTMvJ0fKr3mkaQ+17COqtsVSlsRZB4X4DcdWcSTGWO4+k47fSuc27Yo3MHshjIwxQ5GSOQJ9czMGmXtzNl7e1ifR8x2GJLO0MXt9yMJVMMxcl1ACooYe99rAWST1PNVxnni598EilWVtpCOvsQBm2cbgd0p3Blo7b6i/deh/qMhJ8eKx7SGSRy1jeS277wQiqSLJBPBPCh57c1tMyEafllVyULwLJKHZWjj9rhlMfIdQSL997gDx0UHa7i8rEFgNwGBIAHpORzk+0DFE6GsZxpsORRJGCyIrEhijbhYHwAPm2IsmyST1AtV0nL7V1BdVwHfxs6yurbmBQsLjkrcj+wlNvwCd3BAIncmI+mashRgEVm2OpK2HbiN1De1pF5VvdQB4o8vmsabHnYboyLImRCxYbCuyQIfGVIIsMgUMatiLJViCCguUdWYk2yTuQny8YIB7gximPHkxdbwI8mGJmEpqaL/VjsJPGLNOdhEjNwt/9Rv3dF6dLLgZEeIxZFaMSKtkLsaKmDAM1j2b7G291BRwvUS7TyMjRdUmwJr8Xm3RGRiEUGVTQPJao1AYvwEU1RAcWVqmEJIlkiIeSCUEFQQwVQGKAspBXYQzKaO1QVNEqxQdrAxO05E8xyJxzHOKVfxPK6gO0UZKB2JXcrltri0UK1EGr2+1a2sxLOkEw8SkOJQ25rkhG9CXa4ySrk7CDXIFEACgCW+FDmqr47+NKiM+PJJE/jlREElHlDubybVkCBgY6JZN3TpEMxwxxpJIot5CrEks60AApDzRPJQXaqqzMQqrZJJJKjLFTtEQPXn1rFjVsM4Wv6viOHU4moZ8Do4KtG2Jl5EPI2j3XG6yghfcCAqjgp6RD/dNcwYSjGPzqFjHCJGp4skNXyP8ALELwPxcH1J9pydjfUL64douoiXt71S760uGJn3mODH7l1T9OjyAASsMcoslE7ZVkG5hsZq27EjRtZXIk/wBCSmOQ/Ksg32E5pa3J7v8AU5HG1bgtiSgHcKfxANdG9cI8SMHc0H083P4e9O3eOW0L/o4ikhRVQlK2qqEj/wCFgQSVZiCWAHFAExntnCTN1FEKsUZ0DLtG0LEPI4tiVJI4YMpXYSGBVq6N7lttRk3Ixa23bjZBDEkAiq27a43bqFmmJBHaTR465eQzEMIWCO+xQlsFaT+SQr7NoJsE1dUZY3GeNp/Hj8PzqM7RYcKZNzbLdpEGYniB2ij9fzEytWZ3LlEbwRjgiREJZ1Xx0QGLm7Y8qXO6R5Gby0RGnQHaWZ8oyvJuA2rsaPaAx3FQyF3QVtDRE3u4YisrzR27SmSZhQ5KOrRgt/uByLNUQS13Up1BzAMWJ/G0oKM6VtJfxwK8ZItN1tcgBtWdxRCJuRTLMe0R+n+KgkbVUCAMn3YgBjgCAdogSeKUnleLTpUQkMIoi4UiwE3Sn5IY7JJG3RgHlYm3Dxiy+2SU0nMThgJXLAkRhgVV1JdwF3FkRHIJoLt4Pw35cshxpbqNlCkKASAGeYfcQB719/P+r2jgDojt2W8STFjC+eSHcU2lireQqhlQUW84WVNxPtJjChju2PoT/i+bf5/cf3AojHk3YeTKpHuliiApZStSKj+xSSQryIDRAO48ijY+IP1ePqKuI1ETKsTFLKsoZlLNYYo3O40Q1RWBsBZeMNFj5mEQ3kRwisWfyK8ZUsyJwP8ApBLH3OpYgcdB6Y5MWSxbYjh3I3BmO1LIUjlFG0ogAbn5I5PRSV+GTu/SwTC9kWwuQVKmQ+2n8oLNuIDEqNi7vaaBHvVkaCMqiAm0AZiCJIBHDvf4IcVMNtnjYGs/htzhtjE6lwsbqtOKBZ5UO0luWOwsaJBtT055rNn4mLSpHUSxoFfxsWMcSJYKewM3jjU+Q20m40AASivsSH9CI2PtJjAAvcttSISSzH2bVsAICOdovofNMC4TogZnS1LIvwzGVF91hRytkhyAT7qALdLo7R48fCsVDGRfuBjQt4laVSSSYjE9tsLGl28GmyApkZMkZraqkiMKxHvIUBGH3fJNqCp5okiiUULiBZ8f3sGRt21n+60PyDww2/NEEErwfyI/qOCcYQyqeUcU67uSCwrcgtSWjADFQGv2mhRmcuCIMbcFCoqkWTtU+Smkpi7XwpCg7SbrhQR1F5ZzODCw3WFXi6Gy/cCBbEmzR/7cdFTaefFH/tKvu+USPfkVdXbGqJqWhYsuRKjyyNPG0G5TMvjEca+RASUErSbkZvvAkP8AooxfufsSPOMuXpskZdpN0sS1G4NEmRAhIBc0Su6+SKPyXPsnSt+EykxgpFuUld0iy7MllDABjRP5otuIFURbLnZ/cug5WTW7KgaaQBJImFbW+a8SlRQFA3xZ4tQUZQwg8VGPveQhRPlkwInyj0zgAce1V5EZtPE2nahG1EmMyGyjKCfGxIoja6rJYZixqqq+nOOfUcfDyMXZ+v0/IjISLkvCqXtKBmB9g9optzA2VAupc2t6Nr5MWuYcmDL42UzCIqGcf6k22gUcVbAkE8A9RyXHGmT+XS85c7HDMEiJCOsWwqQwJJYEvxQ5YKbFGgAKABwKkYvO47FLQLjMNxAAABRgVgwAczn83DEzdK1TS00XKU488cREDOAqLIWsOS+5y9twAaVuSxBI6YZNEyMV3gCpJjMdomvduIraAygLyBuskVwDQthLsefTdnjzYsYyTxjYwQGWIjcDvUK7KKdaCt7gr2AKBi2oZ26Qoo8USs4pePJUioSy0pX/AJQCIVUpuIJNkFYU5IJI4gx++9Mnaw8MeGCQSSJ3NiX9hxjmQZptiin0x5TG5MDoSyo/tAJG4Angn/4RYvmxY6Uwu5svt+LVP7durUMPJxGeRxujGUnjkKqGS7U/bZ+OKuwnlZO7GiivxiPc5UBRvWQqu35sMA25fwSSDQ56YQj5OXFGBvLEKAosseOBSL/tzXI5o/MLNujHFT2yt5C97G0s8k4/lbdjR6NuaBnIyZMi6/TLLjx8WSPJcrFkEtZfezyH9tAqhgSyAgkFloiyRVMl6haXDHqy5aHeudGfI4BKmSFVRgGRVQsyxxMWBXlS3uY30BpxOn4uPAzMnIdXkUUfNFMCysRHWxlXYo3EX+5RvqStImv6ZkYso3ZeGq5uNsZTJ5SKkULJssuXJCixSKBXJEw4Ecdqqm6VYOv9TdvQy2OfSoT2jnHCz306RjslVvuIJLIaAJJIYqpY/avDDlr29E6jiyabqM0TIFSWR545CT/ymk4PLKCSysSAjBAGJ+OY1lI+NkLNuqaJlkJIYMVcsRKRtoh9wYD5G6iAeBZEUkPdekw+1P1mAjmKw24wM+3aRYIiDkOx5KsQKAJPRUl0kOXg7bigXGAEAqRHpySo7ck5qGhJcCVclGFFlUFSrLbEcsu1a4BPyeDwOeJFlY390gTOxCn6zGADxrwZRETtNqQUYBgdtMXjKcARDexviTI8mJOHDGRo33owlDokSoV3gKSo3Kf9J3KVY/j1p+RLpuQiM7CMqzg7SQW8sihWFo2wou0tW0naLK2Oio1h5KmQATPrAmP7fOpJiam+TBHBlIEdVCliEHu5DIGY79x54/J+KPPQOZgTxTxZ0TqaKB2jG1ZWS1CSGMFTuVZEZgqsZWF7jJXS80EWoRs+KNpcLRRvHJcfuo0hBKuLsoRt5LNyOi9IzWZJcPLUvTASb7LIS/kM/G0glgjl0YIi2WjUAMSivseos6iDPkSZFLJFNwLZQZFBcEMdgBjXcRt9wMlqLmen5UeRDJESWRd8axoZJNqwgsHG+mUvY2tW0KPz8dQieJEleAxgxmKZ1Z2V3JLbfcFVgokVnLFCxZwrkLuIHzScyTByyZWZ0DNGkkhVS5CkqhYrJscAEA7bAIBI6Kay7ozx/qltb01o8j9QGkJMktDhQyh403EKBbLHIUIEg4XaV91iXadmJk4njSV1kSQAyHycrRYEkgEEMQnkUmkWgPx17yZItRijaNNylFVlmkcoZHibcy/6Nymlb3UWAIG4gADToUic7CARKF2fGwMGeJ2vcQ0ZbbRB5PIPtJKRWmBHA/SKkEUQLO6GR3lYK8cbuzMFFsV2H9yK6cB2ZXCtvUbLL8ixRIiku9KBaSxwkbP26dXCMZPZuZiouxxxZacSF5JQgBhmVgI2QUqllkjLALuNMXXzAD3xsQNprqX42OssW+4UYsxdTjyOA7e5gh8wpAWoLztorZqyUx/vH6foKmP9VPsPI7F+t/1oMuO0GL3jm6P37p5MTrHkR9zaXjZGfkQSuoE8TavHqURaO1jnimj3Fg3WCnaTwwSyu5IfY6IoalawzKAFIZiF5YchTyRYJHQL/Xo9ITj6t6O+tun4kzx5+Fqvp5r+SsamPH/t0765275juV0nyk1LWgiyLIFXT9rTSF2Cc8unOpO1Qg2AuxLWm1l2vGrMCCSCabglbPF9VbJ3BIkwFU8SNoCkmOxj8DmurqkkuQB2PH3sLMYgnnmnzVcV5WnyVLBNmyyyMpkG4OQwUbj5LvaeAQ1kbSQMNlx4FEgdfK8bkKxKkqL2AHapLWPdublr20OXqaVsxYsdAEdpCGQ+0IkyhVBIKqFDAl7WxtAPN9McqgqJCQjQR7NhatpBAV9prkOQvxzyL9nVkkDsc+n0qmoZkKgFZYQDj/2/2x+RpXSlE2pQOiyqIpgyqpPILK0ahWIBosGkLWQA3yLp01SUy6i5ZWuKSlVWolVmtG96+OlUqXP/AOaOAzEdBaTG8UqNIpVpigRt5DRAqV/KmiQObFhSPj8+md/1yu6ho42sAlvdvaQoRuU7qEYQcgAtQIJ6SQigHmScd+KY2HI9gfb3/WveqShMVgAGDOtsygOQY5Nu3bVgqH+21FlRVi3ztorBI8zOSrwMqBSSEcLyHB+4kTFkU3tYCQgC+mHN5kXH/wCWuwu5a63UoA9xIUiSV2/AC/wF4cMGSTClMW9tz0VA2eM2JA5BLUHKtGBYHBjABBUlwyAfWhcJt7zPtTk827JlFhHklLEspYRE+1qZ7Y7LJZvG2wqLb7QUo1ijla2IidIVlDDkCQ24Yqo21+4x3f6XVTVi0zI4nZ1YFJN9llBYLMTuslhTq0ZLAfeLs8Hr26IzTAhg8dNuWgrRAu8jUaBVnkVQwsEqCKvgor5mYqvjyhCrBmLAsGIEglmjLEKC5pJIQQASp+7aK684cyRxRwSnayhQoAIYSIRGSHLAlNsMbA7CNwUcMpPRsAEuKsN8hZWLH3MzSIGsgq4ZzIg3igWahY4BYc4SK5yQAqwsi2tKNqDyEit4G+hYFk7ifbzRRTrklXyGSqU+MqNleXaAVILbSTbJXx+AvINfII2gyZrC7SUKk7twBSRV27S5ARn3AUtEWAaJCuCY82ONsgBFiJXaR5GKR5AKO25QDTxojKylWpwflgFcqIHJE8FnjySIr7FTYWdyFLbQoZwtIoV1cA2AKKKS1WWMxzQ7WAkjFO4YISgL2p9uwSK0YaiWUNVEqQBe3NITUJ3DWVb2JttfCESIliAbU0CPnmyLs9OmZiQzgRF23oCULAlfdsepFLPyaRFB2m1aj+OiMDGkhXyxMoliJMdABGImYli1ke0JHW66Bon3AEpQSOCR8iR+lFY652jTuYg+1JyAoUKq7WVmWQqsjBClAEk7+Qpvjp93wagglyY0UxrUlD8BSv7jtsHKIyjdHuDmNi4CgK2/3GGANLnusdFCzPQRpVZb3bR/BVr8Z4YNRBBMU7g7tTNxZsLTUEEGRLIZchGaKSUhwBGhBRhGN8m+Q7QU27BYagkKJkH2HPb/ACKQKTACCJH3gNse2DBA4xQ/cOraYZxi6VCntZlyZmCttZVrYgX2kpTEkkcsL/zGBlQRMfGtgmMyEhSzCkZgL/BYE+2zYIA56ECgNJcjEqdiqTaUQCSrH53lRzZvn5quhpUYPTVyrHgjbyVYkMBRZQ6qV/FGwCDUJYyYJA/DtVtbdtvKWgbC3mMAgcgCcmeBRKZZZ5SpG5lVaTcGNqwN+0M1sw5o/wDUeAT0gVklYyMSACTbc2t2QQRZs3djn88G+vSRMQHUFtu4MoKufaEZWNg+23IvkWhB9wPRrAJD81vUrWyRVVgygg7QF3kM1bqBq7IHSAsSBJz7mmbt3lARgsqGOD29jxj5RTLlzhyI4wx28RrVN/8A4n43AVdAVVgdPPaulPnZDOLG10phdKmx2LItHksBRNCzwbFdB4mA2RNJGyH5YLYYHkcEEivbyxsmhyeKubaIo06dlVxThQAyU3Kn27QHVSOWIVyQATwCSHqhDKTEDkc/6xTHKhTbkMcboggiBz3jmARnkgURrkjTMkUbEeJajViBatJsAfb9p8zW22yikWPaa86HqLYWXiSk+0sBILB3IrgkMCwVldm2jeeRt+DQ6D1JgHsAO/kG1UNsQFVyyVdG13cKeSSeLPTPjsN7S0EYSkqkYDAglWVfg0bG8gflvgA11JUS5tjynDAZHtiPnGPlmKszujQEynfUNPEZgliMrIpHtq2AUJa0Hl2miQNpWvbQr3EyMvSMlpIy/wB7qiUa8Pw0bkKoJcF2+WUAXe9VU2V2jrsU0TYGRJHuVGMZlUAEElygLAJyHLBSfcykE04tTuDt8N+7jxHZI7yCPhqEi2qgKCAd3s20DbEVaV0UltiGuB9xU8A5U4EQCYxz9McU0Q5uDrSGlx4MxEUCUBCC1lgSxAf3bQQxBJI+CfhDJ04tOY5UBWNHYN7r94lK7bQFlsMwoniyRyOoqIsjS8oTqrARhbUiQxiMFaUkAgsGIKA8kFiPyep3hZkGfhkuyrJGu88vvLISirtYkbpDIbscqTQFEgoA2yBAB7DAggen51H8KHJ09riltvIyDxjfIisgDBluzHscbmFsADt5BpaPLaPIlnRKBREO5WZigKI5QFfhmQKaB3q90Vs9P8uNPzR3VJGzspRULoLZDu8e4hXDAKdzeMr+D01vhUUlYhgEMZjugEDBuG9vA4PywUAi/wAdFLTk7JPArhVKOu0soNKXogqoFRupBDC9oXZtLCyAPE0g2ObKU1sNg/dJCsSCFJCig5OwshUGwwHqAthzbY3cx2pYHbYDJtClfcFdhRJ3MHA4+OinXyLEpRVkULCSjqLVQ6xsoJAUO4ZT5PazBttmMkFFEYUjxhRG5ayGBaXY1lR45NqllYNR5a6K7TtkodPiK9xPJuErwLIJEprRaK7wEUOGXgsGLUPkAWI/jbUyYxtJcShkRSogSVlXeCw9ojciiR7eGYEjcen6CgoDguY5yiILoNzsWxZEZe044JILcHoqIsIMAgnvAHf1FSvT1lyCojRUMrBt5ZmZJQd7LtkNU4joKjUaoEkjqwcfHleFPHp02VsBSR4IwypKGJeNzRIe28m1qZUkRWAI6hWlwEYcEqyB3/eV0YC2ZiEZXH+ihvCVRQoTwTfXQT9B39P3H9Yvp/0z1D1vI/Sy6/3BqsuEmXG80r6dBiaXjwShzFLcbyRTmMhgGUb9iFioQkKpZsDcFk4kkA49Yn5+1PtWvHYIcQNwKzJAYDJGcE45EfhX/9k=', NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-29 10:30:21', '2021-05-29 10:30:21');
INSERT INTO `messages` (`id`, `message_id`, `user_to_id`, `user_from_id`, `message`, `media`, `audio`, `latitude`, `longitude`, `album_given_access`, `album_has_access`, `message_is_read`, `message_is_sent`, `message_is_not_seen`, `created_at`, `updated_at`) VALUES
(99, '05A2C100-92F7-458C-8138-BC32516E7A2F', 38, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-30 03:47:36', '2021-05-30 03:47:36'),
(100, '9CA1E251-DD6A-4DE7-8174-D2FBFD212CAD', 43, 44, 'Hi ', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-30 04:23:02', '2021-05-30 04:23:02'),
(101, '97D39FD4-F2AB-4705-BBD7-4014817903A9', 43, 44, 'Hi ', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-30 04:23:05', '2021-05-30 04:23:05'),
(102, '42C93677-9AA8-4815-8E68-1704EC1F765E', 41, 44, 'Hi ', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-05-31 04:40:04', '2021-05-31 04:40:04'),
(103, '44da3425-3985-4659-8bcb-e764713488e4', 49, 47, NULL, NULL, NULL, '30.7111207', '76.7054123', 0, 0, 1, 0, 0, '2021-06-02 04:54:55', '2021-06-02 04:54:55'),
(104, '0ebc3a4f-4aa6-4081-8932-173a23605266', 49, 47, NULL, NULL, NULL, '30.7111207', '76.7054123', 0, 0, 1, 0, 0, '2021-06-02 04:54:56', '2021-06-02 04:54:56'),
(105, '204afaaf-3393-45b2-84bb-b91294deeb91', 40, 47, 'Jgi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:20', '2021-06-02 04:58:20'),
(106, '6d7ca808-758e-4358-98a2-6c0c24ec7d82', 40, 47, 'Ty', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:21', '2021-06-02 04:58:21'),
(107, 'c824e60b-bf8e-41c8-8460-04e91516c109', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:23', '2021-06-02 04:58:23'),
(108, 'fb829728-9272-45fd-97dd-1428ca209ded', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:24', '2021-06-02 04:58:24'),
(109, '0435dd55-c1d1-42f0-80d9-987796ec4f6b', 40, 47, 'Yyy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:25', '2021-06-02 04:58:25'),
(110, 'ea3e3cdb-4cab-4e8d-b7bf-bd35e1003e20', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:26', '2021-06-02 04:58:26'),
(111, '3acf68f8-b257-49fa-b4eb-cd3e4c04440b', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:26', '2021-06-02 04:58:26'),
(112, '9eca3868-18b7-40b1-aedb-1e622a6c5307', 40, 47, 'Y7', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:27', '2021-06-02 04:58:27'),
(113, '97d670e9-1815-44c8-a3ef-4b39c955a07f', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:28', '2021-06-02 04:58:28'),
(114, '118ef26e-89f4-478c-8270-42e4253947b2', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:29', '2021-06-02 04:58:29'),
(115, '5a1bb0ef-744d-4903-adec-e34f9561c46c', 40, 47, 'Y', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:33', '2021-06-02 04:58:33'),
(116, '1fe0c1a9-e910-49f4-bd73-bbb3fb09d535', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:37', '2021-06-02 04:58:37'),
(117, '68ae1020-2d59-42af-a186-2228f604bf17', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:38', '2021-06-02 04:58:38'),
(118, '03719265-ec8a-403b-acef-181159749839', 40, 47, 'Yy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:41', '2021-06-02 04:58:41'),
(119, '24abc8d4-5587-4340-a8d3-1d7ca5be4328', 40, 47, 'Hh', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:51', '2021-06-02 04:58:51'),
(120, '24b249c7-e599-4a24-af2a-7d896d850401', 40, 47, 'Gy', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:52', '2021-06-02 04:58:52'),
(121, '56f76009-016e-40a6-98af-fccd6d89d5e0', 40, 47, 'Hhh', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:58:56', '2021-06-02 04:58:56'),
(122, 'baa24e19-c297-4990-b121-efd774aeb9e7', 40, 47, 'Jfuff', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:59:12', '2021-06-02 04:59:12'),
(123, 'f4b4a872-e66f-4ef4-ae11-5ae3dc6f62b8', 40, 47, 'Gjjf', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:59:15', '2021-06-02 04:59:15'),
(124, '3760d390-354e-459e-afd4-6d8f0381ee02', 40, 47, 'Uit', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:59:18', '2021-06-02 04:59:18'),
(125, 'f8c7248a-63a7-4f45-b1a6-6fab99d57e0d', 40, 47, 'Giogig', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-02 04:59:36', '2021-06-02 04:59:36'),
(126, '42d53a52-2523-4185-a863-260ab061c12e', 50, 51, 'Ggg', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:21', '2021-06-02 06:49:21'),
(127, 'f8015983-6599-42a9-9820-78ba2cca63eb', 50, 51, 'Sa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:25', '2021-06-02 06:49:25'),
(128, 'e164a994-119a-4d0e-aee0-4d17e2794cdb', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:27', '2021-06-02 06:49:27'),
(129, 'c7864c6c-2ec0-4675-87cd-45afba369a4b', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:31', '2021-06-02 06:49:31'),
(130, 'ec47eba7-5948-4e65-b39e-5f993d0da5cc', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:34', '2021-06-02 06:49:34'),
(131, '55b582f1-f467-4280-868c-ed6b821cb491', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:37', '2021-06-02 06:49:37'),
(132, 'db8c0c12-9136-4ac2-ad22-65d8bdd8954b', 50, 51, 'Cc', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:38', '2021-06-02 06:49:38'),
(133, '6d6efd95-6bab-4515-aa13-db2ab61f27ff', 50, 51, 'Ss', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:41', '2021-06-02 06:49:41'),
(134, 'f8edfd20-a7a3-4817-9a54-a0593ad65c9c', 50, 51, 'Zz', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:45', '2021-06-02 06:49:45'),
(135, 'b9723124-4388-46f4-9d61-1462256ad068', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:47', '2021-06-02 06:49:47'),
(136, '843f77ba-cbee-451d-a141-d8b2fb48541d', 50, 51, 'Mm', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:49', '2021-06-02 06:49:49'),
(137, 'df21bc53-4e62-4416-b9df-f865d30ab265', 50, 51, 'Xx', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:53', '2021-06-02 06:49:53'),
(138, '5078241f-e757-465d-a730-c1efd5637083', 50, 51, 'Nn', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:55', '2021-06-02 06:49:55'),
(139, 'cadb38d5-0bb5-4c7b-8049-4cd48cf3bd69', 50, 51, 'Mm', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:49:58', '2021-06-02 06:49:58'),
(140, '477bfb01-81ca-46f9-b7a8-fe1fb20c20db', 50, 51, 'B', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:02', '2021-06-02 06:50:02'),
(141, '959e7f18-598f-4842-bff8-e6e227451d57', 50, 51, 'Bb', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:06', '2021-06-02 06:50:06'),
(142, '80b6d1f5-8c45-46a5-b273-5873819bef6c', 50, 51, 'Gg', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:10', '2021-06-02 06:50:10'),
(143, '467fba77-658f-4698-9db7-04bef98df1ff', 50, 51, 'Aa', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:19', '2021-06-02 06:50:19'),
(144, 'd0d6dd08-e3a7-4be0-8477-0c9c6451bdc1', 50, 51, 'Nxn', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:20', '2021-06-02 06:50:20'),
(145, 'f0b5ccd6-a5c8-4578-8e93-32f15b1996e2', 50, 51, 'Wgat', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:22', '2021-06-02 06:50:22'),
(146, 'c6afed1e-00da-4f7c-9d22-59d6bc8b73ae', 50, 51, 'Hshs', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:23', '2021-06-02 06:50:23'),
(147, '74cf3cdc-bafc-46de-b1a2-803d405f64c0', 50, 51, 'Gsgs', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-02 06:50:25', '2021-06-02 06:50:25'),
(148, '063E9824-6BC4-4865-8F50-193D91CB4E02', 48, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-03 18:35:46', '2021-06-03 18:35:46'),
(149, 'A1E1DBFA-9DD7-4B0C-A357-31FC3B6F1455', 48, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-03 18:36:01', '2021-06-03 18:36:01'),
(150, 'd57328f0-f816-4def-9f07-8ca86c8bd7c7', 44, 37, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-05 04:34:02', '2021-06-05 04:34:02'),
(151, '41EAE51D-A55A-4B64-9F05-78579595897F', 41, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-07 18:07:51', '2021-06-07 18:07:51'),
(152, '8C665E9A-CFDE-4DDE-BC95-BFEB3816523E', 41, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-07 18:07:57', '2021-06-07 18:07:57'),
(153, 'C3FCD7CD-AB9A-4CCA-BEAA-EE98839CA241', 41, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-07 18:08:20', '2021-06-07 18:08:20'),
(154, '6739234b-ecc6-44fe-8fa4-d831f6c8dc97', 33, 49, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:10:36', '2021-06-11 10:10:36'),
(155, '0B528887-F7FA-402A-A1E0-790CFB70CDD4', 49, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:29:00', '2021-06-11 10:29:00'),
(156, '0A623789-0B69-4D88-8778-059C7BBCC19F', 49, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:30:07', '2021-06-11 10:30:07'),
(157, 'b350f862-3875-42c6-82eb-6054cf9036fb', 33, 49, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:30:17', '2021-06-11 10:30:17'),
(158, '69aa09d6-a60a-438d-b3eb-6714ff874379', 33, 49, 'Hhh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:30:37', '2021-06-11 10:30:37'),
(159, '96506EAA-EBB0-4A7C-94D9-5165CB753C28', 49, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:31:19', '2021-06-11 10:31:19'),
(160, 'B171460F-C3A4-4170-94C2-A14105973957', 49, 33, 'Gh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-11 10:31:32', '2021-06-11 10:31:32'),
(161, '3A3F1A72-0D9C-4586-AEDB-9F74F5F6B5B3', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 05:00:08', '2021-06-13 05:00:08'),
(162, '752B2D48-EC50-4A61-9F30-FE5216A0A7AA', 37, 44, 'What’s up ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 05:00:12', '2021-06-13 05:00:12'),
(163, 'B245228A-2AF4-499A-8A6C-A201BE5A033A', 37, 44, 'How are you ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 05:00:19', '2021-06-13 05:00:19'),
(164, '0804C1BA-CA53-45D1-AFDB-3CABD57F20AD', 44, 52, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:50:14', '2021-06-13 18:50:14'),
(165, 'AABFE9EF-37DA-496A-8061-C1E6EBBD53E4', 44, 52, 'What’s up ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:32', '2021-06-13 18:51:32'),
(166, '5CC055CB-9272-487B-ADA8-1A9507FF48D8', 44, 52, 'How ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:33', '2021-06-13 18:51:33'),
(167, '1669EFBD-2F94-43A3-9A82-9185DABDF811', 44, 52, 'Are ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:35', '2021-06-13 18:51:35'),
(168, 'CCD70EE6-A828-4819-9AB9-154E019DE71D', 44, 52, 'You ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:36', '2021-06-13 18:51:36'),
(169, '967A27CC-9BA8-4273-9610-881FD3F15772', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:41', '2021-06-13 18:51:41'),
(170, '5F506A65-871D-47BB-A142-0548AF5FDE3D', 44, 52, 'Hey hey', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:44', '2021-06-13 18:51:44'),
(171, '19374D17-447A-44EA-A60F-5E6C39ED25A7', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:50', '2021-06-13 18:51:50'),
(172, '09A2E56C-FCDF-4388-AF79-535244748602', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:52', '2021-06-13 18:51:52'),
(173, 'E3AAA667-DACE-41FE-B25B-A2CCF78612A3', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:54', '2021-06-13 18:51:54'),
(174, 'C1F8ECCD-93CC-45E6-9890-8CE9A5FC61E1', 44, 52, 'Hey', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:56', '2021-06-13 18:51:56'),
(175, '73FC3E0D-B209-42F0-AF25-BB2A879DB0F1', 44, 52, 'Hey', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:58', '2021-06-13 18:51:58'),
(176, '163D94AE-39E2-44A4-A0D6-B048A849F5C6', 44, 52, 'Hey', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:51:59', '2021-06-13 18:51:59'),
(177, '4D83A63F-7296-48C6-BD4D-66580AED4041', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:52:00', '2021-06-13 18:52:00'),
(178, '99C81932-99C6-4FA3-A364-D82C7D804232', 44, 52, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:52:02', '2021-06-13 18:52:02'),
(179, '6BEDFF67-50F2-413F-9D4A-76DD90781540', 44, 52, 'What’s up ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-13 18:52:09', '2021-06-13 18:52:09'),
(180, 'EB93BFBD-9100-469F-9F45-A0D716487CD1', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-19 07:33:52', '2021-06-19 07:33:52'),
(181, '177CB2D7-84F0-4193-A30F-6AB4B322E899', 37, 44, 'Ho', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-19 07:33:57', '2021-06-19 07:33:57'),
(182, '012891e1-9fec-4b99-8845-a042319786b6', 47, 49, '', '', '//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg//AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30o\nhS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS//8VxAGD/8AUAigKN9KIUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBgf/AFAIoCjfSCFLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0v//FcQBg/\n/AFAIoCjfSiFLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tL//xXEAYP/wBQCKAo30ohS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0t\nLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS//8VxAGF/8APQ2rfDGIghEARCC\nFZu5u+vIrdybtl0KWwGpFJXQFUpKf4tx/3/4+q8Z9I/OfUP2nFn2zenp/7f9vzX/So/SMW2JAcXt\nu/05FOMOSJHcTYGuRwABgZ+CZs8/gRSkZ9RCW5Z2Hk20to8g8MCb+RtLQdxuKioRnnTwTEy5G0RL\nuNoSVkRMuNUYceBFtLfKhZNmKDBklg3bJEzec5bmluSLJYC6jOVI9isiO3A4eAn3eNHyZ9f4UgS4\nXoZwcP/xXEAfP/wBIjarcGQgocN3eVkvSq0Kil0RMuhUu64yhECl9jZ+jPgDbSf2XqvWLIImQQPl\nSzt14/U4fY/5/LpFa3YVh/MAEP8hSU6TVb1ixGZBmfuoMccAtgsn9yulxo42NVFZQhFYVyDGzvbu\nLRVdSbY9/h1M+yFo15YrG+WNTzY4zzJvGy3HtrzQU7eF9UMsbaRUIx32a8WIYi3PDTBkWEJY3BdA\nQZzj3glBWLgQTJhBzHBOKXnA1ymm7t0jH4Y9g6PECHgtP18qiWOk3I+6QnsKqCI+aiAvENT+nDpt\npBz/p30CQILFrIQE2i7mAkaVs6NogpAIDv/xXEAb3/wBJjapMWYqC06q4yl3Squ6XUzVVWhEIHpr\nfGsoRBwdWdaKzsAdbZ1fP4wm2W+oAt20sI72p55OoO8A93e1VV0bdSE16LISioM3iKcYF0sqgjyj\ntYPC6LGLz5RWRcUK6iV4wZUtGMArfwSMtib5CUxkiYIWV2Ef1x11CtedfplXnFDgkhc8B9QyS1No\nF1zokTf11KSPI3sPz8rWNTFK8BT8+vV1HngHZZPLNrve4MHlCRzAxHPQGt0Od6hZQdhMSOEHY5uK\nvbPZRHANtob67Zs3fKXrL1GnwP/xXEAZv/wBKjap0SYemEOjKlXlcCuN2SsuZEaySofF6VL4yCJG\nBu3OdUwt9ZsH+jx0YDEoNsqtKHzRaepjczhENjVbvxslzee01BeanSmJtagRdUdNVsSRDfdLg0NN\nLXVrdNsSVzkF39TgKTnWIQk1t7ioI2zhElJxzSjJwPyssJKjEeRGs6iJVG3iJgXnxCcN/AeeqmST\nIRbqyquZ2lROQntCrRVCy/VwpszefZPJc1N3zBFCQWqvmdoPncphOEUis+sj2wlX4xlREcD/8VxA\nGP/8ATA2gDSYuwtMIdGRly6q6lXMioqSVIIe9rtfGQVN0TZfpmo+Va3EKsfLFjXpXhIZ9cKZemZO\nLZMm0qoUnrKYjOm7D6Y3AAEomGmTQNhZKvHiVY5ga+JfMC+hB2BOUe4gkl2xC+IRWiGMeCiR3ZfF\nEfxW0UwVOBxF4dfPt7856kQJMqQYhJjAgXOae1Ji3rgaUiphRnshOzLUnU7xVF8D7Yc1OTCZoyWm\nWMfG7KdYEjoTbVKYVtVF5gCg0MEhZVbg//FcQBff/AE0Nq0xRjapYrWZpdQSlWqQQWe0iScZAAMM\n0Tuuap3qwMkTZ9ovCeHYIEirpWSaXafRRqqjSgfpMScp+qtKq5VGDIiIYWBtpjjQjKkyDy71ZJaT\nPRYqx2xFPIuyJD1RiN2uo37oxKaFe3rUIXlR0VQirN9URLRLQPb6OMkEUWSRspeiaxpPlJXL17fv\nJ2ozLV7q5m8Frr9cq/RB9+iIy/2rw05tDN2SrhIp5xLmkwib4WiD0w6UJ//xXEAaH/wBNDaANOhb\nJQWnEKTKqSSVxuVCrokywkHos4nkBZaa6rttQHdx9u0dQhjwAmpAjUqMdqTWcMV3q229jq3jSJTp\ntkFGGU2vfavgXJTjE0YbFZUhu1QgFREjAuUaxauqm231Vd7EnR3I79tOoJkzvH1dlkkbi/E8w8tc\nr7qbqpjQ5CQZQou6PLbJP+6E8xfOxfHRZTAUksm/wosEJRq0zfpUUOdtFShIVijKbg2ypIKGvB7d\nt8L3VCegZLvhjjCoS+UjssCkS910pnD/8VxAFz/8ATQ2gDGmRpSDpCrxYhAQESUiPYl3OFBDMexs\n2HyaOip9SXY5GMkjvRbIwdj5OqM4e/X0r9NT21JE/SIpqr44B0kLBx8Lz4zPQz06JLPjKSDfjkRF\nzOfbVM7a1DdVjsRq1C1wKvw3mlatAkMzswxAMhJ2IuBJYMV6XK/k81OOHWa3+TdrUbWyYJ+EU4xb\n7JxDWNj2R3yWUHhypDAJRNWImcBkIqoIYiIFANEiFaGQpftKjv/xXEAXH/wBNDapcQZGqVMgkWEV\nAiIQj1haalCJC+hUvEZM9bvOJL4Fg7m3ap9Ka7PoUCgdx7S66IS7Js5wqzMMKqQxSRkClhbJHoJ9\nUpOLI0UeVaU2u9OLJ0C42kaussiQZIu1EIVQXBNz77432mJ3eKKKg+63Z1375J6eXThLNv6V9LKK\naaWd9VPPRRcxvJqmz1+p+/ClqDnjw2dprs07NNuYRjJNxYGObCVUR0h5HYEpFZCtZOD/8VxAHj/8\nATY2og00GJy+9XWVxAQJUQhCEC0DlWSy5PfFvPETarMF+DhMYAt2HKpIT40VbR6G9WTN81qlJk2j\netL41ZZl2WZeBctfkf77oAO4PQnWWj/k91volcJ5Z/BPXTpnt7s8f1tLx3rpm1XV79yzVT8lmuvs\nc1nlsV3sqyCe1oLftzEAS/O6WwCvc0cdlNtz0WRY6PnWiizA1EIYKQk1gSyre6QD3FwtU8MGqfJP\nIfX1fGcIXXrJsSOD1+0AOEaVl3fesskU+MJXhb8o8Nb57HRFbQ/2MECFfzTsjFG1olC6EF7ikL0E\nl6iwmKxc//FcQBef/AE4NqlxJjaoipi3G4LyUul0kqRKfOpUtxhaQ2kokEh6WAgBpdaOf8GDfR0M\nbBQx3n+e8ZGuhpPgs0UKwTWPSgmQRJMe3wnGtIcx9O+IC8EmPKopxcq0PejIy2oZ9hMLel10drZ9\numnMn0xsEUFBRtJVApsTb7xRCUSg9PlENrnqsQZKnyoSSuBStrsLIYT16OmFfOlGl6h9VOV+i+z7\n00K+uu/fdXZNYmbytzi5IMSCq9zU4bj/8VxAHx/8AUQ2m4hWCgWEhBI4cxUqamTCUhKiUuoSAigA\nU7ADUQ6cvLpm7T42c7bMMZLzG5+TWFO8lck0whbag4vDIcStQo3zeSicovcyVHnm1Y6eyXDGwq8G\nNAwor8CWLP25klxbZDKBza7tdUzZ6j3Wz9t/b6gpo2fE9+99z8QRTa2tRybHIVb8KERpWEolwpNl\n/oK2N2viDbBy5gAmQkgRBjc0r5WIyPCHfu01Cwl9jwCBBGw2YeZBdXNe6IlpkNbcysDnBrXSrjYs\nd6KwMABudZ7NDXq01fnwzuQcbx2EHj1tWnfe+/1/CerTDYadjPCuqgVgHP/xXEAXX/wBQjaUUFbK\nE0SB0ghAJplRCIShFRCEI97ZqXAPABJE/BNcrg1k2hqsn7Nmd3At3mLLRSvwRnzc7ZYqUpHERrQM\nXDdutwazZLi5Wys+gk4z/Ims6TjLsLpkX7j80jKz4x8ppFB/C4BoeuW+evAGtGWYKCNJbEla2ANz\nE2CFpOMAmbAv5U5d+bdnHW3j9ato8/AcSo/wdWAWS7CNWkEdZtPk/uq3g5VchWuoMCUccSYlOwWr\nwP/xXEAWP/wBODaAMaZGmEOjKIKuVAVYhEEfS1pq4Jc4XlIkX2rKM3RSogNEbjrjXFGknOZNYMVO\nBG4ahJllgpItPzmTNUj2pBghOURR4qqK2GW6dFrEzVEhuDiZkkvyJxJ58CLLFHY0EyosDpE+k1L6\n3WSrdY8ptO50SreqWsSXZwK2CWLuZSyXw81smc0m5orGqLjdXqRDZdqX2FWDQjTNML4wDheMpKs2\nEvm9Mr1s4P/xXEAWv/wBOjapDZQmlAOiRMlSKsBKhIqQgPWra1BEjAmipHr9fmmnou5TBFS9a9n8\nbJ7O7yY4fh9tO/n6+tX7uC69UnopQFvvDiMPXPhsT6b6mtGTOr1PtHzH/G+XjS++onnvk3RRFD6p\ndmMmdLDGjLEVKvMzJCwBYvuBgrWEKolE8L1MZmpmOxhuuFZ8ZlQ2e1pZYq9ew9FDauYVxIgSCpFL\naKl8S3b2dPWYGWDbywrCtnz/8VxAF7/8ATQ2oc0UJpRDohEqhVi6lSKmXKiElQfMuVeuOgcuWQPZ\n6afvEvauRiNWsxwZkcTk2YvFRtWdtU7fHo8eZVX6Z9tKYZ2mrcp0fCM5v1u52/el/jK0thXY+fLo\neCddExdnz+G3r6jpP+upFsevBbdFlrwgleWfVYpcIchwUK4vVxbFK1yBXokPfQ/O/PGBgG1rzTus\nOWM9Gzvaw8W8D6TjyrBwHVheqEqi/NihOsrLpb1w2lUATif/8VxAFn/8ATQ2gDGmRqlJWXatBACS\nrIR8SXdTiBh0FT6veDwpuw0/Xid7d62dmW+MHFdQ2ce1iTtshYWrSWXIHaXDE2WJ+kUYfnVNVx2a\naDRNeyTobMrfqboqPMwCen1MfGMXBWxYilFRImOlx4JU5I8kutuGe5z2h6zHTt7f3nuurOLKqQsK\noYfTfWao0qXCiwlBYxbICkFatgkXNKNta6lN5xUDB6weComNKwAJ4EWfB//xXEAY//wBODaAMMaa\nFIYB0iKq6krVECpBERKhB43dBV4dhQvTvPdRsyollWvz0L5bMg71vfJpvq6MPm7a9N0Je/HNZpN2\nFDyq8uIa+FwPFHd/t3q7scmtoCcbdl1W7w+sv329ajh5Eum8O3CemYa8LqUKqxbpMRqrOVEI0Sy8\n7aHnGBLg0tB1G0IUyyEKK+zcqpDbPkULglyJj31hl2WOLyTzQuqRQ0hNyzmDvXswh2jHoTfEHTJX\nc7fUozpr1HGBlthRKLj/8VxAF1/8ATY2owxkNohDoxDolWxUkrWRBMi93EQI9rSruWHKVeIjTmJe\n4S7iuDt7u5K1lSQmzBrmt7JuTb0S7qM+WiU1VTuKBGQOeNNpyTamxIoqPhWgC4jNVVmWdpJipoWW\nZJ0P9xOBpdtsyHXym2hs7kOfDHLqnRXi3O1aUWqKcGChFZLV5T6MACGNSGsdFmWw8vohgVsaUWUo\n5A2M5biGT5PvReXqwhpqUxUaglIuWTLixCc7LcD/8VxAF//8ATg2qREGMhdKIdEIkTdawu1RBUER\nED1S5cvgIkIBac0ub8ly1Nbv+OjgnoClV8Rind2zVjRhbmUt+CNfRtW1a2YO7SJFCY0LcM4lBRIi\nYr7U64eJr6p1zFHRxgW9pgq/jLoGri4Tx8Pl4BJzf455zKT75e88IOLzq6rgSm0kdGdT2MY8nqm8\nDmugfo3mJennpd+F94HGtf+jNt8BrlxBNDQqZzgVU5q2WS81/NNCzhetcqLuEF22Dv/xXEAXX/wB\nNjajLK0gh0pMq6iJKRdKukRIIe8akqaoOVqm78/8Wkvt7Th9kDWvZjUnXZFRVWeNdM0oJZCcPQz7\nOURRW3WAyCbFiU0IkL8RhuQmXPXipyKTHjp4b/FvcaOnG5uJeqKVrLq6+/CqP8w57uwcvYHhk3Sj\ncN9pIpy7OI05MqZGFcDPOTKaTWSlCsldjOb3RTKQ5TXy1SWXENDXu2UiPYgJWgORTWzWQeDkQjSF\nXJNJHNpJ8P/xXEAXf/wBNDaAMcY2nEOiGKtdWCAQiQR82uVd6CRbkeTWxvL8pXB85jv2gR0I0etq\ntVOiirfGSqoqa2cqXxTTWAsbBIljIdQsx7IlK4KMOcKypGKtvJjd+jrC51/Gs8OXGqqsl9HenFUM\neCsix8RPC3M1yAG7FMi8kE4Hj70eOpOcA7UAiOAUhM0S6oxksspCJKpSXAIURSWWUUjFazIyYQov\nGoAmpeV4hBQoW+VJygRnWsNKSEolEeD/8VxAF3/8ATg2gDTIgg2HqBETmxqtURFJS6iII9rq5U1o\nHVXIqUZKI1GEqN6MYKGBRk5G7sDZKaeZL10c8x8Nw89PqXtqkQEq8zmEPtkT5DSBKZ40DBLa6dvG\nt0iT0HrreKuLarrm4wMQgYAxAV5pnVUg0nnEiTLmsuLDOhPQLeJNSOlRT/XtXC5Jp+Moc62qOddw\nHjW08pGIpkBEexwUCrkIYN6CFmUTqEJzOIpKuJAgRIXlWgC0WkMH//FcQBhf/AE8NoAxliIRgkLS\nEHRrqpUmXIIFSrEhAeVVL4QQ2x27tdhqWSdhqL9DCjgfeqtRIek0o1VNVJAigCfZRaipVCQMsiS9\nLCOSWaKgtADisDkaGiSowIVEbAiHJh4h1ciz66KQQ108MijVzNvso08MbXLnckt09frveuzS5X5b\nF70pWga5sR4FM4U4x5H2jEx9C1PSCKYfyuIeL3RIY6O/UtqSDFCuBmgI0gaTELgzqZfUy0SFIyUN\nPwwAN8D/8VxAFr/8ATY2gDGWTqBCAVMlXKaqEVKlRCIg+pcHFjDSF4qvjj7qCLluxIQs1M3HtrQl\n4OG/tpadoTNqU41RNm0J0+aUNpimIwIZ5QzhkHKvpqO1UrCjA3x3aKqretUql1BiBObocsioTILK\nlLSv74osm4bO6qfKUq5HK/wxmAK5QaHFKK7cDJ5MgWawe5W2weYlSjpnMTV3YX0OZd6hXDYTM89o\niuHc4YyCYM7oKzCJkWrw//FcQBff/AEyNqixVk6pW9c3V2zVXRdEqLpISo+q1SOgRI1NRFh7uGMZ\n5aH83BxMYK+2sdQ/wbUE/U3VlPn8Ty5KpObfNcSMGzA8ko9A/GlkyhuLqmkSHriUDbMNPZnFMf4T\ndzDrql96ZW7KGxpssz5NXBz8bntLt+G7hbus5yBHXwwyyU4XsJZuwLq8psnCuKa/FaLpmG7bhfrZ\nWWrhPHdjZ2cJcyx5t4dnXa19h5pfPSQ9qeEyVvQdStDDwP/xXEAYn/wBNDaAMYZCD0RC0rc3dSKu\nuMRUqKkJKhD4RK6gIxH6yoLlcuXOux49HbCZiQTiJ+ovWG1j1muxK7bNhmOnoxynmWwvpFPvhRwE\nJSjJNpYbccg1dRelSr0luGVYpF426tdMUw8/Sa74als+CfOB1YJvDYKbTfW12ZNbdaMNlEOLhS5D\nzWgZ0Qwd7LoFVl85yS1hLG2EqadYjSAJmJ961ViJkEvaNvGVeuk0PztNO1wcujwQY7qGrBHGdSnA\nE8D/8VxAF7/8ATY2gDGWTpUDpGSpRa6iSioLVaVB8xa68gVmr3WYnV+9a/PyI/bsHS3krFlgWVpE\n0tQlcM5JFvhN4chrJxi0Wn4KbJsFOGYG+xTKiXN4lkqxvtWrQ70t+NJqoL1azbSRoGnz51eoBRWP\nfdfv/COn5tkOkVN7tNn30wU91S1gsTzSPHnyfIabpEaUL3oqrC1g141YEvNoJ8NktA0dQlEAS3RQ\nJwLgpZQIOQCeF1Dexzdgr4d06vj/8VxAFv/8ATI2o0ydUZMvdrqCXRSF1IgPqNVU4A4EipZ7XHyX\n8xnH8nQ6/a7Ycmk/J0Vvk6W/lU9Z4VYmTSvJ7G44pSVLHqsuffQOxGZTTfmmvzIzAtk+mPUV+TZB\no6GORjVd1i7SVvINjaTqbegmlrXZfUib9Ppz3+TDBevZhove93jG2mhmOlBeXCk4zit4avCzOpHV\nZKmOQHoSmb3yX44xS6luxpttKcWQaq0KSAujkp2s48D/8VxAF3/8ATQ2gDGWTqRCy8vEupAugSos\nhHwljqrG8SHlZb1LpF/Y8Jq30YU0Z4OVXcDJuK3bvUrAi5j5Gehm7EYVuPS1xwtJNgp27d62t7Wd\nMYpVuXiG35uQ1Hxo7dZcnqraqTbnSQCk2IUeoiD78JoiaPThjuk4mXfRpDuS8773k2s3KGIqZAor\ntYNFNW3Fs9e2aKb7U6F9x3q5zOzZWqtDyK1HMzMESa+l5YCp55qTc7SphxVJgJsH//FcQBgf/AE0\nNoA0mJojUOdVVXLySkhUERIEHvcpwgmJw9OmAdzxd40E02oX2E3jHsTd7vXUie3iGpK0zwFai3Qn\n8eCjWgqbcRa7YyEgTGDBTfnyl7SdRNVeOIxKhdWksZItemwunEcffJIltZyE+c6ahl01PJ4VNvBm\njAitREcK3ajoQiNG7yn44Y9KftP8Rnpe3CvI+UdB+5HJx33L9GGIhhTxNr/Y6B1jNeRb/4YIMLq4\ntLc940pfXB156yPg//FcQB4f/AE2NoAw9mochoISMlMukshKRSyIgguklDUd+C0q6iloOAxogWdS\nd3fuCYJmHbDBWVL06xfRQQ4renHIfHRv2SNrnvCOC6qdOuKk7ZZJddcaf6Ffd4J8LejVF09fx6uv\nPU+Pz9Iz6vFsnmlSkKdoXRKkhFxGzvXZU/2tvc7IfUzXBMqiUYTKgn+ymPl72OWUXZMv7LSRp83W\nEkelh3wDSY3dwDrNW2W/l0RHptbJM5JEXLKPzjCAit26i1IG814yi9LXIHoYYlqy3htVFmVqys55\n/BCzWuwghTLOHuyRInOMwUACMUN9Lgi4//FcQBa//AEyNqNsjVMRu0yWWqKhBJCVH1cl1NQHKXoi\ntvKwZTew6ngV3SiXM3tSR6TxXQ5sf3yCwG6FxWVUmeEIa/akyMqyjYEkobiWW3Y2WZKPVtlEcXIY\nTtKzaU65O6jUy9vYZN6Pi29jjTp9K3eafFoE7yZMhPNZbq97XSEW+3qYm5xRbWkTNKM2Z7AuKYjR\nVxA7aBcb2JQlrJcBaiuQ5yOhiJCNLyaRDExGtB22ix1hwP/xXEAYP/wBODajqI0Qh0gBUuskhLJk\nKuklSIlQe3ETWg5S69e086DxD+eqx+Rzqe0tNMw/dYdpuG5VbFkxkrN9u7It9rk8W9aSVHlqmvAR\nLw3w0XxOm1FGMnHzCYUNpj2dVPo+BkkcWQ20xZDC1lxMCQqAEoqK18Y8BxFy5RODDElPPtmMSX7p\nFrUQKzjFx8sxgpjkAHe3G9iEWXLw5vDbPbTMj82KGGSGYpqy/jRfrRtXJuvRoUuloUuCNqRiPS7/\n8VxAF5/8ATY2gDUIahdIItIGF5NIpbIipERB7rkmtShBtio98ApMrtFXpHNaoFMhRnTYdk+U5/8F\nECJAskjCntzymIghYFrfMkshMQgJjhtVnEUEFLSNU4I0edSjTW4lm5tlVNEvBc7wtHZ15M8tCHfo\nVgHwYBsR624HDJQow04JDyEDDlAWoLsAU1ZXdf16BHLw6OgMkRzcadYOzueBSAN9+3BvEOiVSYOV\nE5VNKtVWYvOVsdMA428eR//xXEAXv/wBODao0XRIB0xbe9SUtdQAREgQeUriWIkYFQIpdlo1bcIu\nDxbXY1rWUOGzx7eTJkc+tx5AJolTXt8pZCaCLSG89Q8VMl8uNSIqqBN3c99rLQr6OC4KZaXQ8ciR\ngs0L9i2ojVY1EF+k/UPpEuKeu6USOyQCAmY1CKaNKBahbzWJh+Y6fsuQdzcB4muQJRYUJQF7lVld\nxpS1JyWL1czoJ4w6qE9cGoWEuXR2HyXOmqRIqmLu9NdW///xXEAYH/wBNDaANFizDQmkEOlW7mXI\nlaKRSC0hKj4sl3xUDOkoPKtocD6nj5P017LpSJepYMxp9JuUo2dvj0mddeXfGXK8OwvTrfCWwgW/\nZ0kLo6nk6pNuPcsWf4Kn1uq4XfbpXezFcrHK1HHDcZRqrRgyLOvJZlQU5sFW1Wa1soiMqc3Pb0oo\n4eDP3PsoLukrNc7eQUXz3OqIAEPMF9a9v1DZznF3Q9P10qBLSneVVK1Z1X9fWCDNEU6MELL04P/x\nXEAW//wBNjaANOhjI1SVUy7lXVzcAguIQ97OMeVB3VA4PWUmBzh8tvjQA76yJNFEw6uOubacAdIP\nClyDyl4zTNq2eu3K2F6ttKRDd2W/fp0qhaNhN3sbg5UMvdNB/DSzpeK145udbWm9ha4sXstqVPft\nSMnV62GXfEJGiluUkgz4tINOnOYaJQkaQ9Erz02HPi1yyMSSkiPJDQZmyuVpCBFdOTRdFTQJQJZv\nfRIYvScE5JMCJ//xXEAYf/wBNjaANLiKJ0wBSsmaWSVBKKkSIlQj0s4BNCmT6RSHaZ3feSGm0Yc0\nhNY9BT7ROvNjNzKNNPF1GPV8TbcxjJRnjbsUXzlxsKaCBLiKJUZ2mIjTjyaH6SYtrZWU4Hfy06Uy\n/eyCOgmVKxCBQVehIZLOmkmnLq0UAqWmpYS2EFjeeK/y+kfNEeupZByq8aHte85dppvaN8DjbTVm\ncep/88wDwbEEZ+2ikPAhyGpbYo2PVuNW6nyIROhhpLOR8P/xXEAZX/wBMjapESYiGIWiEIB0apzq\nubuUtlwvFSEkIB73OARBmgU/z5IrGBXvmXjA16UwWNOyuOSKxobWTtd9JKf45R37N5JKyfmslXiX\noqJmLeruI8W1Kt7dGPoaVbGTJwcOqp7a+k+aoryp9Bqi3evyS2P1hQyq93b66Alxsouiu9sr6qLo\nASiW061rhGYZsYCgO9CEmOgmRAoKOW6cK/aKAEGxhoqr3/Xr5WwSucRKq5gpXfpPKt28CM+E6zCC\nEHMSiRjE1OD/8VxAFr/8ATI2o2xEJqBCAVbarJLqJUJQRFog+IlnHAcHIDe8kaq/7dH1PA7+DPEj\napVgqe33n+YaXJj5VQHErpMgUG0HDgIw2BlIRrfFjnkvqIIdcnsNlEat9NJmdKlJYJ7MmY4VLcKF\nTty++pistLkEbGpFOKvfk+skPpN6a4LM6q7rGkyhaSntZF3MWLQLshlsEJiYTWq6gwWb0lZ5fMzY\nKn9sYoM6b6AktKdJBGsMKQ83//FcQBff/AEwNqoQ5C6YQ6MrcyruRVqQSkiSEHzI1UuWIgkxTeP1\nIJXimXUNaAhALkVAnuzMHZFTzD5N+p4xZIv7Fvkxl+iWTbl2+HDeDi3UojONf44MiPV1l+e2Rwzt\neN1s4Wh36jhWETBZ2L9B1tRv1uagF+jaWyBVSfWyGhSR0UiS3BloRGZBKTzcmYoBmShVfXQq9HXS\nBNX9zV4bcefB/63Mg/3ElUdIRs6CvXOb7Ruu+tDVI8/yTsODB//xXEAW//wBNDajrCQmoEIBQxu7\niVCCUq4RIR7yLjWg5aGipWuLtx9DS9n1ebTk2wcqJAjChramzX2LvHWZAdkK5XX8cu5o0do9exJc\n5jHkIqc/Rl1FgzoBF2P3rfRyvFVqrtjQgOLXRqjZXG2ZOg3DVSRXNdLtK+ihGt/JxjxMOApDRyoy\nZogIXhMwiBOc59bgJQYMiFiUD+gNSUX0lzjRoWA+cHEuDCYTci4fIAYRk0kUVIgQcP/xXEAXf/wB\nMjapkRQ2oESYbXIhAlQq5VoI9y7q74oRIDw8PuG/DD6Obm+t1e5x7IW6ehZj5k8U2IzMAUr+6e7U\nohmd2keXMYn5Ak+OgWhkwCEg7btpU9YhGy5sa0YyOHURuUnglFabZWcsQ71DiBvwSwXqmg3EWcpK\n7jIuvzRjUMVSMJVg2hSzHTdv9DL6vcrEGmZaSQHv7P0CPYVXYe1yDcJru1/n7vVkvzhAFqUojGNA\nQoXSXpApPk7/8VxAHh/8ATQ2gDHESJ0yVllhFTW5Mi6kQQQuqEmOlGhrceMRa8xqLi20JtPjbMN0\n7YQU9jRgbnb1SXQsxpcd6KN0zeEQFC3gVl5ORMQBQ0VY2psm2ltuLhiGuo7Wmq51wmtbdi7DepnQ\nVZE0Vyo2m205cNIregXeEWdEQ31BG/cUGo2wrEO4r79tSc1mav3PMyQ0/TvMfqACo2mjHVaOLB1L\nPnrZyW+cV1z/18T8ZESDBGzTUZv7ydFS8LDmuEWOdTiYoZJWl8LXlSO/bjva2GkM3mszuC7hVw15\n5w91IAFjZYA374hQC7Z0+AVUswf/8VxAGB/8ATI2gDG2Po0DpkxN50XUmRULyEkQPu7l1fFQS62J\n5GVM5LVDe8ngKPHmo5MWNOiYmW7LVd0dsZlCXKmVQvFJtla+qZxTR455NrbLoKechS3oY1zlWt1s\nqhHU6DG9gOPNEq5AXSJlsloYOtbnXax8MSqPCudDfFx1sHbSFGYkJU1WTz9MbpTpaV5JgAagBMu6\n+KmMzSBQJS3UsJBaq1RaZEJkF9ESgZgdbWko15LO9RaiSa9rmWlVHwf/8VxAFt/8ATQ2o4x9UlQp\nCy8lXQRERB86WjUByk/Mi8/FFYWFLv7Lw2ND5CGbsBEyfLxtpt8aq51pj12rZRv0tfbDeo4ZJ669\nhuKlKK9GJsHrjJNV19c9xs3ok/Qi50UDBVvtH+IWoL2jdTpuLeJeq5Dxlvxkk4mjn4S0WU1Y9/hz\n2TUpug5YiwBxjkukrc3C6DnuAJpRMEvtUcl2gGZLiuNlzvwRg4yPa5yrRDFfbAGSQDle/P/xXEAX\nP/wBNDaAMZZOjIOmVdMkQuUSoqEhEHxaXV3A3q02Z+aYVt7j7O40JflssW1B+IPODo8NnmVrEWwo\naPqNpGntJQEzGEmI1ZUMsDEsVshOQdFmZ23jRor+ls6cxPDKn2NKC+N2KbTZhZUxTani3/0+5X7P\nlfgtd2zRbH1EzBdhEdL6pOGsK5DlVq578SozZAN6XMQIVxdKQFZBOx1pBleWECeZ7zVTqhL2N4oe\nkz1mfYmpl6bO//FcQBa//AEyNoAxpiITUiFlqyXUSBKlQkJBHvKsu8gugpcLOQjUJ2h7Gpebwt6U\nlOndUaro8TLtu1kT2GrG5Xk24LaiCLSxpoZeO3Qomw1Mq8hJJPX22NvFZRdOplQXHWy8VjtSaTfR\nj0b8irTgpGkohp0xWMaGnDxztQKPBLLzkW+wzjM6+lFzDdW9uUXY2usRQ8LsWOt/BAta8x/4A6+U\no7mh+ypzJ8qMmv6CU2K41FBEcP/xXEAW//wBLjajbI1Sqk3JCpJUqCkiRCHykhrfAcuwLBk4sS7i\n0/1yvJZqZq505qolx7f8ZmsJ442lu0WhbKo2r78mU9YUI9W1xol0b8t99GovFVeNoty9dVK5nBj9\nnjMN5en++lV3pMFmmVYgqbdmRigqhpXUkrO8D7LifOlMLu3zS2yLdc+3Eb62pqWbBrLt2chkJrD1\nzXVnIZMDWprlndDpAKwiCtmeL0vRiuSdFQ4E6ZiYOP/xXEAY3/wBNDaANJiTFQejIOlVczfSl0Wy\nSiRBIQ9mpV6oBqsbqT6kCNTtY5JxrPL6osYsj0KIqU+nadlrZu1JC+lkR5VjUkUmphq2uJdeIJGk\naXSey7OEGbV1CHavU65ovjYwWVe3a1h51A7KiJVkSUTLSZo0GCbJSGlsHqsqUWBVX1yafgY3sYc6\ngvnAgSFATltMFOhUlWHayGZ4KJGc23KaHXV6Whs5cVxvkRW0vor4rtvEcM42Wn970tw6SZifH0Mz\nB//xXEAYf/wBMjajTGQuiIOlSlVlrRKiUSohaBD0RNVA5bNclZzQnhqOy/fsNBuL66V+orENuUiU\nbrUMg1Em1Mq3qW6mLt0y21IFttRaD5lxqpOXVuuapG6FRSbjiFuioWadCWGyQtEo9hdXp9cabBb/\nFjN2ajssevVwCmbCTOZd8AldU80K6NxhZiUWcapQNorongr1qUP/ybhvVCeL1I/qtstM5Xq5V/mO\nVHSOTzlqpt53I9kPYYRp8chNcu3FS2jlf//xXEAa//wBMDaUVFibHQpC0qqk7ktV5dJKCREQQg9m\nqgW6DMZYmG7sQOC0VmXOwUG5CFbEv3575UOhta+LWV9dLYrkENMnhotLLJRsY5U9IFwEx104qoG7\nnTKgHtoVaPHKUrhX4Z2FU1HGu/GxJTSdlzOXfojj0OuS4r5J56WfYElbq1UQa3KNgOrRTKjCRVlh\nDASnEtj4NSqzhOmOgR21nqKX1ReB36UEM9HEZ8ZpdtXHQzWoar0j2v7g83saDRbN6C/6qSoEozbn\nzP1a7TXRhJmLITm4//FcQBd//AEyNqpwpjakQr5k5lyJSF1KKkiIg+EiXqrESNRuxKcg8w6F0L2K\nxqrCukWQgrw3OuphcG7qsZXrtXCEOSZmKUKErSrjYydjCVAStgzEHJrJiJU2fIdubSp2kEMNqWHx\nVTPg0UOmsBFGnFBZ4bUk2wt2WCmEZFqOQZlo2jIERQOgwJpwa/wn1nOVyFVjUb5OBRDicOST3VTo\nNdkp2xYVYDm7zKqwCg6EhFdC3vAKBmJgCbahd//xXEAYH/wBMDaANHijEQmpbubStJUVdSUmRFkI\nfWkOJQYoRUuvlYHfNNVbAXWFSh5Ouu5OhJilX7mv0Bq96u4VOGLncVmnQnUAxZbkODpaq7v5OfiJ\n2ABVlVPZuPILjt4cD8jDjs6hGsDp1BvxVpYyLpubK3hGARdJEY5rGpgQazpNdoVXDLimEmVXGFg6\n8pWRt9hAbCc5sq3tvrq/mKHgYfy7X4UORSxeEKL8v1h7GtJ8o67r/9J/6nsl6UhT4P/xXEAXf/wB\nMDajqG0hB0Qh0TKvJUlLlEUkqrSIIfUXEnGBwMN6DTTEbPciyeDsSpm71LFi1WsM7JFrejrsMlDe\nbpPVgbC5j1elpnnpKoFBvTNNVRRQotnbnW8k+XMMMfjeLZLmk/mnZ5cBMaLNvJtpWVQo7CDcJNrN\nmNiwBYYvMPRAhgBGTI7t/iHJvCuYfcrxanTCRSaOtBzkvoW6TPr13uVKnFyZywXvWN5MPYmW0b6i\nUhFWVSVkwHD/8VxAGH/8ATI2o8h9GQdIrLVlpF1UTIkpIQiD3kk4mBylxQ6fWqYuubS8DNxRL323\nwpY2dxko5em+6uPLq2gaiGZMEanPjPIkMQOFgjzmL6yHyH6uyQJp0m0pVVUlai0aewVHAeq0gERz\nE2FXOlWokpBEMEguoLpoYIQCtUMFFE5rMt9x1BQYYCmZzaNQiHZ0/aE7rQnJNovDs6AnkkfIAEDA\nAs/MXOUcb01WWP++eTFCS14WGxMdGpjzk9My7Gs5jjj/8VxAF9/8ATI2o2xkFpCDpBCrNVlyVdSp\nV0IESIHzouXwsOUVocO9UUENJF+WqrUMAqvb79ZreHcY8LyU6st88XmoV5p+NhmpUzJFTX4WCz4L\nWIlLJVhhZjWbc+vXTy2aoBEuMKQqInKpbkLRtpbYBrpr11/ftjSaipDXrqHs+7ZWd6hDVZjvRBV6\nTUqwlVagMGnRYuhMIYWQhqMmWm685yiCpY0RZwyTy8zx5xaFha5kK23xKOkskyJQqrQc//FcQBff\n/AEyNqNsjTiHREyquyrm7WoCJISo+Ekk1xQcqwqcVHJJO/1+18mh091uJcomXiWoa4nY/RwZ+qfN\nQ2nX82Lo4bmqY5OJxIIIhlPT4sZhiYfW7/um0l2zLgxZzGo1snQ99i9gV22XQoz0xUjW0QV+y28C\nRI5r8g7/KHkibHqWrcv5egVzFaBa1cmfuGp2g4XulaZgNkNBqrGe9a0lW5nh2R6EUhrFipSMa0Iz\nRrIAZbTa66cq3EUTB//xXEAXn/wBMDapkQY+qVKmU0l5WqXkqKkiIQ+ZGqdUESFEUmsFSOnl7XHX\nmroOH2XgVJoTxGm2+vUMHBkiLX5aL5FQ/bZKM/m3VZuNdcwCqR1+NFho19zUCtzYfJKBMsk+iKLj\n6tjnXw2VfdSmvM0Sw3GrYpCResmZjbaUZq3y4BPSCuw26C/NwdfJcj8I6BRb5fJJbzwg8RcBh6XS\n1JxQ2tsR3vmSUCLGtzXjD21zNKdxDj3BMhvCGYMX//FcQBnf/AEyNoAxpiIfSCHRKTEhIlVEVKkh\nICHqq5oEs9rXreu73ZTeFWXv3lWtxaMKlajIu7NM9XFqI8oiVZ5LFnbtvUpmqrpDJ49vZ1hM1CQN\nK6lVDGku5B03+xmS4wsVUrLxeKemw6fZvUmZxEgYJ6UmkzbEW+vTmOezT1JVJ2PRI/SwWnCxZYC3\nEooQopKG/kU9CW9xft9A9r+cGTHvNC9Ya6TEf089DGzsch/MHoBD+H3rvjDcHRg0e5dwH5MUb9pV\nZmtjCKigCBz/8VxAFz/8ATA2q2yNUxdN2WupUCrqREQPmLRxUEQYWjDqIT5VbhizWh3QE5SnXpg0\nf39NOaHnWZVrUla/RoDVFo2gxXUPD05pOCzVvb2pCap2ykYdWLJwI9Sna9aoOGHdtKUxA3SgxUFa\ng3VJcmOGZOn11LWwS69BprsFpuyjq2UXJ2W3+nQDuSYTacofupsV9QS21zVoxCUS98GLspW3AuM1\nhZqJ0i7bYqnrAoBxAWJicJ3oFnBTjv/xXEAXv/wBMDajTHQmoVK3JS4Eq6VdJKtEpB8RGroORRtg\nvhq0OmVu+PIk8sWFG6FJK0VoO3s8vBFzt9/S29pi2iMLe2mRKcxGQXfJnIRLNU6tXqJ4UZyOYmTR\nwVF1cvQaWUwap3yjMXbzDdRcb8abvW263/wcK7JwWMKd/PGqiaUzsmLNx0KMkGRlCQQvTfNFDkri\nMO5RTuBQMzGGDyPr6CZ8l8ktx6amx1EusjWez8LhBezOrgPCq0C8eP/xXEAYH/wBNDaANGi6G1Ah\nXTd71WhFQq6SQRA9rVdcVYMw83cX9u3bt7h4PgQYGG65XetaT06wKn2OmuNu1aiuWaiyvsJD2AGr\nAhtkwJWAM1KfxDj4Ljg69jGY7OjpNlMEI9CHHBW6aNTYgwHlRUJFOomhC2TMRhwXmbRqIU0nPWpl\n6FAiSuEox1tLWNND2NXxQPwgLlxQsAJuQ6NNcFiHz92AczyLbF3fCFCqatv72caVdpFuDCXhQTqN\n189zB//xXEAXX/wBMDajTJ1RiZUuSZaroghaEqPiokaUHL1eNB4u8Fj/HuNJ/ZF/5dOy9fHdbtGo\ntZrXq6LQGE3YSdagnJ6z+iiz4jc/a09jUnyr8zt3yad/U45NVacmIO8ini6CjoLea9sqNZIRIiWc\n3CrSzy+MvvsvBPdGP2/OI7bJNiS1687vWdc+KS5vs6Q3potlmO3MGoAyKyOKKaSPjjooE4fvau4I\nnF7SkQtlw126JwdBdQvllA7zbv/xXEAYP/wBMjapETZGnEOiJKzcal5ZUmLpIkQR9JWidYIjqEYp\n5WpZiMDcO2qrBzwR3lQRWJ+9B8uHSg5qfF2hOXYwsXOt028trExVqo1acZJ3GmnUEY8D5DNjX8XM\nvt2sLlZD2+PDGfqB7eLvo62JlTKDIaFQVI7PFRo36c32CVT5rXfpxyCM+4WR27Z8u6nWV2WCyywy\nxlUU0gDyrQZYxLTJTcrDNuERvmpncaa4acmRhpycHikJmRwRupGNYg7/8VxAFx/8ATI2q2yNUqW5\nNIVKklJkhIiD4RdXoER826rwlbz1W/FeI12vHqVJLR8zWzR0YWdOvYUPcYpMuYRUrSwb4IIZ1BpE\n5h9CSc3ZvMMzYis18Mzwws+93mCdzwvoXaWAVKoUnTmTk5b0ZYpI1vWGRg0sEr1DLXpU/Hklm9Oc\n/m8rMuFF+S63kk36KLprz5m5SryaFlCysodZCZAAhXMThVciRTaJTMGpzW0SgzEiekBnPE6e//Fc\nQBrf/AE0NoAxpjIXSCHRIUXSNVkkoESJCB7Wu9BJ7z223Rbfj6ovC0i58mf2dTa2dXEZ99WtTjFM\nXaNXW1Uryq4tgO3IuJkUmIgAqbAzxozIbOSvR6Bh+3q5cOm5s7PDNx8Z60FZDn0O+/hUndd7Dqp9\nRl1niDIXnrGJk77/89XwmlhVl43mlRoZgWsEaszQZgssukCRolMiZ4i/nXXqrncWSHQC7o3yNZGR\nQZ7h4ZzPRPZbjs1jEcUZDENNbjH0I3dyS0DhCZ+U7weqQXEVq1SL8P/xXEAXP/wBNDaqELZGqVaY\nklRCrUEhIhHskqTWQRIOrye8yNYfhKint91UUy0NQmbGGtjeynhvyUIboAplynG2fJx52u43fYfi\n3mx0h11sldQVyAWM3Wtanqfm3uzPHspN5ndwz2TWe3xOOXMi0AWUoqVEIHzKvHjbS47LU5fKWhsD\ngMPvZyQmQ5ouw4zRK+14s3NOqDxZicVpmMLFnFK5OaKblPE6baUM4vpUZeIiI0EsxTVGspvw//Fc\nQBg//AE0NqtMdBacQ6Ii+VxVlSrVJl1ZdQR7wvLvgIkL6spcVCNN98BGXp3c3cdGUhbtuEygYYsl\ntfNH0qTNvY12w0zQdu+XRN8S7MCpDExjKOhDKZcbjVsp4CVz8non+awgL5kobtA6jQKTh5TOiFjM\nBHVmllx9XVbmCDZVykyM3GTCyEm1PFybWFKhuiFPZrmDEbrJknTPKyGqxQJ6aw4ihQdMEl4TjGOh\nIyJXVGAPoZshD3pUwYYZKqJqcP/xXEAXX/wBMDapUTY2qb1VKkvNVBKVYSREH1ZacVBE6qSJiwy5\nV1RYxp0SN54gcrriuQQPeCbacyTU0FWnBXis1lZmw2ketZvUtXU4cOPWGiZ0KB89+LM1/Q7/Axpz\nOsunuCxO05T4WZkTNz2yqbbHTcbK3I23sJ6EO9t9GWIcEFOda0QWb0Ji3u7k105UjJbZBU71Ga6N\naIsWz2IRZ0WAMgWW04LcrMJhBg1yPMJzCNppMw0gxrQZ8P/xXEAeX/wBODaAMaYaDYSDE5GU4pxu\nFQXkXUiVICF3Qsr7sE8yVwDgctlBwWKiTSj7XqyLtu4yQy0CqALRkdHfy8FkcC743lyJZpA7NBKo\nIeJZEJl+i9evwVkwEqogIMTcx6VGEmSAbCFK2IGQovYpQHprtOmkn7KZ1OUri5qzxFDIYH9IYBJU\nsWPsVXzzGdlUTZ4HZY+TST3izT01wSNZKFS5QdU65cnaL3gPZtpEI0uVBx4tvaduvpLrdGm+ZfOs\n+WGXhqloWWlbfm+OamefT1NBD8Nd7n+Hy+/7Q/DNEq+IoEVBK4RpKFgoq7QDags4//FcQBp//AE6\nNoA0+FMRBqFB6IQ6IRrUy1S11UCFSEgg9rnGXYCXZb2nJYSahlH3bwzdwcLNWMXSeHDqfu8dm+E2\nr+Stqcy1yEUtEGDJK54CUEQEQUT0aAH5B3mIjMuoILTnPjfLvYzdSCbftDlqCzp9A/TC+3aE2Vhx\nFJRXZ15/ZkQwV2fpTERWjGIN15Vjr1sMs3D0e4CbblIaMVTekK5eF72XoH4wnAwPLuKGcwAWvCYd\nGpN2SAPtXG6UhWlV79qXmCAXQaSuQFQCuRSxWu+VnP/xXEAWn/wBODaAMbROoVdMkpqs1CUlRCIQ\nPa4akCVclxbUc/Krf2ci3NcCEt25EQ/cbebiewfTfvfQtcWqjz71zNqRnmNugJhZJTkgGVFomtZE\n5mRm1N5+zM82omlUtniyVQSkhRmZ962nplyxEWiCCBk2UXWsK/hX85ucqbqU3R1VvtfnEihh6woV\njjbvF+1F1yDNw7VY8drnBDl7odiocBjdAqnHZdE7vE0KPu3trICN4P/xXEAXH/wBNDaANJiDFQmq\nVeUlzfFZcJS8RIiEfBqTNaoGdGz0clGgTmnuevZTf1At94XR7Y91dULohjso3x0e/Q1UGJPrQyIg\n26zDIlQQwYnkS8llNvTwRoJdJvIZrYhGxTzJr3HEiTOFr79TH1FmnU/pnDLuqjt+/Z0fyE+jyfJl\nCybw81kk2jASO6tWM57MKCIqLN5m1l8/aUL3TY6kJDfmPUi6OecJGDzOVCwfjmqhyXv2ea//8VxA\nFv/8ATQ2gDW2PqhdKWvJVlQEiRCPeLlOJAPVisTbRWSt4sc2oj1XTDwZbWMZpRdPOXIRaVVRiYWB\nK1sdUZsZQl1NMnw7dLUl7YSymmxlqqaM2Dh1o69TrzMZYplkg78GuRMoSonviVVO3TbAP9YWqoKg\nbujd0pVqbp9PZEKy8LE8og0+jddXaCTzFiHGtiQAdUO9FCiytaiRnUKqqryEkXFIAjQWY1Yr6p5z\nYyVWe5p1c+D/8VxAF5/8ATI2qpCGEgmCgtUqRuJU1SrFRKirkgj5iVd8VBEjgVRirEwtG0XW6Vo7\n0lh5NG6qarYc8665ykQuGvttd1gWBwmJgWah21MUrV8atSU6tm8s00FHMOtZVrHppWvIuJVpO5Ou\nKwv2PeyLnYzCZewnzvM+mi29afiKwNPWGGHZpx5MaLb6O+Z1rEMxqyhWmVlm8wzzRZDcfFHh3ZKQ\nEnWgPsntmYIEFQeh6PFLNMXTUNTvJM9OcP/xXEAXf/wBNjajyG0wh0RdYSKl0uiVdILQQ9c1VuOg\ncaxVPgiIvaiT2D/TLJqu0vth/JkcNO/lRKulgFv5k2HaXpmAopu0Lnl78W/A1jpz6pAQii99OcLS\nzZ98rJwEM1c0RW3p1OHA+MoxVSzhnFysBrzdqkAEI+MuMUNCLERGyTJpT0hMEDU4yaYL2PzfZk3s\nRXB3vmbPFUd8sL80BuIZ4kdPDGy4m7bX8iuHzx8rlcrINVjWwrZAvQ7/8VxAHR/8ATg2gDTYehCG\ngSEJmsqiKuKupUIgkCCEihBqGH3lzum9dqxut2OYnSPV0jB1Gqmo8ttHT4Um/OlDrBYnVCISwXcR\nyLmTxnn6lirLKhiie1oyBQx6uVBPpCmoWjN1oOpdTb2gMxf/CRT2tN6kDBQLBNpmQTYISIQJmpJO\nANyGkA07bqpsNz9+6JRDrcDpD5oUCLt25bc5mA6ljIfCWx+NQ77fjsZMVtmEMt9gj48CRc6SCnXe\nuRh+nssvbw83Dw0xdYEhrmVaCSMXl3Q+af3pULoq0lQIAJ1PnCiwIh93DSq4//FcQBb//AE6NoAx\npiITRIHTkZFkSpAQJEQelSSVxMEOgN2+fhqep7kHqTovfqsizjXONydxsdbUziuFk5ugmwWch0iO\nlakJyXopTBY7a+VNGymGfX1lXTvbabbMzbeuPSZu0xS8jHqG0KldnJ0AhthrIMRm9FqzESyBo6rc\ncdsuZYyHSDWpVWJAcz2kLTONIENDKBm2heBd1wi8oKp6KMdLjgBt0TYeOj1R2vKq026YGs2maVnA\n//FcQBff/AE2NoA0tooch6ZkKkZd5dQIIkqQSoebfsA0tM12rKIoyfWYNhUQRpHYd+KqtAW9VHU9\nR/83jJ6Ws76OXm9u8+zK7SVZ90+DU+rRfnl8O7jdcMmXu29Wv9tum0d4T7aQq29dm+alD/UfHXPS\nVhXnVzzKlp3hkwpJ7+YBXl43SoUZdRUaDG6AJBzrot2W3qL146FuUypTs4R4SnypTuhvbhtpV7F0\nJBoWKNHSXqMf+q8ZBOCjzAxlcP/xXEAXH/wBMDaANHirCQmpELlKq15esEqVeSIiIPucZE1dgwoD\ntd4CC2XKmfM36Nw8cNZNiZ9+7xLAiW02/pqNWiVmy66OMudvDvvNX8BNsMyMeqEHTTD2Oz12h4Vj\noMe/S4YxW0alJbp05bps6OSKLP0efqkthUVSkSaeeSWFlIhCpfVPCWGYQCYCCeq8xTheR3TNTYTK\n2tj1fwi6L/IdHb+Pj6siCURy64ZntvXrvs2xmEQaeBz/8VxAGD/8ATI2gDToehdGQdEIdGqqol1d\n4iVKESriB8y4k1KDIgeJTVHF/jAsU6wTcaXxkeDGhdG/P53rsOphZYYsiwhBgJYTRUSL+5JtbQ8w\nXrTaNRaYDJvXxXd2jU2OrrSGvAoZcyTG5VHb49vNCShQ2zinyaV29IotvyFXt9y6I26Ua7SQIvDT\ncg+JIyxBKhG7yi62edOE7zex5728QGQy1cP5dnfxUScTz1eaWrx6s6swUqlLr890qbJrpSq4//Fc\nQBw//AE2NoA05nIcjCNfMyCSEVBUQshBBLAsp56hdO2WSwZmwRsm8uAXd8nWRxCh9BmAnGqw4YtH\nCV4bsoDrXC7GX5IkmZT3y9l61ZeND+OzWhZ3n9JLXD7HM2r53WPf/eNf+U9PNN2+XaGhxC6XuyAa\nYloznDMBq83Wf1x2wtLYEPizmKtd2C1SSC+jcHbcPoULGpQAhcFd08zyn1p55wjLNEBfKXcF8wbD\nFU+ESbbFqHp4Skpe26XBCot9UgvQPDwpSZlGBO4VewBlXnRqBmAitRmpYCo4quyClKjg//FcQBdf\n/AE2NqpstC6kgqzXLWXUcbXS8ipUXCB85cRpoRInJnuG/eW26lo6JcaxL1gdwhVdLpdgrzsHlVdv\nhEjecRHs0iajqVk+yBRlfT1WOYsLVtPPEezx9fbTu29Q8/vZs7NH36a5tnc1Mp9du76xw+j+nyXB\nd7ebwlDxEt7vqCWs9EGjzXG4xJWUNUabhcbC1/JN0TFHJSvhKSajsqAcFILM9ODtIUduf14U0Ke6\nMONXYh5lZGWO\n', NULL, NULL, 0, 0, 0, 1, 0, '2021-06-23 04:26:16', '2021-06-23 04:26:16'),
(183, '54061bb2-9c2d-43e2-9008-4ae3136cd6b6', 47, 49, 'Jjjhjj', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-23 04:26:56', '2021-06-23 04:26:56'),
(184, '586eee13-0781-4c7f-94e8-d480e3599c17', 47, 49, 'Ggg', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-06-23 04:27:11', '2021-06-23 04:27:11'),
(185, '81439512-1C83-4CBE-A02F-A371EABF9C5B', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-25 05:54:17', '2021-06-25 05:54:17'),
(186, 'BD1F1B66-A18D-4616-AC95-64DC75B946B9', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-25 05:54:25', '2021-06-25 05:54:25'),
(187, '6256BAA0-6CF8-4DD2-8200-17ED6495EDEA', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-26 03:38:48', '2021-06-26 03:38:48'),
(188, '98D84D16-8006-41FB-B4F0-81CC50773AE3', 37, 44, 'Hey ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-06-26 03:38:54', '2021-06-26 03:38:54'),
(189, '6C635B0D-6EED-4CFD-945B-968843564435', 43, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-06-28 07:43:45', '2021-06-28 07:43:45'),
(190, '1EF70886-D5E4-466E-9001-F9D2B8AC3D72', 47, 33, 'Hdfh', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:28:32', '2021-07-02 13:28:32'),
(191, 'F52B0970-BF2E-45D4-BEDD-47FD050B9D87', 47, 33, 'Fhh', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:29:16', '2021-07-02 13:29:16'),
(192, '4DA0C546-B0A2-4E42-B29E-F6A5634737F6', 47, 33, 'Bhshd', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:29:40', '2021-07-02 13:29:40'),
(193, '9E44772E-0294-48E4-8072-35B8CE725BCA', 47, 33, 'Hddh', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:39:55', '2021-07-02 13:39:55'),
(194, 'a9c8ccf2-3dcb-4577-893d-b4326cfd8fc8', 33, 49, ' Gy', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-02 13:51:06', '2021-07-02 13:51:06'),
(195, 'f29c9eb3-ec2d-413b-aced-976b35524a82', 34, 49, ' Hu', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-02 13:51:19', '2021-07-02 13:51:19'),
(196, '7bb4d66c-7f21-4409-b0aa-1527fd52d005', 39, 49, 'Hii', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:51:34', '2021-07-02 13:51:34'),
(197, '834c4083-7251-4ec3-aefb-24a0f2b00e41', 50, 49, 'Hii', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:51:52', '2021-07-02 13:51:52'),
(198, '4670D1F7-33E6-4C04-876A-0CF51B1358EC', 49, 33, 'Hdh', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-02 13:52:47', '2021-07-02 13:52:47'),
(199, 'E4B14CDA-C8AA-42BC-8BF4-ECA8358B3058', 47, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-03 07:24:20', '2021-07-03 07:24:20'),
(200, '7803C476-6962-41E7-9CFD-75B367A23D46', 47, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-03 07:24:23', '2021-07-03 07:24:23'),
(201, '1D8BDBC2-9A4C-4575-A760-4BD661A88DAB', 47, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-03 07:24:25', '2021-07-03 07:24:25'),
(202, '25278960-6B46-4F85-A34E-31B8A2C3AC08', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-03 08:06:23', '2021-07-03 08:06:23'),
(203, '9E9E44FB-01E4-4759-9F37-3B65278EC9FB', 37, 44, NULL, NULL, NULL, '34.0844462346603', '-118.38511945690809', 0, 0, 1, 0, 0, '2021-07-03 08:06:30', '2021-07-03 08:06:30'),
(204, '68FA21F0-2799-48E6-BC52-EB4E86ED4FCB', 51, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-03 09:07:57', '2021-07-03 09:07:57'),
(205, '32C489C4-1F34-42B4-A735-607DBE71194C', 51, 44, 'Ho', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-03 09:15:57', '2021-07-03 09:15:57'),
(206, '3BCDC0D8-D048-4CDB-AF38-DD9BB86F9EF7', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-04 01:35:04', '2021-07-04 01:35:04'),
(207, 'AF3A4BC2-B173-4B4B-BB54-C2649ED8ECA5', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-04 01:35:07', '2021-07-04 01:35:07'),
(208, '850AFCB8-7FAA-45C0-9FD4-58AE140537F9', 37, 44, NULL, NULL, NULL, '34.084366671098024', '-118.38481021920244', 0, 0, 1, 0, 0, '2021-07-04 01:35:22', '2021-07-04 01:35:22'),
(209, '17BB79A4-B772-4406-ADEA-0C145730610D', 51, 44, 'Hu', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-04 22:41:57', '2021-07-04 22:41:57'),
(210, '6AC4E48B-688A-44BE-89D0-80D8253D1A88', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 00:33:55', '2021-07-05 00:33:55'),
(211, '214E73C0-A83E-40F4-B87E-0C5EB4C47B8C', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 00:33:59', '2021-07-05 00:33:59'),
(212, '6488B62C-2433-4526-9BDB-A9EC2A2EAFBC', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 02:02:56', '2021-07-05 02:02:56'),
(213, '533CF1F7-9DEA-41E4-A8BB-EC7E6B12B4DF', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 02:28:48', '2021-07-05 02:28:48'),
(214, '18F49882-C2F7-4E3F-B7D3-2424A9325624', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 03:19:54', '2021-07-05 03:19:54'),
(215, 'BD247A13-87ED-41E8-8F93-90D749C2C724', 37, 44, 'Ho', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 03:19:58', '2021-07-05 03:19:58'),
(216, 'C73B1429-1986-4B5F-AD30-71C9EB682F64', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 04:06:42', '2021-07-05 04:06:42'),
(217, '358FBB78-D83C-4FE1-9023-8E0C53EBCA4B', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-05 04:06:47', '2021-07-05 04:06:47'),
(218, '5ADEEF63-BDE1-408F-B475-60E0162D389C', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-07 06:00:42', '2021-07-07 06:00:42'),
(219, 'BE7B303F-91A0-4255-862B-A8D241267FB8', 37, 44, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-07 06:00:46', '2021-07-07 06:00:46'),
(220, '182C126E-1F0E-4C19-84E5-23D30DCF9A08', 37, 44, NULL, NULL, NULL, '33.689360194463205', '-117.72981539959613', 0, 0, 1, 0, 0, '2021-07-07 06:00:50', '2021-07-07 06:00:50'),
(221, '89999A17-FD20-4D2F-8F61-1288832CC093', 37, 44, NULL, NULL, NULL, '34.06597693871766', '-118.44653997436679', 0, 0, 1, 0, 0, '2021-07-09 19:12:01', '2021-07-09 19:12:01'),
(222, 'EA8BEEEF-8B36-4714-A7DE-CAD2A111A2A7', 50, 47, 'Hhh', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-14 06:52:49', '2021-07-14 06:52:49'),
(223, '18b577a6-988c-4967-b2e2-2c1ce5b2cf9e', 43, 33, 'Hrhhhd', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-14 11:50:45', '2021-07-14 11:50:45'),
(224, 'af12cea2-13d7-409b-808e-b504ddabdc43', 43, 33, 'Dhd', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-14 11:50:51', '2021-07-14 11:50:51'),
(225, '2bd4b064-4fd9-4b7f-b3ea-a421ffb845b0', 43, 33, 'Hd', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-14 11:50:55', '2021-07-14 11:50:55'),
(226, '4bd599cc-09ad-4664-b17e-aca3fc8a40a7', 38, 33, 'Hfh', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-14 11:51:23', '2021-07-14 11:51:23'),
(227, '9be8e93d-a01b-4cec-a35b-aaf6f7111418', 38, 33, 'Gg', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-14 11:51:32', '2021-07-14 11:51:32'),
(228, '12b94e47-0142-4123-bc6a-cedeb43bc0e5', 41, 33, 'Dhhf', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-14 11:51:57', '2021-07-14 11:51:57'),
(229, '8c45ebe8-5449-4008-9c74-803288b29786', 41, 33, 'Hdh', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-14 11:52:00', '2021-07-14 11:52:00'),
(230, 'fd20a0d4-750e-418d-8c5b-803d65222071', 39, 33, 'Jfdu', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-14 11:57:03', '2021-07-14 11:57:03'),
(231, 'C5F7B710-19D8-4F7F-A113-E54EFF256296', 37, 44, 'Hi ', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-15 21:32:14', '2021-07-15 21:32:14'),
(232, '95ad6cf6-fd83-484a-aa51-41352dd3f7ca', 50, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:04:59', '2021-07-16 13:04:59'),
(233, '5fd7462e-9054-4254-824e-d3c8cc189505', 50, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:23:35', '2021-07-16 13:23:35'),
(234, '567d1ad5-22fd-4a36-a377-bc1da60871aa', 50, 33, 'Hr', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:23:38', '2021-07-16 13:23:38'),
(235, '3bc23f39-a975-4fa0-a1cf-a3f55052f574', 49, 53, 'Hii', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:58:43', '2021-07-16 13:58:43'),
(236, 'de8520f1-e143-45bd-a953-cef639bf3b95', 49, 53, 'Hello', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:59:11', '2021-07-16 13:59:11'),
(237, '5f55104c-dfe8-43ab-a745-b637d6c0d2c9', 45, 53, 'Hii', NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, '2021-07-16 13:59:24', '2021-07-16 13:59:24'),
(238, '1de1f397-aceb-4884-ba28-b61b3681f35b', 54, 33, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-16 14:00:58', '2021-07-16 14:00:58'),
(239, '2649B289-E8F7-462F-B703-BF23758041D1', 56, 55, 'Hhj', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-17 06:39:58', '2021-07-17 06:39:58'),
(240, '94abb1cf-1e6f-48c5-9f0e-dab6a0b0b7dc', 64, 63, 'Hi', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-27 04:43:04', '2021-07-27 04:43:04'),
(241, '30bb214d-00bf-4cda-9212-9afdf28354ef', 63, 64, 'Hello', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, '2021-07-27 04:43:11', '2021-07-27 04:43:11'),
(242, '26659a44-1c01-4daf-b421-7ebaea16b56e', 63, 64, NULL, NULL, NULL, '23.0443702', '72.5162005', 0, 0, 1, 0, 0, '2021-07-27 04:43:33', '2021-07-27 04:43:33'),
(243, '7885301c-9324-42f3-a301-1b26b5d92d59', 61, 63, 'Hiii', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1, '2021-07-27 04:45:47', '2021-07-27 04:45:47');

-- --------------------------------------------------------

--
-- Table structure for table `messages_read_unread`
--

CREATE TABLE `messages_read_unread` (
  `id` int(11) NOT NULL,
  `user_from_id` int(11) DEFAULT NULL,
  `user_to_id` int(11) DEFAULT NULL,
  `read` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages_read_unread`
--

INSERT INTO `messages_read_unread` (`id`, `user_from_id`, `user_to_id`, `read`) VALUES
(2, 32, 33, 0),
(3, 33, 32, 0),
(4, 34, 32, 0),
(5, 39, 32, 0),
(6, 40, 33, 0),
(7, 40, 32, 0),
(8, 38, 33, 0),
(9, 41, 32, 0),
(10, 32, 41, 0),
(11, 41, 33, 0),
(12, 33, 41, 1),
(13, 38, 32, 0),
(14, 42, 33, 0),
(15, 39, 33, 0),
(16, 37, 33, 0),
(17, 35, 33, 0),
(18, 34, 33, 0),
(19, 38, 35, 0),
(20, 32, 35, 0),
(21, 37, 36, 1),
(22, 35, 32, 0),
(23, 36, 37, 1),
(24, 37, 44, 0),
(25, 44, 37, 0),
(26, 45, 34, 0),
(27, 34, 45, 0),
(28, 41, 34, 0),
(29, 42, 47, 0),
(30, 32, NULL, 0),
(31, 44, 32, 0),
(32, 47, 32, 0),
(33, 33, NULL, 0),
(34, 47, 44, 0),
(35, 32, 34, 1),
(36, 32, 44, 0),
(37, 48, 32, 0),
(38, 45, 32, 0),
(39, 48, 34, 0),
(40, 43, 34, 0),
(41, 48, 44, 0),
(42, 32, 47, 0),
(43, 47, 33, 0),
(44, 33, 47, 0),
(45, 45, 33, 0),
(46, 44, 47, 0),
(47, 33, NULL, 0),
(48, 43, 33, 0),
(49, 43, 32, 0),
(50, 44, 48, 0),
(51, 32, 32, 0),
(52, 43, 48, 0),
(53, 34, 48, 0),
(54, 32, 48, 0),
(55, 47, 49, 0),
(56, 49, 47, 0),
(57, 49, 34, 0),
(58, 37, 32, 0),
(59, 45, 36, 1),
(60, 32, 37, 0),
(61, 40, 37, 0),
(62, 43, 36, 0),
(63, 33, 36, 1),
(64, 36, 33, 0),
(65, 50, 34, 1),
(66, 34, 50, 0),
(67, 50, 47, 0),
(68, 34, 47, 0),
(69, 47, 34, 0),
(70, 33, 34, 0),
(71, 47, 50, 0),
(72, 34, 39, 0),
(73, 39, 34, 1),
(74, 32, 39, 1),
(75, 33, 39, 0),
(76, 50, 33, 0),
(77, 43, 47, 0),
(78, 47, NULL, 0),
(79, 33, NULL, 0),
(80, 33, 50, 0),
(81, 50, 32, 0),
(82, 32, 50, 0),
(83, 49, 50, 0),
(84, 49, 39, 0),
(85, 50, 39, 0),
(86, 39, 49, 0),
(87, 42, 44, 0),
(88, 34, 49, 0),
(89, 50, 49, 0),
(90, 38, 44, 0),
(91, 43, 44, 0),
(92, 41, 44, 0),
(93, 40, 47, 0),
(94, 41, 51, 0),
(95, 50, 51, 0),
(96, 51, 44, 0),
(97, 33, 49, 0),
(98, 49, 33, 1),
(99, 44, 52, 0),
(100, 52, 44, 0),
(101, 49, 53, 0),
(102, 45, 53, 0),
(103, 54, 33, 0),
(104, 56, 55, 0),
(105, 59, 44, 0),
(106, 53, 60, 0),
(107, 63, 64, 0),
(108, 64, 63, 0),
(109, 61, 63, 0),
(110, 56, 33, 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages_test`
--

CREATE TABLE `messages_test` (
  `id` int(11) NOT NULL,
  `user_from_id` int(11) DEFAULT NULL,
  `user_to_id` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_08_051210_add_first_name_to_users_table', 2),
(5, '2020_09_08_052051_add_first_name_to_users', 3),
(6, '2020_09_08_052821_add_last_name_to_users_table', 4),
(9, '2020_09_08_060725_add_first_name_and_last_name_and_phone_and_photo_and_date_of_birth_and_about_me_to_users', 5),
(10, '2020_09_08_062335_add_height_and_weight_and_relationship_status_and_i_am_and_body_type_to_users', 6),
(12, '2020_09_08_062849_add_position_and_ethnicity_and_what_looking_for_and_latitude_and_longitude_and_zipcode_and_address_to_users', 7),
(14, '2020_09_08_063736_add_notification_enable_status_and_profile_enabled_and_online_status_and_to_users', 8),
(15, '2016_06_01_000001_create_oauth_auth_codes_table', 9),
(16, '2016_06_01_000002_create_oauth_access_tokens_table', 9),
(17, '2016_06_01_000003_create_oauth_refresh_tokens_table', 9),
(18, '2016_06_01_000004_create_oauth_clients_table', 9),
(19, '2016_06_01_000005_create_oauth_personal_access_clients_table', 9),
(20, '2020_09_08_111304_create_favourites_table', 10),
(21, '2020_09_08_114601_add_visitor_counts_and_like_count_to_users', 11),
(22, '2020_09_08_115250_create_blocked_users_table', 12),
(23, '2020_09_08_115546_create_report_users_table', 13),
(24, '2020_09_08_121105_add_health_report_to_users', 14),
(25, '2020_09_10_102914_create_app_logins_table', 15),
(26, '2020_09_19_091131_add_session_id_to_app_logins', 16),
(27, '2020_09_22_063332_add_health_status_to_users', 17),
(28, '2020_09_22_075212_create_ethnicities_table', 18),
(29, '2020_09_22_081344_create_lookingfors_table', 19),
(30, '2020_09_25_071941_create_body_types_table', 20),
(31, '2020_09_25_072153_create_iams_table', 20),
(35, '2020_09_25_072919_create_positions_table', 21),
(36, '2020_09_26_051325_create_user_likes_table', 21),
(37, '2020_09_29_073241_create_admins_table', 22),
(38, '2020_10_19_072816_create_premium_table', 23),
(39, '2020_10_19_111231_create_user_subscription_table', 24),
(40, '2020_11_18_080211_create_messages_table', 25),
(41, '2020_11_18_104128_create_purrs_table', 26),
(42, '2020_11_21_052322_create_report_table', 27),
(43, '2020_11_21_101446_create_report_msgs_table', 28),
(45, '2020_11_25_121616_create_health_table', 29),
(46, '2020_11_25_122334_create_health_user_table', 29),
(48, '2020_11_27_073054_create_user_video_call_tokens_table', 30),
(49, '2020_09_08_111304_create_views_table', 31),
(50, '2014_10_12_000000_create_premium_history_table', 32),
(51, '2020_09_08_111304_create_chat_user_table', 33),
(52, '2020_09_08_111304_create_private_album_table', 34);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'NtvbUdSDqvYqTEYXuR59MBfTcjZnr2A4J82CeiW7', NULL, 'http://localhost', 1, 0, 0, '2020-09-08 10:07:54', '2020-09-08 10:07:54'),
(2, NULL, 'Laravel Password Grant Client', 'F7iHlNakz7dLm6oSIdo1KtVc4vzho7YQV4FXZ6BH', 'users', 'http://localhost', 0, 1, 0, '2020-09-08 10:07:54', '2020-09-08 10:07:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-08 10:07:54', '2020-09-08 10:07:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`id`, `phone`, `otp`, `created_at`, `updated_at`) VALUES
(1, '', '5555', NULL, NULL),
(2, '+9759855748751', '5555', NULL, NULL),
(3, '+19090909090', '5555', NULL, NULL),
(4, '+19090909090', '5555', NULL, NULL),
(5, '+19090909090', '5555', NULL, NULL),
(6, '+19090909090', '5555', NULL, NULL),
(7, '+16309882244', '5555', NULL, NULL),
(8, '+16309882244', '5555', NULL, NULL),
(11, '+11234567890', '5555', NULL, NULL),
(12, '+919501759261', '5555', NULL, NULL),
(14, '+19501759261', '5555', NULL, NULL),
(15, '+919501759261', '5555', NULL, NULL),
(17, '+19501759261', '5555', NULL, NULL),
(18, '+919501759261', '5555', NULL, NULL),
(20, '+19501759261', '5555', NULL, NULL),
(21, '+919501759261', '5555', NULL, NULL),
(23, '+19501759261', '5555', NULL, NULL),
(24, '+919501759261', '5555', NULL, NULL),
(26, '+917500036170', '5555', NULL, NULL),
(28, '+919154792513', '5555', NULL, NULL),
(29, '+919154792513', '3087', NULL, NULL),
(30, '+919154792513', '5555', NULL, NULL),
(32, '+919464032007', '5555', NULL, NULL),
(33, '+919464032007', '7019', NULL, NULL),
(34, '+16693334215', '5555', NULL, NULL),
(36, '+13322176301', '5555', NULL, NULL),
(37, '+13322176301', '2710', NULL, NULL),
(38, '+16203221059', '5555', NULL, NULL),
(40, '+16203221059', '5555', NULL, NULL),
(41, '+16203221059', '6246', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Submissive', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(2, 'Dominant', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(3, 'Versatile', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(4, 'Oral only', '2020-09-26 00:00:00', '2020-09-26 00:00:00'),
(5, 'Other', '2020-09-26 00:00:00', '2020-09-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `premium`
--

CREATE TABLE `premium` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float NOT NULL,
  `purr_limit` int(11) DEFAULT NULL,
  `favourite_limit` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `premium`
--

INSERT INTO `premium` (`id`, `title`, `plan`, `plan_id`, `amount`, `purr_limit`, `favourite_limit`, `created_at`, `updated_at`) VALUES
(1, 'Plan1', '1', 'plan_IigWc9fr43IhQp', 4.99, 100, 100, '2021-01-08 07:13:03', '2021-01-08 07:13:04'),
(2, 'Plan2', '12', 'plan_IigXWTMOCD5sKo', 54.89, 500, 500, '2021-01-08 07:13:47', '2021-06-11 06:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `premium_history`
--

CREATE TABLE `premium_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `premium_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `other_user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=favourite, 2=purr',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '1=count',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `premium_history`
--

INSERT INTO `premium_history` (`id`, `premium_id`, `user_id`, `other_user_id`, `type`, `count`, `created_at`, `updated_at`) VALUES
(1, 1, 33, 32, 2, 1, '2021-04-05 14:28:42', '2021-04-05 14:28:42'),
(2, 1, 32, 33, 2, 1, '2021-04-05 14:35:10', '2021-04-05 14:35:10'),
(3, 1, 33, 32, 1, 1, '2021-04-05 14:38:36', '2021-04-05 14:38:36'),
(4, 1, 32, 33, 1, 1, '2021-04-05 14:38:43', '2021-04-05 14:38:43'),
(5, 1, 34, 32, 2, 1, '2021-04-06 05:46:21', '2021-04-06 05:46:21'),
(6, 1, 32, 34, 2, 1, '2021-04-06 05:47:38', '2021-04-06 05:47:38'),
(7, 1, 34, 33, 1, 1, '2021-04-06 06:32:20', '2021-04-06 06:32:20'),
(8, 2, 37, 36, 1, 1, '2021-04-09 18:48:19', '2021-04-09 18:48:19'),
(9, 2, 36, 37, 1, 1, '2021-04-09 18:48:28', '2021-04-09 18:48:28'),
(10, 2, 36, 37, 2, 1, '2021-04-09 18:53:45', '2021-04-09 18:53:45'),
(11, 2, 37, 36, 2, 1, '2021-04-09 18:55:00', '2021-04-09 18:55:00'),
(12, 1, 32, 37, 2, 1, '2021-04-12 11:49:52', '2021-04-12 11:49:52'),
(13, 1, 39, 32, 2, 1, '2021-04-13 07:48:11', '2021-04-13 07:48:11'),
(14, 1, 32, 39, 2, 1, '2021-04-13 08:07:41', '2021-04-13 08:07:41'),
(15, 1, 32, 40, 2, 1, '2021-04-13 09:39:58', '2021-04-13 09:39:58'),
(16, 1, 32, 38, 2, 1, '2021-04-13 09:45:15', '2021-04-13 09:45:15'),
(17, 1, 32, 38, 1, 1, '2021-04-13 09:46:06', '2021-04-13 09:46:06'),
(18, 1, 40, 32, 2, 1, '2021-04-13 09:48:31', '2021-04-13 09:48:31'),
(19, 2, 36, 5, 2, 1, '2021-04-15 09:28:22', '2021-04-15 09:28:22'),
(20, 2, 37, 35, 2, 1, '2021-04-15 12:03:30', '2021-04-15 12:03:30'),
(21, 2, 37, 32, 1, 1, '2021-04-15 12:42:41', '2021-04-15 12:42:41'),
(22, 1, 33, 35, 2, 1, '2021-04-15 13:12:35', '2021-04-15 13:12:35'),
(23, 1, 33, 42, 2, 1, '2021-04-16 04:59:41', '2021-04-16 04:59:41'),
(24, 1, 33, 42, 1, 1, '2021-04-16 05:36:34', '2021-04-16 05:36:34'),
(25, 1, 33, 38, 1, 1, '2021-04-16 12:35:11', '2021-04-16 12:35:11'),
(26, 2, 36, 38, 2, 1, '2021-04-16 16:28:52', '2021-04-16 16:28:52'),
(27, 2, 36, 42, 2, 1, '2021-04-16 21:09:19', '2021-04-16 21:09:19'),
(28, 2, 36, 40, 2, 1, '2021-04-18 03:53:29', '2021-04-18 03:53:29'),
(29, 2, 44, 37, 1, 1, '2021-04-23 15:43:10', '2021-04-23 15:43:10'),
(30, 2, 44, 37, 2, 1, '2021-04-23 17:11:54', '2021-04-23 17:11:54'),
(31, 2, 37, 44, 1, 1, '2021-04-23 17:38:33', '2021-04-23 17:38:33'),
(32, 2, 37, 44, 2, 1, '2021-04-23 19:24:55', '2021-04-23 19:24:55'),
(33, 1, 34, 45, 1, 1, '2021-04-24 04:40:42', '2021-04-24 04:40:42'),
(34, 1, 34, 45, 2, 1, '2021-04-24 04:48:48', '2021-04-24 04:48:48'),
(35, 1, 34, 44, 2, 1, '2021-04-24 06:09:54', '2021-04-24 06:09:54'),
(36, 1, 34, 42, 1, 1, '2021-04-24 06:21:35', '2021-04-24 06:21:35'),
(37, 1, 34, 42, 2, 1, '2021-04-24 06:21:39', '2021-04-24 06:21:39'),
(38, 1, 33, 38, 2, 1, '2021-05-01 06:52:40', '2021-05-01 06:52:40'),
(39, 1, 33, 34, 2, 1, '2021-05-01 06:52:56', '2021-05-01 06:52:56'),
(40, 1, 34, 33, 2, 1, '2021-05-01 06:55:22', '2021-05-01 06:55:22'),
(41, 2, 44, 47, 2, 1, '2021-05-01 06:58:56', '2021-05-01 06:58:56'),
(42, 2, 44, 47, 1, 1, '2021-05-01 06:58:59', '2021-05-01 06:58:59'),
(43, 1, 34, 43, 1, 1, '2021-05-03 04:10:43', '2021-05-03 04:10:43'),
(44, 1, 34, 43, 2, 1, '2021-05-03 04:28:20', '2021-05-03 04:28:20'),
(45, 1, 34, 48, 2, 1, '2021-05-08 05:00:55', '2021-05-08 05:00:55'),
(46, 1, 34, 48, 1, 1, '2021-05-08 05:05:06', '2021-05-08 05:05:06'),
(47, 2, 44, 34, 2, 1, '2021-05-19 01:19:06', '2021-05-19 01:19:06'),
(48, 2, 44, 38, 2, 1, '2021-05-19 01:19:11', '2021-05-19 01:19:11'),
(49, 1, 34, 49, 1, 1, '2021-05-19 04:53:41', '2021-05-19 04:53:41'),
(50, 1, 34, 49, 2, 1, '2021-05-19 04:56:19', '2021-05-19 04:56:19'),
(51, 2, 36, 33, 2, 1, '2021-05-19 05:50:58', '2021-05-19 05:50:58'),
(52, 1, 34, 50, 1, 1, '2021-05-19 06:11:56', '2021-05-19 06:11:56'),
(53, 2, 50, 34, 1, 1, '2021-05-19 07:07:54', '2021-05-19 07:07:54'),
(54, 2, 50, 47, 1, 1, '2021-05-19 07:27:23', '2021-05-19 07:27:23'),
(55, 1, 39, 34, 1, 1, '2021-05-19 08:57:44', '2021-05-19 08:57:44'),
(56, 1, 33, 47, 2, 1, '2021-05-21 07:12:21', '2021-05-21 07:12:21'),
(57, 2, 50, 33, 2, 1, '2021-05-28 10:48:36', '2021-05-28 10:48:36'),
(58, 1, 33, 50, 2, 1, '2021-05-28 11:13:08', '2021-05-28 11:13:08'),
(59, 2, 50, 49, 2, 1, '2021-05-28 11:22:55', '2021-05-28 11:22:55'),
(60, 2, 44, 43, 1, 1, '2021-05-29 03:24:39', '2021-05-29 03:24:39'),
(61, 2, 44, 42, 1, 1, '2021-05-29 03:25:15', '2021-05-29 03:25:15'),
(62, 2, 44, 42, 2, 1, '2021-05-29 03:25:45', '2021-05-29 03:25:45'),
(63, 1, 34, 50, 2, 1, '2021-05-29 10:35:44', '2021-05-29 10:35:44'),
(64, 2, 44, 43, 2, 1, '2021-05-30 04:23:17', '2021-05-30 04:23:17'),
(65, 2, 44, 41, 2, 1, '2021-05-31 04:39:57', '2021-05-31 04:39:57'),
(66, 2, 50, 49, 1, 1, '2021-06-01 07:24:40', '2021-06-01 07:24:40'),
(67, 2, 44, 52, 1, 1, '2021-06-14 11:21:02', '2021-06-14 11:21:02'),
(68, 2, 44, 52, 2, 1, '2021-06-25 05:54:50', '2021-06-25 05:54:50'),
(69, 1, 33, 47, 1, 1, '2021-06-25 10:48:58', '2021-06-25 10:48:58'),
(70, 2, 44, 51, 2, 1, '2021-07-03 09:08:15', '2021-07-03 09:08:15'),
(71, 1, 49, 38, 1, 1, '2021-07-14 07:10:26', '2021-07-14 07:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `private_album`
--

CREATE TABLE `private_album` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `private_album`
--

INSERT INTO `private_album` (`id`, `user_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 36, 'drxpJWqto4_1DADD87F-334D-4A4A-AA0F-00AFF897CF8A.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(2, 36, 'vN6pE7ghza_99484172-7489-4799-B72C-CBD7E123C132.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(3, 36, 'mziil9E3ih_F0C993F1-004B-47A6-8DEA-E380B48E30C4.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(4, 36, 'Qv510d3ZWp_A0A222D1-F5E1-4C3D-9261-2576ECA5F626.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(5, 36, '5zRGMCvaMr_4CEFA395-75F8-4616-9120-25FC2284BBC7.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(6, 36, 'nj8fcKvJRS_8440867A-C8D5-4FF3-A2ED-2CA397C20DE6.jpg', '2021-04-09 17:28:22', '2021-04-09 17:28:22'),
(7, 37, 'XItrRJFPTV_52CA63FD-5F1B-4E39-9611-D125ADF2D68D.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(8, 37, '1TEJ2ZLMxS_01DFA93C-0FD3-419D-99F3-1C71F3BD167F.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(9, 37, '1FF7Zxlzjd_67591778-C84C-4A0E-85B6-B1717435FD68.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(10, 37, 'NmDp7GA6ZJ_0B8674AE-8920-4320-ADE5-1BD825C7063D.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(11, 37, 'D0rxdWlvH9_27A46661-E0C5-46E4-9B23-4BA0E222D745.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(12, 37, 'JIlFyQY34N_4C1033DA-194E-4DE1-A7CC-CAD922AF4B7E.jpg', '2021-04-09 18:33:46', '2021-04-09 18:33:46'),
(13, 32, 'DAgbB9Y6tJ_73928c72-e7a2-435b-8fbe-01be2c0a7136.jpg', '2021-04-13 10:04:32', '2021-04-13 10:04:32'),
(14, 32, 'erIOLUlEg9_0ec778a9-9404-49a4-9e64-aaa8329dfb5b.jpg', '2021-04-13 10:05:09', '2021-04-13 10:05:09'),
(15, 44, '1x6yUARDXl_34C7D07F-9736-42A8-8BBD-13D40A6061F5.jpg', '2021-04-23 15:42:41', '2021-04-23 15:42:41'),
(16, 44, 'TC9NJ1eJLC_DA133E7F-4BFE-44C8-A6E8-75DBF3675D11.jpg', '2021-04-23 15:42:41', '2021-04-23 15:42:41'),
(17, 44, 'Z7QumsxejE_6DFDFD3B-B69F-45BC-B9DF-EC5B35ED15FD.jpg', '2021-04-23 15:42:41', '2021-04-23 15:42:41'),
(18, 44, 'acumoFOQZA_7CF90A0A-2DF7-41B4-8E84-E806B6F6AA85.jpg', '2021-04-23 15:42:41', '2021-04-23 15:42:41'),
(19, 44, 'nfYP7ObQ0o_1D26DB59-016C-4981-B2CC-DC2BCC035E05.jpg', '2021-04-23 15:42:41', '2021-04-23 15:42:41'),
(20, 34, 'lPKBFZoezl_f2c99fcf-1d4e-4190-9873-72349f08e6e4.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(21, 34, 'OhV9mvnHnk_3ca5dff8-cc82-47e5-bc9a-bd16b08f3901.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(23, 34, 'fCMCgN24k4_5dc87d6a-6168-4795-8aa9-f77c5f5e01c1.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(24, 34, 'Q8tA6afcvp_fe618cc4-7734-4885-817c-2b66e104f833.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(25, 34, 'QIuEHnzIHV_4977b29e-ced8-491a-b7c4-581f0e69eff3.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(26, 34, 'DDknmRbn6u_e4204838-8cfa-478e-a248-fb8321116d7a.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(27, 34, 'lnqsuByHg6_aa549410-0a27-48f1-8745-1be40032ad8f.jpg', '2021-05-08 07:00:00', '2021-05-08 07:00:00'),
(28, 34, 'Q2BKScgvTm_f4374eaf-b7bf-42d1-b2a8-574a6e370a8d.jpg', '2021-05-19 05:29:15', '2021-05-19 05:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `private_album_rights`
--

CREATE TABLE `private_album_rights` (
  `id` int(11) NOT NULL,
  `user_from_id` int(11) DEFAULT NULL,
  `user_to_id` int(11) DEFAULT NULL,
  `album_given_access` int(11) NOT NULL DEFAULT '0',
  `album_has_access` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `private_album_rights`
--

INSERT INTO `private_album_rights` (`id`, `user_from_id`, `user_to_id`, `album_given_access`, `album_has_access`) VALUES
(1, 33, 32, 0, 0),
(2, 32, 33, 0, 0),
(3, 37, 36, 1, 1),
(4, 36, 37, 1, 1),
(5, 32, 39, 0, 0),
(6, 39, 32, 0, 0),
(7, 37, 32, 0, 0),
(8, 32, 37, 0, 0),
(9, 33, 38, 1, 0),
(10, 38, 33, 0, 1),
(11, 37, 44, 0, 0),
(12, 44, 37, 0, 0),
(13, 34, 45, 0, 0),
(14, 45, 34, 0, 0),
(15, 32, 47, 0, 0),
(16, 47, 32, 0, 0),
(17, 34, 43, 0, 0),
(18, 43, 34, 0, 0),
(19, 44, 48, 0, 0),
(20, 48, 44, 0, 0),
(21, 34, 49, 0, 0),
(22, 49, 34, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purrs`
--

CREATE TABLE `purrs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `liked_user_id` int(11) NOT NULL,
  `accept` int(11) NOT NULL DEFAULT '0' COMMENT '1 = accepted, 0 = not',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purrs`
--

INSERT INTO `purrs` (`id`, `user_id`, `liked_user_id`, `accept`, `created_at`, `updated_at`) VALUES
(2, 47, 32, 0, '2021-05-18 10:45:11', '2021-05-18 10:45:11'),
(7, 32, 47, 0, '2021-05-18 10:58:57', '2021-05-18 10:58:57'),
(9, 49, 32, 0, '2021-05-18 11:40:03', '2021-05-18 11:40:03'),
(17, 49, 47, 0, '2021-05-18 11:51:39', '2021-05-18 11:51:39'),
(22, 44, 38, 0, '2021-05-19 01:19:11', '2021-05-19 01:19:11'),
(23, 34, 49, 0, '2021-05-19 04:56:19', '2021-05-19 04:56:19'),
(27, 34, 44, 0, '2021-05-19 04:57:22', '2021-05-19 04:57:22'),
(29, 36, 37, 0, '2021-05-19 05:50:41', '2021-05-19 05:50:41'),
(32, 36, 33, 0, '2021-05-19 05:53:30', '2021-05-19 05:53:30'),
(42, 47, 33, 0, '2021-05-19 06:08:56', '2021-05-19 06:08:56'),
(43, 50, 34, 0, '2021-05-19 06:15:55', '2021-05-19 06:15:55'),
(46, 34, 33, 0, '2021-05-19 06:46:05', '2021-05-19 06:46:05'),
(47, 33, 34, 0, '2021-05-19 06:46:49', '2021-05-19 06:46:49'),
(52, 33, 47, 0, '2021-05-21 07:12:37', '2021-05-21 07:12:37'),
(53, 33, 38, 0, '2021-05-21 10:49:35', '2021-05-21 10:49:35'),
(59, 44, 34, 0, '2021-05-21 19:42:23', '2021-05-21 19:42:23'),
(64, 32, 33, 0, '2021-05-28 10:47:28', '2021-05-28 10:47:28'),
(68, 33, 50, 0, '2021-05-28 11:13:14', '2021-05-28 11:13:14'),
(69, 50, 33, 0, '2021-05-28 11:15:09', '2021-05-28 11:15:09'),
(70, 50, 49, 0, '2021-05-28 11:22:55', '2021-05-28 11:22:55'),
(71, 49, 39, 0, '2021-05-28 13:04:59', '2021-05-28 13:04:59'),
(76, 39, 49, 0, '2021-05-28 13:52:15', '2021-05-28 13:52:15'),
(77, 44, 42, 0, '2021-05-29 03:25:45', '2021-05-29 03:25:45'),
(78, 34, 50, 0, '2021-05-29 10:35:44', '2021-05-29 10:35:44'),
(89, 34, 42, 0, '2021-05-29 11:04:33', '2021-05-29 11:04:33'),
(94, 44, 43, 0, '2021-05-30 22:32:07', '2021-05-30 22:32:07'),
(105, 47, 49, 0, '2021-06-02 04:56:25', '2021-06-02 04:56:25'),
(115, 51, 50, 0, '2021-06-02 05:44:01', '2021-06-02 05:44:01'),
(118, 37, 44, 0, '2021-06-05 04:06:47', '2021-06-05 04:06:47'),
(122, 44, 41, 0, '2021-06-07 18:08:14', '2021-06-07 18:08:14'),
(125, 51, 33, 0, '2021-06-10 10:26:50', '2021-06-10 10:26:50'),
(137, 52, 44, 0, '2021-06-13 18:49:38', '2021-06-13 18:49:38'),
(146, 44, 52, 0, '2021-07-02 22:13:21', '2021-07-02 22:13:21'),
(152, 44, 51, 0, '2021-07-03 09:16:34', '2021-07-03 09:16:34'),
(163, 44, 37, 0, '2021-07-15 21:32:47', '2021-07-15 21:32:47'),
(164, 63, 64, 0, '2021-07-27 04:44:47', '2021-07-27 04:44:47'),
(165, 64, 62, 0, '2021-07-27 04:45:06', '2021-07-27 04:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `report_images`
--

CREATE TABLE `report_images` (
  `id` int(11) NOT NULL,
  `report_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_images`
--

INSERT INTO `report_images` (`id`, `report_id`, `user_id`, `images`, `created_at`, `updated_at`) VALUES
(1, 1, 36, 'AT5hpUXvPD_IMG_2643.PNG', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(2, 2, 36, 'gWaSlpgRPI_IMG_2642.PNG', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(3, 3, 36, 'bj2tVPcJHw_IMG_2641.PNG', '2021-04-09 17:16:57', '2021-04-09 17:16:57'),
(4, 1, 37, 'uDJUAn9uvC_IMG_2641.PNG', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(5, 2, 37, 'msyLTb0eiO_IMG_2642.PNG', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(6, 3, 37, 'Ta1VfaWfIg_IMG_2643.PNG', '2021-04-09 18:31:53', '2021-04-09 18:31:53'),
(7, 2, 32, 'YEMFKCEgBD_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:05', '2021-04-13 10:18:05'),
(8, 2, 32, 'F2IzwwVuU5_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:09', '2021-04-13 10:18:09'),
(9, 2, 32, 'g8SfWXeapW_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:09', '2021-04-13 10:18:09'),
(10, 2, 32, '7SJAQcRjjg_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:09', '2021-04-13 10:18:09'),
(11, 2, 32, 'sV8nPnZ1wv_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:13', '2021-04-13 10:18:13'),
(12, 2, 32, '4vLhZMfdg0_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:18', '2021-04-13 10:18:18'),
(13, 2, 32, 'mAMR4taiMG_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:21', '2021-04-13 10:18:21'),
(14, 2, 32, 'Zc2g2MwAqj_b9a46da1-3b21-4be3-9460-9b5881b67afc.jpg', '2021-04-13 10:18:22', '2021-04-13 10:18:22'),
(15, 2, 43, '3uxyFdNd4f_0275f9d4-a763-40ab-ac65-a77fdb212b48.jpg', '2021-04-19 11:56:05', '2021-04-19 11:56:05'),
(17, 2, 44, 'ddzEIYlanE_IMG_3412.HEIC', '2021-04-23 15:38:55', '2021-04-23 15:38:55'),
(18, 3, 44, 'TgRyB8sXst_IMG_3417.HEIC', '2021-04-23 15:38:55', '2021-04-23 15:38:55'),
(19, 3, 45, 'Q64mQq0hUs_ea80e9ec-00f2-4026-921c-8f09c6060aa6.jpg', '2021-04-24 04:40:00', '2021-04-24 04:40:00'),
(21, 1, 47, 'EP2yXcgnmY_d48f4593-f65c-41e0-9f4c-a86f2dd82031.jpg', '2021-04-30 06:15:09', '2021-04-30 06:15:09'),
(22, 3, 48, 'yXyQ4WVH6o_4bb2e3e3-3bc7-47a3-9d20-5a28ec343e72.jpg', '2021-05-03 05:49:28', '2021-05-03 05:49:28'),
(23, 3, 34, '0BcH84pIPQ_9d71a8b8-5af4-462a-b11e-02936154cf94.jpg', '2021-05-08 06:37:13', '2021-05-08 06:37:13'),
(24, 3, 50, 'a8gKq9h6sH_733a8f53-b917-43d6-a81e-af76bb4dc49a.jpg', '2021-05-19 06:11:11', '2021-05-19 06:11:11'),
(25, 3, 51, 'UYnv8rFtLt_e4c38196-34c0-4503-8f26-231a8beff71d.jpg', '2021-06-02 05:38:40', '2021-06-02 05:38:40'),
(26, 1, 44, 'wOKqbcMvhL_IMG_5680.HEIC', '2021-06-25 19:26:30', '2021-06-25 19:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `report_msgs`
--

CREATE TABLE `report_msgs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_users`
--

CREATE TABLE `report_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `reported_user_id` int(11) NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `report_users`
--

INSERT INTO `report_users` (`id`, `user_id`, `reported_user_id`, `reason`, `created_at`, `updated_at`) VALUES
(1, 32, 50, '6', '2021-05-18 05:54:34', '2021-05-18 05:54:34'),
(2, 38, 50, '6', '2021-05-18 05:54:34', '2021-05-18 05:54:34'),
(3, 39, 49, '1', '2021-05-29 10:43:45', '2021-05-29 10:43:45'),
(4, 33, 50, '1', '2021-06-11 12:14:55', '2021-06-11 12:14:55'),
(5, 44, 37, '1', '2021-06-11 22:20:53', '2021-06-11 22:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_me` text COLLATE utf8mb4_unicode_ci,
  `is_incognito` int(11) DEFAULT '1' COMMENT '1= not_incognito, 2= incognito',
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height_inches` int(11) DEFAULT NULL,
  `height_feet` int(11) DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight_unit` int(11) DEFAULT NULL,
  `covid_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hiv_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_am` text COLLATE utf8mb4_unicode_ci,
  `body_type` text COLLATE utf8mb4_unicode_ci,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ethnicity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `what_looking_for` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_enable_status` int(11) NOT NULL DEFAULT '1' COMMENT '1=enable, 0= disable',
  `profile_enabled` int(11) NOT NULL DEFAULT '1' COMMENT '1=enable, 2= disable',
  `online_status` int(11) NOT NULL DEFAULT '1' COMMENT '1=online, 0= offline',
  `like_count` int(11) NOT NULL,
  `visitor_count` int(11) NOT NULL,
  `purr_count` int(11) DEFAULT NULL,
  `favourite_count` int(11) DEFAULT NULL,
  `membership` int(11) NOT NULL DEFAULT '0' COMMENT '1 = premium, 0=not',
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_report` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `name`, `email`, `phone`, `photo`, `email_verified_at`, `password`, `about_me`, `is_incognito`, `date_of_birth`, `height`, `height_inches`, `height_feet`, `weight`, `weight_unit`, `covid_status`, `hiv_status`, `other_status`, `relationship_status`, `i_am`, `body_type`, `position`, `ethnicity`, `what_looking_for`, `latitude`, `longitude`, `distance`, `zipcode`, `address`, `notification_enable_status`, `profile_enabled`, `online_status`, `like_count`, `visitor_count`, `purr_count`, `favourite_count`, `membership`, `facebook_link`, `instagram_link`, `twitter_link`, `health_status`, `health_report`, `remember_token`, `created_at`, `updated_at`) VALUES
(32, 'Ana', 'Winson', 'Ana ', 'Ana@gmail.com', '123456789', 'g00uKby0Qp_614f7cb0-8566-4f55-9bef-0b67551ec922.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'Hlo', 1, '12/12/2000', NULL, 6, 59, '125', 2, '3', NULL, NULL, '2', '9', '1', '3', '6', '1,2,3,4,5', '30.7111238', '76.7054107', NULL, '1234', '', 1, 0, 0, 4, 102, NULL, NULL, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-04-05 14:22:53', '2021-05-28 10:33:54'),
(33, 'Alina', 'Rock', 'Alina ', 'Alina@gmail.com', '123456789', 'MDTzmtMDKS_aaf29ab2-4911-4a7d-a223-5f7a69939c15.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'Hiii', 1, '12/15/2002', NULL, 5, 5, '160', 2, '1', NULL, NULL, '2', '8', '1,2', '1,3', '6', '1,2,3,4,5', '30.7111032', '76.7054127', NULL, '1234', '', 1, 1, 0, 9, 85, NULL, NULL, 1, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-04-05 14:26:14', '2021-07-31 14:26:58'),
(34, 'Martina', 'Cat', 'Martina ', 'Martina@gmail.com', '123456789', '8x8p3UrWGU_c570da3e-6ab4-4f11-8566-36e9ccaef546.jpg', NULL, '06643b6eb64fccc562a7264caa6032ed', 'null', 1, '10/12/2001', NULL, 5, 5, '47', 1, NULL, NULL, '', '2', '8', '1', '2,3', '6', '5', '31.1084372', '76.601375', NULL, '1234', '', 1, 1, 1, 2, 35, NULL, NULL, 1, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-04-06 05:43:52', '2021-05-29 11:20:28'),
(35, 'malik', 'kurdi', 'malik kurdi', 'malikkurdi@yahoo.com', '8872218663', 'ti9O9PbGv5_IMG_1217.JPG', NULL, '0a46a66bca789fc4b10c2bd40c3a225b\r\n', NULL, 1, '09/09/1996', NULL, 10, 5, '205', 2, NULL, NULL, NULL, '1', '3', '2', '2', '1', '1', '41.578986418246', '-87.830374280452', NULL, '1234', 'Orland Township, Illinois, United States', 1, 1, 0, 0, 6, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-09 16:50:36', '2021-04-23 12:16:25'),
(36, 'Ali iOS', 'Ashouri', 'Ali iOS ', 'vanem70122@0pppp.com', '123456789', 'QCv1ooqBdi_IMG_2537.JPG', NULL, '32a61d918b8a6a297b1dbbcab51af5df', 'I like cats and purrs!', 1, '07/26/1984', NULL, 8, 5, '167', 2, '2', '2', '2', '2', '4', '1,2', '2', '4', '1,2,3,4,5,6,7', '30.711077006252', '76.705506522684', NULL, '1234', '', 1, 2, 1, 1, 0, NULL, NULL, 1, 'https://www.facebook.com/aliashouri', 'https://www.instagram.com/aliashouri', 'https://twitter.com/aliashouri', NULL, NULL, NULL, '2021-04-09 17:16:57', '2021-05-19 04:37:32'),
(37, 'Ali Android', 'Ashouri', 'Ali Android ', 'Aliashouri@hotmail.com', '123456789', 'dFZ3ogtBfV_IMG_2133.JPG', NULL, '32a61d918b8a6a297b1dbbcab51af5df', 'null', 1, '07/26/1984', NULL, 8, 5, '167', 2, '2', '2', '2', '2', '1', '1,2', '5', '4', '1,2,3,4,5,6,7', '34.0881865', '-118.3720293', NULL, '1234', '', 1, 1, 0, 4, 97, NULL, NULL, 1, 'https://www.facebook.com/aliashouri', 'https://www.instagram.com/aliashouri', 'https://twitter.com/aliashouri', NULL, NULL, NULL, '2021-04-09 18:31:53', '2021-07-21 09:23:39'),
(38, 'John', 'Jonny', 'John Jonny', 'Tester@gmail.com', '123456789', 'gwIa9WI4E1_50002907-8216-42f6-9df0-0948fbd4f97c.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1999', NULL, 9, 5, '125', 1, NULL, NULL, NULL, '2', '4', '2', '2', '6', '1', '30.7110853', '76.7054351', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 14, 59, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-12 11:53:02', '2021-07-14 07:10:26'),
(39, 'Mark', 'Matin', 'Mark Matin', 'Mark@gmail.com', '123456789', 'nCEtWCJAog_14b77701-9309-49a3-9372-817fa8573bda.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 6, 5, '5', 1, NULL, NULL, NULL, '1', '1', '4', '3,2', '6', '1,2,3,4,5', '30.711059564068', '76.705499277101', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 1, 29, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-13 05:14:39', '2021-07-14 07:10:34'),
(40, 'Justin', 'Hornet', 'Justin Hornet', 'gliterrev@gmail.com', '123456789', 'AHKe99vNc5_5b2f50e7-3e93-40c3-b7e3-fbf7e11a5c66.jpg', NULL, 'f778de9f05a935574310616db258a4a5', NULL, 1, '12/12/1999', NULL, 1, 6, '170', 2, NULL, NULL, NULL, '1', '1', '1', '1,2', '6', '1,2,3', '30.7110773', '76.7054348', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 3, 42, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-13 08:10:53', '2021-06-02 06:24:43'),
(41, 'Dan', 'Dany', 'Dan Dany', 'Dan@gmail.com', '123456789', 'd6vSVKBnlA_3d2abcf8-f0ed-4151-aa92-9a1f898763f1.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1998', NULL, 7, 5, '12', 1, NULL, NULL, NULL, '1', '1', '1', '1', '8', '1', '30.7111018', '76.7054071', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 1, 29, NULL, NULL, 1, '', '', '', NULL, NULL, NULL, '2021-04-13 12:17:12', '2021-07-14 12:23:42'),
(42, 'Mic', 'Mic', 'Mic Mic', 'Mic@gmail.com', '123456789', 'kxcWeCpOYr_da04ec77-8cd5-4ba5-a9f4-44a804517213.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 7, 5, '200', 1, NULL, NULL, NULL, '1', '9', '3', '2', '6', '1,2,3', '30.7093017', '76.7074964', NULL, '1234', '', 1, 1, 1, 11, 52, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-16 04:50:11', '2021-07-16 05:21:39'),
(43, 'Hjj', 'Hh', 'Hjj Hh', 'kocig38882@hype68.com', '123456789', 'EhhD7WihK4_80cc08f2-f113-4685-9f44-caa1f0869876.jpg', NULL, 'e5a7e4bfc7435ada9946f4bd7653c882', NULL, 1, '08/08/1999', NULL, 7, 5, '89', 1, NULL, '2', NULL, '2', '2,8,5', '2,1,3,4', '3,1,4', '3', '1,2,4', '30.711111', '76.7053907', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 4, 38, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-19 11:56:05', '2021-06-28 07:43:30'),
(44, 'Ali iOS', 'Ashouri', 'Ali iOS ', 'Aliashouri1@gmail.com', '123456789', 'ddNC2QKT4j_IMG_3391.JPG', NULL, '32a61d918b8a6a297b1dbbcab51af5df', 'Hi I don’t know what to say. Thanks', 1, '07/26/1984', NULL, 8, 5, '175', 2, '', '2', '2', '2', '4', '1,2', '3', '4', '1,2,3,4,5,6,7', '34.0522342', '-118.2436849', NULL, '1234', 'Los Angeles, California, United States', 1, 1, 0, 3, 43, NULL, NULL, 1, 'https://www.facebook.com/aliashouri', 'https://www.instagram.com/aliashouri', 'https://twitter.com/aliashouri', NULL, NULL, NULL, '2021-04-23 15:38:55', '2021-07-27 05:40:03'),
(45, 'Lavis', 'Ldh', 'Lavis Ldh', 'Lavis@gmail.com', '123456789', 'TXYIPDnwdb_ca378caf-41da-4eba-b1f5-3e1e5063eac8.jpg', NULL, '25d55ad283aa400af464c76d713c07ad', NULL, 1, '02/02/1999', NULL, 4, 5, '59', 1, NULL, NULL, '', '1', '2,5,8', '2,3', '2,4', '1', '2,4,5,6', '30.7111128', '76.7054044', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 4, 42, NULL, NULL, 1, '', '', '', NULL, NULL, NULL, '2021-04-24 04:40:00', '2021-07-16 13:59:20'),
(47, 'Jam', 'Jammm', 'Jam Jammm', 'Jam@gmail.com', '123456789', 'BucNHwV5ku_45ae3250-6a4f-490c-8a8d-b16b5f1d02f3.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 7, 5, '123', 1, '', NULL, NULL, '1', '1', '1', '2', '6', '1,2,3,4,6', '30.7111035', '76.7054143', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 1, 36, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-04-30 06:15:09', '2021-07-16 10:13:29'),
(48, 'Mandy', 'K', 'Mandy K', 'stesting2112@gmail.com', '123456789', 'SRYy4LJ56e_85e54366-ceea-46f9-aa8c-864c19f09a49.jpg', NULL, '25d55ad283aa400af464c76d713c07ad', NULL, 2, '08/08/1999', NULL, 1, 6, '67', 1, NULL, NULL, '2', '1', '2,3,4,6,7', '2,3', '3,4', '6', '2,3,4,5', '30.7111365', '76.7053725', NULL, '1234', 'SAS Nagar, Punjab, India', 1, 1, 0, 0, 6, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-05-03 05:49:28', '2021-05-18 06:55:03'),
(49, 'Cat', 'Cat', 'Cat ', 'Cat@gmail.com', '123456789', 'YfE0R3qCqA_FB7EB619-645E-4EDF-AD17-2B82EF8D049D.JPG', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'null', 1, '12/12/1999', NULL, 6, 5, '56', 1, NULL, NULL, NULL, '2', '1', '1', '1', '6', '1,2', '30.7046', '76.7179', NULL, '1234', '', 1, 1, 0, 5, 84, NULL, NULL, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-05-18 11:20:57', '2021-07-22 00:00:02'),
(50, 'Gill', 'Kaur', 'Gill ', 'Gill@gmail.com', '123456789', 'GGZ98I1BTu_6f7f31be-9826-4c64-b60e-ccc32afcb93c.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'null', 1, '08/02/1999', NULL, 8, 5, '58', 1, NULL, NULL, '2', '2', '2,7,4,6', '2,3', '2,1,5', '3', '2,3,4,6,7', '30.7111235', '76.7054013', NULL, '1234', '', 1, 1, 0, 2, 26, NULL, NULL, 1, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-05-19 06:11:11', '2021-07-17 06:37:33'),
(51, 'Samneet', 'K', 'Samneet K', 'Qualityanalystdavid@gmail.com', '123456789', 'cFc8konqUX_26641235-838f-437d-89ae-a338f54cff19.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '08/08/1999', NULL, 8, 5, '58', 1, NULL, NULL, '2', '1', '2,3,7,6,9', '2,3', '2,1', '2', '1,3,5,6', '30.7111175', '76.7054098', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 0, 1, 0, 0, 2, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-06-02 05:38:40', '2021-06-12 17:51:55'),
(52, 'Eleni', 'Waller', 'Eleni ', 'Rooz2cool@yahoo.com', '123456789', 'yXAnx6dAQ9_IMG_5449.JPG', NULL, '32a61d918b8a6a297b1dbbcab51af5df', 'null', 1, '07/26/1980', NULL, 8, 5, '150', 2, NULL, NULL, NULL, '2', '4', '1,2', '1,3,5', '8', '1,2,3,4,5,6,7', '33.68907434835293', '-117.72975580506852', NULL, '1234', '', 1, 1, 0, 1, 3, NULL, NULL, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-06-13 18:42:07', '2021-07-11 00:53:50'),
(53, 'Chery', 'Chh', 'Chery Chh', 'Chery@gmail.com', '123456789', 'HVfAkqIXuh_baa5aa05-0ec5-4e61-9770-7f3c11e410c5.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 6, 5, '5', 2, NULL, NULL, NULL, '1', '2', '2', '1', '1', '1', '30.7111175', '76.7054', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 0, 2, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-16 11:28:09', '2021-07-17 06:37:53'),
(54, 'Olivia', 'Oll', 'Olivia Oll', 'Oli@gmail.com', '123456789', 'rbv8jKhh7q_7b8c5b74-109d-432b-87f8-6655949b47f3.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 6, 5, '5', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', '1,2', '30.7111175', '76.7054', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 0, 4, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-16 11:38:28', '2021-07-31 14:31:44'),
(55, 'Carmela', 'Carl', 'Carmela Carl', 'Carl@gmail.com', '123456789', 'A4zxagzSl6_CBAA26CE-5E9E-47DE-9AC0-036469982373.jpg', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1999', NULL, 6, 5, '6', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', '1', '30.711067297827', '76.705517898494', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 0, 3, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-17 06:05:55', '2021-07-19 05:04:12'),
(56, 'Candy', 'Ca', 'Candy Ca', 'Candy@gmail.com', '123456789', 'Txbl3zNixr_IMG_0027.PNG', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1998', NULL, 6, 5, '5', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '6', '1', '30.71104660892812', '76.70550483996703', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 0, 6, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-17 06:17:32', '2021-07-31 14:31:36'),
(57, 'Marri', 'Mart', 'Marri Mart', 'Mary@gmail.com', '123456789', '1UfTgGCCKG_IMG_0028.PNG', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1990', NULL, 6, 5, '68', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', '1', '30.711054205369937', '76.70551121323082', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 0, 1, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-17 10:41:02', '2021-07-29 12:03:06'),
(58, 'Kent', 'Kee', 'Kent Kee', 'Kent@gmail.com', '9501759261', 'zr1GbpTQND_IMG_0183.PNG', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/2000', NULL, 6, 5, '56', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', '1', '30.71102294079073', '76.70551041836679', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 0, 2, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-17 11:33:51', '2021-07-19 05:03:56'),
(59, 'Erin', 'East', 'Erin East', 'Test@test.com', '6693334444', 'DD8VbCo798_7F88BD0A-4126-4EB2-A62A-EA71DB884A9D.jpg', NULL, '29aa7acafb73866d6571e1a72f46c146', NULL, 1, '08/08/1978', NULL, 0, 6, '55', 1, NULL, NULL, NULL, '4', NULL, NULL, NULL, '10', '1', '30.7046', '76.7179', NULL, '1234', 'SAS Nagar, Punjab, India', 1, 1, 1, 0, 6, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-17 18:08:58', '2021-07-31 14:30:49'),
(60, 'Jony', 'Jon', 'Jony Jon', 'Jony@gmail.com', '9501759261', 'VPk3AHW8DN_IMG_0030.JPG', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/22/1999', NULL, 6, 5, '56', 2, NULL, NULL, NULL, '1', NULL, NULL, NULL, '6', '1,2', '30.711100365626017', '76.70546327609253', NULL, '1234', 'Sector 74, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 0, 0, 0, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-19 05:40:12', '2021-07-19 05:40:12'),
(61, 'Sam', 'Soni', 'Sam Soni', 'Sam@gmail.com', '9501759261', 'dhGtq4viOK_IMG_0031.JPG', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 1, '12/12/1999', NULL, 7, 5, '56', 1, NULL, NULL, NULL, '1', NULL, NULL, NULL, '6', '1,2', '30.71108920721373', '76.70553049453817', NULL, '1234', 'B, Sahibzada Ajit Singh Nagar, Punjab', 1, 1, 1, 0, 2, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-19 06:13:14', '2021-07-31 14:31:53'),
(62, 'Sam', 'Stark', 'Sam Stark', 'Treej2022@gmail.com', '6693334215', '76KgC72jrx_178FAB66-F211-466D-982C-A0D5EC6EE8A2.jpg', NULL, 'e3717114e55c8f536e22b84e50b50b65', NULL, 1, '09/09/1999', NULL, 1, 6, '180', 2, NULL, NULL, NULL, '1', '2', '1', '5', '8', '1,2,3', '30.7046', '76.7179', NULL, '1234', 'SAS Nagar, Punjab, India', 1, 1, 1, 0, 3, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-20 15:14:50', '2021-07-31 14:31:22'),
(63, 'John', 'Joe', 'John ', 'test@gmail.com', '123456789', 'ge4tdpju0y_8d2a272c-bde5-4878-b9ff-c1fe5b7e4c5b.jpg', NULL, '6865af4701815608d013b1ae84b544f8', 'null', 1, '03/10/1995', NULL, 11, 5, '85', 1, NULL, NULL, NULL, '2', '4', '1', NULL, '1', '1,2,3,4', '23.0115986', '72.5229115', NULL, '1234', '', 1, 1, 0, 1, 2, NULL, NULL, 0, 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://twitter.com/', NULL, NULL, NULL, '2021-07-26 06:37:30', '2021-08-01 02:06:44'),
(64, 'Patric', 'Kool', 'Patric Kool', 'Patrick@exemplarymarketing.com', '123456789', 'fL9HWN5Ze2_86790817-5b10-4e5c-ac7b-de1fa4948a73.jpg', NULL, '6865af4701815608d013b1ae84b544f8', NULL, 1, '11/07/1988', NULL, 3, 5, '70', 1, NULL, NULL, NULL, '1', '4', '1,2', '2,3', '6', '2,3,4,5', '23.0443693', '72.5161976', NULL, '1234', 'Bodakdev, Ahmedabad, Gujarat', 1, 1, 0, 0, 5, NULL, NULL, 0, '', '', '', NULL, NULL, NULL, '2021-07-27 04:41:36', '2021-08-01 02:06:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_image`
--

CREATE TABLE `user_image` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=unaprove,1=approve',
  `key` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_image`
--

INSERT INTO `user_image` (`id`, `user_id`, `image`, `status`, `key`, `created_at`, `updated_at`) VALUES
(1, 2, 'lai4OR4OTr_1614168079000', '0', 1, '2021-02-24 12:01:35', '2021-02-24 12:01:35'),
(2, 2, 'ariP4dVIZ3_1614168085000', '0', 2, '2021-02-24 12:01:35', '2021-02-24 12:01:35'),
(3, 3, 'wxv8E4LmRu_1614230798000', '0', 1, '2021-02-25 05:31:47', '2021-02-25 05:31:47'),
(4, 3, 'tp5SpYa7mM_1614230838000', '0', 2, '2021-02-25 05:31:47', '2021-02-25 05:31:47'),
(5, 3, 'qsoWj2afA3_1614230916000', '0', 3, '2021-02-25 05:31:47', '2021-02-25 05:31:47'),
(6, 3, 'b97ZIJNLn0_1614230932000', '0', 4, '2021-02-25 05:31:47', '2021-02-25 05:31:47'),
(7, 3, 'tuHbd433l9_1614230848000', '0', 5, '2021-02-25 05:31:47', '2021-02-25 05:31:47'),
(8, 6, 'Ydurl4XM7Q_IMG_0759.PNG', '0', 1, '2021-02-25 14:24:32', '2021-02-25 14:25:10'),
(9, 6, 'rkc6EFQB9F_IMG_0759.PNG', '0', 2, '2021-02-25 14:25:10', '2021-02-25 14:25:10'),
(10, 8, 'TOugpfl07a_server.png', '0', 1, '2021-02-26 06:33:46', '2021-03-05 08:08:11'),
(11, 8, 'yhU9A1hFrM_1614320862000', '0', 2, '2021-02-26 06:33:46', '2021-02-26 06:33:59'),
(12, 8, 'pc3y7IEIJF_1614320885000', '0', 3, '2021-02-26 06:33:46', '2021-02-26 06:33:59'),
(13, 8, 'G3ivvfkO3v_1614320869000', '0', 4, '2021-02-26 06:33:46', '2021-02-26 06:33:59'),
(14, 8, 'GX2jH10CWz_1614320876000', '0', 5, '2021-02-26 06:33:46', '2021-02-26 06:33:59'),
(15, 7, 'T7SrOPuHt9_IMG_1395.JPG', '0', 1, '2021-03-01 05:47:19', '2021-03-01 05:47:19'),
(16, 7, 'F8Zda6gon6_IMG_0952.JPG', '0', 2, '2021-03-01 05:47:19', '2021-03-01 05:47:19'),
(17, 7, 'HYjs9x3y0c_IMG_1318.JPG', '0', 3, '2021-03-01 05:47:19', '2021-03-01 05:47:19'),
(18, 7, 'HJc7HV7UU1_IMG_1085.JPG', '0', 4, '2021-03-01 05:47:19', '2021-03-01 05:47:19'),
(19, 7, 'd60rvEssvt_IMG_0036.JPG', '0', 5, '2021-03-01 05:47:19', '2021-03-01 05:47:19'),
(20, 14, 'jz1rnW9Bzp_IMG_0005.JPG', '0', 1, '2021-03-06 06:54:50', '2021-03-06 06:54:50'),
(21, 15, '952O0Z2phZ_1615793792000', '0', 1, '2021-03-15 07:46:35', '2021-03-15 08:04:47'),
(22, 15, 'TunZsAm5XD_1615793805000', '0', 2, '2021-03-15 07:46:35', '2021-03-15 08:04:47'),
(23, 15, '8m6SqQFhl2_1615793868000', '0', 3, '2021-03-15 07:46:35', '2021-03-15 08:04:47'),
(24, 15, 'MEBytIQQtO_1615793835000', '0', 4, '2021-03-15 07:46:35', '2021-03-15 08:04:47'),
(25, 15, 'KykYiaWfZn_1615793822000', '0', 5, '2021-03-15 07:46:35', '2021-03-15 08:04:47'),
(26, 13, 'sUFeoB4Toc_server.png', '0', 1, '2021-03-16 12:11:45', '2021-03-16 12:51:51'),
(27, 14, '7bvlIQD8vr_1616680929000', '0', 2, '2021-03-25 14:02:19', '2021-03-25 14:02:19'),
(28, 18, 'CTPP1VXs4X_1616741123000', '0', 1, '2021-03-26 06:45:53', '2021-03-26 06:49:33'),
(29, 18, 'VDL121Qjdx_1616741130000', '0', 2, '2021-03-26 06:45:53', '2021-03-26 06:49:33'),
(30, 18, 'jOX29Qobg3_1616741142000', '0', 4, '2021-03-26 06:45:53', '2021-03-26 06:49:33'),
(31, 18, 'cKgoYJ7jU5_1616741137000', '0', 5, '2021-03-26 06:45:53', '2021-03-26 06:49:33'),
(32, 28, 'LZroNAYCIX_IMG_2500.JPG', '0', 1, '2021-03-31 23:50:24', '2021-03-31 23:51:55'),
(33, 28, 'Z9xB8Fbzkl_IMG_2498.JPG', '0', 2, '2021-03-31 23:50:24', '2021-03-31 23:51:55'),
(34, 28, 'TIsgXqjNF3_IMG_2493.JPG', '0', 3, '2021-03-31 23:56:05', '2021-03-31 23:56:22'),
(35, 28, 'aTBCJ7nO3b_IMG_2503.JPG', '0', 4, '2021-03-31 23:56:05', '2021-03-31 23:56:22'),
(36, 28, 'puermiUVWs_IMG_2480.JPG', '0', 5, '2021-03-31 23:56:05', '2021-03-31 23:56:22'),
(37, 26, 'biLVUsZ4ym_IMG_0051.HEIC', '0', 1, '2021-04-01 06:17:44', '2021-04-01 06:19:13'),
(38, 26, 'eeV2Uiem6J_IMG_0059.HEIC', '0', 2, '2021-04-01 06:18:54', '2021-04-01 06:19:13'),
(39, 19, '4aQaEQ9Z5R_1617259019000', '0', 1, '2021-04-01 06:37:06', '2021-04-01 06:37:06'),
(40, 25, 'TFKgpDTHmY_IMG_0055.HEIC', '0', 1, '2021-04-02 08:20:30', '2021-04-03 12:49:53'),
(41, 25, 'JuheObZRIk_IMG_0055.HEIC', '0', 2, '2021-04-02 08:21:33', '2021-04-03 12:49:53'),
(42, 31, 'Lv38ce2OwN_1617432806000', '0', 1, '2021-04-03 06:54:01', '2021-04-03 06:54:27'),
(43, 31, 'GYx7Ws9gKu_1617432812000', '0', 2, '2021-04-03 06:54:01', '2021-04-03 06:54:27'),
(44, 31, 'jI3ciSyP93_1617432831000', '0', 3, '2021-04-03 06:54:01', '2021-04-03 06:54:27'),
(45, 31, 'F0VvJzz1lR_1617432825000', '0', 4, '2021-04-03 06:54:01', '2021-04-03 06:54:27'),
(46, 31, 'OA669L3X8W_1617432818000', '0', 5, '2021-04-03 06:54:01', '2021-04-03 06:54:27'),
(47, 36, 'lqQAw9Izcx_IMG_2500.JPG', '0', 1, '2021-04-09 17:25:24', '2021-04-09 17:25:26'),
(48, 36, 'Ug3mek0MNJ_IMG_2503.JPG', '0', 2, '2021-04-09 17:25:24', '2021-04-09 17:25:26'),
(49, 36, '5CoidoobcJ_IMG_2796.PNG', '0', 3, '2021-04-09 17:25:24', '2021-04-09 17:25:26'),
(50, 36, '2jzV0wDLoZ_IMG_2542.JPG', '0', 4, '2021-04-09 17:25:24', '2021-04-09 17:25:26'),
(51, 36, 'mglUlz6PpO_IMG_2787.PNG', '0', 5, '2021-04-09 17:25:24', '2021-04-09 17:25:26'),
(52, 37, 'VpOiyYkpaX_IMG_2504.JPG', '0', 1, '2021-04-09 18:32:57', '2021-04-09 18:32:57'),
(53, 37, 'P62EVde6Zd_IMG_2463.JPG', '0', 2, '2021-04-09 18:32:57', '2021-04-09 18:32:57'),
(54, 37, 'NejdFDrQuY_IMG_2795.JPG', '0', 3, '2021-04-09 18:32:57', '2021-04-09 18:32:57'),
(55, 37, 'DQBr616Bhe_IMG_2788.PNG', '0', 4, '2021-04-09 18:32:57', '2021-04-09 18:32:57'),
(56, 37, 'hwpyIYn0Jo_IMG_2742.JPG', '0', 5, '2021-04-09 18:32:57', '2021-04-09 18:32:57'),
(57, 32, 'ESdYNKls5d_1618308802000', '0', 1, '2021-04-13 10:14:30', '2021-04-13 10:18:22'),
(58, 32, 'NtWm5RQzTe_1618308812000', '0', 2, '2021-04-13 10:14:30', '2021-04-13 10:18:22'),
(59, 32, 'au3BYLy9lD_1618308852000', '0', 3, '2021-04-13 10:14:30', '2021-04-13 10:18:22'),
(60, 32, 'hvhuYcceuH_1618308838000', '0', 4, '2021-04-13 10:14:30', '2021-04-13 10:18:22'),
(61, 32, 'Ptj14m41Rh_1618308821000', '0', 5, '2021-04-13 10:14:30', '2021-04-13 10:18:22'),
(62, 44, 'IAB9OwdxDT_IMG_3396.JPG', '0', 1, '2021-04-23 16:25:01', '2021-04-23 16:25:44'),
(63, 44, 'oSxKcrdHrr_IMG_3394.JPG', '0', 2, '2021-04-23 16:25:01', '2021-04-23 16:25:44'),
(64, 44, 'WltaF8wBOw_IMG_3397.JPG', '0', 3, '2021-04-23 16:25:01', '2021-04-23 16:25:44'),
(65, 44, 'b761omrSxC_IMG_3390.JPG', '0', 4, '2021-04-23 16:25:01', '2021-04-23 16:25:44'),
(66, 44, 'GPFKtVBQB7_IMG_3393.JPG', '0', 5, '2021-04-23 16:25:01', '2021-04-23 16:25:44'),
(67, 33, 'IL8bRyXF5S_1620370018000', '0', 1, '2021-05-07 06:47:57', '2021-05-07 06:49:27'),
(68, 33, 'KIlLd0st0a_1620371618000', '0', 2, '2021-05-07 06:47:57', '2021-05-07 07:13:42'),
(69, 33, 'M1OdXAKBCF_1620370160000', '0', 3, '2021-05-07 06:49:27', '2021-05-07 06:49:27'),
(70, 34, '4xUthT1rRO_1620455819000', '0', 1, '2021-05-08 06:37:13', '2021-05-08 06:37:13'),
(71, 34, 'O7nUgIh90N_1620455826000', '0', 2, '2021-05-08 06:37:13', '2021-05-08 06:37:13'),
(72, 50, 'yqcuO29A9t_1621406811000', '0', 1, '2021-05-19 06:50:46', '2021-05-19 06:51:07'),
(73, 50, 'Vv3GOMlwvs_1621406816000', '0', 2, '2021-05-19 06:50:46', '2021-05-19 06:51:07'),
(74, 50, '8bHOLIRFQz_1621406827000', '0', 3, '2021-05-19 06:50:46', '2021-05-19 06:51:07'),
(75, 50, 'fUrZo4c0rp_1621406833000', '0', 4, '2021-05-19 06:50:46', '2021-05-19 06:51:07'),
(76, 50, 'xkCqf9jJOE_1621406822000', '0', 5, '2021-05-19 06:50:46', '2021-05-19 06:51:07'),
(77, 52, '9tXLyjo2oa_IMG_5307.JPG', '0', 1, '2021-06-13 18:45:17', '2021-06-13 18:48:59'),
(78, 52, 'g1tJ5XwRoy_IMG_5169.JPG', '0', 2, '2021-06-13 18:45:17', '2021-06-13 18:48:59'),
(79, 52, '1azU8Q6BzO_IMG_5452.JPG', '0', 3, '2021-06-13 18:45:17', '2021-06-13 18:48:59'),
(80, 52, 'JF3cBMIafR_IMG_5230.JPG', '0', 4, '2021-06-13 18:45:17', '2021-06-13 18:48:59'),
(81, 52, 'TLCltpJY9q_IMG_5451.JPG', '0', 5, '2021-06-13 18:45:17', '2021-06-13 18:48:59');

-- --------------------------------------------------------

--
-- Table structure for table `user_likes`
--

CREATE TABLE `user_likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `liked_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_subscription`
--

CREATE TABLE `user_subscription` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `premium_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_purr` int(11) DEFAULT NULL,
  `remaining_purr` int(11) DEFAULT NULL,
  `total_favourite` int(11) DEFAULT NULL,
  `remaining_favourite` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active, 2=expired',
  `ends_at` datetime DEFAULT NULL,
  `trial_ends_at` datetime DEFAULT NULL,
  `is_trial` int(11) NOT NULL DEFAULT '1' COMMENT '1=yes, 2=no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trial_updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_subscription`
--

INSERT INTO `user_subscription` (`id`, `transaction_id`, `premium_id`, `user_id`, `total_purr`, `remaining_purr`, `total_favourite`, `remaining_favourite`, `status`, `ends_at`, `trial_ends_at`, `is_trial`, `created_at`, `updated_at`, `trial_updated_date`) VALUES
(1, 'sub_JFOCii8bUd9aRj', 1, 33, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-05-12 14:28:28', '2021-04-12 14:28:27', 2, '2021-04-05 14:28:28', '2021-06-25 10:49:01', '2021-04-05 14:28:28'),
(2, 'sub_JFOIPoHxgipX7g', 1, 32, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-05-12 14:34:40', '2021-04-12 14:34:40', 2, '2021-04-05 14:34:40', '2021-04-14 12:50:02', '2021-04-05 14:34:40'),
(3, 'sub_JFczuDvvFJbKcS', 1, 34, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-05-13 05:46:13', '2021-04-13 05:46:12', 2, '2021-04-06 05:46:13', '2021-05-29 11:05:02', '2021-04-06 05:46:13'),
(4, 'sub_JGvvl5xoCz4qbv', 2, 36, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-10-16 17:23:55', '2021-04-16 17:23:54', 2, '2021-04-09 17:23:55', '2021-05-19 05:54:01', '2021-04-09 17:23:55'),
(5, 'sub_JGx2SHtcljhdo4', 2, 37, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-10-16 18:33:16', '2021-04-16 18:33:15', 2, '2021-04-09 18:33:16', '2021-06-05 04:07:01', '2021-04-09 18:33:16'),
(6, 'sub_JIHTteog3jkyq0', 1, 39, 20, 20, 20, 20, 2, '2021-05-20 07:43:58', '2021-04-20 07:43:57', 2, '2021-04-13 07:43:58', '2021-05-20 00:00:01', '2021-04-13 07:43:58'),
(7, 'sub_JII7WZYJW5BL2c', 1, 40, 0, 0, 0, 0, 2, '2021-05-20 08:24:07', '2021-04-20 08:24:06', 2, '2021-04-13 08:24:07', '2021-05-20 00:00:01', '2021-04-13 08:24:07'),
(8, 'sub_JM9R6BHEexrkYG', 2, 44, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-10-30 15:41:55', '2021-04-30 15:41:54', 2, '2021-04-23 15:41:55', '2021-07-15 21:33:02', '2021-04-23 15:41:55'),
(9, 'sub_JMMCskafS10Y4c', 2, 45, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-10-31 04:52:22', '2021-05-01 04:52:21', 2, '2021-04-24 04:52:22', '2021-05-14 06:12:01', '2021-04-24 04:52:22'),
(10, 'sub_JVkPCrIghaKgmN', 2, 50, 1000000000, 1000000000, 1000000000, 1000000000, 1, '2021-11-26 06:29:48', '2021-05-26 06:29:47', 2, '2021-05-19 06:29:48', '2021-06-01 07:25:01', '2021-05-19 06:29:48'),
(11, 'sub_JfvPLjhHWeoiDs', 1, 49, 0, 0, 0, 0, 2, '2021-07-22 10:31:34', '2021-06-22 10:31:33', 2, '2021-06-15 10:31:34', '2021-07-22 00:00:02', '2021-06-15 10:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_video_call_tokens`
--

CREATE TABLE `user_video_call_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `channelName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_video_call_tokens`
--

INSERT INTO `user_video_call_tokens` (`id`, `user_id`, `token`, `channelName`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 33, '00648fb1890f2c449ed9e29207af9a0e447IACyElksxfSTLgua4dYOx7Dv8jvuAtZbvbF/w2mMQKykWApTMJYAAAAAIgCzdgAALXBsYAQAAQC9LGtgAwC9LGtgAgC9LGtgBAC9LGtg', 'EHYEjccCTG74CCkDRDtPusDqMKEDizo6', 2, '2021-04-05 14:29:01', NULL),
(2, 32, '00648fb1890f2c449ed9e29207af9a0e447IABGcgIdOefE5AT/jkbomdUAu+LogYM1PLWrwRhiT60RgJG7PawAAAAAIgCzTgAAFnJsYAQAAQCmLmtgAwCmLmtgAgCmLmtgBACmLmtg', 'wIHT9wHBSBsoM8kvVWUxLIc5AfqoeXyB', 2, '2021-04-05 14:37:10', NULL),
(3, 32, '00648fb1890f2c449ed9e29207af9a0e447IADaTY/1PcaQJNvA610HU+wVGuTmWsgB4KU4K9c0r4zEA4nFaCsAAAAAIgAmzQAABkFtYAQAAQCW/WtgAwCW/WtgAgCW/WtgBACW/Wtg', 't03PFVOkHRnyPyjFchOSGIjqnvDzibUR', 2, '2021-04-06 05:20:06', NULL),
(4, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAZXDtJFBiewUl+ExBMe2O1tICz9XRNiL8bqEHNzf2UTO+MCSwAAAAAIgDaWAEAU2BxYAQAAQDjHHBgAwDjHHBgAgDjHHBgBADjHHBg', 'VbCSj9G4w3NJgoGNmYkNcbfX48txJuvH', 2, '2021-04-09 08:22:43', NULL),
(5, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAhv1KwCPiiAJe4Ze4rHNgqdpGjD3VVC5vGrkb/aBnrD4jNQYcAAAAAIgAkFwAAfWBxYAQAAQANHXBgAwANHXBgAgANHXBgBAANHXBg', 'Nbb4jdCo8aBG0EakekGgYOsiJMFGLS81', 2, '2021-04-09 08:23:25', NULL),
(6, 36, '00648fb1890f2c449ed9e29207af9a0e447IABhZZscJ1hW0UotjnnaDpQInqlVk4vTGWlry2DdUOm/4Shr3+EAAAAAIgDwmAAAuPJxYAQAAQBIr3BgAwBIr3BgAgBIr3BgBABIr3Bg', 'GGb3gsLuQd5ILqkWPbSLXvZbcQDRuAsi', 2, '2021-04-09 18:47:20', NULL),
(7, 37, '00648fb1890f2c449ed9e29207af9a0e447IAClSZZ8sxZ8UBcFpJ7jG+UUeicur4HwGnzvfM8w0GYex/WFb+AAAAAAIgDwUQEA0PJxYAQAAQBgr3BgAwBgr3BgAgBgr3BgBABgr3Bg', 'Z6F6P6y3WVkFr5sIMFQmeQx34t8Aq4F0', 2, '2021-04-09 18:47:44', NULL),
(8, 36, '00648fb1890f2c449ed9e29207af9a0e447IAAfEDV7OGAv5zXiMQdVPQzrSlRzQrudLP2D/auuYOv02kyrMwoAAAAAIgBSIwEAM/NxYAQAAQDDr3BgAwDDr3BgAgDDr3BgBADDr3Bg', 'ZXcLqJ6wJeWRoiuJ8njoc6k5eiZct3Bd', 2, '2021-04-09 18:49:23', NULL),
(9, 36, '00648fb1890f2c449ed9e29207af9a0e447IADAfvG8/PeuG0ThHNwpkWEgPg7bNSIprPokCvcEEjwHt/3DiDsAAAAAIgCNBwAATPNxYAQAAQDcr3BgAwDcr3BgAgDcr3BgBADcr3Bg', '8nwAiojh5HgJBvgDjpfEeu7C3TQKAHGi', 2, '2021-04-09 18:49:48', NULL),
(10, 37, '00648fb1890f2c449ed9e29207af9a0e447IABDPhdG+jS6GfRYI0J1SZnzFOLO2FtUMzmzN0f91GZAn7OzUkkAAAAAIgAaBQAAW/NxYAQAAQDrr3BgAwDrr3BgAgDrr3BgBADrr3Bg', 'O0jICxDA3veabKzoyihy5zgyv2Is7714', 2, '2021-04-09 18:50:03', NULL),
(11, 37, '00648fb1890f2c449ed9e29207af9a0e447IABDvoNsvERO8q8ftUqV63t+oHEhD8ngz17p0goL6FsOp55kHkkAAAAAIgCwewAAcvNxYAQAAQACsHBgAwACsHBgAgACsHBgBAACsHBg', 'hcTCOkl0F8jU5EGHVFlvyzJSKPY5NW2M', 2, '2021-04-09 18:50:26', NULL),
(12, 33, '00648fb1890f2c449ed9e29207af9a0e447IADcL0BNod7ydXUHvD3bvo+/biWl6jGwcGvxEfZRWo/HYjs40pUAAAAAIgCZ3gAAaJp2YAQAAQD4VnVgAwD4VnVgAgD4VnVgBAD4VnVg', 'ZAz4TFzkio5x1rKs4CSOoDcurLndGp74', 2, '2021-04-13 07:31:52', NULL),
(13, 39, '00648fb1890f2c449ed9e29207af9a0e447IADWR/4ncWUUn7dwH+oZVLRTZwHtaUqay8IjYDmMyygWdQQicHYAAAAAIgDGIQAA3p12YAQAAQBuWnVgAwBuWnVgAgBuWnVgBABuWnVg', 'NGwBB3ORiJ9cgrVwPeb1IyPLHYs3pHt8', 2, '2021-04-13 07:46:38', NULL),
(14, 39, '00648fb1890f2c449ed9e29207af9a0e447IABasFbzBXoxI7iqrwNoHgvzB0jola7KzhEJrCI1ht04HrMVzOIAAAAAIgAfIQAACp52YAQAAQCaWnVgAwCaWnVgAgCaWnVgBACaWnVg', '6vT4p3j2xxuPMSkYORg779JEqR3bc8I0', 2, '2021-04-13 07:47:22', NULL),
(15, 39, '00648fb1890f2c449ed9e29207af9a0e447IADS68Ymc1P+zs2XnR0hw4uGRHzBdw5jBMtuIi+20DF3JOuHSYAAAAAAIgCUIwEApJ52YAQAAQA0W3VgAwA0W3VgAgA0W3VgBAA0W3Vg', 'cTy1E2hQwxVmVNgVcEp1jsfIQDGTj61s', 2, '2021-04-13 07:49:56', NULL),
(16, 39, '00648fb1890f2c449ed9e29207af9a0e447IADP30biDSAt36qSep9Jhm/WvIkRyTvE37B4q59FkGLHkCzjofIAAAAAIgArIgAAbJ92YAQAAQD8W3VgAwD8W3VgAgD8W3VgBAD8W3Vg', '7noeUde51eJvwJli2D9QzpYFsmkRncLA', 2, '2021-04-13 07:53:16', NULL),
(17, 40, '00648fb1890f2c449ed9e29207af9a0e447IADu/ad1r1GZw8L4ZU1O4RG+KTXboI0OYkuXicq5VeERkyTRR/wAAAAAIgCLNAEAzKZ2YAQAAQBcY3VgAwBcY3VgAgBcY3VgBABcY3Vg', 'y7YSVILbHul0BczcOPlAoquid3X9xzJ6', 2, '2021-04-13 08:24:44', NULL),
(18, 32, '00648fb1890f2c449ed9e29207af9a0e447IAD7X09fpYOMpj8wNQ9Yd7izMoNwG2l2H8AqwMIJXTgRr76ck8UAAAAAIgCL0QAAhzh4YAQAAQAX9XZgAwAX9XZgAgAX9XZgBAAX9XZg', 'O0SPjIx0OkmzrjwRZjDC22Y97gJnP326', 2, '2021-04-14 12:58:47', NULL),
(19, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAstt7v5tAq4dM1q/4Ic4ua7sbE2ZguiRbYkJMDqMORKu+bRQEAAAAAIgAECwEAXTl4YAQAAQDt9XZgAwDt9XZgAgDt9XZgBADt9XZg', 'tdNNxmJZs5pAA4z8MTtalT7FNZs8Zf37', 2, '2021-04-14 13:02:21', NULL),
(20, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAl5vCAz90sh5aKPvgYQcdUbPjyZHZD4GpYEMkJclxjJctPAQIAAAAAIgATKgAAwj95YAQAAQBS/HdgAwBS/HdgAgBS/HdgBABS/Hdg', 'WjkB6vfPgNzijnoEUBCYVNWG4kevfmXU', 2, '2021-04-15 07:41:54', NULL),
(21, 32, '00648fb1890f2c449ed9e29207af9a0e447IADphU7c+EMMvJaoPCwXKlMWSSp20O0J6LRpBo7s5iAc+1ExaX4AAAAAIgAqDQEAnbh+YAQAAQAtdX1gAwAtdX1gAgAtdX1gBAAtdX1g', 'PuQPyeNVaTJhb68sFaE99LQ7NP6XbdDx', 2, '2021-04-19 11:18:53', NULL),
(22, 32, '00648fb1890f2c449ed9e29207af9a0e447IAC9NTsAQqAZu8wG2uSp2dEDjf7885EckzsTGDHdpQtFXE6481kAAAAAIgAP6wAAftSDYAQAAQAOkYJgAwAOkYJgAgAOkYJgBAAOkYJg', 'RPUoYkppNaXuoxFnxKf3fXPi4PMqCJlx', 2, '2021-04-23 08:19:10', NULL),
(23, 33, '00648fb1890f2c449ed9e29207af9a0e447IAB/5SunDK3tPuB8twDr1s4yEMlBAcyPpvwWCXvIb3NRfRX47igAAAAAIgA4FgAAsdWDYAQAAQBBkoJgAwBBkoJgAgBBkoJgBABBkoJg', '0zTc05tWaDYUS1bHmo4yMs9u1sUXSJNE', 2, '2021-04-23 08:24:17', NULL),
(24, 33, '00648fb1890f2c449ed9e29207af9a0e447IABvPF+N6em1Y1ui0nJv7st5snIXAZ2ylAkbHeQnz6olsNwZ5mEAAAAAIgB+nQAA4dWDYAQAAQBxkoJgAwBxkoJgAgBxkoJgBABxkoJg', 'W4nmYK4JV7lEzDjKY5uKylmOwqW7QerQ', 2, '2021-04-23 08:25:05', NULL),
(25, 32, '00648fb1890f2c449ed9e29207af9a0e447IAD95b6p8zufiGlVtpnZtAajdOjE7B6/Tyl4RNM673vHSfnQtXsAAAAAIgD4LAAAHtaDYAQAAQCukoJgAwCukoJgAgCukoJgBACukoJg', 'OqaI6qAnYPrYOzgPJYJ0hMqWwGPMHif5', 2, '2021-04-23 08:26:06', NULL),
(26, 32, '00648fb1890f2c449ed9e29207af9a0e447IAA14ZLjQv3wx0UoMH5ZIKGiFg6vkhLyWSutqgyc9D8y1Cg2LwEAAAAAIgBuJwAA7daDYAQAAQB9k4JgAwB9k4JgAgB9k4JgBAB9k4Jg', 'eplTJM0jflQR7WZvasXI2p4DO47Kpydg', 2, '2021-04-23 08:29:33', NULL),
(27, 32, '00648fb1890f2c449ed9e29207af9a0e447IAA37CBXOeCM8a3XessY67ag6aDZfpalZ1UqydB5uZZts5cebq0AAAAAIgBuQAAAXeKDYAQAAQDtnoJgAwDtnoJgAgDtnoJgBADtnoJg', 'QGHaRDBR4xJA6EYv1utjxT5dC9WnwGum', 2, '2021-04-23 09:18:21', NULL),
(28, 32, '00648fb1890f2c449ed9e29207af9a0e447IACSznWOCLUBQGyY2pCfhBeDLQFYh1YRng5dto5VSoZ9QrAhf6gAAAAAIgC1HQAA/eiDYAQAAQCNpYJgAwCNpYJgAgCNpYJgBACNpYJg', 'v7KSi4hVcScanfAF7kWRFlsrYGXrmQla', 2, '2021-04-23 09:46:37', NULL),
(29, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAnIkt8PJv/+kDIGrIVc4J5Bb1DL/CCs0RuFHFzSiUZ0b7fq0kAAAAAIgA6igAApumDYAQAAQA2poJgAwA2poJgAgA2poJgBAA2poJg', '90rgP5iuFp3RqPyqkVqRpgJ7G3ykcJN7', 2, '2021-04-23 09:49:26', NULL),
(30, 37, '00648fb1890f2c449ed9e29207af9a0e447IADrKwG8wHSi5be3Y4QsTyD2UiWcKZ1kRewlrG5VoRB0+SzqyKcAAAAAIgBDrQAAY4SEYAQAAQDzQINgAwDzQINgAgDzQINgBADzQINg', '66SW06C0VjJYpbc5ZCW8nR5SveZHgiP0', 2, '2021-04-23 20:49:39', NULL),
(31, 44, '00648fb1890f2c449ed9e29207af9a0e447IABiFHBp/HdvmJ56rf+3tTNgNGO+VG0yZNi1R9tfRP8VwHuI9m4AAAAAIgD5fwAAcYSEYAQAAQABQYNgAwABQYNgAgABQYNgBAABQYNg', 'tN2AOdNUl1Uh6mKlVhDfSX4lAlm7LXQl', 2, '2021-04-23 20:49:53', NULL),
(32, 37, '00648fb1890f2c449ed9e29207af9a0e447IACescAIeOppP5Ce60ah0hFAt6Kp6I/Bj9iyM6pcghhIJh9RheEAAAAAIgB3FwAAkoSEYAQAAQAiQYNgAwAiQYNgAgAiQYNgBAAiQYNg', 'QUgFqM1e7SmcGkncXH3XS2HrdftDGrjU', 2, '2021-04-23 20:50:26', NULL),
(33, 44, '00648fb1890f2c449ed9e29207af9a0e447IADyEpS7+toGQ1ry2lFd9Um5uiao4Yt4Bg53tOAQ/tT10llbiIoAAAAAIgAlUQAAvoSEYAQAAQBOQYNgAwBOQYNgAgBOQYNgBABOQYNg', '3kbwF9DyDtCv25z3L5rPMAXvTWBYvDkb', 2, '2021-04-23 20:51:10', NULL),
(34, 37, '00648fb1890f2c449ed9e29207af9a0e447IAArBIzcAxYCPmKjN2EjK2zGQbL7L9xmWwB6bj3flnorXJ7Jgj0AAAAAIgA5DgEA7YSEYAQAAQB9QYNgAwB9QYNgAgB9QYNgBAB9QYNg', 'Ugrp8vrskuXCqsC6n8eDa2FqiMSgrOlz', 2, '2021-04-23 20:51:57', NULL),
(35, 44, '00648fb1890f2c449ed9e29207af9a0e447IADagILe+sVvM/8jN0YNn72iLQUryHeccTip/pLCruj4Y3/Y3RUAAAAAIgAg/gAA94SEYAQAAQCHQYNgAwCHQYNgAgCHQYNgBACHQYNg', 'fhy4MsJo0MQiZXxocSK8tHDDrpBpU6ur', 2, '2021-04-23 20:52:07', NULL),
(36, 37, '00648fb1890f2c449ed9e29207af9a0e447IAA93i4EpDtBZywztEQwxpIAP+XG4/kKlsQfu5sdiAjCg1M0jvwAAAAAIgAbCQAAHoWEYAQAAQCuQYNgAwCuQYNgAgCuQYNgBACuQYNg', 'QSB4bNjI7n9kC3YBlHrwuHqvoXAfVwde', 2, '2021-04-23 20:52:46', NULL),
(37, 34, '00648fb1890f2c449ed9e29207af9a0e447IABvTybsvhFCFYMxstqj+Tf9pDcRGuGxxXPLd2xN6Qs4/CW8Ms4AAAAAIgB3hAEAIvaEYAQAAQCysoNgAwCysoNgAgCysoNgBACysoNg', 'Q767WAUU62MPoYhOa81mzkqixdeUCNKV', 2, '2021-04-24 04:54:58', NULL),
(38, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAwGLqiIPNwrT9MwBQ1Q0zV/m1XhlZhU75A7uMOcDgtMn1j4N0AAAAAIgAQ/AAAqMmMYAQAAQA4hotgAwA4hotgAgA4hotgBAA4hotg', 'ht6g23k6vM3YNrvnDDHvI0HX1fj2YRCT', 2, '2021-04-30 03:23:20', NULL),
(39, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAM3eY4XU7O2Go69yueoIWrU114AxzUpWkCQI26fQKzx6UswYoAAAAAIgCjSgAA1cmMYAQAAQBlhotgAwBlhotgAgBlhotgBABlhotg', 'fcAgZR8WVXMgWgs4K1hK1jwRipHUufWU', 2, '2021-04-30 03:24:05', NULL),
(40, 33, '00648fb1890f2c449ed9e29207af9a0e447IADfoywuUuA6MgQy2gmt4vQqe5B1fF7z9+4XMy9cLPqNvxAdS0kAAAAAIgAnZAEABMqMYAQAAQCUhotgAwCUhotgAgCUhotgBACUhotg', 'jLpGwvBHlRQ31CFTkHXoMrcqtvRxCZ5a', 2, '2021-04-30 03:24:52', NULL),
(41, 34, '00648fb1890f2c449ed9e29207af9a0e447IACojTvQnUn5xGiZgRCfSRUmIvmRxU2NGN63u4Ht+YCYTeTeocMAAAAAIgCUqgAAOh2RYAQAAQDK2Y9gAwDK2Y9gAgDK2Y9gBADK2Y9g', '9FRlxx7N2V2IBgJB5sXItAVMqpAiw2wa', 2, '2021-05-03 10:08:58', NULL),
(42, 37, '00648fb1890f2c449ed9e29207af9a0e447IABx1AnS1LOU8Czwboiag4VRVMFw5MPgiAOhIyyvw6Vd90n0mlMAAAAAIgBaBgEAxnmVYAQAAQBWNpRgAwBWNpRgAgBWNpRgBABWNpRg', 'NADJdtku4GNsw0d3enoYMdjNBbHMJJVo', 2, '2021-05-06 17:32:54', NULL),
(43, 37, '00648fb1890f2c449ed9e29207af9a0e447IACbbCaMIlICh39ez/0ocaHLV0wOi+RtsYRuk5pDlfz/diEg8F4AAAAAIgDXRAEADHqVYAQAAQCcNpRgAwCcNpRgAgCcNpRgBACcNpRg', 'lnHSsghu8DW4F2SKKC7zMDQnlMfCChck', 2, '2021-05-06 17:34:04', NULL),
(44, 32, '00648fb1890f2c449ed9e29207af9a0e447IACdxjq+E77dWAjtQbhwsBnqQTK/sxCba3DCOWdpTIL9jiymN0IAAAAAIgDTaAEAmj2WYAQAAQAq+pRgAwAq+pRgAgAq+pRgBAAq+pRg', 'nWKaLEDIfZtoYuL1T4b90y9JKYQKb0s6', 2, '2021-05-07 07:28:26', NULL),
(45, 33, '00648fb1890f2c449ed9e29207af9a0e447IABCZ2Ts6LtdzQjd/0eHHH026+8Yui+fgoUz1bc3LUQOlPTlTLMAAAAAIgCKAAAAHGGWYAQAAQCsHZVgAwCsHZVgAgCsHZVgBACsHZVg', 'Y15kr1wyzOVcja9WRUaNLouCiC8BuIYV', 2, '2021-05-07 09:59:56', NULL),
(46, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAcPjnVUBOvJTF2mWn//v2rjw/omCpsVpRCDSfflRUUy8l3Vj4AAAAAIgAdRAEAP2GWYAQAAQDPHZVgAwDPHZVgAgDPHZVgBADPHZVg', 'IRmklCr37c79273xiTAkSWETPj8275X8', 2, '2021-05-07 10:00:31', NULL),
(47, 33, '00648fb1890f2c449ed9e29207af9a0e447IABEEm1TcY2xaZ85uAoSWr1VIQaUn2rq4K/n++vefIDVAtKKtT8AAAAAIgAK+QAAfGKWYAQAAQAMH5VgAwAMH5VgAgAMH5VgBAAMH5Vg', 'R0RGWNQptMukqJJLnih1qC7nlPSaQeMQ', 2, '2021-05-07 10:05:48', NULL),
(48, 33, '00648fb1890f2c449ed9e29207af9a0e447IABQzfHOqUqq9keWnj58HhCcFG6HsWmGBIENYqit5sjubhU3mF4AAAAAIgDLHQEA3maWYAQAAQBuI5VgAwBuI5VgAgBuI5VgBABuI5Vg', 'fGNwgEGYWpYqxY8nb3atbdhMLkvWPBkp', 2, '2021-05-07 10:24:30', NULL),
(49, 33, '00648fb1890f2c449ed9e29207af9a0e447IAA4NXySq9R9/jocKTKYxydkiAeohDLNtlJENDlRdwFvngf5qoYAAAAAIgBrngAAXmiWYAQAAQDuJJVgAwDuJJVgAgDuJJVgBADuJJVg', 'ZXMrWk78z0tlB3n84mGWdDl0cyQooxy2', 2, '2021-05-07 10:30:54', NULL),
(50, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAVSkuzFJPLeO4+JVcYO4F9vehRzxrHm7p+vPG525evt9G+/noAAAAAIgC3TgAAj3CWYAQAAQAfLZVgAwAfLZVgAgAfLZVgBAAfLZVg', 'vZ669ybXyjlGtO3peLlcreE2JRrGubTA', 2, '2021-05-07 11:05:51', NULL),
(51, 33, '00648fb1890f2c449ed9e29207af9a0e447IADiicsrDKb0k71BpvPodGKcg3+lbVjCpkLQvS0/yjY/YsETau4AAAAAIgDYzQAAh3GWYAQAAQAXLpVgAwAXLpVgAgAXLpVgBAAXLpVg', 'frAodIdXnEyxyqIP6U9Lfd70xpxMQSGU', 2, '2021-05-07 11:09:59', NULL),
(52, 33, '00648fb1890f2c449ed9e29207af9a0e447IACmltw0r2DXY6TF0WxbQiNagVz3Wh1/KXtQKVbeswHuRJkTpaQAAAAAIgDMGAEAw3KWYAQAAQBTL5VgAwBTL5VgAgBTL5VgBABTL5Vg', 'qCKTNZ4djxfJHLj7KxRJu7Ao3GpScgPY', 2, '2021-05-07 11:15:15', NULL),
(53, 33, '00648fb1890f2c449ed9e29207af9a0e447IACLWd53gNVqrcivEPaiL5GzsYMF/tz8rocG6dJ4IXoMNBPGQzQAAAAAIgCU/AAALnOWYAQAAQC+L5VgAwC+L5VgAgC+L5VgBAC+L5Vg', 'gbdbsUc1JqPvOHZPjLCkPohQBBFjwCDl', 2, '2021-05-07 11:17:02', NULL),
(54, 33, '00648fb1890f2c449ed9e29207af9a0e447IAC9XhJwBtv1awJuXxccHKlGIEzmhOi8Cl9azf4q0B2oA9MljaQAAAAAIgAD8wAAeHOWYAQAAQAIMJVgAwAIMJVgAgAIMJVgBAAIMJVg', 'aInBVx5mvSJUdHnJXondtG37nUcyUdEJ', 2, '2021-05-07 11:18:16', NULL),
(55, 37, '00648fb1890f2c449ed9e29207af9a0e447IACnrKzadBrVEoXj0dXYAxrohR2dRngBkdhLUotTr3IeMtxtD2oAAAAAIgDDMwEAeNKWYAQAAQAIj5VgAwAIj5VgAgAIj5VgBAAIj5Vg', 'a4NYI315ZFoPjcYn11TwVaafmvmsnAxv', 2, '2021-05-07 18:03:36', NULL),
(56, 37, '00648fb1890f2c449ed9e29207af9a0e447IAAtoTG41Txj/28i4ceJxElrAWL1HDdevgRX4P5RFRumErVqXyAAAAAAIgAkUQEAjdKWYAQAAQAdj5VgAwAdj5VgAgAdj5VgBAAdj5Vg', 'zQMKTzZeceFxBLy0RbJmu0SxODD2uvhi', 2, '2021-05-07 18:03:57', NULL),
(57, 44, '00648fb1890f2c449ed9e29207af9a0e447IABYIFaM/3XCC8PdoXp7+bDWh6Ly9J0FDwLc2JhKzNJUrLb9m7IAAAAAIgDKdwAAC1KXYAQAAQCbDpZgAwCbDpZgAgCbDpZgBACbDpZg', 'H2noecfpjDNnzICNoy5PXIUj5vL6B7Ht', 2, '2021-05-08 03:07:55', NULL),
(58, 44, '00648fb1890f2c449ed9e29207af9a0e447IADsF0OfgU4+3kY7p79heL7tF9IgkML+ew0P+PamDZIwDe/Lk+0AAAAAIgDlHQEAJlKXYAQAAQC2DpZgAwC2DpZgAgC2DpZgBAC2DpZg', 'EzCJR2rE4mmrg1s7po9sMfqMluBhAWeK', 2, '2021-05-08 03:08:22', NULL),
(59, 37, '00648fb1890f2c449ed9e29207af9a0e447IADy/WPN8SBQSpCapp4uql+dpAb45tglGzEv+lutdlpy9tr6t9AAAAAAIgDdAwAArFKXYAQAAQA8D5ZgAwA8D5ZgAgA8D5ZgBAA8D5Zg', 'UmPO2VK3EvAoEBeccSIXStVZXJTlPgUb', 2, '2021-05-08 03:10:36', NULL),
(60, 34, '00648fb1890f2c449ed9e29207af9a0e447IAAY5f9ePHi+jwtXGi3Mui7L5gjIxPACilB0bgZp7pG6avp6W5UAAAAAIgBbLQEAyYiXYAQAAQBZRZZgAwBZRZZgAgBZRZZgBABZRZZg', 'glWlx88kSlSN85L7HyHodWpnojCFAiik', 2, '2021-05-08 07:01:29', NULL),
(61, 34, '00648fb1890f2c449ed9e29207af9a0e447IAC/j9ZSJLAmYhAQUK4eCyU5ItZsDiif74q0MQgrUWZ27T2Tw7gAAAAAIgDy3QAA8oiXYAQAAQCCRZZgAwCCRZZgAgCCRZZgBACCRZZg', 'n9vEH40WBfhJtP47TX9jIMsemyG9yc8M', 2, '2021-05-08 07:02:10', NULL),
(62, 32, '00648fb1890f2c449ed9e29207af9a0e447IADHbfd9JAngzLfHLT+icUZD3sFreYQbS+Rq06w8meDz5nVG9jIAAAAAIgB3DgEA8m6bYAQAAQCCK5pgAwCCK5pgAgCCK5pgBACCK5pg', 'CUF1qMqL5kjywHoDOTfEKEPFsA4BTVDG', 2, '2021-05-11 06:00:18', NULL),
(63, 32, '00648fb1890f2c449ed9e29207af9a0e447IABFnY657/S3/r3d9IUCpz80tCyDb6Fv4VtQ+rqWbII233sYUNgAAAAAIgAMzQAA3XGbYAQAAQBtLppgAwBtLppgAgBtLppgBABtLppg', '5NYMhuCgN12EikM3ZS2YbNI4to6ysY8R', 2, '2021-05-11 06:12:45', NULL),
(64, 33, '00648fb1890f2c449ed9e29207af9a0e447IADcBdHPvy9xd1iuVt9Mkq45fTkSrjzEgZEiShe6UgnCrFiuZnQAAAAAIgB0RAAAMfKcYAQAAQDBrptgAwDBrptgAgDBrptgBADBrptg', 'cxGOXz4w2NcDpH0ZfH4tDh1eSAagYz8u', 2, '2021-05-12 09:32:33', NULL),
(65, 32, '00648fb1890f2c449ed9e29207af9a0e447IAC+aQLz+U0MktqCPLaD6E1AKrnDVF1GUUHkHgzKfuuL40Rpyl4AAAAAIgCJAQEATwadYAQAAQDfwptgAwDfwptgAgDfwptgBADfwptg', '1xDixtoepIrj3jxxqFM9dU7dFcZVbbfk', 2, '2021-05-12 10:58:23', NULL),
(66, 32, '00648fb1890f2c449ed9e29207af9a0e447IACVla/qbiQa3H6iTEVTqExXbfSFJrJzZGK438MI1u6Bw3c53zoAAAAAIgC6OAAAnQedYAQAAQAtxJtgAwAtxJtgAgAtxJtgBAAtxJtg', 'QCS1FcymJlPD2m4gilMW2wfbVtiNMGMl', 2, '2021-05-12 11:03:57', NULL),
(67, 33, '00648fb1890f2c449ed9e29207af9a0e447IACdkGB0/SBF1TS6u+oEFnVl3RSZpYpaig8oMPODgeohCeIIW58AAAAAIgAYAQAAyAedYAQAAQBYxJtgAwBYxJtgAgBYxJtgBABYxJtg', '94ltAcPQp7p871PO5LNBW16M8ldbtmZs', 2, '2021-05-12 11:04:40', NULL),
(68, 33, '00648fb1890f2c449ed9e29207af9a0e447IACZSHByMUO2J7z4zWEwFxbBNkfJoDYneGakL8wl2zQOS8cH/yYAAAAAIgBlPgEA/wedYAQAAQCPxJtgAwCPxJtgAgCPxJtgBACPxJtg', 'IDWw2EXTdF2hzZGwFrfCdnQwm97U2d9v', 2, '2021-05-12 11:05:35', NULL),
(69, 33, '00648fb1890f2c449ed9e29207af9a0e447IADuWImTStXqJJLmOVWPiSGY7mngKN1DoN28T/XNM8tUxZ2HIn8AAAAAIgCLfwAA2wGeYAQAAQBrvpxgAwBrvpxgAgBrvpxgBABrvpxg', 'K0miSEuhUmbzHsHuZ2EXv5bk8E2e0Rpa', 2, '2021-05-13 04:51:39', NULL),
(70, 33, '00648fb1890f2c449ed9e29207af9a0e447IAArvjapMKgxTG/gGGH53Z5SHMfKlTZfslKraavQM6qqyDXoDisAAAAAIgB9PQAA+QGeYAQAAQCJvpxgAwCJvpxgAgCJvpxgBACJvpxg', 'zXFNqyD8uS2WblPMOIL0PRkIxROVgbDC', 2, '2021-05-13 04:52:09', NULL),
(71, 44, '00648fb1890f2c449ed9e29207af9a0e447IAA8qEKPTBpnRY+VBZR/PqY3qdieKJAYv2tsiVuH3Jqj9wrfgw8AAAAAIgABXAEA/CygYAQAAQCM6Z5gAwCM6Z5gAgCM6Z5gBACM6Z5g', 'nXoodn9VhxT31AeuUL4dRx3UB99hvfB3', 2, '2021-05-14 20:20:12', NULL),
(72, 44, '00648fb1890f2c449ed9e29207af9a0e447IAALXK2aZu98nvs4HwFpRJPIQGC+cIqtm3Lm6qEUZYX1Cwwq6/EAAAAAIgCbuQAAFy2gYAQAAQCn6Z5gAwCn6Z5gAgCn6Z5gBACn6Z5g', '1zfHLssjNaT1Ki9iNpE1GtUEFmMZPXHq', 2, '2021-05-14 20:20:39', NULL),
(73, 37, '00648fb1890f2c449ed9e29207af9a0e447IACJTZ9lGw687iogSTHI/2/O8MBN3pYLSV9ZHtz0uuZUfeMt6dYAAAAAIgC0WgAAUC2gYAQAAQDg6Z5gAwDg6Z5gAgDg6Z5gBADg6Z5g', 'KpYxXEgWwR7ckhA2YlwlF1FdO8kmJ8qm', 2, '2021-05-14 20:21:36', NULL),
(74, 44, '00648fb1890f2c449ed9e29207af9a0e447IACjX4abAKNBtqa9gfr1AE8D/poj6x2n8lfE9XpPc/2vnr5HlxMAAAAAIgBxPQEAai2gYAQAAQD66Z5gAwD66Z5gAgD66Z5gBAD66Z5g', 'ymGc4zzEjBsuQM6GTolSKIDKJMuxCQBM', 2, '2021-05-14 20:22:02', NULL),
(75, 33, '00648fb1890f2c449ed9e29207af9a0e447IABKDY8rqhdlSeu5rmc5vY2qcODmK0aYMQMKJ5vFYN3qFlVC024AAAAAIgBsagAA6kSjYAQAAQB6AaJgAwB6AaJgAgB6AaJgBAB6AaJg', 'qKDMJhCIuALkEG9Re0dTvtKSnsp2swPC', 2, '2021-05-17 04:39:06', NULL),
(76, 33, '00648fb1890f2c449ed9e29207af9a0e447IACPjg9PAYP7L2tV+BKQrivV6pfyfeP4PnCxACplQ3CHb0lGfgwAAAAAIgD6vwAAdlqjYAQAAQAGF6JgAwAGF6JgAgAGF6JgBAAGF6Jg', '4xSpWFQpQPpGyXA0ksbt2KDSD6eRiWIM', 2, '2021-05-17 06:11:02', NULL),
(77, 33, '00648fb1890f2c449ed9e29207af9a0e447IACRWJgfm5piFYyarT5s9PcBw82wwWnAbHMQZL0ZU4CKqxhnxrkAAAAAIgBgewAALrijYAQAAQC+dKJgAwC+dKJgAgC+dKJgBAC+dKJg', '9xZ0YDbmLafkFEYrn8hmMeUrICr3lGxY', 2, '2021-05-17 12:50:54', NULL),
(78, 32, '00648fb1890f2c449ed9e29207af9a0e447IADnXycD+QSVkLScY2ZwHaJXTFyMBBy20i5kO7u5hxCGj6TasMwAAAAAIgChAAAAVbijYAQAAQDldKJgAwDldKJgAgDldKJgBADldKJg', '3Yr6dHNwh1twzzNxCscbJbybSbBY1QUm', 2, '2021-05-17 12:51:33', NULL),
(79, 33, '00648fb1890f2c449ed9e29207af9a0e447IADaJkzkQPqcb/KXQRArW+S7msJxA03mhj36PxLyw/csfCttI/UAAAAAIgDy4QAAepukYAQAAQAKWKNgAwAKWKNgAgAKWKNgBAAKWKNg', 'WKG8vqiBUUM1UaYO886A6tq3qZgPKzvS', 2, '2021-05-18 05:00:42', NULL),
(80, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAgBXeEx2iufrttG20SlrR/eewKIwUE2m38yWEiOfRsjsCc13cAAAAAIgCYowAAkZukYAQAAQAhWKNgAwAhWKNgAgAhWKNgBAAhWKNg', 'vGiehphCbK3tFnGy0ZTNeJIYwPg0Hmzx', 2, '2021-05-18 05:01:05', NULL),
(81, 44, '00648fb1890f2c449ed9e29207af9a0e447IAAog45Z+6vtaQEmruRWzwFQoQC/ygWOYv7zxeBZhh9NkYjrTvsAAAAAIgANfgAAULGlYAQAAQDgbaRgAwDgbaRgAgDgbaRgBADgbaRg', 'Wwn8QhSRVB8KmOz6duMmGBi9L1bsBYpU', 2, '2021-05-19 00:46:08', NULL),
(82, 44, '00648fb1890f2c449ed9e29207af9a0e447IACaun8RUv7vwi3fX55gwomhZrQ8vwragu8sWdjFb0LDfLkadScAAAAAIgDHgwAAZbGlYAQAAQD1baRgAwD1baRgAgD1baRgBAD1baRg', '1CihliP2w288RAWo5am8eLiKlG3iA63h', 2, '2021-05-19 00:46:29', NULL),
(83, 37, '00648fb1890f2c449ed9e29207af9a0e447IADMwaaoZcCmmYc6GrGuaMZ1FKLxBGK3fh5fB1NX2KWHlmVayqcAAAAAIgBCLgEAh7GlYAQAAQAXbqRgAwAXbqRgAgAXbqRgBAAXbqRg', 'B3OtTS1j4yBo12TAr2uI2a4YQtVnL0Nk', 2, '2021-05-19 00:47:03', NULL),
(84, 32, '00648fb1890f2c449ed9e29207af9a0e447IABjRLpH3rDV7BcGLQTx8hj12DpZ/3E2PjjzkHC3EoO/7YiYY9oAAAAAIgAriAAAKOWlYAQAAQC4oaRgAwC4oaRgAgC4oaRgBAC4oaRg', 'FdzxSU3wsL6PGIQDOy3znifhJi9lgvvl', 2, '2021-05-19 04:27:20', NULL),
(85, 32, '00648fb1890f2c449ed9e29207af9a0e447IAC3ZYm4zjHt1rq1Glrv46U6ugRlnvFkalp9HtJVS/q7hvMjtjoAAAAAIgDIJAAAQOWlYAQAAQDQoaRgAwDQoaRgAgDQoaRgBADQoaRg', 'G0gdNhxGkoYxJBn6LuOZ9RwoHPRobeEP', 2, '2021-05-19 04:27:44', NULL),
(86, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAkJixX4AW2SuW7Mgs+soy1A/eIAAbjjTBmyyvy+urAWtZw7SgAAAAAIgDeQQEAZeWlYAQAAQD1oaRgAwD1oaRgAgD1oaRgBAD1oaRg', 'ipycJfHXjvxyLBYi1pE5AhYlw77WlVX6', 2, '2021-05-19 04:28:21', NULL),
(87, 32, '00648fb1890f2c449ed9e29207af9a0e447IADoYZjkyv+pvPqnk0B08gxGjGn2jmXs4fakx99yz03iFOvDxf4AAAAAIgBsiQAAleWlYAQAAQAloqRgAwAloqRgAgAloqRgBAAloqRg', 'G6VuZp9zNb8mZcAIfAnbvyBhNq7IiDgn', 2, '2021-05-19 04:29:09', NULL),
(88, 36, '00648fb1890f2c449ed9e29207af9a0e447IAB9CMb/Q5EP5OLIz6uRX4KzdWrpRCikulMRTFZnkvuTCVEWWXIAAAAAIgBShwAAZOilYAQAAQD0pKRgAwD0pKRgAgD0pKRgBAD0pKRg', 'QJyAIvk7XeQ3OFK7k2RSnYNN0J6TkTmt', 2, '2021-05-19 04:41:08', NULL),
(89, 36, '00648fb1890f2c449ed9e29207af9a0e447IADRLMMqBBBy4VR4pn8U6QZ/A3/LonpdSiMnpAtsym5qkoGfqXcAAAAAIgDxlwAAiOilYAQAAQAYpaRgAwAYpaRgAgAYpaRgBAAYpaRg', '9YqqSpSTA5dZfAzLtZs6oOCea5ms9HqH', 2, '2021-05-19 04:41:44', NULL),
(90, 36, '00648fb1890f2c449ed9e29207af9a0e447IAALj3TgtsJ9uVP9In1NI4ujic57ff5Q2S61U89iA1u7rVG2y0UAAAAAIgBMkwAApOilYAQAAQA0paRgAwA0paRgAgA0paRgBAA0paRg', 'jD3yt8IYLZBJsXmWve236mSsKLGAaDy8', 2, '2021-05-19 04:42:12', NULL),
(91, 36, '00648fb1890f2c449ed9e29207af9a0e447IADb1A7kpuXxdd1T/MjkfttNosZn3WwoHhlhLv9iGRbnpDLPkGUAAAAAIgBkjwAAJ+mlYAQAAQC3paRgAwC3paRgAgC3paRgBAC3paRg', 'uUOe78rK0YrkEaBgWxUO2Rdu5uv7oJAv', 2, '2021-05-19 04:44:23', NULL),
(92, 37, '00648fb1890f2c449ed9e29207af9a0e447IAA/s8UAI26s0gv4aH8tRn2Mp3Pj2BersOsqS3ztEG5P1jb3kpUAAAAAIgDbcgAA9OqlYAQAAQCEp6RgAwCEp6RgAgCEp6RgBACEp6Rg', 'xaYYVGDZAHO1meXzvXN1w5RCV9w2qQkb', 2, '2021-05-19 04:52:04', NULL),
(93, 36, '00648fb1890f2c449ed9e29207af9a0e447IADlPEKzmjvKRA+lMcjGlQzethrNJkmS8vv0OCDtObUjyVb+kZgAAAAAIgDnPgEArOylYAQAAQA8qaRgAwA8qaRgAgA8qaRgBAA8qaRg', '7Udi66xUI4OnntjbCce6N8CiwqW39VJi', 2, '2021-05-19 04:59:24', NULL),
(94, 36, '00648fb1890f2c449ed9e29207af9a0e447IABqdh3rKo8SUqeyVS9ws97WKm0UQShY2y+nzfJfe90R6jTqwtsAAAAAIgDhDQEA9+ylYAQAAQCHqaRgAwCHqaRgAgCHqaRgBACHqaRg', 'uzG0BK7i5yixLzKCEMLuhLh4hlmXl3gL', 2, '2021-05-19 05:00:39', NULL),
(95, 36, '00648fb1890f2c449ed9e29207af9a0e447IAD4G8k0Pg8en1Qp6I86xB5GoDdCCnDFHweQ+GtRTIzzsuN4VUIAAAAAIgABVgAAbu2lYAQAAQD+qaRgAwD+qaRgAgD+qaRgBAD+qaRg', 'mrW6AboigEfnDK6Y1Xuv37fNH5XyNxx5', 2, '2021-05-19 05:02:38', NULL),
(96, 36, '00648fb1890f2c449ed9e29207af9a0e447IAAy4+INFqReTxiuT97PJYqiOcu5/WtdvlEmhlZX12pKWUewxV8AAAAAIgAhIAAAh+6lYAQAAQAXq6RgAwAXq6RgAgAXq6RgBAAXq6Rg', 'Yu5FXKivJTWg6nLhGMg0GshmK0vH4h6i', 2, '2021-05-19 05:07:19', NULL),
(97, 36, '00648fb1890f2c449ed9e29207af9a0e447IAACt6CblSmafJlIm9B1ZlyG0TlnWTUc2vak3cyqGe1dWOC2s5QAAAAAIgAbUQAAr/GlYAQAAQA/rqRgAwA/rqRgAgA/rqRgBAA/rqRg', 'L73Tli9nM7vSs6SgkpskEkuwyopWdYHc', 2, '2021-05-19 05:20:47', NULL),
(98, 36, '00648fb1890f2c449ed9e29207af9a0e447IACjUK94cYgZiwE0Rc70dAxuF/Zy28Hs1qh9b24M5u+pwsFFhS4AAAAAIgDSGQEAyPGlYAQAAQBYrqRgAwBYrqRgAgBYrqRgBABYrqRg', 'hPBmbNjlBV6r1DaH2l00aBHzX0HzdFqS', 2, '2021-05-19 05:21:12', NULL),
(99, 36, '00648fb1890f2c449ed9e29207af9a0e447IAAl8CTTlGd69DWzioG+MEL9OSV5nDRBEl05yUB9pNSFp47s3PEAAAAAIgBznQAAUvKlYAQAAQDirqRgAwDirqRgAgDirqRgBADirqRg', 'g2ufRkFtTnLhvvAnsbDcI059krJvrEIP', 2, '2021-05-19 05:23:30', NULL),
(100, 36, '00648fb1890f2c449ed9e29207af9a0e447IACwMNmfouBVHn2cEvBypseD43VCBDdCNWd1FGfTZ+g6lRXxMuEAAAAAIgANGAEAMPSlYAQAAQDAsKRgAwDAsKRgAgDAsKRgBADAsKRg', 'gxYeum44ZjgPNRsHyCZOy5lwNlDCjNh4', 2, '2021-05-19 05:31:28', NULL),
(101, 36, '00648fb1890f2c449ed9e29207af9a0e447IACFtvzEolgMYCrZD45Kv1eo+9eOAveBFezJZdkCS5aHqtS6NewAAAAAIgDfbAAATPSlYAQAAQDcsKRgAwDcsKRgAgDcsKRgBADcsKRg', 'yqxn2vKlBXspHYEPBg7AKtDN7Xgfx8Q2', 2, '2021-05-19 05:31:56', NULL),
(102, 36, '00648fb1890f2c449ed9e29207af9a0e447IADkIwxw5ZfLy0Kyl4e4IOsQWfsA4Gpfd3HVLTASSaGC//r8Pn4AAAAAIgBIbgAAY/SlYAQAAQDzsKRgAwDzsKRgAgDzsKRgBADzsKRg', 'tde2a02C9nbV7BYuqqOd6uK6dXFWQQae', 2, '2021-05-19 05:32:19', NULL),
(103, 36, '00648fb1890f2c449ed9e29207af9a0e447IAA0mD1nNHLhwXUtXYdtuNfuRH4PIRfUVigGU56DOcbY3mUww7kAAAAAIgDbZgAAnfSlYAQAAQAtsaRgAwAtsaRgAgAtsaRgBAAtsaRg', 'eciq0d0eIH1KsH6lRtGTkcLXDleTXiaI', 2, '2021-05-19 05:33:17', NULL),
(104, 37, '00648fb1890f2c449ed9e29207af9a0e447IADE05HM0BoFyGtH6r0JcPt1xt/fVsjS3yrJgJex6RQnIsV8h/gAAAAAIgBAeAAA9PSlYAQAAQCEsaRgAwCEsaRgAgCEsaRgBACEsaRg', 'RCBdLssvEraqjl0WmIaCkGSkWeUo4xfs', 2, '2021-05-19 05:34:44', NULL),
(105, 37, '00648fb1890f2c449ed9e29207af9a0e447IAAKXrNer21dt9Ee2OzzYred/7z3ygRwManFPZ3A18Zo71Z6rEQAAAAAIgB2vQAAGvWlYAQAAQCqsaRgAwCqsaRgAgCqsaRgBACqsaRg', 'yxR7eeWbtYRuXoV1G25ymkvBZ2ec0Nkv', 2, '2021-05-19 05:35:22', NULL),
(106, 36, '00648fb1890f2c449ed9e29207af9a0e447IACgScMmkPeP7Ll5WNjuoJ0fw3UYuTER06KrOuJ8wGo5fwTceuMAAAAAIgCCQQAALvWlYAQAAQC+saRgAwC+saRgAgC+saRgBAC+saRg', 'zBzxoFSN6VWXRktLmXWmqxQ3VsTOEA7Y', 2, '2021-05-19 05:35:42', NULL),
(107, 37, '00648fb1890f2c449ed9e29207af9a0e447IADI/ZUonW0c2J4y9Uuas2f9WYBhQuoXyFyaJZ13wfiXt2cpjg0AAAAAIgCGWAEAcPWlYAQAAQAAsqRgAwAAsqRgAgAAsqRgBAAAsqRg', 'IxZw4DC5U2Yi2iP5iHKgAG7u3VEu3DDe', 2, '2021-05-19 05:36:48', NULL),
(108, 36, '00648fb1890f2c449ed9e29207af9a0e447IACYfsowVla4KY5dioahUdEpl9aC17cN7uMrooC4FXHx0o2toIoAAAAAIgBI/wAAjvWlYAQAAQAesqRgAwAesqRgAgAesqRgBAAesqRg', 'SGKljUqfc6SqcMdLgIpFy6R2hVORaqqK', 2, '2021-05-19 05:37:18', NULL),
(109, 36, '00648fb1890f2c449ed9e29207af9a0e447IADnPlQrJCk52WMBClgy7fz9oG0nqBwV7KI2ek4oTZo/m/UN53IAAAAAIgBXTAEAHfalYAQAAQCtsqRgAwCtsqRgAgCtsqRgBACtsqRg', 'n0BVMOQ6ZKixDa1U4lEtiNxenQ4zXrHw', 2, '2021-05-19 05:39:41', NULL),
(110, 33, '00648fb1890f2c449ed9e29207af9a0e447IACPjhfInWaqJis2o36bWL3A6QZ2xw01TXRtO8VS2tbLjuiG1MEAAAAAIgBhugAAXvalYAQAAQDusqRgAwDusqRgAgDusqRgBADusqRg', '5jjwKDkLNVd6bIhoLhwiKg1qKyEOTZwT', 2, '2021-05-19 05:40:46', NULL),
(111, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAM0bVXfYTyMlSvUZVhR6olhuhmc/WUncIWQ/lJ3a4U/6Wp95EAAAAAIgADOQAA1falYAQAAQBls6RgAwBls6RgAgBls6RgBABls6Rg', 'I8rOhkK2S1x4phCZvahL6hGC3W8q3nLp', 2, '2021-05-19 05:42:45', NULL),
(112, 36, '00648fb1890f2c449ed9e29207af9a0e447IADb3bEccNPeksl2p7wQO8pLrk6oM/90hf1c68DvuguB/vISk5cAAAAAIgA5CAEAKPelYAQAAQC4s6RgAwC4s6RgAgC4s6RgBAC4s6Rg', 'fsta8DyR0ZZxnL94RaA3n57mOOCfwSzm', 2, '2021-05-19 05:44:08', NULL),
(113, 36, '00648fb1890f2c449ed9e29207af9a0e447IAAtZrEmWSZh2hma4fAfAXvF8B0uoPRvHb/K07bec5Tq6lQQI6QAAAAAIgBRIAAA3velYAQAAQButKRgAwButKRgAgButKRgBAButKRg', 'BBluIoAu0qOviaLMDFHW9BYyqL8u7M5p', 2, '2021-05-19 05:47:10', NULL),
(114, 36, '00648fb1890f2c449ed9e29207af9a0e447IABZ4XtNOMXI1PPadVfDOdsG9VL/rmohtGSPl7GZF8FLlAR7b8YAAAAAIgCFUwAA+PelYAQAAQCItKRgAwCItKRgAgCItKRgBACItKRg', 'TDsQpG5HkF8K09MKZt7p9Vadf9mIycZC', 2, '2021-05-19 05:47:36', NULL),
(115, 36, '00648fb1890f2c449ed9e29207af9a0e447IAC1NFD5aDqfVNQEToHnLJB2zvIiv02Uaii69t6gLju3h5hkD1cAAAAAIgAvdQAARvilYAQAAQDWtKRgAwDWtKRgAgDWtKRgBADWtKRg', '45yUK5nDliQLD1NJxEpxBTR59MqXkVyK', 2, '2021-05-19 05:48:54', NULL),
(116, 33, '00648fb1890f2c449ed9e29207af9a0e447IABKqZ3CXuW5LgVYK2iHdIHbqzHhT3kwR2duRg4h+QNsszuiEfEAAAAAIgCcMgAAqgCmYAQAAQA6vaRgAwA6vaRgAgA6vaRgBAA6vaRg', 'G8prHN6oJws48ZAT5fTq3OG5pv2DREZG', 2, '2021-05-19 06:24:42', NULL),
(117, 34, '00648fb1890f2c449ed9e29207af9a0e447IABky09tALCpqHrvUziisBvyt8jBos8HSPtVM0ofnqU+06zDr9UAAAAAIgDQSwAAFwGmYAQAAQCnvaRgAwCnvaRgAgCnvaRgBACnvaRg', 'zX4TtP656Ych6GYwTqn3Yu0Kw33Sw211', 2, '2021-05-19 06:26:31', NULL),
(118, 32, '00648fb1890f2c449ed9e29207af9a0e447IAAqY2kh28osFARpS13w8Y5sUj73VMQCNvUqa53Z46EK4uyYiUMAAAAAIgCi3QAAcwGmYAQAAQADvqRgAwADvqRgAgADvqRgBAADvqRg', 'peZ1JTm1EDE2R2hhgeh87BQDY2P48t3c', 2, '2021-05-19 06:28:03', NULL),
(119, 33, '00648fb1890f2c449ed9e29207af9a0e447IAC+zjNZ6ud5QWTvps7VjkjAmcb0ETNnS9MqEYB1Tf4h255AXLYAAAAAIgCdJQEAtgGmYAQAAQBGvqRgAwBGvqRgAgBGvqRgBABGvqRg', 'CeeUwxcifS0jKdVxs3KjL0goh0YkxIFV', 2, '2021-05-19 06:29:10', NULL),
(120, 50, '00648fb1890f2c449ed9e29207af9a0e447IAAD9DT0bklMNvgBTNJ9WznDRk+TuG486KkRwh2vwYnM5t2+G3sAAAAAIgCQVQAA4wGmYAQAAQBzvqRgAwBzvqRgAgBzvqRgBABzvqRg', 'Ss8cK1gx5huM7aA5iBcFarl9aISKQOAZ', 2, '2021-05-19 06:29:55', NULL),
(121, 32, '00648fb1890f2c449ed9e29207af9a0e447IABx0UQtaui9jJUT9SprHSXCGa+9Gmllu8esWi0kkh5+4av9CG4AAAAAIgClPgAAyAOmYAQAAQBYwKRgAwBYwKRgAgBYwKRgBABYwKRg', 'TcJvjCXoHCVip6Bd9G4jUqIz2w9mY2Wi', 2, '2021-05-19 06:38:00', NULL),
(122, 33, '00648fb1890f2c449ed9e29207af9a0e447IADwOP184EXpWIG5lOxcpj715c5GHIDj2B2uVkWC6k4x6j0a01IAAAAAIgD8UQAA5gWmYAQAAQB2wqRgAwB2wqRgAgB2wqRgBAB2wqRg', '4cuBmusqs8YKMvbM4JXmYau0Z4P0speV', 2, '2021-05-19 06:47:02', NULL),
(123, 34, '00648fb1890f2c449ed9e29207af9a0e447IAB8sBEoejpp7r7AhM6O8q+FA+7oeUeyCe0li7q6DD9BFedMhNEAAAAAIgAasgAABQamYAQAAQCVwqRgAwCVwqRgAgCVwqRgBACVwqRg', 'Qm5pqZytceRi6XWi8J8JHRYTWv70jLgo', 2, '2021-05-19 06:47:33', NULL),
(124, 33, '00648fb1890f2c449ed9e29207af9a0e447IADUQiMC9aImR0OcqApaNh71m48Y0WtScXHrt/SGnvkT3C94bNIAAAAAIgBW2AAAIwamYAQAAQCzwqRgAwCzwqRgAgCzwqRgBACzwqRg', 'KdKBSwqOQpNNSUzUc5kcAFZ6VllTaFEb', 2, '2021-05-19 06:48:03', NULL),
(125, 34, '00648fb1890f2c449ed9e29207af9a0e447IACKAEeLKIF3mfLl/QNIIjI4W89p5lhZTsorcCBN0UfjaHSCRrUAAAAAIgAZAwEAQgamYAQAAQDSwqRgAwDSwqRgAgDSwqRgBADSwqRg', 'S6xIe5IkLoQnuRIKlKQ7B3NVMc7iYqOO', 2, '2021-05-19 06:48:34', NULL),
(126, 34, '00648fb1890f2c449ed9e29207af9a0e447IAB2MNJklxajUR3XYhyUtS+O9CDwoxRL+G11hTCBdZ0reC1b/0gAAAAAIgD87QAAqAamYAQAAQA4w6RgAwA4w6RgAgA4w6RgBAA4w6Rg', 'jmrNBTQU5nIc1EQO2iNv70LZe0vA1Pdt', 2, '2021-05-19 06:50:16', NULL),
(127, 33, '00648fb1890f2c449ed9e29207af9a0e447IADM0sjMLCi6bMgKLmKbCdk50IUPh+zOhyW9es3np8so9p7ChEUAAAAAIgDOlAAA6gamYAQAAQB6w6RgAwB6w6RgAgB6w6RgBAB6w6Rg', 'ev3oirOdEFpk5ixvPvM31JikofVkMtgj', 2, '2021-05-19 06:51:22', NULL),
(128, 39, '00648fb1890f2c449ed9e29207af9a0e447IAAFoUnRi4piiOPNSQf0QKk+vCBrvqD7bSx+O6Fi6HCD0/6R5G0AAAAAIgDDgQAAagemYAQAAQD6w6RgAwD6w6RgAgD6w6RgBAD6w6Rg', 'trkapPCm4Dk4XysEDU1ltKdR6TGuivq7', 2, '2021-05-19 06:53:30', NULL),
(129, 34, '00648fb1890f2c449ed9e29207af9a0e447IABO5r2mhAuMGqZhyz78rdOWH7uSPmp45JnQYzRPbCtW5qsVZmwAAAAAIgBRNwEAgQemYAQAAQARxKRgAwARxKRgAgARxKRgBAARxKRg', 'kcG0PPuR1pnX9f0U9CKPoCZodA3ELXuV', 2, '2021-05-19 06:53:53', NULL),
(130, 39, '00648fb1890f2c449ed9e29207af9a0e447IAA5V+3qUqxN+OxTk+6FAomauacpdYzavErsBsdHwFE6yk0/ZEEAAAAAIgAVKwAApAemYAQAAQA0xKRgAwA0xKRgAgA0xKRgBAA0xKRg', 'DrEZix1kjzgeqo5NaZwTxxR2AR4JMVxw', 2, '2021-05-19 06:54:28', NULL),
(131, 39, '00648fb1890f2c449ed9e29207af9a0e447IAD2XqtQCuSCTdFHINktQuAbBQhsq5OxB61BeYTq0Li0ZHFLtXUAAAAAIgC8WAAADQimYAQAAQCdxKRgAwCdxKRgAgCdxKRgBACdxKRg', 'qKW8u3Y95qMX3gw946ZDFysEndvRmriK', 2, '2021-05-19 06:56:13', NULL),
(132, 34, '00648fb1890f2c449ed9e29207af9a0e447IAD+0ma40G3k9LfMhGD3CdH2H4sXF3KhgessijpLOq7PbX5WUdkAAAAAIgDZJgEAQQimYAQAAQDRxKRgAwDRxKRgAgDRxKRgBADRxKRg', 'tT9yO5NlkL0K2Iwr5E4XmWoDzBzm9EcQ', 2, '2021-05-19 06:57:05', NULL),
(133, 39, '00648fb1890f2c449ed9e29207af9a0e447IADp+QeqPZ1bYjuAW4QGsYR2DG6cZOxDRuU+ylCAJJTGiLIUQU0AAAAAIgDeuAAAwAmmYAQAAQBQxqRgAwBQxqRgAgBQxqRgBABQxqRg', 'oC70tknC7yiEJfj7C6uxSOBwc0qSqJfo', 2, '2021-05-19 07:03:28', NULL),
(134, 39, '00648fb1890f2c449ed9e29207af9a0e447IACSgm52AxoZa4oc4UBeYj5zoJrz3BsfnNg/DE2iCIwOE96U1vkAAAAAIgD8qwAAkAqmYAQAAQAgx6RgAwAgx6RgAgAgx6RgBAAgx6Rg', '2k9s8JqnuiAEkwAQRcq1NCwVRRoH8jg2', 2, '2021-05-19 07:06:56', NULL),
(135, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAw1EDXVDFqFXqNX3E/yDndK3BlZh27lbZQ1rMCqbpDKhC6LnIAAAAAIgA/MAEAvAqmYAQAAQBMx6RgAwBMx6RgAgBMx6RgBABMx6Rg', 'bq95YQFm81v253B1ADKSjkrK4vZGXS4i', 2, '2021-05-19 07:07:40', NULL),
(136, 39, '00648fb1890f2c449ed9e29207af9a0e447IAAp+sFMdkvYrfofzRipjodulqaL0H5xknALzIb9WkiMaX33wocAAAAAIgBycwAAWwumYAQAAQDrx6RgAwDrx6RgAgDrx6RgBADrx6Rg', 'ULTRVsuB4UJ8Xgqd7ZI7Xg0svOEWgPvj', 2, '2021-05-19 07:10:19', NULL),
(137, 33, '00648fb1890f2c449ed9e29207af9a0e447IAC9JYuZ50cGf3+CLnMK4TVK8go4NPeWEiNKUXlfPVTXGLBFpOsAAAAAIgCgOAAAdAumYAQAAQAEyKRgAwAEyKRgAgAEyKRgBAAEyKRg', 'B4WMj7rBPQv0zrsCrBvoAxiDHOAekC82', 2, '2021-05-19 07:10:44', NULL),
(138, 33, '00648fb1890f2c449ed9e29207af9a0e447IADYfRnAodkHtNPZ5LnyqzRkCwiQmEH9QMwLRWtmvdbhXFZcSrIAAAAAIgCG+gAA/QumYAQAAQCNyKRgAwCNyKRgAgCNyKRgBACNyKRg', 'EPRuFfbqfsuaJLMEvGVCRvvLZpsxuDjP', 2, '2021-05-19 07:13:01', NULL),
(139, 33, '00648fb1890f2c449ed9e29207af9a0e447IADPXbNxE2XzRQGQnfyrDtSpUf2GLIbqJbDoFIW8z3ZjOoHPLeoAAAAAIgAm6wAASwymYAQAAQDbyKRgAwDbyKRgAgDbyKRgBADbyKRg', 'nuDn2WMrvNlbLPUK70EEX8mCHhmjs0Il', 2, '2021-05-19 07:14:19', NULL),
(140, 33, '00648fb1890f2c449ed9e29207af9a0e447IADKWWJQXCfhl0uYytSE/nhjUCoAIyHtxMW9aUa4TuIC04tvNPYAAAAAIgApEwAAkAymYAQAAQAgyaRgAwAgyaRgAgAgyaRgBAAgyaRg', 'aVO12YyFCB8h2ltX153Ka2PkgI1B80S3', 2, '2021-05-19 07:15:28', NULL),
(141, 33, '00648fb1890f2c449ed9e29207af9a0e447IAAVSSGgUojruzQUpEvh6ApzjOBr8Mhk2hP2EI8q5BX3BIxV4pYAAAAAIgBoUQEAMA2mYAQAAQDAyaRgAwDAyaRgAgDAyaRgBADAyaRg', 'V0s4wHTFutwL1f9b24ONtNzfFCuTT9Mt', 2, '2021-05-19 07:18:08', NULL),
(142, 39, '00648fb1890f2c449ed9e29207af9a0e447IAD2SRvmb9/5adWeQPmaC0cJIKqpBjxNO/9NCfkUBFCFngeyaPMAAAAAIgAM5AAAZg2mYAQAAQD2yaRgAwD2yaRgAgD2yaRgBAD2yaRg', '2xS3tfbGE7xxigTTotxeOrlsMoWqpI82', 2, '2021-05-19 07:19:02', NULL),
(143, 33, '00648fb1890f2c449ed9e29207af9a0e447IADWwBrOOjjCKB5s3GVcULc9a8bvpRJLBTLtMBu0+wEWQVC/id0AAAAAIgBkCQEAfA2mYAQAAQAMyqRgAwAMyqRgAgAMyqRgBAAMyqRg', 'ap3rkhYNuR7JmpfDQNargoTONPKP9SxY', 2, '2021-05-19 07:19:24', NULL),
(144, 37, '00648fb1890f2c449ed9e29207af9a0e447IABzLK8lsVEfmFuFNpYrvK8Q6cHfIlcMdT6LaQjXPG+DDwWdH9EAAAAAIgD7WwAAyMemYAQAAQBYhKVgAwBYhKVgAgBYhKVgBABYhKVg', 'zPFM3mQuFGBrafkxd56QH7JizfZQ6P2r', 2, '2021-05-19 20:34:16', NULL),
(145, 33, '00648fb1890f2c449ed9e29207af9a0e447IABr8qrh+TKEL16hkuQ55AEUDVQVIGcI5Oa81lL2ESWmSJlMgKQAAAAAIgAJhwAAvdaoYAQAAQBNk6dgAwBNk6dgAgBNk6dgBABNk6dg', 'VFMTXP0XBjqxTyX0xnJjRi1jmXS2rg4o', 2, '2021-05-21 10:02:37', NULL),
(146, 34, '00648fb1890f2c449ed9e29207af9a0e447IACzfSkuWW5nb5j2NYCZei4DTX6p1lY3Vy57SDnNB5QbHbggrJUAAAAAIgCsBgEAOmqzYAQAAQDKJrJgAwDKJrJgAgDKJrJgBADKJrJg', 'CHWoSbNA9DoT4LdOji77Iwo7jKDPfKG6', 2, '2021-05-29 10:34:34', NULL),
(147, 34, '00648fb1890f2c449ed9e29207af9a0e447IAAc54vhg3hNq/yv4t7HdsVhBO3W/pOWrTFAorcBE02WkqKBu/gAAAAAIgDLJQAA2HOzYAQAAQBoMLJgAwBoMLJgAgBoMLJgBABoMLJg', 'Oy3vQiMpa1Tf6MsUWgPlvIUednbNZ0HC', 2, '2021-05-29 11:15:36', NULL),
(148, 34, '00648fb1890f2c449ed9e29207af9a0e447IABuPXzl6qQNyGLJMukouEQE8wlB+yvvylSHF3CKygzECdzsXWUAAAAAIgDEXgAA/XOzYAQAAQCNMLJgAwCNMLJgAgCNMLJgBACNMLJg', '1L3M5Kg8RJdHnQt3Q0m0h8nhAUu2CIpA', 2, '2021-05-29 11:16:13', NULL),
(149, 50, '00648fb1890f2c449ed9e29207af9a0e447IABWuqtt8ulgWKoa+JqNU/o7YR2ZU4pfTLSfTzaAQ/4eyoDs6AYAAAAAIgD3WAEAW2C4YAQAAQDrHLdgAwDrHLdgAgDrHLdgBADrHLdg', 'FZIB9mzbmKcP1ygmJENSiUVjaaURZTf2', 2, '2021-06-02 04:53:47', NULL),
(150, 44, '00648fb1890f2c449ed9e29207af9a0e447IAB5ENJGly7CVas+WkKHsPUWILqz3FpiQJ/t3AemJd3msm4GC+oAAAAAIgCJLAAAY+XGYAQAAQDzocVgAwDzocVgAgDzocVgBADzocVg', '65G3QfEMvGf2SOAGpUi3RcKdEADgrCwt', 2, '2021-06-13 05:13:07', NULL),
(151, 37, '00648fb1890f2c449ed9e29207af9a0e447IADB+NPC9dulAX4r9mYJyB8d3FwZ47wzIS+s9YasMdNdkLWEQsgAAAAAIgDQSQAAeYPfYAQAAQAJQN5gAwAJQN5gAgAJQN5gBAAJQN5g', 'JvZleSajGHj7586tw0FT1L8Lr6UWktLj', 2, '2021-07-01 21:22:01', NULL),
(152, 33, '00648fb1890f2c449ed9e29207af9a0e447IAALCOWSvLV5u8QXUoZ+OdLJH2VQd26y0bVHumCfOZQ72tNev88AAAAAIgBnDgEA0+vvYAQAAQBjqO5gAwBjqO5gAgBjqO5gBABjqO5g', 'KttWAdEv6xFJrYJoJX01rWYVIYLIv2W8', 2, '2021-07-14 08:03:31', NULL),
(153, 33, '00634bc8052f40242feb77f58537d9d9b31IABCgTwiFy7AQRerFqp6RnOJS5CZfObGYaCJTK7n1606o6HQ2sYAAAAAIgAyMwAAexHwYAQAAQALzu5gAwALzu5gAgALzu5gBAALzu5g', 'HMBa0pwBPGLTk2lFkY6N8qjJ5RArLLkY', 2, '2021-07-14 10:44:11', NULL),
(154, 33, '00634bc8052f40242feb77f58537d9d9b31IAB+0WhEuKFvZI02SBb+olIp1N8EFWaDRKx3A/4kXe5cXNChpaUAAAAAIgBYUAEAnCLwYAQAAQAs3+5gAwAs3+5gAgAs3+5gBAAs3+5g', 'OApjBfwWOmUyjGZt8NGcBNMqVaWgyCzc', 2, '2021-07-14 11:57:16', NULL),
(155, 33, '00634bc8052f40242feb77f58537d9d9b31IABhW8EBJW885uQmrzygGGhR7B6Ce525sU29BXIftVMw6fzYnX4AAAAAIgC/LAAAFizwYAQAAQCm6O5gAwCm6O5gAgCm6O5gBACm6O5g', 'wrKABjA5yH0Jrr8v2pSSvbc66r1aRymi', 2, '2021-07-14 12:37:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video_call`
--

CREATE TABLE `video_call` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_from_id` int(11) NOT NULL,
  `user_to_id` int(11) NOT NULL,
  `channel_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_token` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'agora_token',
  `call_type` int(11) NOT NULL DEFAULT '0' COMMENT '0=video_call',
  `call_duration_time` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `call_status` int(11) NOT NULL DEFAULT '0',
  `call_created_date` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_call`
--

INSERT INTO `video_call` (`id`, `user_from_id`, `user_to_id`, `channel_id`, `channel_name`, `channel_token`, `call_type`, `call_duration_time`, `call_status`, `call_created_date`, `created_at`, `updated_at`) VALUES
(1, 33, 32, '1', 'EHYEjccCTG74CCkDRDtPusDqMKEDizo6', '00648fb1890f2c449ed9e29207af9a0e447IACyElksxfSTLgua4dYOx7Dv8jvuAtZbvbF/w2mMQKykWApTMJYAAAAAIgCzdgAALXBsYAQAAQC9LGtgAwC9LGtgAgC9LGtgBAC9LGtg', 2, '0', 2, '2021-04-05 07:58:58', NULL, NULL),
(2, 32, 33, '2', 'wIHT9wHBSBsoM8kvVWUxLIc5AfqoeXyB', '00648fb1890f2c449ed9e29207af9a0e447IABGcgIdOefE5AT/jkbomdUAu+LogYM1PLWrwRhiT60RgJG7PawAAAAAIgCzTgAAFnJsYAQAAQCmLmtgAwCmLmtgAgCmLmtgBACmLmtg', 2, '0', 2, '2021-04-05 08:07:09', NULL, NULL),
(3, 32, 33, '3', 't03PFVOkHRnyPyjFchOSGIjqnvDzibUR', '00648fb1890f2c449ed9e29207af9a0e447IADaTY/1PcaQJNvA610HU+wVGuTmWsgB4KU4K9c0r4zEA4nFaCsAAAAAIgAmzQAABkFtYAQAAQCW/WtgAwCW/WtgAgCW/WtgBACW/Wtg', 2, '0', 2, '2021-04-06 10:50:06', NULL, NULL),
(4, 32, 33, '4', 'VbCSj9G4w3NJgoGNmYkNcbfX48txJuvH', '00648fb1890f2c449ed9e29207af9a0e447IAAZXDtJFBiewUl+ExBMe2O1tICz9XRNiL8bqEHNzf2UTO+MCSwAAAAAIgDaWAEAU2BxYAQAAQDjHHBgAwDjHHBgAgDjHHBgBADjHHBg', 2, '0', 2, '2021-04-09 01:52:39', NULL, NULL),
(5, 32, 33, '5', 'Nbb4jdCo8aBG0EakekGgYOsiJMFGLS81', '00648fb1890f2c449ed9e29207af9a0e447IAAhv1KwCPiiAJe4Ze4rHNgqdpGjD3VVC5vGrkb/aBnrD4jNQYcAAAAAIgAkFwAAfWBxYAQAAQANHXBgAwANHXBgAgANHXBgBAANHXBg', 2, '0', 2, '2021-04-09 01:53:21', NULL, NULL),
(6, 36, 37, '6', 'GGb3gsLuQd5ILqkWPbSLXvZbcQDRuAsi', '00648fb1890f2c449ed9e29207af9a0e447IABhZZscJ1hW0UotjnnaDpQInqlVk4vTGWlry2DdUOm/4Shr3+EAAAAAIgDwmAAAuPJxYAQAAQBIr3BgAwBIr3BgAgBIr3BgBABIr3Bg', 2, '0', 1, '2021-04-09 11:47:20', NULL, NULL),
(7, 37, 36, '7', 'Z6F6P6y3WVkFr5sIMFQmeQx34t8Aq4F0', '00648fb1890f2c449ed9e29207af9a0e447IAClSZZ8sxZ8UBcFpJ7jG+UUeicur4HwGnzvfM8w0GYex/WFb+AAAAAAIgDwUQEA0PJxYAQAAQBgr3BgAwBgr3BgAgBgr3BgBABgr3Bg', 2, '0', 2, '2021-04-09 02:47:45', NULL, NULL),
(8, 36, 37, '8', 'ZXcLqJ6wJeWRoiuJ8njoc6k5eiZct3Bd', '00648fb1890f2c449ed9e29207af9a0e447IAAfEDV7OGAv5zXiMQdVPQzrSlRzQrudLP2D/auuYOv02kyrMwoAAAAAIgBSIwEAM/NxYAQAAQDDr3BgAwDDr3BgAgDDr3BgBADDr3Bg', 2, '0', 2, '2021-04-09 11:49:23', NULL, NULL),
(9, 36, 37, '9', '8nwAiojh5HgJBvgDjpfEeu7C3TQKAHGi', '00648fb1890f2c449ed9e29207af9a0e447IADAfvG8/PeuG0ThHNwpkWEgPg7bNSIprPokCvcEEjwHt/3DiDsAAAAAIgCNBwAATPNxYAQAAQDcr3BgAwDcr3BgAgDcr3BgBADcr3Bg', 2, '0', 2, '2021-04-09 11:49:48', NULL, NULL),
(10, 37, 36, '10', 'O0jICxDA3veabKzoyihy5zgyv2Is7714', '00648fb1890f2c449ed9e29207af9a0e447IABDPhdG+jS6GfRYI0J1SZnzFOLO2FtUMzmzN0f91GZAn7OzUkkAAAAAIgAaBQAAW/NxYAQAAQDrr3BgAwDrr3BgAgDrr3BgBADrr3Bg', 2, '0', 2, '2021-04-09 02:50:04', NULL, NULL),
(11, 37, 36, '11', 'hcTCOkl0F8jU5EGHVFlvyzJSKPY5NW2M', '00648fb1890f2c449ed9e29207af9a0e447IABDvoNsvERO8q8ftUqV63t+oHEhD8ngz17p0goL6FsOp55kHkkAAAAAIgCwewAAcvNxYAQAAQACsHBgAwACsHBgAgACsHBgBAACsHBg', 2, '0', 2, '2021-04-09 02:50:27', NULL, NULL),
(12, 33, 32, '12', 'ZAz4TFzkio5x1rKs4CSOoDcurLndGp74', '00648fb1890f2c449ed9e29207af9a0e447IADcL0BNod7ydXUHvD3bvo+/biWl6jGwcGvxEfZRWo/HYjs40pUAAAAAIgCZ3gAAaJp2YAQAAQD4VnVgAwD4VnVgAgD4VnVgBAD4VnVg', 2, '0', 2, '2021-04-13 01:01:49', NULL, NULL),
(13, 39, 32, '13', 'NGwBB3ORiJ9cgrVwPeb1IyPLHYs3pHt8', '00648fb1890f2c449ed9e29207af9a0e447IADWR/4ncWUUn7dwH+oZVLRTZwHtaUqay8IjYDmMyygWdQQicHYAAAAAIgDGIQAA3p12YAQAAQBuWnVgAwBuWnVgAgBuWnVgBABuWnVg', 2, '0', 2, '2021-04-13 01:16:35', NULL, NULL),
(14, 39, 32, '14', '6vT4p3j2xxuPMSkYORg779JEqR3bc8I0', '00648fb1890f2c449ed9e29207af9a0e447IABasFbzBXoxI7iqrwNoHgvzB0jola7KzhEJrCI1ht04HrMVzOIAAAAAIgAfIQAACp52YAQAAQCaWnVgAwCaWnVgAgCaWnVgBACaWnVg', 2, '0', 2, '2021-04-13 01:17:19', NULL, NULL),
(15, 39, 32, '15', 'cTy1E2hQwxVmVNgVcEp1jsfIQDGTj61s', '00648fb1890f2c449ed9e29207af9a0e447IADS68Ymc1P+zs2XnR0hw4uGRHzBdw5jBMtuIi+20DF3JOuHSYAAAAAAIgCUIwEApJ52YAQAAQA0W3VgAwA0W3VgAgA0W3VgBAA0W3Vg', 2, '0', 2, '2021-04-13 01:19:52', NULL, NULL),
(16, 39, 32, '16', '7noeUde51eJvwJli2D9QzpYFsmkRncLA', '00648fb1890f2c449ed9e29207af9a0e447IADP30biDSAt36qSep9Jhm/WvIkRyTvE37B4q59FkGLHkCzjofIAAAAAIgArIgAAbJ92YAQAAQD8W3VgAwD8W3VgAgD8W3VgBAD8W3Vg', 2, '0', 2, '2021-04-13 01:23:12', NULL, NULL),
(17, 40, 32, '17', 'y7YSVILbHul0BczcOPlAoquid3X9xzJ6', '00648fb1890f2c449ed9e29207af9a0e447IADu/ad1r1GZw8L4ZU1O4RG+KTXboI0OYkuXicq5VeERkyTRR/wAAAAAIgCLNAEAzKZ2YAQAAQBcY3VgAwBcY3VgAgBcY3VgBABcY3Vg', 2, '0', 2, '2021-04-13 01:54:41', NULL, NULL),
(18, 32, 33, '18', 'O0SPjIx0OkmzrjwRZjDC22Y97gJnP326', '00648fb1890f2c449ed9e29207af9a0e447IAD7X09fpYOMpj8wNQ9Yd7izMoNwG2l2H8AqwMIJXTgRr76ck8UAAAAAIgCL0QAAhzh4YAQAAQAX9XZgAwAX9XZgAgAX9XZgBAAX9XZg', 2, '0', 2, '2021-04-14 06:28:48', NULL, NULL),
(19, 32, 33, '19', 'tdNNxmJZs5pAA4z8MTtalT7FNZs8Zf37', '00648fb1890f2c449ed9e29207af9a0e447IAAstt7v5tAq4dM1q/4Ic4ua7sbE2ZguiRbYkJMDqMORKu+bRQEAAAAAIgAECwEAXTl4YAQAAQDt9XZgAwDt9XZgAgDt9XZgBADt9XZg', 2, '0', 2, '2021-04-14 06:32:22', NULL, NULL),
(20, 32, 33, '20', 'WjkB6vfPgNzijnoEUBCYVNWG4kevfmXU', '00648fb1890f2c449ed9e29207af9a0e447IAAl5vCAz90sh5aKPvgYQcdUbPjyZHZD4GpYEMkJclxjJctPAQIAAAAAIgATKgAAwj95YAQAAQBS/HdgAwBS/HdgAgBS/HdgBABS/Hdg', 2, '0', 2, '2021-04-15 01:12:00', NULL, NULL),
(21, 32, 35, '21', 'PuQPyeNVaTJhb68sFaE99LQ7NP6XbdDx', '00648fb1890f2c449ed9e29207af9a0e447IADphU7c+EMMvJaoPCwXKlMWSSp20O0J6LRpBo7s5iAc+1ExaX4AAAAAIgAqDQEAnbh+YAQAAQAtdX1gAwAtdX1gAgAtdX1gBAAtdX1g', 2, '0', 0, '2021-04-19 04:48:50', NULL, NULL),
(22, 32, 33, '22', 'RPUoYkppNaXuoxFnxKf3fXPi4PMqCJlx', '00648fb1890f2c449ed9e29207af9a0e447IAC9NTsAQqAZu8wG2uSp2dEDjf7885EckzsTGDHdpQtFXE6481kAAAAAIgAP6wAAftSDYAQAAQAOkYJgAwAOkYJgAgAOkYJgBAAOkYJg', 2, '0', 2, '2021-04-23 01:49:11', NULL, NULL),
(23, 33, 32, '23', '0zTc05tWaDYUS1bHmo4yMs9u1sUXSJNE', '00648fb1890f2c449ed9e29207af9a0e447IAB/5SunDK3tPuB8twDr1s4yEMlBAcyPpvwWCXvIb3NRfRX47igAAAAAIgA4FgAAsdWDYAQAAQBBkoJgAwBBkoJgAgBBkoJgBABBkoJg', 2, '0', 2, '2021-04-23 01:54:18', NULL, NULL),
(24, 33, 32, '24', 'W4nmYK4JV7lEzDjKY5uKylmOwqW7QerQ', '00648fb1890f2c449ed9e29207af9a0e447IABvPF+N6em1Y1ui0nJv7st5snIXAZ2ylAkbHeQnz6olsNwZ5mEAAAAAIgB+nQAA4dWDYAQAAQBxkoJgAwBxkoJgAgBxkoJgBABxkoJg', 2, '0', 2, '2021-04-23 01:55:05', NULL, NULL),
(25, 32, 33, '25', 'OqaI6qAnYPrYOzgPJYJ0hMqWwGPMHif5', '00648fb1890f2c449ed9e29207af9a0e447IAD95b6p8zufiGlVtpnZtAajdOjE7B6/Tyl4RNM673vHSfnQtXsAAAAAIgD4LAAAHtaDYAQAAQCukoJgAwCukoJgAgCukoJgBACukoJg', 2, '0', 2, '2021-04-23 01:56:06', NULL, NULL),
(26, 32, 33, '26', 'eplTJM0jflQR7WZvasXI2p4DO47Kpydg', '00648fb1890f2c449ed9e29207af9a0e447IAA14ZLjQv3wx0UoMH5ZIKGiFg6vkhLyWSutqgyc9D8y1Cg2LwEAAAAAIgBuJwAA7daDYAQAAQB9k4JgAwB9k4JgAgB9k4JgBAB9k4Jg', 2, '0', 2, '2021-04-23 01:59:34', NULL, NULL),
(27, 32, 33, '27', 'QGHaRDBR4xJA6EYv1utjxT5dC9WnwGum', '00648fb1890f2c449ed9e29207af9a0e447IAA37CBXOeCM8a3XessY67ag6aDZfpalZ1UqydB5uZZts5cebq0AAAAAIgBuQAAAXeKDYAQAAQDtnoJgAwDtnoJgAgDtnoJgBADtnoJg', 2, '0', 2, '2021-04-23 02:48:22', NULL, NULL),
(28, 32, 33, '28', 'v7KSi4hVcScanfAF7kWRFlsrYGXrmQla', '00648fb1890f2c449ed9e29207af9a0e447IACSznWOCLUBQGyY2pCfhBeDLQFYh1YRng5dto5VSoZ9QrAhf6gAAAAAIgC1HQAA/eiDYAQAAQCNpYJgAwCNpYJgAgCNpYJgBACNpYJg', 2, '0', 2, '2021-04-23 03:16:38', NULL, NULL),
(29, 32, 33, '29', '90rgP5iuFp3RqPyqkVqRpgJ7G3ykcJN7', '00648fb1890f2c449ed9e29207af9a0e447IAAnIkt8PJv/+kDIGrIVc4J5Bb1DL/CCs0RuFHFzSiUZ0b7fq0kAAAAAIgA6igAApumDYAQAAQA2poJgAwA2poJgAgA2poJgBAA2poJg', 2, '0', 2, '2021-04-23 03:19:26', NULL, NULL),
(30, 37, 44, '30', '66SW06C0VjJYpbc5ZCW8nR5SveZHgiP0', '00648fb1890f2c449ed9e29207af9a0e447IADrKwG8wHSi5be3Y4QsTyD2UiWcKZ1kRewlrG5VoRB0+SzqyKcAAAAAIgBDrQAAY4SEYAQAAQDzQINgAwDzQINgAgDzQINgBADzQINg', 2, '0', 1, '2021-04-23 04:49:40', NULL, NULL),
(31, 44, 37, '31', 'tN2AOdNUl1Uh6mKlVhDfSX4lAlm7LXQl', '00648fb1890f2c449ed9e29207af9a0e447IABiFHBp/HdvmJ56rf+3tTNgNGO+VG0yZNi1R9tfRP8VwHuI9m4AAAAAIgD5fwAAcYSEYAQAAQABQYNgAwABQYNgAgABQYNgBAABQYNg', 2, '0', 2, '2021-04-23 01:49:53', NULL, NULL),
(32, 44, 37, '33', '3kbwF9DyDtCv25z3L5rPMAXvTWBYvDkb', '00648fb1890f2c449ed9e29207af9a0e447IADyEpS7+toGQ1ry2lFd9Um5uiao4Yt4Bg53tOAQ/tT10llbiIoAAAAAIgAlUQAAvoSEYAQAAQBOQYNgAwBOQYNgAgBOQYNgBABOQYNg', 2, '0', 2, '2021-04-23 01:51:10', NULL, NULL),
(33, 44, 37, '35', 'fhy4MsJo0MQiZXxocSK8tHDDrpBpU6ur', '00648fb1890f2c449ed9e29207af9a0e447IADagILe+sVvM/8jN0YNn72iLQUryHeccTip/pLCruj4Y3/Y3RUAAAAAIgAg/gAA94SEYAQAAQCHQYNgAwCHQYNgAgCHQYNgBACHQYNg', 2, '0', 2, '2021-04-23 01:52:07', NULL, NULL),
(34, 37, 44, '36', 'QSB4bNjI7n9kC3YBlHrwuHqvoXAfVwde', '00648fb1890f2c449ed9e29207af9a0e447IAA93i4EpDtBZywztEQwxpIAP+XG4/kKlsQfu5sdiAjCg1M0jvwAAAAAIgAbCQAAHoWEYAQAAQCuQYNgAwCuQYNgAgCuQYNgBACuQYNg', 2, '0', 2, '2021-04-23 04:52:47', NULL, NULL),
(35, 34, 45, '37', 'Q767WAUU62MPoYhOa81mzkqixdeUCNKV', '00648fb1890f2c449ed9e29207af9a0e447IABvTybsvhFCFYMxstqj+Tf9pDcRGuGxxXPLd2xN6Qs4/CW8Ms4AAAAAIgB3hAEAIvaEYAQAAQCysoNgAwCysoNgAgCysoNgBACysoNg', 2, '0', 2, '2021-04-24 10:24:53', NULL, NULL),
(36, 33, 32, '38', 'ht6g23k6vM3YNrvnDDHvI0HX1fj2YRCT', '00648fb1890f2c449ed9e29207af9a0e447IAAwGLqiIPNwrT9MwBQ1Q0zV/m1XhlZhU75A7uMOcDgtMn1j4N0AAAAAIgAQ/AAAqMmMYAQAAQA4hotgAwA4hotgAgA4hotgBAA4hotg', 2, '0', 2, '2021-04-30 08:53:21', NULL, NULL),
(37, 32, 33, '39', 'fcAgZR8WVXMgWgs4K1hK1jwRipHUufWU', '00648fb1890f2c449ed9e29207af9a0e447IAAM3eY4XU7O2Go69yueoIWrU114AxzUpWkCQI26fQKzx6UswYoAAAAAIgCjSgAA1cmMYAQAAQBlhotgAwBlhotgAgBlhotgBABlhotg', 2, '0', 2, '2021-04-30 08:54:02', NULL, NULL),
(38, 33, 32, '40', 'jLpGwvBHlRQ31CFTkHXoMrcqtvRxCZ5a', '00648fb1890f2c449ed9e29207af9a0e447IADfoywuUuA6MgQy2gmt4vQqe5B1fF7z9+4XMy9cLPqNvxAdS0kAAAAAIgAnZAEABMqMYAQAAQCUhotgAwCUhotgAgCUhotgBACUhotg', 2, '0', 2, '2021-04-30 08:54:52', NULL, NULL),
(39, 34, 32, '41', '9FRlxx7N2V2IBgJB5sXItAVMqpAiw2wa', '00648fb1890f2c449ed9e29207af9a0e447IACojTvQnUn5xGiZgRCfSRUmIvmRxU2NGN63u4Ht+YCYTeTeocMAAAAAIgCUqgAAOh2RYAQAAQDK2Y9gAwDK2Y9gAgDK2Y9gBADK2Y9g', 2, '0', 2, '2021-05-03 03:38:59', NULL, NULL),
(40, 37, 44, '42', 'NADJdtku4GNsw0d3enoYMdjNBbHMJJVo', '00648fb1890f2c449ed9e29207af9a0e447IABx1AnS1LOU8Czwboiag4VRVMFw5MPgiAOhIyyvw6Vd90n0mlMAAAAAIgBaBgEAxnmVYAQAAQBWNpRgAwBWNpRgAgBWNpRgBABWNpRg', 2, '0', 2, '2021-05-06 01:32:54', NULL, NULL),
(41, 37, 44, '43', 'lnHSsghu8DW4F2SKKC7zMDQnlMfCChck', '00648fb1890f2c449ed9e29207af9a0e447IACbbCaMIlICh39ez/0ocaHLV0wOi+RtsYRuk5pDlfz/diEg8F4AAAAAIgDXRAEADHqVYAQAAQCcNpRgAwCcNpRgAgCcNpRgBACcNpRg', 2, '0', 2, '2021-05-06 01:34:04', NULL, NULL),
(42, 32, 33, '44', 'nWKaLEDIfZtoYuL1T4b90y9JKYQKb0s6', '00648fb1890f2c449ed9e29207af9a0e447IACdxjq+E77dWAjtQbhwsBnqQTK/sxCba3DCOWdpTIL9jiymN0IAAAAAIgDTaAEAmj2WYAQAAQAq+pRgAwAq+pRgAgAq+pRgBAAq+pRg', 2, '0', 2, '2021-05-07 12:58:28', NULL, NULL),
(43, 33, 32, '45', 'Y15kr1wyzOVcja9WRUaNLouCiC8BuIYV', '00648fb1890f2c449ed9e29207af9a0e447IABCZ2Ts6LtdzQjd/0eHHH026+8Yui+fgoUz1bc3LUQOlPTlTLMAAAAAIgCKAAAAHGGWYAQAAQCsHZVgAwCsHZVgAgCsHZVgBACsHZVg', 2, '0', 2, '2021-05-07 03:29:58', NULL, NULL),
(44, 33, 32, '46', 'IRmklCr37c79273xiTAkSWETPj8275X8', '00648fb1890f2c449ed9e29207af9a0e447IAAcPjnVUBOvJTF2mWn//v2rjw/omCpsVpRCDSfflRUUy8l3Vj4AAAAAIgAdRAEAP2GWYAQAAQDPHZVgAwDPHZVgAgDPHZVgBADPHZVg', 2, '0', 2, '2021-05-07 03:30:32', NULL, NULL),
(45, 33, 32, '47', 'R0RGWNQptMukqJJLnih1qC7nlPSaQeMQ', '00648fb1890f2c449ed9e29207af9a0e447IABEEm1TcY2xaZ85uAoSWr1VIQaUn2rq4K/n++vefIDVAtKKtT8AAAAAIgAK+QAAfGKWYAQAAQAMH5VgAwAMH5VgAgAMH5VgBAAMH5Vg', 2, '0', 5, '2021-05-07 03:35:50', NULL, NULL),
(46, 33, 32, '48', 'fGNwgEGYWpYqxY8nb3atbdhMLkvWPBkp', '00648fb1890f2c449ed9e29207af9a0e447IABQzfHOqUqq9keWnj58HhCcFG6HsWmGBIENYqit5sjubhU3mF4AAAAAIgDLHQEA3maWYAQAAQBuI5VgAwBuI5VgAgBuI5VgBABuI5Vg', 2, '0', 5, '2021-05-07 03:54:31', NULL, NULL),
(47, 33, 32, '49', 'ZXMrWk78z0tlB3n84mGWdDl0cyQooxy2', '00648fb1890f2c449ed9e29207af9a0e447IAA4NXySq9R9/jocKTKYxydkiAeohDLNtlJENDlRdwFvngf5qoYAAAAAIgBrngAAXmiWYAQAAQDuJJVgAwDuJJVgAgDuJJVgBADuJJVg', 2, '0', 5, '2021-05-07 04:00:55', NULL, NULL),
(48, 33, 32, '50', 'vZ669ybXyjlGtO3peLlcreE2JRrGubTA', '00648fb1890f2c449ed9e29207af9a0e447IAAVSkuzFJPLeO4+JVcYO4F9vehRzxrHm7p+vPG525evt9G+/noAAAAAIgC3TgAAj3CWYAQAAQAfLZVgAwAfLZVgAgAfLZVgBAAfLZVg', 2, '0', 5, '2021-05-07 04:35:53', NULL, NULL),
(49, 33, 32, '51', 'frAodIdXnEyxyqIP6U9Lfd70xpxMQSGU', '00648fb1890f2c449ed9e29207af9a0e447IADiicsrDKb0k71BpvPodGKcg3+lbVjCpkLQvS0/yjY/YsETau4AAAAAIgDYzQAAh3GWYAQAAQAXLpVgAwAXLpVgAgAXLpVgBAAXLpVg', 2, '0', 5, '2021-05-07 04:40:00', NULL, NULL),
(50, 33, 32, '52', 'qCKTNZ4djxfJHLj7KxRJu7Ao3GpScgPY', '00648fb1890f2c449ed9e29207af9a0e447IACmltw0r2DXY6TF0WxbQiNagVz3Wh1/KXtQKVbeswHuRJkTpaQAAAAAIgDMGAEAw3KWYAQAAQBTL5VgAwBTL5VgAgBTL5VgBABTL5Vg', 2, '0', 2, '2021-05-07 04:45:18', NULL, NULL),
(51, 33, 32, '53', 'gbdbsUc1JqPvOHZPjLCkPohQBBFjwCDl', '00648fb1890f2c449ed9e29207af9a0e447IACLWd53gNVqrcivEPaiL5GzsYMF/tz8rocG6dJ4IXoMNBPGQzQAAAAAIgCU/AAALnOWYAQAAQC+L5VgAwC+L5VgAgC+L5VgBAC+L5Vg', 2, '0', 2, '2021-05-07 04:47:03', NULL, NULL),
(52, 33, 32, '54', 'aInBVx5mvSJUdHnJXondtG37nUcyUdEJ', '00648fb1890f2c449ed9e29207af9a0e447IAC9XhJwBtv1awJuXxccHKlGIEzmhOi8Cl9azf4q0B2oA9MljaQAAAAAIgAD8wAAeHOWYAQAAQAIMJVgAwAIMJVgAgAIMJVgBAAIMJVg', 2, '0', 2, '2021-05-07 04:48:17', NULL, NULL),
(53, 37, 44, '55', 'a4NYI315ZFoPjcYn11TwVaafmvmsnAxv', '00648fb1890f2c449ed9e29207af9a0e447IACnrKzadBrVEoXj0dXYAxrohR2dRngBkdhLUotTr3IeMtxtD2oAAAAAIgDDMwEAeNKWYAQAAQAIj5VgAwAIj5VgAgAIj5VgBAAIj5Vg', 2, '0', 2, '2021-05-07 02:03:37', NULL, NULL),
(54, 37, 44, '56', 'zQMKTzZeceFxBLy0RbJmu0SxODD2uvhi', '00648fb1890f2c449ed9e29207af9a0e447IAAtoTG41Txj/28i4ceJxElrAWL1HDdevgRX4P5RFRumErVqXyAAAAAAIgAkUQEAjdKWYAQAAQAdj5VgAwAdj5VgAgAdj5VgBAAdj5Vg', 2, '0', 2, '2021-05-07 02:03:58', NULL, NULL),
(55, 44, 37, '57', 'H2noecfpjDNnzICNoy5PXIUj5vL6B7Ht', '00648fb1890f2c449ed9e29207af9a0e447IABYIFaM/3XCC8PdoXp7+bDWh6Ly9J0FDwLc2JhKzNJUrLb9m7IAAAAAIgDKdwAAC1KXYAQAAQCbDpZgAwCbDpZgAgCbDpZgBACbDpZg', 2, '0', 2, '2021-05-07 08:07:55', NULL, NULL),
(56, 44, 37, '58', 'EzCJR2rE4mmrg1s7po9sMfqMluBhAWeK', '00648fb1890f2c449ed9e29207af9a0e447IADsF0OfgU4+3kY7p79heL7tF9IgkML+ew0P+PamDZIwDe/Lk+0AAAAAIgDlHQEAJlKXYAQAAQC2DpZgAwC2DpZgAgC2DpZgBAC2DpZg', 2, '0', 5, '2021-05-07 08:08:22', NULL, NULL),
(57, 37, 44, '59', 'UmPO2VK3EvAoEBeccSIXStVZXJTlPgUb', '00648fb1890f2c449ed9e29207af9a0e447IADy/WPN8SBQSpCapp4uql+dpAb45tglGzEv+lutdlpy9tr6t9AAAAAAIgDdAwAArFKXYAQAAQA8D5ZgAwA8D5ZgAgA8D5ZgBAA8D5Zg', 2, '0', 2, '2021-05-07 11:10:37', NULL, NULL),
(58, 34, 43, '60', 'glWlx88kSlSN85L7HyHodWpnojCFAiik', '00648fb1890f2c449ed9e29207af9a0e447IAAY5f9ePHi+jwtXGi3Mui7L5gjIxPACilB0bgZp7pG6avp6W5UAAAAAIgBbLQEAyYiXYAQAAQBZRZZgAwBZRZZgAgBZRZZgBABZRZZg', 2, '0', 0, '2021-05-08 12:31:27', NULL, NULL),
(59, 34, 43, '61', 'n9vEH40WBfhJtP47TX9jIMsemyG9yc8M', '00648fb1890f2c449ed9e29207af9a0e447IAC/j9ZSJLAmYhAQUK4eCyU5ItZsDiif74q0MQgrUWZ27T2Tw7gAAAAAIgDy3QAA8oiXYAQAAQCCRZZgAwCCRZZgAgCCRZZgBACCRZZg', 2, '0', 2, '2021-05-08 12:32:09', NULL, NULL),
(60, 32, 33, '62', 'CUF1qMqL5kjywHoDOTfEKEPFsA4BTVDG', '00648fb1890f2c449ed9e29207af9a0e447IADHbfd9JAngzLfHLT+icUZD3sFreYQbS+Rq06w8meDz5nVG9jIAAAAAIgB3DgEA8m6bYAQAAQCCK5pgAwCCK5pgAgCCK5pgBACCK5pg', 2, '0', 0, '2021-05-11 11:30:16', NULL, NULL),
(61, 32, 33, '63', '5NYMhuCgN12EikM3ZS2YbNI4to6ysY8R', '00648fb1890f2c449ed9e29207af9a0e447IABFnY657/S3/r3d9IUCpz80tCyDb6Fv4VtQ+rqWbII233sYUNgAAAAAIgAMzQAA3XGbYAQAAQBtLppgAwBtLppgAgBtLppgBABtLppg', 2, '0', 2, '2021-05-11 11:42:44', NULL, NULL),
(62, 33, 32, '64', 'cxGOXz4w2NcDpH0ZfH4tDh1eSAagYz8u', '00648fb1890f2c449ed9e29207af9a0e447IADcBdHPvy9xd1iuVt9Mkq45fTkSrjzEgZEiShe6UgnCrFiuZnQAAAAAIgB0RAAAMfKcYAQAAQDBrptgAwDBrptgAgDBrptgBADBrptg', 2, '0', 2, '2021-05-12 03:02:31', NULL, NULL),
(63, 32, 33, '65', '1xDixtoepIrj3jxxqFM9dU7dFcZVbbfk', '00648fb1890f2c449ed9e29207af9a0e447IAC+aQLz+U0MktqCPLaD6E1AKrnDVF1GUUHkHgzKfuuL40Rpyl4AAAAAIgCJAQEATwadYAQAAQDfwptgAwDfwptgAgDfwptgBADfwptg', 2, '0', 2, '2021-05-12 04:28:21', NULL, NULL),
(64, 32, 33, '66', 'QCS1FcymJlPD2m4gilMW2wfbVtiNMGMl', '00648fb1890f2c449ed9e29207af9a0e447IACVla/qbiQa3H6iTEVTqExXbfSFJrJzZGK438MI1u6Bw3c53zoAAAAAIgC6OAAAnQedYAQAAQAtxJtgAwAtxJtgAgAtxJtgBAAtxJtg', 2, '0', 2, '2021-05-12 04:33:56', NULL, NULL),
(65, 33, 32, '67', '94ltAcPQp7p871PO5LNBW16M8ldbtmZs', '00648fb1890f2c449ed9e29207af9a0e447IACdkGB0/SBF1TS6u+oEFnVl3RSZpYpaig8oMPODgeohCeIIW58AAAAAIgAYAQAAyAedYAQAAQBYxJtgAwBYxJtgAgBYxJtgBABYxJtg', 2, '0', 5, '2021-05-12 04:34:41', NULL, NULL),
(66, 33, 32, '68', 'IDWw2EXTdF2hzZGwFrfCdnQwm97U2d9v', '00648fb1890f2c449ed9e29207af9a0e447IACZSHByMUO2J7z4zWEwFxbBNkfJoDYneGakL8wl2zQOS8cH/yYAAAAAIgBlPgEA/wedYAQAAQCPxJtgAwCPxJtgAgCPxJtgBACPxJtg', 2, '0', 2, '2021-05-12 04:35:35', NULL, NULL),
(67, 33, 32, '69', 'K0miSEuhUmbzHsHuZ2EXv5bk8E2e0Rpa', '00648fb1890f2c449ed9e29207af9a0e447IADuWImTStXqJJLmOVWPiSGY7mngKN1DoN28T/XNM8tUxZ2HIn8AAAAAIgCLfwAA2wGeYAQAAQBrvpxgAwBrvpxgAgBrvpxgBABrvpxg', 2, '0', 0, '2021-05-13 10:21:38', NULL, NULL),
(68, 33, 32, '70', 'zXFNqyD8uS2WblPMOIL0PRkIxROVgbDC', '00648fb1890f2c449ed9e29207af9a0e447IAArvjapMKgxTG/gGGH53Z5SHMfKlTZfslKraavQM6qqyDXoDisAAAAAIgB9PQAA+QGeYAQAAQCJvpxgAwCJvpxgAgCJvpxgBACJvpxg', 2, '0', 2, '2021-05-13 10:22:09', NULL, NULL),
(69, 44, 37, '71', 'nXoodn9VhxT31AeuUL4dRx3UB99hvfB3', '00648fb1890f2c449ed9e29207af9a0e447IAA8qEKPTBpnRY+VBZR/PqY3qdieKJAYv2tsiVuH3Jqj9wrfgw8AAAAAIgABXAEA/CygYAQAAQCM6Z5gAwCM6Z5gAgCM6Z5gBACM6Z5g', 2, '0', 2, '2021-05-14 01:20:12', NULL, NULL),
(70, 44, 37, '72', '1zfHLssjNaT1Ki9iNpE1GtUEFmMZPXHq', '00648fb1890f2c449ed9e29207af9a0e447IAALXK2aZu98nvs4HwFpRJPIQGC+cIqtm3Lm6qEUZYX1Cwwq6/EAAAAAIgCbuQAAFy2gYAQAAQCn6Z5gAwCn6Z5gAgCn6Z5gBACn6Z5g', 2, '0', 2, '2021-05-14 01:20:39', NULL, NULL),
(71, 37, 44, '73', 'KpYxXEgWwR7ckhA2YlwlF1FdO8kmJ8qm', '00648fb1890f2c449ed9e29207af9a0e447IACJTZ9lGw687iogSTHI/2/O8MBN3pYLSV9ZHtz0uuZUfeMt6dYAAAAAIgC0WgAAUC2gYAQAAQDg6Z5gAwDg6Z5gAgDg6Z5gBADg6Z5g', 2, '0', 2, '2021-05-14 04:21:36', NULL, NULL),
(72, 44, 37, '74', 'ymGc4zzEjBsuQM6GTolSKIDKJMuxCQBM', '00648fb1890f2c449ed9e29207af9a0e447IACjX4abAKNBtqa9gfr1AE8D/poj6x2n8lfE9XpPc/2vnr5HlxMAAAAAIgBxPQEAai2gYAQAAQD66Z5gAwD66Z5gAgD66Z5gBAD66Z5g', 2, '0', 2, '2021-05-14 01:22:03', NULL, NULL),
(73, 33, 32, '75', 'qKDMJhCIuALkEG9Re0dTvtKSnsp2swPC', '00648fb1890f2c449ed9e29207af9a0e447IABKDY8rqhdlSeu5rmc5vY2qcODmK0aYMQMKJ5vFYN3qFlVC024AAAAAIgBsagAA6kSjYAQAAQB6AaJgAwB6AaJgAgB6AaJgBAB6AaJg', 2, '0', 2, '2021-05-17 10:09:07', NULL, NULL),
(74, 33, 32, '76', '4xSpWFQpQPpGyXA0ksbt2KDSD6eRiWIM', '00648fb1890f2c449ed9e29207af9a0e447IACPjg9PAYP7L2tV+BKQrivV6pfyfeP4PnCxACplQ3CHb0lGfgwAAAAAIgD6vwAAdlqjYAQAAQAGF6JgAwAGF6JgAgAGF6JgBAAGF6Jg', 2, '0', 2, '2021-05-17 11:41:03', NULL, NULL),
(75, 33, 32, '77', '9xZ0YDbmLafkFEYrn8hmMeUrICr3lGxY', '00648fb1890f2c449ed9e29207af9a0e447IACRWJgfm5piFYyarT5s9PcBw82wwWnAbHMQZL0ZU4CKqxhnxrkAAAAAIgBgewAALrijYAQAAQC+dKJgAwC+dKJgAgC+dKJgBAC+dKJg', 2, '0', 2, '2021-05-17 06:20:54', NULL, NULL),
(76, 32, 33, '78', '3Yr6dHNwh1twzzNxCscbJbybSbBY1QUm', '00648fb1890f2c449ed9e29207af9a0e447IADnXycD+QSVkLScY2ZwHaJXTFyMBBy20i5kO7u5hxCGj6TasMwAAAAAIgChAAAAVbijYAQAAQDldKJgAwDldKJgAgDldKJgBADldKJg', 2, '0', 2, '2021-05-17 06:21:34', NULL, NULL),
(77, 33, 32, '79', 'WKG8vqiBUUM1UaYO886A6tq3qZgPKzvS', '00648fb1890f2c449ed9e29207af9a0e447IADaJkzkQPqcb/KXQRArW+S7msJxA03mhj36PxLyw/csfCttI/UAAAAAIgDy4QAAepukYAQAAQAKWKNgAwAKWKNgAgAKWKNgBAAKWKNg', 2, '0', 2, '2021-05-18 10:30:43', NULL, NULL),
(78, 33, 32, '80', 'vGiehphCbK3tFnGy0ZTNeJIYwPg0Hmzx', '00648fb1890f2c449ed9e29207af9a0e447IAAgBXeEx2iufrttG20SlrR/eewKIwUE2m38yWEiOfRsjsCc13cAAAAAIgCYowAAkZukYAQAAQAhWKNgAwAhWKNgAgAhWKNgBAAhWKNg', 2, '0', 2, '2021-05-18 10:31:06', NULL, NULL),
(79, 44, 37, '81', 'Wwn8QhSRVB8KmOz6duMmGBi9L1bsBYpU', '00648fb1890f2c449ed9e29207af9a0e447IAAog45Z+6vtaQEmruRWzwFQoQC/ygWOYv7zxeBZhh9NkYjrTvsAAAAAIgANfgAAULGlYAQAAQDgbaRgAwDgbaRgAgDgbaRgBADgbaRg', 2, '0', 2, '2021-05-18 05:46:08', NULL, NULL),
(80, 44, 37, '82', '1CihliP2w288RAWo5am8eLiKlG3iA63h', '00648fb1890f2c449ed9e29207af9a0e447IACaun8RUv7vwi3fX55gwomhZrQ8vwragu8sWdjFb0LDfLkadScAAAAAIgDHgwAAZbGlYAQAAQD1baRgAwD1baRgAgD1baRgBAD1baRg', 2, '0', 2, '2021-05-18 05:46:30', NULL, NULL),
(81, 37, 44, '83', 'B3OtTS1j4yBo12TAr2uI2a4YQtVnL0Nk', '00648fb1890f2c449ed9e29207af9a0e447IADMwaaoZcCmmYc6GrGuaMZ1FKLxBGK3fh5fB1NX2KWHlmVayqcAAAAAIgBCLgEAh7GlYAQAAQAXbqRgAwAXbqRgAgAXbqRgBAAXbqRg', 2, '0', 2, '2021-05-18 08:47:02', NULL, NULL),
(82, 32, 33, '84', 'FdzxSU3wsL6PGIQDOy3znifhJi9lgvvl', '00648fb1890f2c449ed9e29207af9a0e447IABjRLpH3rDV7BcGLQTx8hj12DpZ/3E2PjjzkHC3EoO/7YiYY9oAAAAAIgAriAAAKOWlYAQAAQC4oaRgAwC4oaRgAgC4oaRgBAC4oaRg', 2, '0', 2, '2021-05-19 09:57:20', NULL, NULL),
(83, 32, 33, '85', 'G0gdNhxGkoYxJBn6LuOZ9RwoHPRobeEP', '00648fb1890f2c449ed9e29207af9a0e447IAC3ZYm4zjHt1rq1Glrv46U6ugRlnvFkalp9HtJVS/q7hvMjtjoAAAAAIgDIJAAAQOWlYAQAAQDQoaRgAwDQoaRgAgDQoaRgBADQoaRg', 2, '0', 2, '2021-05-19 09:57:44', NULL, NULL),
(84, 32, 33, '86', 'ipycJfHXjvxyLBYi1pE5AhYlw77WlVX6', '00648fb1890f2c449ed9e29207af9a0e447IAAkJixX4AW2SuW7Mgs+soy1A/eIAAbjjTBmyyvy+urAWtZw7SgAAAAAIgDeQQEAZeWlYAQAAQD1oaRgAwD1oaRgAgD1oaRgBAD1oaRg', 2, '0', 2, '2021-05-19 09:58:21', NULL, NULL),
(85, 32, 33, '87', 'G6VuZp9zNb8mZcAIfAnbvyBhNq7IiDgn', '00648fb1890f2c449ed9e29207af9a0e447IADoYZjkyv+pvPqnk0B08gxGjGn2jmXs4fakx99yz03iFOvDxf4AAAAAIgBsiQAAleWlYAQAAQAloqRgAwAloqRgAgAloqRgBAAloqRg', 2, '0', 2, '2021-05-19 09:59:09', NULL, NULL),
(86, 36, 37, '88', 'QJyAIvk7XeQ3OFK7k2RSnYNN0J6TkTmt', '00648fb1890f2c449ed9e29207af9a0e447IAB9CMb/Q5EP5OLIz6uRX4KzdWrpRCikulMRTFZnkvuTCVEWWXIAAAAAIgBShwAAZOilYAQAAQD0pKRgAwD0pKRgAgD0pKRgBAD0pKRg', 2, '0', 2, '2021-05-19 10:11:08', NULL, NULL),
(87, 36, 37, '89', '9YqqSpSTA5dZfAzLtZs6oOCea5ms9HqH', '00648fb1890f2c449ed9e29207af9a0e447IADRLMMqBBBy4VR4pn8U6QZ/A3/LonpdSiMnpAtsym5qkoGfqXcAAAAAIgDxlwAAiOilYAQAAQAYpaRgAwAYpaRgAgAYpaRgBAAYpaRg', 2, '0', 2, '2021-05-19 10:11:44', NULL, NULL),
(88, 36, 37, '90', 'jD3yt8IYLZBJsXmWve236mSsKLGAaDy8', '00648fb1890f2c449ed9e29207af9a0e447IAALj3TgtsJ9uVP9In1NI4ujic57ff5Q2S61U89iA1u7rVG2y0UAAAAAIgBMkwAApOilYAQAAQA0paRgAwA0paRgAgA0paRgBAA0paRg', 2, '0', 2, '2021-05-19 10:12:12', NULL, NULL),
(89, 36, 45, '91', 'uUOe78rK0YrkEaBgWxUO2Rdu5uv7oJAv', '00648fb1890f2c449ed9e29207af9a0e447IADb1A7kpuXxdd1T/MjkfttNosZn3WwoHhlhLv9iGRbnpDLPkGUAAAAAIgBkjwAAJ+mlYAQAAQC3paRgAwC3paRgAgC3paRgBAC3paRg', 2, '0', 2, '2021-05-19 10:14:23', NULL, NULL),
(90, 37, 40, '92', 'xaYYVGDZAHO1meXzvXN1w5RCV9w2qQkb', '00648fb1890f2c449ed9e29207af9a0e447IAA/s8UAI26s0gv4aH8tRn2Mp3Pj2BersOsqS3ztEG5P1jb3kpUAAAAAIgDbcgAA9OqlYAQAAQCEp6RgAwCEp6RgAgCEp6RgBACEp6Rg', 2, '0', 2, '2021-05-19 10:22:05', NULL, NULL),
(91, 36, 37, '93', '7Udi66xUI4OnntjbCce6N8CiwqW39VJi', '00648fb1890f2c449ed9e29207af9a0e447IADlPEKzmjvKRA+lMcjGlQzethrNJkmS8vv0OCDtObUjyVb+kZgAAAAAIgDnPgEArOylYAQAAQA8qaRgAwA8qaRgAgA8qaRgBAA8qaRg', 2, '0', 2, '2021-05-19 10:29:24', NULL, NULL),
(92, 36, 37, '94', 'uzG0BK7i5yixLzKCEMLuhLh4hlmXl3gL', '00648fb1890f2c449ed9e29207af9a0e447IABqdh3rKo8SUqeyVS9ws97WKm0UQShY2y+nzfJfe90R6jTqwtsAAAAAIgDhDQEA9+ylYAQAAQCHqaRgAwCHqaRgAgCHqaRgBACHqaRg', 2, '0', 2, '2021-05-19 10:30:39', NULL, NULL),
(93, 36, 45, '95', 'mrW6AboigEfnDK6Y1Xuv37fNH5XyNxx5', '00648fb1890f2c449ed9e29207af9a0e447IAD4G8k0Pg8en1Qp6I86xB5GoDdCCnDFHweQ+GtRTIzzsuN4VUIAAAAAIgABVgAAbu2lYAQAAQD+qaRgAwD+qaRgAgD+qaRgBAD+qaRg', 2, '0', 2, '2021-05-19 10:32:38', NULL, NULL),
(94, 36, 37, '96', 'Yu5FXKivJTWg6nLhGMg0GshmK0vH4h6i', '00648fb1890f2c449ed9e29207af9a0e447IAAy4+INFqReTxiuT97PJYqiOcu5/WtdvlEmhlZX12pKWUewxV8AAAAAIgAhIAAAh+6lYAQAAQAXq6RgAwAXq6RgAgAXq6RgBAAXq6Rg', 2, '0', 2, '2021-05-19 10:37:20', NULL, NULL),
(95, 36, 33, '97', 'L73Tli9nM7vSs6SgkpskEkuwyopWdYHc', '00648fb1890f2c449ed9e29207af9a0e447IAACt6CblSmafJlIm9B1ZlyG0TlnWTUc2vak3cyqGe1dWOC2s5QAAAAAIgAbUQAAr/GlYAQAAQA/rqRgAwA/rqRgAgA/rqRgBAA/rqRg', 2, '0', 2, '2021-05-19 10:50:47', NULL, NULL),
(96, 36, 33, '98', 'hPBmbNjlBV6r1DaH2l00aBHzX0HzdFqS', '00648fb1890f2c449ed9e29207af9a0e447IACjUK94cYgZiwE0Rc70dAxuF/Zy28Hs1qh9b24M5u+pwsFFhS4AAAAAIgDSGQEAyPGlYAQAAQBYrqRgAwBYrqRgAgBYrqRgBABYrqRg', 2, '0', 2, '2021-05-19 10:51:12', NULL, NULL),
(97, 36, 33, '99', 'g2ufRkFtTnLhvvAnsbDcI059krJvrEIP', '00648fb1890f2c449ed9e29207af9a0e447IAAl8CTTlGd69DWzioG+MEL9OSV5nDRBEl05yUB9pNSFp47s3PEAAAAAIgBznQAAUvKlYAQAAQDirqRgAwDirqRgAgDirqRgBADirqRg', 2, '0', 2, '2021-05-19 10:53:30', NULL, NULL),
(98, 36, 37, '100', 'gxYeum44ZjgPNRsHyCZOy5lwNlDCjNh4', '00648fb1890f2c449ed9e29207af9a0e447IACwMNmfouBVHn2cEvBypseD43VCBDdCNWd1FGfTZ+g6lRXxMuEAAAAAIgANGAEAMPSlYAQAAQDAsKRgAwDAsKRgAgDAsKRgBADAsKRg', 2, '0', 2, '2021-05-19 11:01:29', NULL, NULL),
(99, 36, 37, '101', 'yqxn2vKlBXspHYEPBg7AKtDN7Xgfx8Q2', '00648fb1890f2c449ed9e29207af9a0e447IACFtvzEolgMYCrZD45Kv1eo+9eOAveBFezJZdkCS5aHqtS6NewAAAAAIgDfbAAATPSlYAQAAQDcsKRgAwDcsKRgAgDcsKRgBADcsKRg', 2, '0', 2, '2021-05-19 11:01:56', NULL, NULL),
(100, 36, 37, '102', 'tde2a02C9nbV7BYuqqOd6uK6dXFWQQae', '00648fb1890f2c449ed9e29207af9a0e447IADkIwxw5ZfLy0Kyl4e4IOsQWfsA4Gpfd3HVLTASSaGC//r8Pn4AAAAAIgBIbgAAY/SlYAQAAQDzsKRgAwDzsKRgAgDzsKRgBADzsKRg', 2, '0', 2, '2021-05-19 11:02:20', NULL, NULL),
(101, 36, 37, '103', 'eciq0d0eIH1KsH6lRtGTkcLXDleTXiaI', '00648fb1890f2c449ed9e29207af9a0e447IAA0mD1nNHLhwXUtXYdtuNfuRH4PIRfUVigGU56DOcbY3mUww7kAAAAAIgDbZgAAnfSlYAQAAQAtsaRgAwAtsaRgAgAtsaRgBAAtsaRg', 2, '0', 2, '2021-05-19 11:03:17', NULL, NULL),
(102, 37, 44, '104', 'RCBdLssvEraqjl0WmIaCkGSkWeUo4xfs', '00648fb1890f2c449ed9e29207af9a0e447IADE05HM0BoFyGtH6r0JcPt1xt/fVsjS3yrJgJex6RQnIsV8h/gAAAAAIgBAeAAA9PSlYAQAAQCEsaRgAwCEsaRgAgCEsaRgBACEsaRg', 2, '0', 2, '2021-05-19 11:04:46', NULL, NULL),
(103, 37, 44, '105', 'yxR7eeWbtYRuXoV1G25ymkvBZ2ec0Nkv', '00648fb1890f2c449ed9e29207af9a0e447IAAKXrNer21dt9Ee2OzzYred/7z3ygRwManFPZ3A18Zo71Z6rEQAAAAAIgB2vQAAGvWlYAQAAQCqsaRgAwCqsaRgAgCqsaRgBACqsaRg', 2, '0', 2, '2021-05-19 11:05:23', NULL, NULL),
(104, 36, 37, '106', 'zBzxoFSN6VWXRktLmXWmqxQ3VsTOEA7Y', '00648fb1890f2c449ed9e29207af9a0e447IACgScMmkPeP7Ll5WNjuoJ0fw3UYuTER06KrOuJ8wGo5fwTceuMAAAAAIgCCQQAALvWlYAQAAQC+saRgAwC+saRgAgC+saRgBAC+saRg', 2, '0', 2, '2021-05-19 11:05:43', NULL, NULL),
(105, 37, 44, '107', 'IxZw4DC5U2Yi2iP5iHKgAG7u3VEu3DDe', '00648fb1890f2c449ed9e29207af9a0e447IADI/ZUonW0c2J4y9Uuas2f9WYBhQuoXyFyaJZ13wfiXt2cpjg0AAAAAIgCGWAEAcPWlYAQAAQAAsqRgAwAAsqRgAgAAsqRgBAAAsqRg', 2, '0', 2, '2021-05-19 11:06:50', NULL, NULL),
(106, 36, 37, '108', 'SGKljUqfc6SqcMdLgIpFy6R2hVORaqqK', '00648fb1890f2c449ed9e29207af9a0e447IACYfsowVla4KY5dioahUdEpl9aC17cN7uMrooC4FXHx0o2toIoAAAAAIgBI/wAAjvWlYAQAAQAesqRgAwAesqRgAgAesqRgBAAesqRg', 2, '0', 2, '2021-05-19 11:07:19', NULL, NULL),
(107, 36, 33, '109', 'n0BVMOQ6ZKixDa1U4lEtiNxenQ4zXrHw', '00648fb1890f2c449ed9e29207af9a0e447IADnPlQrJCk52WMBClgy7fz9oG0nqBwV7KI2ek4oTZo/m/UN53IAAAAAIgBXTAEAHfalYAQAAQCtsqRgAwCtsqRgAgCtsqRgBACtsqRg', 2, '0', 2, '2021-05-19 11:09:41', NULL, NULL),
(108, 33, 36, '110', '5jjwKDkLNVd6bIhoLhwiKg1qKyEOTZwT', '00648fb1890f2c449ed9e29207af9a0e447IACPjhfInWaqJis2o36bWL3A6QZ2xw01TXRtO8VS2tbLjuiG1MEAAAAAIgBhugAAXvalYAQAAQDusqRgAwDusqRgAgDusqRgBADusqRg', 2, '0', 2, '2021-05-19 11:10:48', NULL, NULL),
(109, 33, 36, '111', 'I8rOhkK2S1x4phCZvahL6hGC3W8q3nLp', '00648fb1890f2c449ed9e29207af9a0e447IAAM0bVXfYTyMlSvUZVhR6olhuhmc/WUncIWQ/lJ3a4U/6Wp95EAAAAAIgADOQAA1falYAQAAQBls6RgAwBls6RgAgBls6RgBABls6Rg', 2, '0', 5, '2021-05-19 11:12:46', NULL, NULL),
(110, 36, 33, '112', 'fsta8DyR0ZZxnL94RaA3n57mOOCfwSzm', '00648fb1890f2c449ed9e29207af9a0e447IADb3bEccNPeksl2p7wQO8pLrk6oM/90hf1c68DvuguB/vISk5cAAAAAIgA5CAEAKPelYAQAAQC4s6RgAwC4s6RgAgC4s6RgBAC4s6Rg', 2, '0', 2, '2021-05-19 11:14:08', NULL, NULL),
(111, 36, 37, '113', 'BBluIoAu0qOviaLMDFHW9BYyqL8u7M5p', '00648fb1890f2c449ed9e29207af9a0e447IAAtZrEmWSZh2hma4fAfAXvF8B0uoPRvHb/K07bec5Tq6lQQI6QAAAAAIgBRIAAA3velYAQAAQButKRgAwButKRgAgButKRgBAButKRg', 2, '0', 2, '2021-05-19 11:17:10', NULL, NULL),
(112, 36, 33, '114', 'TDsQpG5HkF8K09MKZt7p9Vadf9mIycZC', '00648fb1890f2c449ed9e29207af9a0e447IABZ4XtNOMXI1PPadVfDOdsG9VL/rmohtGSPl7GZF8FLlAR7b8YAAAAAIgCFUwAA+PelYAQAAQCItKRgAwCItKRgAgCItKRgBACItKRg', 2, '0', 2, '2021-05-19 11:17:36', NULL, NULL),
(113, 36, 33, '115', '45yUK5nDliQLD1NJxEpxBTR59MqXkVyK', '00648fb1890f2c449ed9e29207af9a0e447IAC1NFD5aDqfVNQEToHnLJB2zvIiv02Uaii69t6gLju3h5hkD1cAAAAAIgAvdQAARvilYAQAAQDWtKRgAwDWtKRgAgDWtKRgBADWtKRg', 2, '0', 2, '2021-05-19 11:18:54', NULL, NULL),
(114, 33, 34, '116', 'G8prHN6oJws48ZAT5fTq3OG5pv2DREZG', '00648fb1890f2c449ed9e29207af9a0e447IABKqZ3CXuW5LgVYK2iHdIHbqzHhT3kwR2duRg4h+QNsszuiEfEAAAAAIgCcMgAAqgCmYAQAAQA6vaRgAwA6vaRgAgA6vaRgBAA6vaRg', 2, '0', 2, '2021-05-19 11:54:43', NULL, NULL),
(115, 34, 50, '117', 'zX4TtP656Ych6GYwTqn3Yu0Kw33Sw211', '00648fb1890f2c449ed9e29207af9a0e447IABky09tALCpqHrvUziisBvyt8jBos8HSPtVM0ofnqU+06zDr9UAAAAAIgDQSwAAFwGmYAQAAQCnvaRgAwCnvaRgAgCnvaRgBACnvaRg', 2, '0', 2, '2021-05-19 11:56:29', NULL, NULL),
(116, 32, 33, '118', 'peZ1JTm1EDE2R2hhgeh87BQDY2P48t3c', '00648fb1890f2c449ed9e29207af9a0e447IAAqY2kh28osFARpS13w8Y5sUj73VMQCNvUqa53Z46EK4uyYiUMAAAAAIgCi3QAAcwGmYAQAAQADvqRgAwADvqRgAgADvqRgBAADvqRg', 2, '0', 2, '2021-05-19 11:58:03', NULL, NULL),
(117, 33, 32, '119', 'CeeUwxcifS0jKdVxs3KjL0goh0YkxIFV', '00648fb1890f2c449ed9e29207af9a0e447IAC+zjNZ6ud5QWTvps7VjkjAmcb0ETNnS9MqEYB1Tf4h255AXLYAAAAAIgCdJQEAtgGmYAQAAQBGvqRgAwBGvqRgAgBGvqRgBABGvqRg', 2, '0', 2, '2021-05-19 11:59:10', NULL, NULL),
(118, 50, 34, '120', 'Ss8cK1gx5huM7aA5iBcFarl9aISKQOAZ', '00648fb1890f2c449ed9e29207af9a0e447IAAD9DT0bklMNvgBTNJ9WznDRk+TuG486KkRwh2vwYnM5t2+G3sAAAAAIgCQVQAA4wGmYAQAAQBzvqRgAwBzvqRgAgBzvqRgBABzvqRg', 2, '0', 2, '2021-05-19 11:56:25', NULL, NULL),
(119, 32, 33, '121', 'TcJvjCXoHCVip6Bd9G4jUqIz2w9mY2Wi', '00648fb1890f2c449ed9e29207af9a0e447IABx0UQtaui9jJUT9SprHSXCGa+9Gmllu8esWi0kkh5+4av9CG4AAAAAIgClPgAAyAOmYAQAAQBYwKRgAwBYwKRgAgBYwKRgBABYwKRg', 2, '0', 2, '2021-05-19 12:08:01', NULL, NULL),
(120, 33, 34, '122', '4cuBmusqs8YKMvbM4JXmYau0Z4P0speV', '00648fb1890f2c449ed9e29207af9a0e447IADwOP184EXpWIG5lOxcpj715c5GHIDj2B2uVkWC6k4x6j0a01IAAAAAIgD8UQAA5gWmYAQAAQB2wqRgAwB2wqRgAgB2wqRgBAB2wqRg', 2, '0', 2, '2021-05-19 12:17:02', NULL, NULL),
(121, 34, 33, '123', 'Qm5pqZytceRi6XWi8J8JHRYTWv70jLgo', '00648fb1890f2c449ed9e29207af9a0e447IAB8sBEoejpp7r7AhM6O8q+FA+7oeUeyCe0li7q6DD9BFedMhNEAAAAAIgAasgAABQamYAQAAQCVwqRgAwCVwqRgAgCVwqRgBACVwqRg', 2, '0', 2, '2021-05-19 12:17:31', NULL, NULL),
(122, 33, 34, '124', 'KdKBSwqOQpNNSUzUc5kcAFZ6VllTaFEb', '00648fb1890f2c449ed9e29207af9a0e447IADUQiMC9aImR0OcqApaNh71m48Y0WtScXHrt/SGnvkT3C94bNIAAAAAIgBW2AAAIwamYAQAAQCzwqRgAwCzwqRgAgCzwqRgBACzwqRg', 2, '0', 2, '2021-05-19 12:18:03', NULL, NULL),
(123, 34, 33, '125', 'S6xIe5IkLoQnuRIKlKQ7B3NVMc7iYqOO', '00648fb1890f2c449ed9e29207af9a0e447IACKAEeLKIF3mfLl/QNIIjI4W89p5lhZTsorcCBN0UfjaHSCRrUAAAAAIgAZAwEAQgamYAQAAQDSwqRgAwDSwqRgAgDSwqRgBADSwqRg', 2, '0', 2, '2021-05-19 12:18:32', NULL, NULL),
(124, 34, 33, '126', 'jmrNBTQU5nIc1EQO2iNv70LZe0vA1Pdt', '00648fb1890f2c449ed9e29207af9a0e447IAB2MNJklxajUR3XYhyUtS+O9CDwoxRL+G11hTCBdZ0reC1b/0gAAAAAIgD87QAAqAamYAQAAQA4w6RgAwA4w6RgAgA4w6RgBAA4w6Rg', 2, '0', 2, '2021-05-19 12:20:13', NULL, NULL),
(125, 33, 34, '127', 'ev3oirOdEFpk5ixvPvM31JikofVkMtgj', '00648fb1890f2c449ed9e29207af9a0e447IADM0sjMLCi6bMgKLmKbCdk50IUPh+zOhyW9es3np8so9p7ChEUAAAAAIgDOlAAA6gamYAQAAQB6w6RgAwB6w6RgAgB6w6RgBAB6w6Rg', 2, '0', 2, '2021-05-19 12:21:22', NULL, NULL),
(126, 39, 34, '128', 'trkapPCm4Dk4XysEDU1ltKdR6TGuivq7', '00648fb1890f2c449ed9e29207af9a0e447IAAFoUnRi4piiOPNSQf0QKk+vCBrvqD7bSx+O6Fi6HCD0/6R5G0AAAAAIgDDgQAAagemYAQAAQD6w6RgAwD6w6RgAgD6w6RgBAD6w6Rg', 2, '0', 2, '2021-05-19 12:23:30', NULL, NULL),
(127, 34, 39, '129', 'kcG0PPuR1pnX9f0U9CKPoCZodA3ELXuV', '00648fb1890f2c449ed9e29207af9a0e447IABO5r2mhAuMGqZhyz78rdOWH7uSPmp45JnQYzRPbCtW5qsVZmwAAAAAIgBRNwEAgQemYAQAAQARxKRgAwARxKRgAgARxKRgBAARxKRg', 2, '0', 2, '2021-05-19 12:23:51', NULL, NULL),
(128, 39, 34, '130', 'DrEZix1kjzgeqo5NaZwTxxR2AR4JMVxw', '00648fb1890f2c449ed9e29207af9a0e447IAA5V+3qUqxN+OxTk+6FAomauacpdYzavErsBsdHwFE6yk0/ZEEAAAAAIgAVKwAApAemYAQAAQA0xKRgAwA0xKRgAgA0xKRgBAA0xKRg', 2, '0', 2, '2021-05-19 12:24:28', NULL, NULL),
(129, 39, 34, '131', 'qKW8u3Y95qMX3gw946ZDFysEndvRmriK', '00648fb1890f2c449ed9e29207af9a0e447IAD2XqtQCuSCTdFHINktQuAbBQhsq5OxB61BeYTq0Li0ZHFLtXUAAAAAIgC8WAAADQimYAQAAQCdxKRgAwCdxKRgAgCdxKRgBACdxKRg', 2, '0', 2, '2021-05-19 12:26:13', NULL, NULL),
(130, 34, 39, '132', 'tT9yO5NlkL0K2Iwr5E4XmWoDzBzm9EcQ', '00648fb1890f2c449ed9e29207af9a0e447IAD+0ma40G3k9LfMhGD3CdH2H4sXF3KhgessijpLOq7PbX5WUdkAAAAAIgDZJgEAQQimYAQAAQDRxKRgAwDRxKRgAgDRxKRgBADRxKRg', 2, '0', 2, '2021-05-19 12:27:02', NULL, NULL),
(131, 39, 32, '133', 'oC70tknC7yiEJfj7C6uxSOBwc0qSqJfo', '00648fb1890f2c449ed9e29207af9a0e447IADp+QeqPZ1bYjuAW4QGsYR2DG6cZOxDRuU+ylCAJJTGiLIUQU0AAAAAIgDeuAAAwAmmYAQAAQBQxqRgAwBQxqRgAgBQxqRgBABQxqRg', 2, '0', 2, '2021-05-19 12:33:28', NULL, NULL),
(132, 39, 33, '134', '2k9s8JqnuiAEkwAQRcq1NCwVRRoH8jg2', '00648fb1890f2c449ed9e29207af9a0e447IACSgm52AxoZa4oc4UBeYj5zoJrz3BsfnNg/DE2iCIwOE96U1vkAAAAAIgD8qwAAkAqmYAQAAQAgx6RgAwAgx6RgAgAgx6RgBAAgx6Rg', 2, '0', 2, '2021-05-19 12:36:56', NULL, NULL),
(133, 33, 39, '135', 'bq95YQFm81v253B1ADKSjkrK4vZGXS4i', '00648fb1890f2c449ed9e29207af9a0e447IAAw1EDXVDFqFXqNX3E/yDndK3BlZh27lbZQ1rMCqbpDKhC6LnIAAAAAIgA/MAEAvAqmYAQAAQBMx6RgAwBMx6RgAgBMx6RgBABMx6Rg', 2, '0', 2, '2021-05-19 12:37:38', NULL, NULL),
(134, 39, 33, '136', 'ULTRVsuB4UJ8Xgqd7ZI7Xg0svOEWgPvj', '00648fb1890f2c449ed9e29207af9a0e447IAAp+sFMdkvYrfofzRipjodulqaL0H5xknALzIb9WkiMaX33wocAAAAAIgBycwAAWwumYAQAAQDrx6RgAwDrx6RgAgDrx6RgBADrx6Rg', 2, '0', 2, '2021-05-19 12:40:19', NULL, NULL),
(135, 33, 39, '137', 'B4WMj7rBPQv0zrsCrBvoAxiDHOAekC82', '00648fb1890f2c449ed9e29207af9a0e447IAC9JYuZ50cGf3+CLnMK4TVK8go4NPeWEiNKUXlfPVTXGLBFpOsAAAAAIgCgOAAAdAumYAQAAQAEyKRgAwAEyKRgAgAEyKRgBAAEyKRg', 2, '0', 5, '2021-05-19 12:40:42', NULL, NULL),
(136, 33, 39, '138', 'EPRuFfbqfsuaJLMEvGVCRvvLZpsxuDjP', '00648fb1890f2c449ed9e29207af9a0e447IADYfRnAodkHtNPZ5LnyqzRkCwiQmEH9QMwLRWtmvdbhXFZcSrIAAAAAIgCG+gAA/QumYAQAAQCNyKRgAwCNyKRgAgCNyKRgBACNyKRg', 2, '0', 2, '2021-05-19 12:42:58', NULL, NULL),
(137, 33, 39, '139', 'nuDn2WMrvNlbLPUK70EEX8mCHhmjs0Il', '00648fb1890f2c449ed9e29207af9a0e447IADPXbNxE2XzRQGQnfyrDtSpUf2GLIbqJbDoFIW8z3ZjOoHPLeoAAAAAIgAm6wAASwymYAQAAQDbyKRgAwDbyKRgAgDbyKRgBADbyKRg', 2, '0', 2, '2021-05-19 12:44:17', NULL, NULL),
(138, 33, 39, '140', 'aVO12YyFCB8h2ltX153Ka2PkgI1B80S3', '00648fb1890f2c449ed9e29207af9a0e447IADKWWJQXCfhl0uYytSE/nhjUCoAIyHtxMW9aUa4TuIC04tvNPYAAAAAIgApEwAAkAymYAQAAQAgyaRgAwAgyaRgAgAgyaRgBAAgyaRg', 2, '0', 5, '2021-05-19 12:45:26', NULL, NULL),
(139, 33, 39, '141', 'V0s4wHTFutwL1f9b24ONtNzfFCuTT9Mt', '00648fb1890f2c449ed9e29207af9a0e447IAAVSSGgUojruzQUpEvh6ApzjOBr8Mhk2hP2EI8q5BX3BIxV4pYAAAAAIgBoUQEAMA2mYAQAAQDAyaRgAwDAyaRgAgDAyaRgBADAyaRg', 2, '0', 2, '2021-05-19 12:48:06', NULL, NULL),
(140, 39, 33, '142', '2xS3tfbGE7xxigTTotxeOrlsMoWqpI82', '00648fb1890f2c449ed9e29207af9a0e447IAD2SRvmb9/5adWeQPmaC0cJIKqpBjxNO/9NCfkUBFCFngeyaPMAAAAAIgAM5AAAZg2mYAQAAQD2yaRgAwD2yaRgAgD2yaRgBAD2yaRg', 2, '0', 2, '2021-05-19 12:49:02', NULL, NULL),
(141, 33, 39, '143', 'ap3rkhYNuR7JmpfDQNargoTONPKP9SxY', '00648fb1890f2c449ed9e29207af9a0e447IADWwBrOOjjCKB5s3GVcULc9a8bvpRJLBTLtMBu0+wEWQVC/id0AAAAAIgBkCQEAfA2mYAQAAQAMyqRgAwAMyqRgAgAMyqRgBAAMyqRg', 2, '0', 2, '2021-05-19 12:49:22', NULL, NULL),
(142, 37, 44, '144', 'zPFM3mQuFGBrafkxd56QH7JizfZQ6P2r', '00648fb1890f2c449ed9e29207af9a0e447IABzLK8lsVEfmFuFNpYrvK8Q6cHfIlcMdT6LaQjXPG+DDwWdH9EAAAAAIgD7WwAAyMemYAQAAQBYhKVgAwBYhKVgAgBYhKVgBABYhKVg', 2, '0', 2, '2021-05-19 04:34:15', NULL, NULL),
(143, 33, 47, '145', 'VFMTXP0XBjqxTyX0xnJjRi1jmXS2rg4o', '00648fb1890f2c449ed9e29207af9a0e447IABr8qrh+TKEL16hkuQ55AEUDVQVIGcI5Oa81lL2ESWmSJlMgKQAAAAAIgAJhwAAvdaoYAQAAQBNk6dgAwBNk6dgAgBNk6dgBABNk6dg', 2, '0', 2, '2021-05-21 03:32:34', NULL, NULL),
(144, 34, 43, '146', 'CHWoSbNA9DoT4LdOji77Iwo7jKDPfKG6', '00648fb1890f2c449ed9e29207af9a0e447IACzfSkuWW5nb5j2NYCZei4DTX6p1lY3Vy57SDnNB5QbHbggrJUAAAAAIgCsBgEAOmqzYAQAAQDKJrJgAwDKJrJgAgDKJrJgBADKJrJg', 2, '0', 2, '2021-05-29 04:04:30', NULL, NULL),
(145, 34, 50, '147', 'Oy3vQiMpa1Tf6MsUWgPlvIUednbNZ0HC', '00648fb1890f2c449ed9e29207af9a0e447IAAc54vhg3hNq/yv4t7HdsVhBO3W/pOWrTFAorcBE02WkqKBu/gAAAAAIgDLJQAA2HOzYAQAAQBoMLJgAwBoMLJgAgBoMLJgBABoMLJg', 2, '0', 2, '2021-05-29 04:45:33', NULL, NULL),
(146, 34, 50, '148', '1L3M5Kg8RJdHnQt3Q0m0h8nhAUu2CIpA', '00648fb1890f2c449ed9e29207af9a0e447IABuPXzl6qQNyGLJMukouEQE8wlB+yvvylSHF3CKygzECdzsXWUAAAAAIgDEXgAA/XOzYAQAAQCNMLJgAwCNMLJgAgCNMLJgBACNMLJg', 2, '0', 2, '2021-05-29 04:46:10', NULL, NULL),
(147, 50, 34, '149', 'FZIB9mzbmKcP1ygmJENSiUVjaaURZTf2', '00648fb1890f2c449ed9e29207af9a0e447IABWuqtt8ulgWKoa+JqNU/o7YR2ZU4pfTLSfTzaAQ/4eyoDs6AYAAAAAIgD3WAEAW2C4YAQAAQDrHLdgAwDrHLdgAgDrHLdgBADrHLdg', 2, '0', 2, '2021-06-02 10:23:44', NULL, NULL),
(148, 44, 37, '150', '65G3QfEMvGf2SOAGpUi3RcKdEADgrCwt', '00648fb1890f2c449ed9e29207af9a0e447IAB5ENJGly7CVas+WkKHsPUWILqz3FpiQJ/t3AemJd3msm4GC+oAAAAAIgCJLAAAY+XGYAQAAQDzocVgAwDzocVgAgDzocVgBADzocVg', 2, '0', 2, '2021-06-12 10:13:07', NULL, NULL),
(149, 37, 44, '151', 'JvZleSajGHj7586tw0FT1L8Lr6UWktLj', '00648fb1890f2c449ed9e29207af9a0e447IADB+NPC9dulAX4r9mYJyB8d3FwZ47wzIS+s9YasMdNdkLWEQsgAAAAAIgDQSQAAeYPfYAQAAQAJQN5gAwAJQN5gAgAJQN5gBAAJQN5g', 2, '0', 2, '2021-07-01 05:22:02', NULL, NULL),
(150, 33, 41, '152', 'KttWAdEv6xFJrYJoJX01rWYVIYLIv2W8', '00648fb1890f2c449ed9e29207af9a0e447IAALCOWSvLV5u8QXUoZ+OdLJH2VQd26y0bVHumCfOZQ72tNev88AAAAAIgBnDgEA0+vvYAQAAQBjqO5gAwBjqO5gAgBjqO5gBABjqO5g', 2, '0', 2, '2021-07-14 01:33:32', NULL, NULL),
(151, 33, 43, '153', 'HMBa0pwBPGLTk2lFkY6N8qjJ5RArLLkY', '00634bc8052f40242feb77f58537d9d9b31IABCgTwiFy7AQRerFqp6RnOJS5CZfObGYaCJTK7n1606o6HQ2sYAAAAAIgAyMwAAexHwYAQAAQALzu5gAwALzu5gAgALzu5gBAALzu5g', 2, '0', 2, '2021-07-14 04:14:05', NULL, NULL),
(152, 33, 39, '154', 'OApjBfwWOmUyjGZt8NGcBNMqVaWgyCzc', '00634bc8052f40242feb77f58537d9d9b31IAB+0WhEuKFvZI02SBb+olIp1N8EFWaDRKx3A/4kXe5cXNChpaUAAAAAIgBYUAEAnCLwYAQAAQAs3+5gAwAs3+5gAgAs3+5gBAAs3+5g', 2, '0', 2, '2021-07-14 05:27:10', NULL, NULL),
(153, 33, 49, '155', 'wrKABjA5yH0Jrr8v2pSSvbc66r1aRymi', '00634bc8052f40242feb77f58537d9d9b31IABhW8EBJW885uQmrzygGGhR7B6Ce525sU29BXIftVMw6fzYnX4AAAAAIgC/LAAAFizwYAQAAQCm6O5gAwCm6O5gAgCm6O5gBACm6O5g', 2, '0', 2, '2021-07-14 06:07:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `view_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `user_id`, `view_user_id`, `created_at`, `updated_at`) VALUES
(6, 44, 50, '2021-05-29 03:24:28', '2021-05-29 03:24:28'),
(45, 39, 45, '2021-05-29 08:13:15', '2021-05-29 08:13:15'),
(52, 39, 49, '2021-05-29 09:23:01', '2021-05-29 09:23:01'),
(53, 34, 43, '2021-05-29 10:26:52', '2021-05-29 10:26:52'),
(57, 34, 42, '2021-05-29 11:11:30', '2021-05-29 11:11:30'),
(58, 34, 50, '2021-05-29 11:16:06', '2021-05-29 11:16:06'),
(59, 34, 49, '2021-05-29 11:17:39', '2021-05-29 11:17:39'),
(60, 44, 38, '2021-05-30 03:47:31', '2021-05-30 03:47:31'),
(66, 50, 49, '2021-06-01 07:24:18', '2021-06-01 07:24:18'),
(67, 50, 42, '2021-06-01 08:34:07', '2021-06-01 08:34:07'),
(68, 50, 47, '2021-06-02 04:53:20', '2021-06-02 04:53:20'),
(70, 47, 49, '2021-06-02 04:55:57', '2021-06-02 04:55:57'),
(71, 47, 39, '2021-06-02 04:57:34', '2021-06-02 04:57:34'),
(72, 47, 40, '2021-06-02 04:58:07', '2021-06-02 04:58:07'),
(77, 51, 47, '2021-06-02 06:24:02', '2021-06-02 06:24:02'),
(78, 51, 45, '2021-06-02 06:24:07', '2021-06-02 06:24:07'),
(79, 51, 43, '2021-06-02 06:24:19', '2021-06-02 06:24:19'),
(80, 51, 42, '2021-06-02 06:24:25', '2021-06-02 06:24:25'),
(82, 51, 40, '2021-06-02 06:24:42', '2021-06-02 06:24:42'),
(83, 51, 39, '2021-06-02 06:24:49', '2021-06-02 06:24:49'),
(85, 51, 41, '2021-06-02 06:32:39', '2021-06-02 06:32:39'),
(86, 51, 38, '2021-06-02 06:33:00', '2021-06-02 06:33:00'),
(92, 51, 50, '2021-06-04 03:31:04', '2021-06-04 03:31:04'),
(98, 37, 44, '2021-06-05 04:33:59', '2021-06-05 04:33:59'),
(99, 44, 41, '2021-06-07 18:07:43', '2021-06-07 18:07:43'),
(100, 51, 49, '2021-06-10 10:18:25', '2021-06-10 10:18:25'),
(101, 51, 33, '2021-06-10 10:18:42', '2021-06-10 10:18:42'),
(107, 33, 45, '2021-06-11 08:01:14', '2021-06-11 08:01:14'),
(110, 49, 41, '2021-06-11 10:09:09', '2021-06-11 10:09:09'),
(111, 49, 33, '2021-06-11 10:10:05', '2021-06-11 10:10:05'),
(120, 52, 44, '2021-06-13 18:49:19', '2021-06-13 18:49:19'),
(125, 33, 43, '2021-06-16 08:29:09', '2021-06-16 08:29:09'),
(126, 33, 49, '2021-06-16 08:29:14', '2021-06-16 08:29:14'),
(130, 33, 42, '2021-06-25 10:48:51', '2021-06-25 10:48:51'),
(133, 44, 43, '2021-06-28 07:43:30', '2021-06-28 07:43:30'),
(155, 44, 52, '2021-07-11 00:53:50', '2021-07-11 00:53:50'),
(156, 33, 47, '2021-07-14 06:44:50', '2021-07-14 06:44:50'),
(157, 49, 47, '2021-07-14 07:10:09', '2021-07-14 07:10:09'),
(158, 49, 38, '2021-07-14 07:10:25', '2021-07-14 07:10:25'),
(159, 49, 39, '2021-07-14 07:10:31', '2021-07-14 07:10:31'),
(160, 44, 51, '2021-07-14 16:04:41', '2021-07-14 16:04:41'),
(161, 44, 37, '2021-07-15 21:32:29', '2021-07-15 21:32:29'),
(163, 44, 42, '2021-07-16 05:21:39', '2021-07-16 05:21:39'),
(165, 33, 50, '2021-07-16 13:23:29', '2021-07-16 13:23:29'),
(167, 53, 49, '2021-07-16 13:59:04', '2021-07-16 13:59:04'),
(168, 53, 45, '2021-07-16 13:59:20', '2021-07-16 13:59:20'),
(171, 56, 55, '2021-07-17 06:20:44', '2021-07-17 06:20:44'),
(174, 55, 50, '2021-07-17 06:37:33', '2021-07-17 06:37:33'),
(176, 55, 53, '2021-07-17 06:37:53', '2021-07-17 06:37:53'),
(179, 55, 56, '2021-07-17 06:39:49', '2021-07-17 06:39:49'),
(180, 49, 58, '2021-07-17 12:06:10', '2021-07-17 12:06:10'),
(182, 44, 59, '2021-07-19 04:55:14', '2021-07-19 04:55:14'),
(183, 44, 58, '2021-07-19 05:03:56', '2021-07-19 05:03:56'),
(184, 44, 54, '2021-07-19 05:04:04', '2021-07-19 05:04:04'),
(185, 44, 55, '2021-07-19 05:04:12', '2021-07-19 05:04:12'),
(187, 49, 59, '2021-07-19 05:30:22', '2021-07-19 05:30:22'),
(188, 64, 54, '2021-07-27 04:42:18', '2021-07-27 04:42:18'),
(190, 64, 63, '2021-07-27 04:42:38', '2021-07-27 04:42:38'),
(193, 63, 64, '2021-07-27 04:44:17', '2021-07-27 04:44:17'),
(194, 64, 59, '2021-07-27 04:44:20', '2021-07-27 04:44:20'),
(195, 64, 62, '2021-07-27 04:45:05', '2021-07-27 04:45:05'),
(196, 63, 61, '2021-07-27 04:45:39', '2021-07-27 04:45:39'),
(197, 64, 57, '2021-07-29 12:03:06', '2021-07-29 12:03:06'),
(200, 33, 59, '2021-07-31 14:30:49', '2021-07-31 14:30:49'),
(201, 33, 62, '2021-07-31 14:31:23', '2021-07-31 14:31:23'),
(202, 33, 56, '2021-07-31 14:31:36', '2021-07-31 14:31:36'),
(203, 33, 54, '2021-07-31 14:31:44', '2021-07-31 14:31:44'),
(204, 33, 61, '2021-07-31 14:31:53', '2021-07-31 14:31:53'),
(205, 44, 64, '2021-08-01 02:06:35', '2021-08-01 02:06:35'),
(206, 44, 63, '2021-08-01 02:06:44', '2021-08-01 02:06:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_logins`
--
ALTER TABLE `app_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocked_users`
--
ALTER TABLE `blocked_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `body_types`
--
ALTER TABLE `body_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user`
--
ALTER TABLE `chat_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_quota`
--
ALTER TABLE `daily_quota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ethnicities`
--
ALTER TABLE `ethnicities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health`
--
ALTER TABLE `health`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_user_report`
--
ALTER TABLE `health_user_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iams`
--
ALTER TABLE `iams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lookingfors`
--
ALTER TABLE `lookingfors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages_read_unread`
--
ALTER TABLE `messages_read_unread`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages_test`
--
ALTER TABLE `messages_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `premium`
--
ALTER TABLE `premium`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `premium_history`
--
ALTER TABLE `premium_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `private_album`
--
ALTER TABLE `private_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `private_album_rights`
--
ALTER TABLE `private_album_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purrs`
--
ALTER TABLE `purrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_images`
--
ALTER TABLE `report_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_msgs`
--
ALTER TABLE `report_msgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_users`
--
ALTER TABLE `report_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_image`
--
ALTER TABLE `user_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_likes`
--
ALTER TABLE `user_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_subscription`
--
ALTER TABLE `user_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_video_call_tokens`
--
ALTER TABLE `user_video_call_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_call`
--
ALTER TABLE `video_call`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `app_logins`
--
ALTER TABLE `app_logins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `blocked_users`
--
ALTER TABLE `blocked_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `body_types`
--
ALTER TABLE `body_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chat_user`
--
ALTER TABLE `chat_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `daily_quota`
--
ALTER TABLE `daily_quota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ethnicities`
--
ALTER TABLE `ethnicities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `health`
--
ALTER TABLE `health`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `health_user_report`
--
ALTER TABLE `health_user_report`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `iams`
--
ALTER TABLE `iams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lookingfors`
--
ALTER TABLE `lookingfors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `messages_read_unread`
--
ALTER TABLE `messages_read_unread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `messages_test`
--
ALTER TABLE `messages_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `premium`
--
ALTER TABLE `premium`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `premium_history`
--
ALTER TABLE `premium_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `private_album`
--
ALTER TABLE `private_album`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `private_album_rights`
--
ALTER TABLE `private_album_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `purrs`
--
ALTER TABLE `purrs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `report_images`
--
ALTER TABLE `report_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `report_msgs`
--
ALTER TABLE `report_msgs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report_users`
--
ALTER TABLE `report_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `user_image`
--
ALTER TABLE `user_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `user_likes`
--
ALTER TABLE `user_likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_subscription`
--
ALTER TABLE `user_subscription`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_video_call_tokens`
--
ALTER TABLE `user_video_call_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `video_call`
--
ALTER TABLE `video_call`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
